
var crypto = require('crypto'),
  algorithm = 'aes-128-ecb',
  token = '83e9f6637021b2a093ec9a2e98d97795bfa00d4c';

// function encrypt(text){
//   var cipher = crypto.createCipher(algorithm,token)
//   var crypted = cipher.update(text,'utf8','hex')
//   crypted += cipher.final('hex');
//   return crypted;
// }

// function decrypt(text){
//   var decipher = crypto.createDecipher('aes-128-ecb', '83e9f6637021b2a093ec9a2e98d97795bfa00d4c');
//
//   chunks = []
//   chunks.push( decipher.update( new Buffer(text, "base64").toString("binary")) );
//   chunks.push( decipher.final('binary') );
//   var txt = chunks.join("");
//   return  new Buffer(text, "binary").toString("utf-8");
// }
var encrypt = function(data, key) {
  var cipher = crypto.createCipher('aes-128-ecb', key);
  return cipher.update(data, 'utf8', 'base64') + cipher.final('base64');
};

var decrypt = function(data, key) {
  var decipher = crypto.createDecipher('aes-128-ecb', key);
  return decipher.update(data, 'base64', 'utf8') + decipher.final('utf8');
};
// var hw = decrypt("8YwlFgOz2Jp3I4V8g57E9A==",'83e9f6637021b2a093ec9a2e98d97795bfa00d4c')
// console.log(encrypt('WjJ3gnQJmFFb75IgyXsE9Q==','6329ad2a8422843185e884aed4bd65b7adc9d52c'))
console.log(decrypt('WjJ3gnQJmFFb75IgyXsE9Q==\n','a3eb119cfcdb01b30f4b1b1158b316c103f253ad'))
// console.log((hw));
