//  var t = ["Vastutus",
//    ]
// var o = {}
// for (var i = 0; i < t.length; i++) {
//   o[i]=t[i]
//
// }
// console.log(o)
// return

global.argv     = require('minimist')(process.argv.slice(2));
global.config   = global.argv
global.dirRoot  = __dirname;
global.options  = {}
var http        = require('http');
var fs          = require('fs');
var path        = require('path');
var url         = require('url');
var cookie      = require('cookie');
var qs          = require('querystring');
var PROTO_PATH  = global.dirRoot + '/proto/worker_service.proto';
var grpc        = require('grpc');
var MongoClient = require('mongodb').MongoClient;
var _proto      = grpc.load(PROTO_PATH).worker_service;
var worker      = new _proto.ServiceWorker(global.config.serv_worker_host + ":" + global.config.serv_worker_port, grpc.credentials.createInsecure());
//db.copyDatabase('project','project','193.189.89.145','project','project')
var path        = require('path'),
    fs          = require('fs');
var CSSFILE     = ''
var CSSFILELINK = ''
ObjectId        = require('mongodb').ObjectID;
MongoClient.connect('mongodb://project:project@localhost:27017/project', function (err, dbLOCAL) {


  global.optLOCAL   = dbLOCAL.collection('options')
  global.usersLOCAL = dbLOCAL.collection('users')
})
console.log("++++++++++++START SERVER+++++++++++++++++++")

function fromDir(startPath, callback) {

  console.log('Starting from dir ' + startPath + '/');

  if (!fs.existsSync(startPath)) {
    console.log("no dir ", startPath);
    return;
  }

  var files = fs.readdirSync(startPath);
  for (var i = 0; i < files.length; i++) {
    var filename = path.join(startPath, files[i]);

    if (filename.indexOf('.bundle.css') > -1) {
      CSSFILE     = '<link rel="stylesheet" href="/' + filename.split('/')[1] + '">'
      CSSFILELINK = '/' + filename.split('/')[1];
      console.log("!!!!!!!!!!", CSSFILE)
    }
  }
  ;
};

fromDir('./dist/Talents', function (filename) {
  console.log('-- found: ', filename);
});

setTimeout(function () {


  global.optLOCAL.find({}).toArray(function (err, data) {
    global.options = Object.assign({}, data[0], data[1], data[2], data[3], {search: data[4]})
    //global.options = JSON.parse(res.data).data
    console.log('=================OPTION===========================')
  })


}, 1000)
console.log('====pppp0', global.config)
http.createServer(function (request, response) {

  if (request.method == 'POST') {
    var body = '';

    request.on('data', function (data) {
      body += data;

      // Too much POST data, kill the connection!
      // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
      if (body.length > 1e6) request.connection.destroy();
    });

    request.on('end', function () {
      var post = qs.parse(body);


      switch (request.url) {
        case '/SetLangsi18':
          var fileRU = "./src/assets/i18n/ru.json"
          var fileEN = "./src/assets/i18n/en.json"
          var fileET = "./src/assets/i18n/et.json"
          fs.readFile(fileRU, "utf8", function (error, filetr) {
            var obj  = {}
            var q    = JSON.parse(body)
            var lang = JSON.parse(filetr)
            console.log(q)
            var t = q.key.split('.');
            if (t.length > 1) {
              lang[t[0]][t[1]] = q.RU
            }
            else {
              lang[t[0]] = q.RU
            }
            fs.writeFile(fileRU, JSON.stringify(lang, null, 4))
            fs.writeFile('./dist/Talents/assets/i18n/ru.json', JSON.stringify(lang, null, 4))
          });
          fs.readFile(fileEN, "utf8", function (error, filetr) {
            var obj  = {}
            var q    = JSON.parse(body)
            var lang = JSON.parse(filetr)
            var t    = q.key.split('.');
            if (t.length > 1) {
              lang[t[0]][t[1]] = q.EN
            }
            else {
              lang[t[0]] = q.EN
            }
            fs.writeFile(fileEN, JSON.stringify(lang, null, 4))
            fs.writeFile('./dist/Talents/assets/i18n/en.json', JSON.stringify(lang, null, 4))
          });
          fs.readFile(fileET, "utf8", function (error, filetr) {
            var obj  = {}
            var q    = JSON.parse(body)
            var lang = JSON.parse(filetr)
            console.log(q)
            var t = q.key.split('.');
            if (t.length > 1) {
              lang[t[0]][t[1]] = q.ET
            }
            else {
              lang[t[0]] = q.ET
            }
            fs.writeFile(fileET, JSON.stringify(lang, null, 4))
            fs.writeFile('./dist/Talents/assets/i18n/et.json', JSON.stringify(lang, null, 4))
          });
          break;
      }

      //console.log(post)
      // use post['blah'], etc.
    });
    return
  }
  var addcss   = ''
  var filePath = './dist/Talents' + request.url;
  var build    = 'html'
  var auth     = 0

  if (request.url == '/config') {
    response.writeHead(200, {'Content-Type': 'Application/Json; charset=utf-8'});
    response.end(JSON.stringify(Object.assign(global.config, {'public_css': CSSFILELINK})), 'utf-8');
    return;
  }

  if (request.url.indexOf('assets') < 0 && request.url.indexOf('.js') < 0 && request.url.indexOf('.css') < 0) {
    var query   = url.parse(request.url, true).query;
    var cookies = cookie.parse(request.headers.cookie || '');
    auth        = 1
    if (typeof query.token !== 'undefined' || typeof cookies.token !== 'undefined') {
      var build = 'dist/Talents'
      auth      = 2
    }
    else {
      response.setHeader('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
      if (global.config.hasOwnProperty('serv_cms_host') && global.config.serv_cms_host == 'wms-dev.xyz') {
        response.writeHead(301, {Location: '//wms-dev.xyz/?logout='+JSON.stringify({url:request.url})});
      }
      else {
        response.writeHead(301, {Location: '//talents.fund/?logout='+JSON.stringify({url:request.url})});
      }

      return response.end();

    }
    filePath = './' + build + '/index.html';
  }

  // if(request.url == '/company') {
  //     filePath = './html/company-rating.html';
  // }
  // if(request.url == '/public.js') {
  //     build = 'html'
  //     filePath = './html/public.js';
  // }
  //
  //   if(request.url == '/public.css') {
  //       response.writeHead(200, {'Content-Type': 'Application/Json; charset=utf-8'});
  //       response.end(JSON.stringify({'link':CSSFILELINK}), 'utf-8');
  //       return;
  //   }

  // if(request.url.indexOf('public-vacancy')>-1) {
  //   filePath = './html/public-vacancy.html';
  // }
  // if(request.url == '/public-vacancy.js') {
  //   build = 'html'
  //   filePath = './html/public-vacancy.js';
  // }

  //
  // if(request.url.indexOf('rating-search')>-1) {
  //     filePath = './html/student-companies-rating.html';
  // }
  // if(request.url == '/student-companies-rating.js') {
  //     build = 'html'
  //     filePath = './html/student-companies-rating.js';
  // }


  // if(request.url == '/news') {
  //
  //     filePath = './html/news.html';
  // }
  // if(request.url == '/public-payment-plans') {
  //
  //     filePath = './html/public-payment-plans.html';
  // }
  // if(request.url == '/typography') {
  //
  //     filePath = './html/typography.html';
  // }
  //
  //
  // if(request.url.indexOf('public-company') > -1) {
  //   addcss = CSSFILE
  //     filePath = './html/public-company.html';
  // }
  // if(request.url == '/public.js') {
  //
  //     filePath = './html/public.js';
  // }

  var extname = path.extname(filePath);

  var contentType = 'text/html';
  switch (extname) {
    case '.json':
      contentType = 'application/json; charset=utf-8';
      break;
    case '.js':
      contentType = 'text/javascript';
      break;
    case '.css':
      contentType = 'text/css';
      break;
    case '.svg':
      contentType = 'image/svg+xml';
      break;
  }
  console.log("filePath", filePath)
  fs.exists(filePath, function (exists) {
    console.log("filePath", filePath)
    if (exists) {
      fs.readFile(filePath, function (error, content) {
        if (error) {

          response.writeHead(500);
          response.end();

        }
        else {
          responseEnd(auth, contentType, content, response, request, addcss)

        }
      });
    }
    else {
      fs.readFile('./html/error.html', function (error, content) {
        console.log("ERROR FOUND", './html/error.html')
        //response.writeHead(404);
        response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});
        response.end(content, 'utf-8');
      })

    }
  });

}).listen(global.config.serv_client_port);

function responseEnd(auth, contentType, content, response, request, addcss) {
  if (contentType == 'text/html') {

    // content = content.replace('[[sasas]]',CSSFILE)
  }
  var scripts = ""
  console.log('auth', auth)
  if (auth == 0) {
    switch (contentType) {
      case 'text/javascript':
      case 'text/css':
      case 'image/svg+xml':
        response.writeHead(200, {
          'Content-Type':  contentType,
          'Cache-Control': 'public, max-age=2592000',
          'Expires':       new Date(Date.now() + 2592000000).toUTCString()
        });
        break;
      default:
        response.writeHead(200, {'Content-Type': contentType});
    }

    response.end(content, 'utf-8');

  }
  else if (auth == 1) {
    //response.writeHead(200, { "Set-Cookie": "SOCKET="+(parseInt(global.config.serv_wsproxy_wsport)+1)});

    scripts = '<script>' + 'window.PROFILE = ' + JSON.stringify(Object.assign({}, global.options, {
      PORT:        (parseInt(global.config.serv_wsproxy_wsport) + 1),
      HOST:        global.config.serv_wsproxy_wshost,
      PORT_STATIC: (parseInt(global.config.serv_static_httpport)),
      HOST_STATIC: global.config.serv_static_host
    })) + '</script>'
    if (contentType != 'text/javascript') {
      content = content + '';
      content = content.replace('</html>', '')
    }
    response.end(contentType == 'text/javascript' ? content : content + addcss + scripts+'</html>', 'utf-8');
  }
  else if (auth == 2) {
    var query   = url.parse(request.url, true).query;
    var cookies = cookie.parse(request.headers.cookie || '');
    var token   = (typeof query.token !== 'undefined') ? query.token : cookies.token;

    response.setHeader('Set-Cookie', cookie.serialize('token', token, {
      maxAge: 60 * 60 * 2,
      path: '/'
    }));

    response.writeHead(200, {'Content-Type': 'text/html; charset=utf-8'});

    var profile = {
      PORT:        (parseInt(global.config.serv_wsproxy_wsport) + 1),
      HOST:        global.config.serv_wsproxy_wshost,
      PORT_STATIC: (parseInt(global.config.serv_static_httpport)),
      HOST_STATIC: global.config.serv_static_host,
      options:     global.options
    };

    scripts = '<script>' + 'window.PROFILE = ' + JSON.stringify(profile) + '</script>'
    if (contentType != 'text/javascript') {
      content = content + '';
      content = content.replace('</html>', '')
    }
    response.end(contentType == 'text/javascript' ? content : content + addcss + scripts+'</html>', 'utf-8');
  }


}
