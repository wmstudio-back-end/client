var MongoClient = require('mongodb').MongoClient;

MongoClient.connect('mongodb://project:project@localhost:27017/project', function(err, dbPROD) {

  dbPROD.collection('options_notification').find({}).limit(1).toArray(function(err,data){
    for (var i = 0; i < data.length; i++) {
      upd(dbPROD,data[i]);

    }

  })
})
function upd(dbPROD,data){
  console.log(data)
  var icon = {
    "default": "info",
    "": "info",
    "check": "info",
    "error": "success",
    "warning": "warning"
  }

  var n = {
    pkey_id:data.pkey_id,
    name:data.name,
    to:data.to,
    pri:data.pri,
    icon:icon[data.icon],
    url:data.url,
    trigger:data.trigger,
    EN:{text:data.content_en,btn:data.button_en},
    RU:{text:data.content_ru,btn:data.button_ru},
    ET:{text:data.content_ee,btn:data.button_ee},
  }

  dbPROD.collection('options_notification').update({_id:data._id},n,function(err,data){
    console.log(data)
  })
}
