import {Options} from "./vacancyOptions";

export interface Filter {
  query: any
  skip: number
  limit: number
  sort: any
  items: fItems[]
  options: Options
}

export interface fItems {
  key: string
  operand: string
  values: any
}

