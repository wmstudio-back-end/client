export interface Isubscriptions {
  packageId: string
  periodId: number
}

export interface Iservice {
  vacancy: number
  premiumListing: number
  hightFrame: number
  insocialMedia: number
  targetGroup: number
  bannerofEmail: number
}

export interface Icart {
  subscriptions: Isubscriptions
  services: Iservice
  promocode: string[]
  TotalPrice: number
  Discount: number
}

export interface IaccessPackage {
  publish: boolean
  applicants: boolean
  rating: boolean
  search: boolean
  notify: boolean
  match: boolean
  projBmark: boolean

}

export interface IcartPrice {
  TotalPrice: number
  subscription: number
  subscriptionDebt: number
  Discount: number
  service: {
    vacancy: number
    premiumListing: number
    hightFrame: number
    insocialMedia: number
    targetGroup: number
    bannerofEmail: number
  }
}
