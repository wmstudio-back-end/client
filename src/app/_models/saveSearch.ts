import {Options} from "./vacancyOptions";

export interface SaveSearch {
  _id?: string
  uid?: string
  name: string
  period: number
  project?: string
  alertEmail: boolean
  options?: Options
  filter?: any
  count: number
  query?: any
  discovered?: number
  count_new: string[]

  count_total: string[]
  count_unread: string[]

}
