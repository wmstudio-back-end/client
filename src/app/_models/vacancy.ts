export interface IVacancy {
  _id?: string
  type_temp: number
  edit: boolean
  published_at: number
  publish: boolean
  subscribeCount: number
  languageID: number;
  positionTitle: string
  type: number
  proof: number
  match: Imatch
  descriptionStep: number
  fieldActivity: number
  occupationField: number
  minEducation: number
  deleted_at: number
  projectID: string
  subscribe: string[]
  subscribeNew: string[]
  subscribeUnread: string[]
  LanguagesRequired: ILanguageKnowledge[]
  Previous_work: boolean
  Worktime: number[]
  Remote_work: boolean
  scholarshipOffer: number[]
  Desired_personality_traits: any
  logInstruction: any[]
  uploadFiles: any
  proofUploadFiles: any[]
  contract: IContract
  deadLine: number
  salary: Salary
  shotDescription: string
  roleTasks: string
  requirements: string
  weOffer: string
  Location: any
  notify: boolean
  address: string
  vacancyUrl: string
  aplicatCont: string
  proofUrl: string
  showInfo: number
  messages: boolean
  contactPerson: string
  contactPersonPosition: string
  phoneNumber: string
  contactEmail: string
  rating: number
  activInstruction: ActivInstruction
  PROMpremiumListing: number
  PROMtargetGroup: number
  PROMinsocialMedia: number
  PROMhightFrame: number
  PROMbannerofEmail: number
  submitted_at: number

  experience_work: number
  created_at: number
  updated_at: number
  keySkills: any
  uid: string
  bookmark?: boolean
  postedTime?: string
}

export interface ActivInstruction {
  premiumListing: any
  hightFrame: any
  insocialMedia: any
  targetGroup: any
  bannerofEmail: any
}

export interface Salary {
  type: number,
  summ: number,
  summ2: number,
  hide: boolean
}

export interface Imatch {
  e1: number
  e2: number
  e3: number
  cand: number
  work: number
}

interface IContract {
  def?: string
  start?: number
  end?: number
}

interface ILanguageKnowledge {
  levelID: number;
  languageID: number
}

interface ItemsSelect {
  id: number;
  text?: string;
  value?: string
}
