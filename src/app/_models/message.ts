export interface Message {
  method: string,
  data: any,
  error: string
}
