export interface Options {
  whereToSearch: any
  interestedIn: any
  sex: any
  ageFrom: any
  ageTo: any
  countItems: any
  onlyVerified: any
  fieldActivity: any
  languages: any
  expectedGraduationMonth: any
  expectedGraduationYear: any
  educationLevel: any
  majors: any
  occupationField: any
  industry: any
  typeOfVacancy: any
  statusForEmployment: any
  address: any
  city: any
  keywords: any
  workTime: any
  workExp: any
  languagesSkills: any
  previosWork: any
  recommendations: any
  profileScore: number
  avgProgressEdu: any
  vacancyType: any

}
