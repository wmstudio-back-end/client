export interface Notify {
  _id:string;
  type: string;
  text: string;
  buttonUrl: string;
  buttonName: string;
  show:boolean
}
