export interface Reviews {
  _id: string
  uid: string
  created_at: string
  updated_at: string
  comment_recommend: boolean
  comment_text: string
  comment_anonim: boolean
  rating_poise: number
  rating_dilligance: number
  rating_sociability: number

  rating_culture: number
  rating_work: number
  rating_salary: number
  rating_managment: number
  rating_career: number
  comment_author: string
  companyid: string
}
