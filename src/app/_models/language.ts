export interface Language {
  certificate: boolean;
  everyday_use: boolean;
  level: string;
  lang: string;
}
