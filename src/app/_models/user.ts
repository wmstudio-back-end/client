
import {IVacancy}        from "./vacancy";
import {ICoverLetter}    from "./cover-letter";
import {IlanguageSkills} from "./resume";
import {Icart}           from "./service";

export interface oAuth {
  oauth_id: string,
  type: string,
  uid: string,
  withoutPassword: boolean,
  _id: string
}

export interface IProfileService {
  active: {
    orderId: string
    packageId: string
    periodId: number
    vacancy: number
    premiumListing: number
    hightFrame: number
    insocialMedia: number
    targetGroup: number
    bannerofEmail: number
    internship: number
  },
  orders: any[]
}

export interface User {

    id: number;
    _id: string;
  email_trusted:number
  userDataComplited:number
    service: IProfileService;
  employee: any[];
    notification: any[];
    username: string;
    password: string;
    first_name: string;
    last_visit: number
    rating: number
    email: string;
    newemail: string;
    last_name: string;
    ready_to_work: number;
    access: number;
    subscribeVacancy: any
    subscribeCandidate: any
    resumes: any;
    proof: number
    savedSearches: any;
    messages: any;
    subaccounts: any;
    languages: any;
    skills: any;
    currentLang: number;
    currentCountry: number;
    type_profile: string;
    Student: Student;
    Company: Company;
    CoverLetter: ICoverLetter[]
    Organization: Organization;
    Educator: Educator;
    personalId: number
    Settings: Settings;
    Address: string;
    phone: number;
    photo: string;
    pay: boolean
    Photo: string;
    Location: ILocation;
    oAuth: oAuth;
    displayAs: number
    invite: any;
    dialogs: any
    bookmarks?: any
    birthday: any
    recommendations: any[]
    cart: Icart
    prefLang: number
    smartId: {
        idcode: any,
        lastname: any,
        firstname: any,
        email: any,
        email_verified: any,
        last_login_method: any,
        current_login_method: any
    }
    preferCompactView: boolean

}

export interface Educator {
  schools: Organization[];
}

export interface Organization {
  _id: string
  schoolId: string
  schools_edu: any
  place_id: string
  addr: string
  status: number
  schoolName: string;
  schoolLocation: ILocation;
  educationLevel: number;
  typeOfSchool: number
  regCode: number
  mobilePhone: string
  landlinePhone: string

  degree: number;
  updated_at: number
  created_at: number
  aboutOrg: string
  videoUrl: string
  webSite: string
  Established: string
  AdditionalAddresses: ILocation[]
  Additional_information: string
  photo: any

  major: string;
  minor: string;
  speciality: string;
  startDateMonth: number;
  startDateYear: string;
  endDateMonth: number;
  endDateYear: string;
  expectedGraduationMonth: number;
  expectedGraduationYear: number;
  curently: boolean;
  SchoolProgressEducation: IschoolProgressEducation,
}

export interface Student {
  gender: number;
  ready_to_work: number;
  schools: Organization[];
  language: IlanguageSkills[];
  resumes: any
}

export interface Company {
  projects: CompanyProjects[];
  Established: string
  regCode: number
  vatnumber: number
  mobilePhone: number
  landlinePhone: number
  aboutCompany: string
  videoUrl: string
  webSite: string
  activityField: number[]
  legalFormBusiness: number
  online: boolean
  companySize: number
  nameCompany: string
  codeCompany: string
  vacancy: IVacancy[]
  Address: string
  skills: any
  rating_undefuser: boolean
  rating_websitevis: boolean
  AdditionalAddresses: ILocation[]
}

export interface CompanyProjects {
  _id: string
  name: string
  description: string
  created_at: number
  update_at: number
  uid: string,
  count: number
}

export interface Settings {
  contact_info: boolean;
  news: boolean;
  progress_in_edu: boolean;
  progress_in_edu2: boolean;
}

export interface ILocation {
  lat: number;
  lng: number;
  place_id: string;
  addr: string;
}

export interface IschoolProgressEducation {
  gradeStarted: string
  evaluationSystem: number
  avarage: string
  updated_at: number
  created_at: number
  status: number
  file: any
}

