export interface ICoverLetter {
  title: string
  text: string
  language: number
  created_at: number
  updated_at: number
  defaultLang: boolean
  _id: string
}
