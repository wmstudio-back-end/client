export interface Recommendation {

  _id: string
  type: number
  educatorId: string
  letter: string
  created_at: number
  updated_at: number
  uuid: string
  status: number
  visibility: boolean
  comment_recommend: boolean
  rating_poise: number
  rating_dilligance: number
  rating_sociability: number,
  comment_text: string

}
