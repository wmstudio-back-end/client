import {User} from "./user";

export interface IResume {
  uploadFiles: any
  objective: string
  deleted_at: number
  additional: string
  informationAboutComputerSkills: string
  otherSpecificSkills: any
  interestsAndHobbies: string
  yourMainValues: string
  participationInCommunities: string
  currentLevelOfEducation: number
  accomplishmentsInformation: string
  pickedSkills: any
  name: string
  language: number
  officeTools: IOfficeTools[];
  otherSoftware: IOtherSoftware[];
  certificationsAndLicences: ICertificationsAndLicences[];
  drivingLicense: IDrivingLicense[];
  workExp: IWorkExp[];
  dBlanguage: number
  characterTraits: any
  strenghs: any
  idditonalInfo: string
  internshipOptions: IinternshipOptions
  jobOptions: IjobOptions
  created_at: number
  updated_at: number
  status: number
  publish: boolean
  _id: string
  bookmark?: boolean
  user?: User
  uid?: string
  prc: number
  rating: number
  languageSkills: IlanguageSkills[]

}

export interface IWorkExp {
  companyName: string
  companySize: string
  fieldOfActivity: string
  occupationField: any
  position: string
  workCountry: string
  startDateMonth: number
  endDateMonth: number
  startDateYear: number
  endDateYear: number
  currentlyWorking: boolean
  jobDesciprion: string
  accomplishmentsiInformation: string
  _id: string
  updated_at: number
  created_at: number

}

export interface IDrivingLicense {
  category: number;
  drivingStatus: boolean;
}

export interface ICertificationsAndLicences {
  name: string;
  year: number;
  issuer: string;
}

export interface IOtherSoftware {
  otherSoft: string;
  level: number;
  use: boolean;
}

export interface IOfficeTools {
  officeTool: number;
  level: number;
  use: boolean;
}

export interface IlanguageSkills {
  langId: number
  langLevel: number
  certificate: boolean
  useEveryday: boolean
}


export interface IjobOptions {
  jobType: number[]
  occupationField: number[]
  possible_position: string[]
  fieldActivity: number[]
  remote_work: boolean
  locations: Ilocations[]
  preferredWorkTime: number[]
  preferredPeriod: IpreferredPeriod
  salary: Isalary
  additonalInformation: string
}

export interface IinternshipOptions {
  init: boolean
  internshipType: number[]
  occupationField: number[]
  possible_internship: string[]
  locations: Ilocations[]
  remote_work: boolean
  preferredWorkTime: number[]
  preferredPeriod: IpreferredPeriod
  scholarshipOpt: IscholarshipOpt
  additonalInformation: string


}

export interface IscholarshipOpt {
  returnWork: boolean
  returnComWork: boolean
  anyKind: boolean
  sizescholarship: number


}

export interface Isalary {
  monthly: number

  hourly: number


}

export interface IpreferredPeriod {
  type: number
  start: number
  end: number
  startWork: number
  hour: number

}

export interface Ilocations {
  adress: any
  locate: any

}


// interface IContract{
//   def?:string
//   start?:string
//   end?:string
// }
// interface ILanguageKnowledge{
//   levelID:number;
//   languageID:number
// }
// interface ItemsSelect{
//   id:number;
//   text?:string;
//   value?:string
// }
