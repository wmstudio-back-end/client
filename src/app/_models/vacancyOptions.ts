export interface Options {
  occupationField: any
  vacancyType: any
  countItems: any
  fieldActivity: any
  languages: any
  educationLevel: any
  workExp: any
  workTime: any
  salary: any
  address: any
  keywords: any
  reqLanguages: any
  minSalary: any
  olderThan: any
  vacancyTypeSlice: any
}
