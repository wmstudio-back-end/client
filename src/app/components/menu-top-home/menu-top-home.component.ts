import {
  Component,
  HostListener,
  Inject,
  OnInit
}                              from "@angular/core";
import {UserService}           from "../../_services/user.service";
import {DataService}           from "../../_services/data.service";
import {TranslateService}      from "@ngx-translate/core";
import {AuthenticationService} from "../../_services/authentication.service";
import {DOCUMENT}              from "@angular/platform-browser";
import {ComponentService}      from "../../_services/component.service";
import {
  ActivatedRoute,
  NavigationEnd,
  Router
}                              from "@angular/router";
import {HelpersService}        from "../../_services/helpers.service";
import {DownPopupService}      from "../../_services/down-popup.service";
import {ResumeService}         from "../../_services/resume.service";
import {VacancyService}        from "../../_services/vacancy.service";

@Component({
  selector:    'app-menu-top-home',
  templateUrl: './menu-top-home.component.html',
  styleUrls:   ['./menu-top-home.component.scss']
})


export class MenuTopHomeComponent implements OnInit {
  userModalActive         = false;
  selectLangM: boolean    = false;
  selectCountryM: boolean = false;
  svg: string = '';
  ///////HEADER-DROPDOWN/////////
  employersLink: boolean  = false;
  educatorsLink: boolean  = false;
  aboutLink: boolean      = false;
  studentsLink: boolean   = false;
  selectCountry: boolean  = false;
  selectLang: boolean     = false;
  mobileMenu: boolean     = false;
  studentsLinkM: boolean  = false;
  employersLinkM: boolean = false;
  educatorsLinkM: boolean = false;
  aboutLinkM: boolean     = false;
  sectionUsb: boolean     = false;
  sectionUsb2: boolean    = false;
  sectionUsb3: boolean    = false;
  port: string
  host: string
  name: string
  route: string

  public navIsFixed: boolean = false;

  @HostListener("window:scroll", [])
  onWindowScroll() {
    let number = window.pageYOffset;
    if (number > 1) {
      this.navIsFixed = true;


    } else if (this.navIsFixed && number < 1) {
      this.navIsFixed = false;

    }

  }


  constructor(public user: UserService,
              public dataService: DataService,
              public translate: TranslateService,
              public authenticationService: AuthenticationService,
              public component: ComponentService,
              private router: Router,
              public popS: DownPopupService,
              public help: HelpersService,
              private rs: ResumeService,
              private vs: VacancyService,
              @Inject(DOCUMENT) document: any) {
    this.port = dataService.PORT_STATIC
    this.host = dataService.HOST_STATIC
    router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        this.route = data.urlAfterRedirects
        if (help.isMobile()) this.userModalActive = false
      }
    });
  }


  customRoute(path) {
    let body = document.getElementsByTagName('body')[0];
    body.classList.remove('blackFon')
    if (path === '/resume/search') {
      this.rs.clearFilter()
    } else if (path === '/vacancy/search') {
      this.vs.clearFilter()
    }

    this.router.navigate([path]);
  }


  getPrc() {
    var t   = 0;
    var prc = 0
    if (this.user.profile.type_profile == 'Student') {

      if (this.user.profile.Student.resumes.length > 0) {
        for (var i = 0; i < this.user.profile.Student.resumes.length; i++) {
          if (prc < this.user.profile.Student.resumes[i].prc) {
            prc = this.user.profile.Student.resumes[i].prc
          }
        }
      }
      return prc + this.user.userDataComplited
    } else if (this.user.profile.type_profile == 'Company') {
      return this.user.GetPrsCompanyInformationComplete()
    } else {
      return this.user.GetPrsUserdataComplete()
    }


  }

  ngOnInit() {
    this.user.profile.currentCountry               = 27
    document.getElementById("header").style.height = document.getElementById("header-wrap").offsetHeight.toString() + "px";
    window.addEventListener('scroll', () => {
      document.getElementById("header").style.height = document.getElementById("header-wrap").offsetHeight.toString() + "px";
    })
    window.addEventListener('resize', () => {
      document.getElementById("header").style.height = document.getElementById("header-wrap").offsetHeight.toString() + "px";
    })

  }

  shadowBody() {
    let body = document.getElementsByTagName('body')[0];
    // console.log(body)
    if (this.mobileMenu) {
      body.classList.add('blackout');
    } else {
      body.classList.remove('blackout');
    }


  }


  getCountry() {
    let arrCountry = [];
    arrCountry.push(this.dataService.getOptionsFromId('languages', 27))
    arrCountry.push(this.dataService.getOptionsFromId('languages', 68))
    //console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',arrCountry)
    //console.log('aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',this.dataService.getOptionsFromId('country',this.user.profile.currentCountry))
    return arrCountry;
  }

  logout() {
  }

  setLang(l) {
    this.user.profile.currentLang = l.id
    this.selectLang               = false
    let lUp                       = this.dataService.options.languages[l.id].value
    lUp                           = lUp.toLowerCase()

    this.translate.use(lUp)
    this.dataService.send('User/editProfile', {currentLang: l.id})

  }

  setCountry(l) {
    this.selectCountry               = false
    this.user.profile.currentCountry = l.id
    this.dataService.send('User/editProfile', {currentCountry: l.id})
  }

}
