import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../../_services/data.service";
import {IVacancy} from "../../../../_models/vacancy";

@Component({
  selector: 'app-vacancy-list',
  templateUrl: './vacancy-list.component.html',
  styleUrls: ['./vacancy-list.component.scss']
})
export class VacancyListComponent implements OnInit {
  viwe = false
  skip = 0
  limit = 4
  vacancyList: IVacancy[]
  constructor(public dataService:DataService) {
    this.getVacancy()
  }
  getVacancy(){
    this.dataService.sendws('Admin/getVacancy',{skip:this.skip,limit:this.limit}).then((data)=>{
      this.viwe = true
      this.vacancyList = data['vacancy']
    })

  }
  getVacancyFrom_id(_id){
    for(var i in this.vacancyList){
      if (_id==this.vacancyList[i]['_id']){
        return this.vacancyList[i]
      }
    }
    return null
  }
  publicVacancy(_id){
    this.dataService.sendws('Admin/publicVacancy',{_id:_id}).then((data)=>{
      this.getVacancyFrom_id(_id)['proof'] = 2
      console.log(data)
    })

  }
  next(){
    this.skip+=this.limit
    this.getVacancy()
  }
  prev(){
    this.skip-=this.limit
    this.getVacancy()
  }
  ngOnInit() {
  }

}
