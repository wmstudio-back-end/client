import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../../_services/data.service";
import {IVacancy} from "../../../../_models/vacancy";
import {TranslateService} from "@ngx-translate/core";
import {CreatVacancyService} from "../../../vacancy/create/create.service";
import {Router} from "@angular/router";


@Component({
  selector: 'app-vacancy-proof',
  templateUrl: './vacancy-proof.component.html',
  styleUrls: ['./vacancy-proof.component.scss']
})
export class VacancyProofComponent implements OnInit {
  viwe = false
  skip = 0
  limit = 8
  vacancyList = []
  constructor(public dataService:DataService,public cS:CreatVacancyService,public router: Router) {
    this.getVacancy({proof:{$in:[1]}})
  }
  getVacancy(query){
    this.dataService.sendws('Admin/getVacancy',{skip:this.skip,limit:this.limit,query:query}).then((data)=>{
      this.viwe = true
      this.vacancyList = data['vacancy']
      this.vacancyList.sort((a,b)=>{
        return a.proof-b.proof
      })
    })

  }
  formatData(time){
    var d = new Date(time*1000)
    return d.getDate()+'.'+(d.getMonth() + 1)+'.'+d.getFullYear()
  }
  getVacancyFrom_id(_id){
    for(var i in this.vacancyList){
      if (_id==this.vacancyList[i]['_id']){
        return this.vacancyList[i]
      }
    }
    return null
  }
  publicVacancy(_id){
    this.dataService.sendws('Admin/publicVacancy',{_id:_id}).then((data)=>{
      this.getVacancyFrom_id(_id)['proof'] = 2
      console.log(data)
    })

  }
  setEditVacancy(vacancy){

    this.cS.vacancy = JSON.parse(JSON.stringify(vacancy));
    this.cS._id = vacancy._id

    if (vacancy.type_temp==1){
      this.router.navigate(['/vacancy/create/manual']);
    }
    else if (vacancy.type_temp==2){
      this.router.navigate(['/vacancy/create/manual']);
    }
    else {
      this.router.navigate(['/vacancy/create/']);
    }
  }
  ngOnInit() {
  }

}
