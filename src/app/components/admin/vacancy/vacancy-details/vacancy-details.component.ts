import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DataService} from "../../../../_services/data.service";

@Component({
  selector: 'app-vacancy-details',
  templateUrl: './vacancy-details.component.html',
  styleUrls: ['./vacancy-details.component.scss']
})
export class VacancyDetailsComponent implements OnInit {
  _id:string
  constructor( private route : ActivatedRoute,
               public dataService :DataService) { }

  ngOnInit() {
    this._id = this.route.snapshot.params['id']
  }

}
