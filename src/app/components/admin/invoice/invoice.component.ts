import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../_services/data.service";

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css']
})
export class InvoiceComponent implements OnInit {

  constructor(
    public dS: DataService,
  ) { }

  ngOnInit() {
    this.dS.sendws('Admin/getInvoces', {}).then((data) => {

    })
  }

}
