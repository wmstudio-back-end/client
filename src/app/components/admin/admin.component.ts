﻿import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {ComponentService} from "../../_services/component.service";


@Component({
  moduleId:module.id.toString(),
  template:`<sidebar-link [componentData]="component.componentSidebarLink" class="sidebar"></sidebar-link>
           <div class="sideContent">
           <router-outlet></router-outlet>
           </div>`,

})

export class AdminComponent implements OnInit {

  constructor(public component:ComponentService) {

  }

  ngOnInit() {}

}
