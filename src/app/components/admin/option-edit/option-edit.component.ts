import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../_services/data.service";

@Component({
  selector: 'app-option-edit',
  templateUrl: './option-edit.component.html',
  styleUrls: ['./option-edit.component.scss']
})
export class OptionEditComponent implements OnInit {
  obKeys = Object.keys
  keys = []
  newKey
  text_RU
  text_EN
  text_ET
  AddNewKey = false
  constructor(public dS:DataService) { }

  ngOnInit() {

       var  k, i, len;

    for (k in this.dS.options.translate.RU) {
      if (this.dS.options.translate.EN.hasOwnProperty(k)) {
        this.keys.push(k);
      }
    }

    //this.keys.sort();


  }
  editShow(key){
    document.getElementById(key+'_show1').classList.contains('active')?
      document.getElementById(key+'_show1').classList.remove('active'):
      document.getElementById(key+'_show1').classList.add("active");


  }
  addnew(key,newKey){
    console.log('key',key)
    console.log('newKey',newKey)
    this.dS.sendws("Admin/editOpton",{
      key:key,
      st:newKey,
      RU:this.text_RU,
      EN:this.text_EN,
      ET:this.text_ET,
    }).then(data=>{
      alert("OK")
      this.dS.options.translate.RU[key][newKey] = this.text_RU
      this.dS.options.translate.EN[key][newKey] = this.text_EN
      this.dS.options.translate.ET[key][newKey] = this.text_ET
      console.log(data)
    })
  }
  del(key,st){
    if (confirm('Delete?')){
      this.dS.sendws("Admin/deleteOption",{
        key:key,
        st:st,
      }).then(data=>{
        alert("OK")
        delete this.dS.options.translate.RU[key][st]
        delete this.dS.options.translate.EN[key][st]
        delete this.dS.options.translate.ET[key][st]
        console.log(data)
      })
    }

  }
  save(key,st){
    console.log("key - ",key)
    console.log('st - ',st)
    this.dS.sendws("Admin/editOpton",{
      key:key,
      st:st,
      RU:this.dS.options.translate.RU[key][st],
      EN:this.dS.options.translate.EN[key][st],
      ET:this.dS.options.translate.ET[key][st],
    }).then(data=>{
      alert("OK")
      console.log(data)
    })
    console.log(this.dS.options.translate.RU[key][st])
    console.log(this.dS.options.translate.EN[key][st])
    console.log(this.dS.options.translate.ET[key][st])
  }
}
