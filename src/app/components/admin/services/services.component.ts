import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../_services/data.service";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  orders = []
  constructor(public dS:DataService) { }

  ngOnInit() {
    this.dS.sendws('Admin/getOrders',{}).then((data)=>{
      this.orders = data['orders']
      console.log(data['orders'])
    })
  }
  applyOrder(order_id,i){
    this.dS.sendws('Admin/applyOrder',this.orders[i]).then((data)=>{
      this.orders[i].status = 1
    })

  }

}
