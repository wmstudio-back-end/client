import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../_services/data.service";
import {Organization} from "../../../_models/user";

@Component({
  selector: 'app-progress-education',
  templateUrl: './progress-education.component.html',
  styleUrls: ['./progress-education.component.scss']
})
export class ProgressEducationComponent implements OnInit {
  viwe = false
  skip = 0
  limit = 4
  progress = []


  constructor(public dS:DataService) {

  }

  ngOnInit() {
    this.dS.sendws('Admin/getProgressEducation',{skip:this.skip,limit:this.limit,query:{}}).then((data)=>{
      this.viwe = true
      this.progress = data['progress']
    })
  }
  verify(_id,status){
    this.dS.sendws('Admin/verifyProgressEducation',{_id:_id,status:status}).then((data)=>{
     console.log(data)
    })
  }
}
