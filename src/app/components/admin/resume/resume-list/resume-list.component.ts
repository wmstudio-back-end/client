import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../../_services/data.service";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-resume-list',
  templateUrl: './resume-list.component.html',
  styleUrls: ['./resume-list.component.scss']
})
export class ResumeListComponent implements OnInit {
  resumes = [];
  objectKeys = Object.keys;
  returnTrueAny = this.returnTrueValueAny;
  date = new DatePipe('en-US');



  returnTrueValue(obj) {
    for(let key in obj) {
      if(obj[key]) {
       return key;
      }
      else {
        return ;
      }
    }

  }

  returnTrueValueAny(obj) {
    let arr = [];
    for(let key in obj) {
      if(obj[key]) {
       arr.push(key);
      }
    }
    if(arr.length) {
      return arr
    } else {
      return null
    }

  }





  constructor(public dS : DataService) { }
  publicResume(_id){
    this.dS.sendws('Admin/publicResume',{_id:_id}).then((data)=>{
      this.getResumeFrom_id(_id)['proof'] = 1
      console.log(data)
    })

  }
  getResumeFrom_id(_id){
    for(var i in this.resumes){
      if (_id==this.resumes[i]['_id']){
        return this.resumes[i]
      }
    }
    return null
  }
  ngOnInit() {
    this.dS.sendws('Resume/GetResume', {}).then((data)=>{
      this.resumes = data['resumes'];
      console.log('резюме:', this.resumes)
    });



  }

}
