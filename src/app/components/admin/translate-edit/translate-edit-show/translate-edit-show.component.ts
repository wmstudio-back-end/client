import {Component, Input, OnInit} from '@angular/core';
import {Http, Response, Headers, RequestOptions, RequestMethod} from '@angular/http';
import {EditTranslateserviceService} from "../edit-translateservice.service";
import {HttpHeaders, HttpParams} from "@angular/common/http";
@Component({
  selector: 'app-translate-edit-show',
  templateUrl: './translate-edit-show.component.html',
  styleUrls: ['./translate-edit-show.component.scss']
})
export class TranslateEditShowComponent implements OnInit {
  @Input() key:string

  @Input() obKey:any
 
  objectKeys = []
  constructor(private http: Http,public tS:EditTranslateserviceService) { }
  save(){
    var t = this.key.split('.');
    //let obj = new HttpParams();
    let obj = {RU:'',EN:'',ET:'',key:this.key};
  
    //obj = obj.append('key', this.key);
    
    console.log('pppppppppp',this.tS.transl['RU'][t[0]])
      if (t.length>1){
        //obj = obj.append('RU', this.tS.transl['RU'][t[0]][t[1]]);
        //obj = obj.append('EN', this.tS.transl['EN'][t[0]][t[1]]);
        //obj = obj.append('ET', this.tS.transl['ET'][t[0]][t[1]]);
        obj.RU = this.tS.transl['RU'][t[0]][t[1]]
        obj.EN = this.tS.transl['EN'][t[0]][t[1]]
        obj.ET = this.tS.transl['ET'][t[0]][t[1]]
      }else{
        //obj = obj.append('RU', this.tS.transl['RU'][t[0]]);
        //obj = obj.append('EN', this.tS.transl['EN'][t[0]]);
        //obj = obj.append('ET', this.tS.transl['ET'][t[0]]);
        obj.RU = this.tS.transl['RU'][t[0]]
        obj.EN = this.tS.transl['EN'][t[0]]
        obj.ET = this.tS.transl['ET'][t[0]]
      }

    console.log('OBJ',obj)
 
   
    this.http.post('/SetLangsi18',JSON.stringify(obj),{headers: new Headers({
      'Content-Type': 'application/json'
    })}).map(res => res.json())
      .subscribe(data => this.setData(data),
        err => console.log(err),
        () => console.log('Completed'));
    console.log('key',obj)
  }
  setData(data){
    console.log("&&&&&&&&&&&&&&",data)
  }
  ngOnInit() {
    var t = this.key.split('.');
    typeof this.tS.transl['EN'][t[0]]=='object'?
    this.objectKeys = Object.keys(this.tS.transl['EN'][t[0]]):
      this.objectKeys.push(t[0])
  
  
  }

}
