import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {TranslateService} from "@ngx-translate/core";
@Injectable()
export class EditTranslateserviceService {
public transl = {}
public activeEdit = ''
  constructor(private http: Http) {
    this.http.get('/assets/i18n/ru.json')
      .map(res => res.json())
      .subscribe(data => this.setData(data,"RU"),
        err => console.log(err),
        () => console.log('Completed'));
    this.http.get('/assets/i18n/en.json')
      .map(res => res.json())
      .subscribe(data => this.setData(data,"EN"),
        err => console.log(err),
        () => console.log('Completed'));
    this.http.get('/assets/i18n/et.json')
      .map(res => res.json())
      .subscribe(data => this.setData(data,"ET"),
        err => console.log(err),
        () => console.log('Completed'));
  
  
  }
  setData(data,lang){
    this.transl[lang]=data
  

  }

}
