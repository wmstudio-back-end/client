import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../_services/data.service";
import {TranslateService} from "@ngx-translate/core";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import {EditTranslateserviceService} from "./edit-translateservice.service";
@Component({
  selector: 'app-translate-edit',
  templateUrl: './translate-edit.component.html',
  styleUrls: ['./translate-edit.component.scss']
})
export class TranslateEditComponent implements OnInit {
  data: Object;

  obKeys = Object.keys

  activeEdit = ''
  constructor(public dS:DataService,public tS:EditTranslateserviceService,private http: Http) {

  }
  setKey(key){
    this.tS.activeEdit = key

  }
  ifKey(key){
    var t = this.tS.activeEdit.split('.');
    return key==t[0]


  }
  editShow(key){
    document.getElementById(key+'_show1').classList.contains('active')?
      document.getElementById(key+'_show1').classList.remove('active'):
      document.getElementById(key+'_show1').classList.add("active");


  }
  ngOnInit() {


  }

}
