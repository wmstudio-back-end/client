import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VacancyComponent } from './vacancy/vacancy.component';
import { ResumeComponent } from './resume/resume.component';
import { HelpersComponent } from './helpers/helpers.component';
import { MessagesComponent } from './messages/messages.component';
import { ProfilesComponent } from './profiles/profiles.component';
import {FormsModule} from "@angular/forms";
import {ClickOutsideModule} from "ng-click-outside/lib";
import {TranslateModule} from "ng2-translate";
import {RouterModule} from "@angular/router";

import {AdminComponent} from "./admin.component";
import {SidebarModule} from "app/components//sidebar/sidebar.module";
import {MyDatePickerModule} from "mydatepicker";
import { VacancyListComponent } from './vacancy/vacancy-list/vacancy-list.component';
import { VacancyProofComponent } from './vacancy/vacancy-proof/vacancy-proof.component';
import { ResumeListComponent } from './resume/resume-list/resume-list.component';
import { ResumeProofComponent } from './resume/resume-proof/resume-proof.component';

import { VacancyDetailsComponent } from './vacancy/vacancy-details/vacancy-details.component';
import { ProgressEducationComponent } from './progress-education/progress-education.component';
import { ServicesComponent } from './services/services.component';
import { OptionEditComponent } from './option-edit/option-edit.component';
import { TranslateEditComponent } from './translate-edit/translate-edit.component';
import { TranslateEditShowComponent } from './translate-edit/translate-edit-show/translate-edit-show.component';
import {EditTranslateserviceService} from "./translate-edit/edit-translateservice.service";
import {ModulesModule} from "../../modules/modules.module";
import { InvoiceComponent } from './invoice/invoice.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    ModulesModule,
    SidebarModule,
    MyDatePickerModule,
    RouterModule.forChild([
      {
        path: '',
        component: AdminComponent,
        data: {
          breadcrumb: ''
        },
        children: [
          {
            path:     'vacancy',
            component:VacancyComponent,
            children: [
              {
                path:     '',
                component:VacancyListComponent
              },
              {
                path:     'proof',
                component:VacancyProofComponent
              },
              {
                path:     'details/:id',
                component:VacancyDetailsComponent
              }
            ]
          },
          {
            path:     'optionedit',
            component:OptionEditComponent,
            children: [

            ]
          },
          {
            path:     'invoice',
            component:InvoiceComponent,
            children: [

            ]
          },
          {
            path:     'translateedit',
            component:TranslateEditComponent,
            children: [

            ]
          },
          {
            path:     'progressEducation',
            component:ProgressEducationComponent,
            children: [

            ]
          },
          {
            path:     'services',
            component:ServicesComponent,
            children: [

            ]
          },
          {
            path:     'resume',
            component:ResumeComponent,
            children: [
              {
                path:     '',
                component:ResumeListComponent
              },
              {
                path:     'proof',
                component:ResumeProofComponent
              }
            ]
          },
          {
            path:     'upload',
            component:VacancyComponent
          }
        ]
      },
      {
        path: 'vacancy',
        component: VacancyComponent,
        data: {
          breadcrumb: ''
        }
      },
      {
        path: 'userdata',
        component: VacancyComponent,
        data: {
          breadcrumb: ''
        }
      }
    ])

  ],
  declarations: [
    VacancyComponent,
    ResumeComponent,
    HelpersComponent,
    InvoiceComponent,
    MessagesComponent,
    ProfilesComponent,
    AdminComponent,
    VacancyListComponent,
    VacancyProofComponent,
    ResumeListComponent,
    ResumeProofComponent,
    VacancyDetailsComponent,
    ProgressEducationComponent,
    ServicesComponent,
    OptionEditComponent,
    TranslateEditComponent,
    TranslateEditShowComponent,
    InvoiceComponent],
  exports: [VacancyComponent, ResumeComponent, HelpersComponent, MessagesComponent, ProfilesComponent],
  providers: [EditTranslateserviceService,]

})
export class AdminModule { }
