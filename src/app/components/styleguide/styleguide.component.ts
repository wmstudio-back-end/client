import {Component} from "@angular/core";
@Component({
  moduleId:module.id.toString(),
  styleUrls:  ['styleguide.component.css'],
  templateUrl:'styleguide.component.html'
})

export class StyleguideComponent {
  arrIn = [{
    "id":  1,
    "text":"Basic"
  },
    {
      "id":  2,
      "text":"Intermediate"
    },
    {
      "id":  3,
      "text":"Advanced"
    },
    {
      "id":  4,
      "text":"Expert"
    }]
  arrOut = []
  private icn = {
    asd: "sad",
    asd2:"azsd"
  }
  unitySelect: boolean = false;
  accordionAcitve: boolean = false;
  accordionValues: any = Array();
  accordionSelect: any = Array();
  html: boolean = true;
  ts: boolean = false;
  btn: boolean = false;
  sel: boolean = false;
  acc: boolean = false;
  ic: boolean = false;
  inp: boolean = false;
  popup: boolean = false;
  checkbox: boolean = false;
  modal: boolean = false;
  typography: boolean = false;
  table: boolean = false;
  pagination: boolean = false;

  elements: boolean = false;


  // elements: boolean = false;
  rating: boolean = false;

  
  countChange(ee): any {
    return ee
  }
  
  clear() {
    this.btn = false;
    this.sel = false;
    this.acc = false;
    this.ic = false;
    this.inp = false;
    this.checkbox = false;
    this.modal = false;
    this.typography = false;
    this.table = false;
    this.pagination = false;

    this.elements = false;
    this.rating = false;

  }
  
  clearT() {
    this.html = false;
    this.ts = false;
  }
  
  constructor() {
    this.accordionValues = [
      {name:'russia', val:false, id:1},
      {name:'russia2', val:false, id:2},
      {name:'russ123ia', val:false, id:3},
      {name:'russ123ia', val:false, id:4},
      {name:'russ123ia', val:false, id:5}
    ];
    
  }


}
