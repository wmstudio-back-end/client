﻿import {Component} from "@angular/core";
@Component({
  selector:   'toolbar',
  moduleId:   module.id.toString(),
  templateUrl:'toolbar.component.html',
  styleUrls:  ['toolbar.component.css']

})
export class ToolBar {
  tabList = [
    {
      link:   'My account',
      content:'t vero voluptatum. Accusamus, alias autem consequatur dicta error ipsam, iure nam natus optio provident quasi quod reiciendis, repudiandae sapiente unde ut veniam?',
      active: false
    },
    {
      link:   'My resume',
      content:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet animi consequatur consequuntur corporis cupiditate enim inventore laudantium, nam, obcaecati odio officia optio placeat reiciendis repudiandae sequi velit vero voluptatum. Acc ut veniam?',
      active: false
    },
    {
      link:   'Ways to search vacancies effectively',
      content:'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet animi consequatur consequuntur corporis cupiditate enim inventore laudantium, nam, obcaecati odio officia optio placeat reiciendis repudiandae sequi velit vero voluptatum. A',
      active: false
    },
    {
      link:   'Profile information and company rating',
      content:'Lor co ur corporis cupiditate enim inventore laudantium, nam, obcaecati odio officia optio placeat reiciendis repudiandae sequi velit vero voluptatum. A',
      active: false
    }
  ];
  
  initTab(ob: any) {
    this.tabList = ob;
  }
  
  toggle(tabItem: any) {
    console.log(123);
    tabItem.active = !tabItem.active;
  }
}
