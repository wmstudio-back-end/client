"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var ToolBar = (function () {
  function ToolBar() {
    this.tabList = [
      {
        link: 'My account',
        content: 't vero voluptatum. Accusamus, alias autem consequatur dicta error ipsam, iure nam natus optio provident quasi quod reiciendis, repudiandae sapiente unde ut veniam?',
        active: false
      },
      {
        link: 'My resume',
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet animi consequatur consequuntur corporis cupiditate enim inventore laudantium, nam, obcaecati odio officia optio placeat reiciendis repudiandae sequi velit vero voluptatum. Acc ut veniam?',
        active: false
      },
      {
        link: 'Ways to search vacancies effectively',
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam amet animi consequatur consequuntur corporis cupiditate enim inventore laudantium, nam, obcaecati odio officia optio placeat reiciendis repudiandae sequi velit vero voluptatum. A',
        active: false
      },
      {
        link: 'Profile information and company rating',
        content: 'Lor co ur corporis cupiditate enim inventore laudantium, nam, obcaecati odio officia optio placeat reiciendis repudiandae sequi velit vero voluptatum. A',
        active: false
      }
    ];
  }

  ToolBar.prototype.initTab = function (ob) {
    this.tabList = ob;
  };
  ToolBar.prototype.toggle = function (tabItem) {
    console.log(123);
    tabItem.active = !tabItem.active;
  };
  return ToolBar;
}());
ToolBar = __decorate([
  core_1.Component({
    selector: 'toolbar',
    moduleId: module.id.toString(),
    templateUrl: 'toolbar.component.html',
    styleUrls: ['toolbar.component.css'],
  })
], ToolBar);
exports.ToolBar = ToolBar;
