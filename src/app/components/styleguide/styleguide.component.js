"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var StyleguideComponent = (function () {
  function StyleguideComponent() {
    this.arrIn = [{
      "id": 1,
      "text": "Basic"
    },
      {
        "id": 2,
        "text": "Intermediate"
      },
      {
        "id": 3,
        "text": "Advanced"
      },
      {
        "id": 4,
        "text": "Expert"
      }];
    this.arrOut = [];
    this.unitySelect = false;
    this.accordionAcitve = false;
    this.accordionValues = Array();
    this.accordionSelect = Array();
    this.html = true;
    this.ts = false;
    this.btn = false;
    this.sel = false;
    this.acc = false;
    this.ic = false;
    this.inp = false;
    this.popup = false;
    this.accordionValues = [
      {name: 'russia', val: false, id: 1},
      {name: 'russia2', val: false, id: 2},
      {name: 'russ123ia', val: false, id: 3},
      {name: 'russ123ia', val: false, id: 4},
      {name: 'russ123ia', val: false, id: 5}
    ];
    console.log(this.accordionValues);
  }

  StyleguideComponent.prototype.countChange = function (ee) {
    return ee;
  };
  StyleguideComponent.prototype.clear = function () {
    this.btn = false;
    this.sel = false;
    this.acc = false;
    this.ic = false;
    this.inp = false;
  };
  StyleguideComponent.prototype.clearT = function () {
    this.html = false;
    this.ts = false;
  };
  return StyleguideComponent;
}());
StyleguideComponent = __decorate([
  core_1.Component({
    moduleId: module.id.toString(),
    styleUrls: ['styleguide.component.css'],
    templateUrl: 'styleguide.component.html'
  })
], StyleguideComponent);
exports.StyleguideComponent = StyleguideComponent;
