import {NgModule}                               from "@angular/core";
import {CommonModule}                           from "@angular/common";
import {RouterModule}                           from "@angular/router";
import {TranslateModule}                        from "@ngx-translate/core";
import {FormsModule}                            from "@angular/forms";
import {SidebarModule}                          from "../sidebar/sidebar.module";
import {CreateComponent}                        from "./create/create.component";
import {CreateResumeObjectiveComponent}         from "./create/create-resume-objective/create-resume-objective.component";
import {CreateResumeKeyskillsComponent}         from "./create/create-resume-education-keyskills/create-resume-keyskills.component";
import {CreateResumeLanguageComponent}          from "./create/create-resume-language/create-resume-language.component";
import {CreateResumeComputerSkillsComponent}    from "./create/create-resume-computer-skills/create-resume-computer-skills.component";
import {CreateResumeIntershipComponent}         from "./create/create-resume-intership/create-resume-intership.component";
import {CreateResumeJobOptionComponent}         from "./create/create-resume-job-option/create-resume-job-option.component";
import {CreateResumeEducationProgressComponent} from "./create/create-resume-education-progress/create-resume-education-progress.component";
import {CreateResumeWorkExpComponent}           from "./create/create-resume-work-exp/create-resume-work-exp.component";
import {CreateResumeRecommendationComponent}    from "./create/create-resume-recommendation/create-resume-recommendation.component";
import {CreateResumePesonalityComponent}        from "./create/create-resume-pesonality/create-resume-pesonality.component";
import {CreateResumeInterestComponent}          from "./create/create-resume-interest/create-resume-interest.component";
import {CreateResumeAdditionalComponent}        from "./create/create-resume-additional/create-resume-additional.component";
import {ResumeCreateIndexComponent}             from "./create/resume-create-index/resume-create-index.component";
import {PopupComponent}                         from "./create/popup.component";
import {HttpModule}                             from "@angular/http";
import {CreateResumeService}                    from "./create/create.service";
import {SearchCandidatesComponent}              from "./search-candidates/search-candidates.component";

import {MyDatePickerModule}             from "mydatepicker";
import {NextLinkResumeComponent}        from './create/next-link-resume/next-link-resume.component';
import {TextMaskModule}                 from "angular2-text-mask";
import {RequestRecommendationComponent} from "./create/_components/request-recommendation/request-recommendation.component";
import {CandidateResumeComponent}       from "./candidate-resumes/candidate-resumes.component";
import {ClipboardModule}                from 'ngx-clipboard';
import {ModalRecommendationComponent}   from '../../popup-modals/modal-recommendation/modal-recommendation.component';
import {ListComponent}                  from "./list/list.component";
import {AllResumesComponent}            from './all-resumes/all-resumes.component';
import {ModulesModule} from "../../modules/modules.module";
import {ModalModule} from "../modal/modal.module";



@NgModule({
    imports:      [
        CommonModule,
        RouterModule,
        TranslateModule,
        FormsModule,
        ModulesModule,
        SidebarModule,
        MyDatePickerModule,
        ModalModule,
        TextMaskModule,
        HttpModule,
        ClipboardModule,
        RouterModule.forChild([
            {
                path:      '',
                component: ListComponent,
                data:      {
                    breadcrumb: ''
                }

            },
            {
                path:      'details/:id',
                component: CandidateResumeComponent,
                data:      {}
            },
            {
                path:      'search',
                component: SearchCandidatesComponent,
                data:      {}

            }, {
                path:      'search/:id',
                component: SearchCandidatesComponent
            },
            {
                path:      'all-resumes',
                component: AllResumesComponent,

            },

            {
                path:      'create',
                component: CreateComponent,
                data:      {},
                children:  [
                    {
                        path:      '',
                        component: ResumeCreateIndexComponent,
                        data:      {
                            name: "INDEX",
                            url:  ""
                        }
                    }]
            },
            {
                path:      'create/:id',
                component: CreateComponent,
                data:      {
                    breadcrumb: ''
                },

                children: [
                    {
                        path:      'dublicate',
                        component: ResumeCreateIndexComponent,
                        data:      {
                            name: "DUBLICATE",
                            url:  ""
                        }
                    },
                    {
                        path:      '',
                        component: ResumeCreateIndexComponent,
                        data:      {
                            name: "",
                            url:  ""
                        }
                    },
                    {
                        path:      'objective',
                        component: CreateResumeObjectiveComponent,
                        data:      {
                            name: "Education & Key skills",
                            url:  "keyskills"
                        }
                    },
                    {
                        path:      'keyskills',
                        component: CreateResumeKeyskillsComponent,
                        data:      {
                            name: "Languages skills",
                            url:  "language"
                        }
                    },
                    {
                        path:      'language',
                        component: CreateResumeLanguageComponent,
                        data:      {
                            name: "Computer skills & other",
                            url:  "computer"
                        }
                    },
                    {
                        path:      'computer',
                        component: CreateResumeComputerSkillsComponent,
                        data:      {
                            name: "Work experience",
                            url:  "work-exp"
                        }
                    },
                    {
                        path:      'work-exp',
                        component: CreateResumeWorkExpComponent,
                        data:      {
                            name: "Personality traits",
                            url:  "personality"
                        }
                    },
                    {
                        path:      'personality',
                        component: CreateResumePesonalityComponent,
                        data:      {
                            name: "Interests & values",
                            url:  "interest"
                        }
                    },
                    {
                        path:      'interest',
                        component: CreateResumeInterestComponent,
                        data:      {
                            name: "Job options",
                            url:  "job-option"
                        }
                    },
                    {
                        path:      'job-option',
                        component: CreateResumeJobOptionComponent,
                        data:      {
                            name: "Internship options",
                            url:  "intership-option"
                        }

                    },
                    {
                        path:      'intership-option',
                        component: CreateResumeIntershipComponent,
                        data:      {
                            name: "Additional information",
                            url:  "additional"
                        }

                    },
                    {
                        path:      'additional',
                        component: CreateResumeAdditionalComponent,
                        data:      {
                            name: "Progress in education",
                            url:  "education-progress"
                        }
                    },
                    {
                        path:      'education-progress',
                        component: CreateResumeEducationProgressComponent,
                        data:      {
                            name: "Recommendations",
                            url:  "recommendations"
                        }
                    },
                    {
                        path:      'recommendations',
                        component: CreateResumeRecommendationComponent,
                        data:      {
                            name: "Objective",
                            url:  "objective"
                        }
                    }
                ]


            }
        ])
    ],
    declarations: [
        CreateComponent,
        ListComponent,
        PopupComponent,
        CreateResumeObjectiveComponent,
        CreateResumeKeyskillsComponent,
        CreateResumeLanguageComponent,
        CreateResumeComputerSkillsComponent,
        CreateResumeIntershipComponent,
        CreateResumeJobOptionComponent,
        CreateResumeEducationProgressComponent,
        CreateResumeWorkExpComponent,
        CreateResumeRecommendationComponent,
        CreateResumePesonalityComponent,
        CreateResumeInterestComponent,
        CreateResumeAdditionalComponent,
        ResumeCreateIndexComponent,
        SearchCandidatesComponent,
        NextLinkResumeComponent,
        AllResumesComponent,
        RequestRecommendationComponent,
        CandidateResumeComponent,
        ModalRecommendationComponent,

    ],
    exports:      []
    //schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    //bootstrap: [ CreateComponent]
})
export class ResumeModule {

}
