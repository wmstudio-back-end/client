import { Component, OnInit }    from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DataService}            from "../../../_services";
import {ComponentService}       from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";

@Component({
  selector: 'app-all-resumes',
  templateUrl: './all-resumes.component.html',
  styleUrls: ['./all-resumes.component.scss']
})
export class AllResumesComponent implements OnInit {
    resumeIds = []


    constructor(public uS:UserService,private route: ActivatedRoute, private router: Router, public dS: DataService, public component: ComponentService) {
    }

    ngOnInit() {
        this.resumeIds = this.route.snapshot.params['ids'].split(',')
    }

}
