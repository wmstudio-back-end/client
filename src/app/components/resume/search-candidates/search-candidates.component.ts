import {
  Component,
  ElementRef,
  OnInit,
  ViewChild
}                         from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {DataService}      from "../../../_services/data.service";
import {User}             from "../../../_models/user";
import {Location}         from '@angular/common';
import {UserService}      from "../../../_services/user.service";
import {IResume}          from "../../../_models/resume";
import {ResumeService}    from "../../../_services/resume.service";
import {MessageService}   from "../../../_services/message.service";
import {
  ActivatedRoute,
  Router
}                         from "@angular/router";

import {DownPopupService} from "../../../_services/down-popup.service";
import {TranslateService} from "@ngx-translate/core";


@Component({
  templateUrl: './search-candidates.component.html',
  styleUrls:   ['./search-candidates.component.scss']
})
export class SearchCandidatesComponent implements OnInit {
  private message                 = {}
          resume                  = []
          resumeIds               = []
          resArr: IResume[]
          users: User[]           = []
          usersArr: User[]
          skip                    = 0
          limit                   = 8
          count                   = 0
          curPage                 = 1
          maxPage                 = 1
          pages                   = []
          saveModal: boolean      = false
          compact: boolean        = false
          sortBtns                = {
            "rating":        0,
            "user.birthday": 0,
            "recommends":    0
          }
          filterSpActive: boolean = false
          _id: string             = ''

  constructor(public ds: DataService,
              public rs: ResumeService,
              public component: ComponentService,
              public location: Location,
              public userService: UserService,
              public mS: MessageService,
              private router: Router,
              private route: ActivatedRoute,
              public popS: DownPopupService,
              public  translate: TranslateService
  ) {
    if (typeof route.snapshot.params['id'] !== "undefined")
      this._id = route.snapshot.params['id']
  }

  ngOnInit() {
    if (confirm(this.translate.get('errors.' + 47)['value'])) {


    }
    // this.popS.show_warning(this.ds.getOptionsFromId('errors', 47).text)
    // this.rs.clearFilter()
    console.log('isset set search - '+this._id);
    if (this._id) {
      this.rs.setSaveSearch(this._id)
    } else {
      this.rs.setSaveSearch()
    }
    //this.translate.onLangChange.subscribe(()=>{
    //    this.getResume();
    //})
    this.getResume()
    this.rs.afterFilter.subscribe((count) => {
      this.count    = count
      let pages = [];
      this.pages    = []
      let activPage = this.skip / this.limit
      this.curPage = activPage + 1;
      this.maxPage = Math.ceil(this.count / this.limit)
      for (var i = 0; i < this.maxPage; i++) {
        let status   = activPage == i ? 3 : 1
        let num: any = i + 1
        let pushPage = false
        if (i === 0) {
          pushPage = true
        }
        if (activPage === 0 && i === 3){
          pushPage = true
        }
        if (activPage > 3 && i === 1) {
          pushPage = true
          num      = '...'
        }
        if (i < activPage + 3 && i > activPage - 3 && i != this.maxPage - 1) {
          pushPage = true
        }
        if (activPage < this.maxPage - 4 && i == this.maxPage - 2) {
          pushPage = true
          num      = '...'
        }
        if (i == this.maxPage - 1) {
          pushPage = true
        }

        if (pushPage) {
          pages.push({
            status: status,
            num:    num,
            style:  0
          })
        }
      }
      this.pages = pages
    })
  }

  setPage(n) {
    if (n == '...') return;
    this.skip = (n - 1) * this.limit
    this.getResume()
  }

  getResume() {
    return new Promise((resolve, reject) => {
      let sort = {}
      for (var key in this.sortBtns) {
        if (this.sortBtns[key] == 1) {
          sort[key] = 1
        }
        if (this.sortBtns[key] == 2) {
          sort[key] = -1
        }
      }
      this.rs.filter.sort  = sort
      this.rs.filter.skip  = this.skip
      this.rs.filter.limit = this.limit
      this.rs.getResume().then(data => {

        for (var i = 0; i < data['resume'].length; i++) {
          this.ds.sendws("Events/setEvent", {
            event: {
              name:   'searchResume',
              option: {_id: data['resume'][i]._id}
            }
          }).then((data) => {

          })

        }
        resolve(data)
      })
    })
  }

  formatData(time) {
    return new Date(time * 1000);
  }

  setSortResume(sort) {
    let {key, val} = sort
    for (var k in Object.keys(this.sortBtns)) {
      if (key != Object.keys(this.sortBtns)[k]) {
        this.sortBtns[Object.keys(this.sortBtns)[k]] = 0
      }
    }
    this.rs.filter.sort = {}
    this.sortBtns[key]  = val
    this.getResume()
  }

  sortResume(key) {
    for (var k in Object.keys(this.sortBtns)) {
      if (key != Object.keys(this.sortBtns)[k]) {
        this.sortBtns[Object.keys(this.sortBtns)[k]] = 0
      }

    }
    this.rs.filter.sort = {}
    switch (this.sortBtns[key]) {
      case 0:
        this.sortBtns[key] = 1
        break;
      case 1:
        this.sortBtns[key] = 2
        break;
      case 2:
        this.sortBtns[key] = 1
        break;
    }

    this.getResume()
  }

  prevPage() {
    this.skip -= this.limit
    if (this.skip < 0) {
      this.skip = 0
    }
    this.getResume()
  }

  nextPage() {
    this.skip += this.limit
    this.getResume()
  }

  setBookmark(id) {
    this.userService.setBookmark(id)
    let i = this.resume.findIndex((e, i, a) => {
      return (e._id == id) ? true : false;
    })
    if (i > -1)
      this.resume[i].bookmark = true
  }

  unsetBookmark(id) {
    this.userService.unsetBookmark(id)
    let i = this.resume.findIndex((e, i, a) => {
      return (e._id == id) ? true : false;
    })
    if (i > -1)
      this.resume[i].bookmark = false
  }

  toggleDetail(n) {
    if (this.rs.viewItems[n])
      window.scrollTo(0, document.getElementById('detailView' + n).offsetTop)
    this.rs.viewItems[n] = !this.rs.viewItems[n]
  }
}
