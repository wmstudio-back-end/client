import { Component, OnInit } from '@angular/core';
import {ComponentService} from '../../../_services/component.service';
import {Router} from "@angular/router";
import {UserService} from "../../../_services/user.service";
import {CreateResumeService} from "../create/create.service";
import {DataService} from "../../../_services/data.service";
import {HelpersService} from "../../../_services/helpers.service";
import {DownPopupService} from "../../../_services/down-popup.service";


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {





  constructor(
    public component: ComponentService,
    public uS:UserService,
    public cS:CreateResumeService,
    private router: Router,
    private helpers:HelpersService,
    private dS:DataService,
    public popS:DownPopupService,
  ) { }

  checkPublish(id,val){
    let i = this.uS.profile.Student.resumes.findIndex((e,k,a)=>{
      return (e._id==id)?true:false;
    })
    if (i!=-1)
      this.uS.profile.Student.resumes[i].publish=val;
    this.dS.sendws('Resume/publish',{_id:id,publish:val}).then((data)=>{
      console.log(data)
      val?this.popS.show_success(this.dS.getOptionsFromId('errors', 30).text):this.popS.show_success(this.dS.getOptionsFromId('errors', 31).text)
    })
  }
  deleteResume(i){
    if (this.helpers.confirm(0)){

      this.dS.sendws('Resume/deleteResume',{_id:this.uS.profile.Student.resumes[i]._id}).then((data)=>{
        this.uS.profile.Student.resumes.splice(i,1)
        console.log(data)
      })
    }
  }

  ngOnInit() {
    this.cS.dublicate = false
  }


}
