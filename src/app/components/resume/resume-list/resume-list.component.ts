import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DataService} from "../../../_services/data.service";
import {ResumeService} from "../../../_services/resume.service";
import {UserService} from "../../../_services/user.service";
import {HelpersService} from "../../../_services/helpers.service";
import {DownPopupService} from "../../../_services/down-popup.service";
import {MessageService} from "../../../_services/message.service";


@Component({
  selector:   'app-resume-list-modules',
  templateUrl:'./resume-list.component.html',
  styleUrls:  ['./resume-list.component.scss']
})
export class ResumeListModuleComponent implements OnInit {
  @Input() btnNotes = false
  @Input() access = false
  @Input() resumeIds = []
  @Input() btnCoverLetter = false
  @Input() bookmarkLine:boolean = false
  @Input() defaultNotesShow:boolean = false
  @Input() btnContact:boolean = false
  @Input() btnBookmark:boolean = false
  @Input() btnView:boolean = false
  @Output() changeBookmark = new EventEmitter<boolean>()
  @Output() changeType = new EventEmitter<boolean>()
  @Output() changeProject = new EventEmitter<string>()
  @Output() countResumes = new EventEmitter<number>()
  @Output() routeDetail = new EventEmitter<string>()
    messCount = {}
  recommCount = {}
  boomarkModal = false
  boomarkId:string
  resume = []
  constructor(
    public dS:DataService,
    public rS:ResumeService,
    public uS:UserService,
    public help: HelpersService,
    public popS:DownPopupService,
    public mS:MessageService,
    private router: Router,
    private route: ActivatedRoute
  ) {

    console.log('searchID!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',this.route.snapshot.params['id'])
  }


  ngOnChanges(e) {
    this.resume = []
    console.log(e.resumeIds)
    if (e.resumeIds && e.resumeIds.hasOwnProperty('currentValue')) {
     this.reloadResume()
    }
    //if (e.resume && e.resume.hasOwnProperty('currentValue')) {
    //
    //}


  }
  getCoverLettersContent(_id){

    for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {
      if (
        this.uS.profile.subscribeCandidate[i].resumeId==_id
        &&this.uS.profile.subscribeCandidate[i].vacancyId==this.route.snapshot.params['id']){return this.uS.profile.subscribeCandidate[i].content}

    }
    return ''
  }
  addedToList(id){

    this.rS.getresumeFromId(id,false).then(resume=> {
      // console.log('------RESUME',resume)
      if (resume&&this.help.findObject(this.resume, '_id', resume['_id']) < 0) {
        resume['content'] = this.getCoverLettersContent(resume['_id'])
        this.resume.push(resume)
      }
    }).catch(data=>{

    })
  }

  reloadResume(){

    this.resume = []
    if (this.resumeIds)
      for (var i = 0; i < this.resumeIds.length; i++) {
        this.addedToList(this.resumeIds[i])


      }
  }

  ngOnInit() {
    this.rS.resumesId.subscribe((data)=>{
      this.resumeIds = data
      this.reloadResume()
    })
    this.reloadResume()
  }
  ifNotes(val){
    console.log('===NOTES',val)
    if (val&&this.btnNotes&&val.value=='true'){
      return true
    }
    return false

  }
  ifCover(val){
    //coverLetter&&btnCoverLetter&&getCoverLetters(item.uid,item._id).length>0&&coverLetter.value=='true'
    if (val&&this.btnCoverLetter&&val.value=='true'){
      return true
    }
    return false
  }
  setBookmark(id){

    if (this.uS.accessPackage.projBmark){
      this.boomarkModal = true
      this.boomarkId = id
    }else{
      this.uS.setBookmark(id)

      let i = this.resume.findIndex((e, i, a) => {
        return (e._id == id) ? true : false;
      })
      if (i > -1)
        this.resume[i].bookmark = false
    }
    this.changeBookmark.emit(true)


  }
  unsetBookmark(id){

    this.uS.unsetBookmark(id)
    let i = this.resume.findIndex((e, i, a) => {
      return (e._id == id) ? true : false;
    })
    if (i > -1)
      this.resume[i].bookmark = false
    this.changeBookmark.emit(true)
  }
  isBookmark(id){

      for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
        if (this.uS.profile.bookmarks.items[i].resumeId==id){
          return true
        }
      }
      return false

  }
  checkTypeBookM(r,e,pr,sett,res){

    //if (!this.uS.accessPackage.projBmark){
    //  alert('нет подписки')
    //  return
    //}
    //for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
    //  if (this.uS.profile.bookmarks.items[i].resumeId==r._id){
    //    this.uS.profile.bookmarks.items[i].type=e.id
    //    this.innerProject(this.uS.profile.bookmarks.items[i],r)
    //  }
    //}
    //this.progetsList[pr].types[sett].resumes.splice(res,1)
    //this.dS.sendws('User/updateBookmark',{id:r._id,type:e.id})
    //console.log(e)
  }
  //checkTypeBookM(r,e,pr,sett,res){
  //  if (!this.uS.accessPackage.projBmark){
  //    alert('нет подписки')
  //    return
  //  }
  //  for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
  //    if (this.uS.profile.bookmarks.items[i].resumeId==r._id){
  //      this.uS.profile.bookmarks.items[i].type=e.id
  //      this.innerProject(this.uS.profile.bookmarks.items[i],r)
  //    }
  //  }
  //  this.progetsList[pr].types[sett].resumes.splice(res,1)
  //  this.dS.sendws('User/updateBookmark',{id:r._id,type:e.id})
  //  console.log(e)
  //}

  //checkActionBookM(r,e){
  //  if (!this.uS.accessPackage.projBmark){
  //    alert('нет подписки')
  //    return
  //  }
  //  for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
  //    if (this.uS.profile.bookmarks.items[i].resumeId==r._id){
  //      this.uS.profile.bookmarks.items[i].action=e.id
  //    }
  //  }
  //
  //  this.dS.sendws('User/updateBookmark',{id:r._id,action:e.id})
  //
  //}
  getCoverLetters(uid,rid){
    var t = []
    for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {
      if (this.uS.profile.subscribeCandidate[i].resumeId==rid){
        t.push({
          text:this.uS.profile.subscribeCandidate[i].content
        })
      }

    }
    return t
  }
  NotesShow(n){

    //if (val=='false'){return true}
    //if (val=='true'){return false}
    document.getElementById('notes_'+n).classList.contains('active')?
      document.getElementById('notes_'+n).classList.remove('active'):
      document.getElementById('notes_'+n).classList.add("active");
  }
  CoverLetterShow(n){
    document.getElementById('covlet_'+n).classList.contains('active')?
      document.getElementById('covlet_'+n).classList.remove('active'):
      document.getElementById('covlet_'+n).classList.add("active");
  }
  toggleDetail(n){
    document.getElementById('toggle_'+n).classList.contains('active')?
      document.getElementById('toggle_'+n).classList.remove('active'):
      document.getElementById('toggle_'+n).classList.add("active");

    document.getElementById('toggle_'+n).classList.contains('active')?
      document.getElementById('toggle_turn'+n).classList.add('turn'):
      document.getElementById('toggle_turn'+n).classList.remove("turn");

    //console.log(document.getElementById('toggle_'+n).classList.contains('active'))

  }
  viewDetail(){

  }
  getStatus(_id){
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==_id){
        return this.uS.profile.bookmarks.items[i].type
      }
    }
  }
  setStatus(_id,statusId){
    if (!this.uS.accessPackage.projBmark){
      this.popS.show_warning(this.dS.getOptionsFromId('errors',10).text)
      return
    }
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==_id){
        this.uS.profile.bookmarks.items[i].type = statusId
        this.dS.sendws('User/updateBookmark',{id:_id,type:statusId})
        this.changeType.emit(true)
      }
    }
  }

  getAction(_id){
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==_id){
        return this.uS.profile.bookmarks.items[i].action
      }
    }
  }
  setAction(_id,actionId){
    if (!this.uS.accessPackage.projBmark){
      this.popS.show_warning(this.dS.getOptionsFromId('errors',10).text)
      return
    }
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==_id){
        this.uS.profile.bookmarks.items[i].action = actionId
        this.dS.sendws('User/updateBookmark',{id:_id,action:actionId})
      }
    }
  }
  getProjects(){
    var t = []
    for (var i = 0; i < this.uS.profile.Company.projects.length; i++) {
      t.push({
        id:this.uS.profile.Company.projects[i]._id,
        text:this.uS.profile.Company.projects[i].name
      })
    }
    return t
  }
  setProject(_id,projectId){
    if (!this.uS.accessPackage.projBmark){
      this.popS.show_warning(this.dS.getOptionsFromId('errors',10).text)
      return
    }
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==_id){
        this.uS.profile.bookmarks.items[i].project = projectId
        this.dS.sendws('User/updateBookmark',{id:_id,project:projectId})
        this.changeProject.emit(projectId)
      }
    }
  }
  getProject(_id){
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==_id){
        return this.uS.profile.bookmarks.items[i].project
      }
    }
    return null
  }
  getFromBookM(r){
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==r._id){
        return this.uS.profile.bookmarks.items[i].savedFrom
      }
    }
    return 0
  }
  getCreatedAtBook(r){
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==r._id){
        return this.uS.profile.bookmarks.items[i].created_at
      }
    }
  }
  getMessCount(uid){
    if (this.messCount[uid]){
      return this.messCount[uid]
    }
    return 0
  }
  getRecommCount(uid){
    if (this.recommCount[uid]){
      return this.recommCount[uid]
    }
    return 0
  }
}
