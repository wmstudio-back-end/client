
import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {MessageService} from "../../../_services/message.service";
import {DataService} from "../../../_services/data.service";
import {IResume} from "../../../_models/resume";
import {User} from "../../../_models/user";
import {ResumeService} from "../../../_services/resume.service";
import {UserService} from "../../../_services/user.service";
import {HelpersService} from "../../../_services/helpers.service";

import {ComponentService} from "../../../_services/component.service";
import {DownPopupService} from "../../../_services/down-popup.service";

@Component({
    selector: 'candidate-resumes',
    templateUrl: './candidate-resumes.component.html',
    styleUrls: ['./candidate-resumes.component.scss']
})
export class CandidateResumeComponent implements OnInit {
    resume: IResume
    user: User
    countRec: number = 25
    recomms = []
  location
    modalRecomm = {}
    openModal: boolean = false;
    blocks = {
        objective: true,
        intern: true,
        job: true,
        edu_skills: true,
        skills: true,
        langs: true,
        comp_skills: true,
        work_exp: true,
        interests: true,
        personaliti: true,
        add_info: true,
        cert: true,
        driving: true,
        recommends: true,
        docs: true,
        contacts: true,
    }
    resumeView = {

    }
    objectKeys = Object.keys;
  Expected_graduation = false
  Major = false
  Minor = false
  isView = true
    constructor(public router: Router,
                public route: ActivatedRoute,
                public mS: MessageService,
                public ds: DataService,
                public rs: ResumeService,
                public us: UserService,

                public component: ComponentService,
                public popS:DownPopupService,
                public hs: HelpersService) {

        this.router.events.subscribe( (e) => {

                this.ngOnInit()
        });


    }

  contacttUser(uid){
    if (this.us.accessPackage.applicants){
      this.mS.setDialogs(uid);
      this.router.navigate(['messages'])
    }else{
      this.popS.show_warning(this.ds.getOptionsFromId('errors',10).text);
    }
  }
  differenceWorkTime(work){
    //startDateMonth
    //startDateYear
    //endDateMonth
    //endDateYear
    let yres = 0;
    let mres = 0;
    let y2 = 0;
    let m2 = 0;
    if (work.currentlyWorking){
      y2 = new Date().getFullYear()
      m2 = new Date().getMonth()+1
    }else{
      y2 = work.endDateYear
      m2 = work.endDateMonth
    }
    yres = y2-work.startDateYear;
    mres = m2-work.startDateMonth;

    if (mres<0){
      yres-=1
      mres-=-12
    }
    var strY = ''
    var strM = ''
    var comma = ''
    if (mres>0&&yres>0){comma = ', '}
    if (yres>0){strY = yres+' year'}
    if (mres>0){strM = mres+' months'}

  return strY+comma+strM
  }

  getSchoolName(id){
    for (var i = 0; i < this.us.users[this.resume.uid].Student.schools.length; i++) {
      if(this.us.users[this.resume.uid].Student.schools[i].schoolId==id){
        return this.us.users[this.resume.uid].Student.schools[i].schoolName
      }

    }
    return ''
  }
    ngOnInit() {
      this.location = window.location.origin


        this.resume = <IResume>{}
      this.rs.getresumeFromId(this.route.snapshot.params['id'],false).then(resume=> {

        var uid = resume['uid']

        //this.rs.getresumeFromId(this.route.snapshot.params['id'],false).then((data) => {
        //this.ds.sendws("Resume/getResumeFromId", {_id: this.route.snapshot.params['id']}).then((data) => {
            //console.log("==============RESUME=============",data)
            // var uid = data['resume']['uid']
            // var resume = data['resume']
          if (resume['uid']!=this.us.profile._id&&this.isView){
            this.rs.setViweResume(this.route.snapshot.params['id'])
            this.isView = false

          }
          // this.rs.setViweResume(this.route.snapshot.params['id'])
            this.us.getUsers([uid]).then((data) => {
                //console.log("==============USER=============",data[0])
                this.us.getSchoolUDFromUid(uid).then((data) => {
                    //console.log("==============USER.STUDENT.SCHOOLS=============",data)
                    this.resume = <IResume>resume
                  if (!this.resume.prc){
                    this.resume.prc = 0
                  }


                  for (var i = 0; i < this.us.users[this.resume.uid].Student.schools.length; i++) {
                    if(this.us.users[this.resume.uid].Student.schools[i].major){
                      this.Major = true
                    }
                    if(this.us.users[this.resume.uid].Student.schools[i].minor){
                      this.Minor = true
                    }
                    if(this.us.users[this.resume.uid].Student.schools[i].expectedGraduationMonth){
                      this.Expected_graduation = true
                    }

                  }
                })
                // this.ds.sendws('Recommendation/getCountRecommFromUid', {uid: uid}).then(data => {
                //   this.countRec = data['res']['count']
                // })
            this.us.getRecommendation(uid).then(data=>{
              this.recomms = []

              // this.recomms = data['res']
              var ids = []
              for (var i = 0; i < this.us.users[uid].recommendations.length; i++) {
                if (this.us.users[uid].recommendations[i].educatorId){
                  ids.push(this.us.users[uid].recommendations[i].educatorId)
                }

                if (this.us.users[uid].recommendations[i].status==1&&this.us.users[uid].recommendations[i].visibility){
                  this.recomms.push(this.us.users[uid].recommendations[i])
                  var rat = 0
                  rat+=this.us.users[uid].recommendations[i].rating_dilligance
                  rat+=this.us.users[uid].recommendations[i].rating_poise
                  rat+=this.us.users[uid].recommendations[i].rating_sociability
                  this.us.users[uid].recommendations[i].rating = parseFloat((rat/3).toFixed(1))
                }

              }
              this.us.getUsers(ids).then(users=>{



              })


             })
          })

        })
    }

    printPdf() {

        //
        // const elementToPrint = window.document.getElementById('resumeCandidate');
        // const pdf = new jsPDF('p', 'pt', 'a4');
        // var specialElementHandlers = {
        //     '#editor': function (element, renderer) {
        //         return true;
        //     }
        // };
        // var options = {
        //     margin: {top: 10, right: 10, bottom: 10, left: 10, useFor: 'content'},
        //     'pagesplit': true,
        //     'elementHandlers': specialElementHandlers
        // };
        //
        // let margins = {
        //     top: 100,
        //     bottom: 100,
        //     left: 40,
        //     width: 750
        // };
        // pdf.addHTML(elementToPrint, 10, 10, options, () => {
        //     pdf.save('web.pdf');
        // }, margins);
    }

    setBookmark(id) {
        this.us.setBookmark(id)
        this.resume.bookmark = true
    }

    unsetBookmark(id) {
        this.us.unsetBookmark(id)
        this.resume.bookmark = false
    }

}
