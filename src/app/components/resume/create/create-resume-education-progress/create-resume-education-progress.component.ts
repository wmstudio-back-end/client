import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "../create.service";
import {HelpersService} from "../../../../_services/helpers.service";
import {IMyDpOptions} from "mydatepicker";
import {DataService} from "../../../../_services/data.service";
import {UserService} from "../../../../_services/user.service";
import {IschoolProgressEducation} from "../../../../_models/user";
import {ifTrue} from "codelyzer/util/function";
import {DownPopupService} from "../../../../_services/down-popup.service";

@Component({
  selector: 'app-create-resume-education-progress',
  templateUrl: './create-resume-education-progress.component.html',
  styleUrls: ['./create-resume-education-progress.component.scss']
})
export class CreateResumeEducationProgressComponent implements OnInit {
  rand:string;
  rand1:string;
  rand2:string;
  rand3:string;
  rand4:string;
  rand5:string;
  rand6:string;
  rand7:string;
  rand8:string;
  rand9:string;
  rand10:string;
  gradeRed: number;
  photo:any;

  public datemmyyyy: Array<string | RegExp>= [  /[0-1]/,/[0-9]/,'/',/[1-2]/,/[0-9]/,/[0-9]/,/[0-9]/]
  public avarage: Array<string | RegExp>= [ /[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/]
  private newIschoolProgressEducation : IschoolProgressEducation
  private myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'mm.yyyy',
    minYear:new Date().getFullYear(),
    width:'150px'
  };
  constructor(
    private route: ActivatedRoute,
    public cS:CreateResumeService,
    public helpers:HelpersService,
    public dS:DataService,
    public popS:DownPopupService,
    public uS:UserService
  ) {
    //this.mask = ['(', /[1-31]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/]

  }
  rebuildAvarage(){
    switch (this.newIschoolProgressEducation.evaluationSystem){
      case 4:
        this.avarage =  [ /[0-4]/,'.',/[0-9]/,/[0-9]/]
        break;
      case 5:
        this.avarage =  [ /[0-5]/,'.',/[0-9]/,/[0-9]/]
        break;
      case 6:
        this.avarage =  [ /[0-6]/,'.',/[0-9]/,/[0-9]/]
        break;
      case 10:
        this.avarage =  [ /[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/]
        break;
      case 100:
        this.avarage =  [ /[0-9]/,/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/]
        break;
      default:
        this.avarage =  [ /[0-9]/,'.',/[0-9]/,/[0-9]/]
        break;
    }
    //if (parseInt(this.dS.getOptionsFromId('evaluationSystem',this.newIschoolProgressEducation.evaluationSystem).id)>9){
    //  this.avarage =  [ /[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/]
    //}else{
    //  this.avarage =  [ /[0-9]/,'.',/[0-9]/,/[0-9]/]
    //}

  }
  ngOnInit() {
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
    this.rand = this.helpers.randomString(5);
    this.rand1 = this.helpers.randomString(5);
    this.rand2 = this.helpers.randomString(5);
    this.rand3 = this.helpers.randomString(5);
    this.rand4 = this.helpers.randomString(5);
    this.rand5 = this.helpers.randomString(5);
    this.rand6 = this.helpers.randomString(5);
    this.rand7 = this.helpers.randomString(5);
    this.rand8 = this.helpers.randomString(5);
    this.rand9 = this.helpers.randomString(5);
    this.rand10 = this.helpers.randomString(5);
    this.clearIEdProg()
  }

  clearIEdProg(){
    this.newIschoolProgressEducation = <IschoolProgressEducation>{
      gradeStarted:'',
      evaluationSystem:null,
      avarage:null,
      status:0
    }
    this.gradeRed = null
  }
  saveProgress(i,SchoolProgressEducation){
    var save = true

  var stY = 0
  var stM = 0
  var enY = 0
  var enM = 0

    this.uS.profile.Student.schools[i].startDateMonth?stM=this.uS.profile.Student.schools[i].startDateMonth:null
    this.uS.profile.Student.schools[i].startDateYear?stY=parseInt(this.uS.profile.Student.schools[i].startDateYear):null

    this.uS.profile.Student.schools[i].endDateYear?enY=parseInt(this.uS.profile.Student.schools[i].endDateYear):null
    this.uS.profile.Student.schools[i].endDateMonth?enM=this.uS.profile.Student.schools[i].endDateMonth:null

    this.uS.profile.Student.schools[i].expectedGraduationYear?enY=this.uS.profile.Student.schools[i].expectedGraduationYear:null
    this.uS.profile.Student.schools[i].expectedGraduationMonth?enM=this.uS.profile.Student.schools[i].expectedGraduationMonth:null
    if (stY&&stM&&enY&&enM){
      var debY = parseInt(this.newIschoolProgressEducation.gradeStarted.split('/')[1])
      var debM = parseInt(this.newIschoolProgressEducation.gradeStarted.split('/')[0])
      //alert(debM)
      //alert(debY)
      //alert(stY)
      //alert(stM)
      //alert(enY)
      //alert(enM)
      if (debY<stY||debY>enY){
        save = false
        this.helpers.alert(3)
        alert('incorrect Year Date '+stY+'><'+enY)
        //this.popS.show_success(this.dS.getOptionsFromId('errors',23).text)
      }
      if (debY==stY&&debM<stM){
        save = false
         alert('incorrect Year Date mounth '+debM+'>'+stM)
        this.helpers.alert(3)
      }
      if (debY==enY&&debM>enM){
        save = false
       alert('incorrect Year Date mounth '+debM+'<'+enM)
        this.helpers.alert(3)
      }

    }else{
      save = false
      this.helpers.alert(3)
      // alert('incorrect Date')

    }
    //startDateMonth
    //startDateYear
    //endDateMonth
    //endDateYear
    //expectedGraduationMonth
    //expectedGraduationYear




    var regex = /[+-]?\d+(\.\d+)?/ig;
    if (!SchoolProgressEducation.avarage){SchoolProgressEducation.avarage = '0.0'}
    var av = SchoolProgressEducation.avarage+''
    av = av.replace(new RegExp(",",'ig'),".")
    SchoolProgressEducation.avarage = av.match(regex).map(function(v) { return parseFloat(v); });
    SchoolProgressEducation.gradeStarted==""?save = false:null
    SchoolProgressEducation.avarage==null||SchoolProgressEducation.avarage==''||isNaN(SchoolProgressEducation.avarage)?save = false:null
    SchoolProgressEducation.evaluationSystem==null?save = false:null
    SchoolProgressEducation.avarage.length>0?SchoolProgressEducation.avarage = SchoolProgressEducation.avarage[0]:'0.0';


    SchoolProgressEducation.avarage
      >
    SchoolProgressEducation.evaluationSystem
      ?save = false:null;

    if (save){

      SchoolProgressEducation.evaluationSystem = parseInt(SchoolProgressEducation.evaluationSystem)
      let avgProgress: number = this.getAvgProgress(i,SchoolProgressEducation);
      this.dS.sendws('Schools/editprogressEducation',{
        _id:this.uS.profile.Student.schools[i]._id,
        SchoolProgressEducation:SchoolProgressEducation,
        avgEduProgress:avgProgress
      }).then(data=>{
        this.clearIEdProg()
        this.uS.profile.Student.schools[i].SchoolProgressEducation = data['SchoolProgressEducation']
        console.log(data)
        this.cS.updateAllResume()
      })
    } else {
      this.helpers.alert(3)
      // alert("Incorrect value")
    }

  }

  getAvgProgress(i,spe){
    let progress = [],
      avgProgress:number = 0
    for (let k in this.uS.profile.Student.schools) {
      let avg:number = undefined
      let school = this.uS.profile.Student.schools[k]
      let schoolPE:any = undefined
      if (k==i)
          schoolPE = spe
      if (!school.hasOwnProperty('SchoolProgressEducation') && !schoolPE) continue
      else schoolPE = school.SchoolProgressEducation
      if (schoolPE.hasOwnProperty('status') && schoolPE.status == 0) continue
      avg = this.helpers.getEvalationSystem(parseInt(this.dS.options.evaluationSystem[schoolPE.evaluationSystem]),schoolPE.avarage)
      if (avg!=null && progress.findIndex((e,i,a)=>{return e==avg?true:false}) == -1) progress.push(avg)
    }
    console.log('progress - ',progress)
    if (!progress.length) return avgProgress
    progress.forEach((e,i,a)=>{avgProgress+=e})
    return avgProgress/progress.length
  }

  updateGrade(i){
    if (this.helpers.confirm(0)) {
      this.newIschoolProgressEducation = this.uS.profile.Student.schools[i].SchoolProgressEducation
      this.gradeRed = i
      this.rebuildAvarage()
    }
  }
  deleteGrade(i){
    if (this.helpers.confirm(0)){
      this.uS.profile.Student.schools[i].SchoolProgressEducation = <IschoolProgressEducation>{
        status:0
      }
      this.dS.sendws('Schools/deleteProgressEducation',{_id:this.uS.profile.Student.schools[i]._id,SchoolProgressEducation:this.uS.profile.Student.schools[i].SchoolProgressEducation}).then(data=>{
        this.clearIEdProg()
        this.uS.profile.Student.schools[i].SchoolProgressEducation = data['SchoolProgressEducation']
        this.cS.updateAllResume()
        console.log(data)
      })
    }

  }
  setfile(i,file){
    console.log("dsddsa",file)
    if (file){
      this.uS.profile.Student.schools[i].SchoolProgressEducation.file = file
    }else{
      this.uS.profile.Student.schools[i].SchoolProgressEducation.file = {}
    }

    this.saveProgress(i,this.uS.profile.Student.schools[i].SchoolProgressEducation)
  }

}
