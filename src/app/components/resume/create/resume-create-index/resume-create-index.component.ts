import {Component, OnInit} from "@angular/core";
import {CreateResumeService} from "../create.service";
import {DataService} from "../../../../_services/data.service";
import {UserService} from "../../../../_services/user.service";
import {ActivatedRoute, Router} from "@angular/router";

import {HelpersService} from "../../../../_services/helpers.service";
import {isUndefined} from "util";
import {ResumeService} from "../../../../_services/resume.service";
@Component({
  selector: 'app-resume-create-index',
  templateUrl: './resume-create-index.component.html',
  styleUrls: ['./resume-create-index.component.scss'],

})
export class ResumeCreateIndexComponent implements OnInit {
  isCopied1: boolean = false;
  isCopied2: boolean = false;
  rand:string;
  rand1:string;
  rand2:string;
  photo:any
  location
  listItemAc:boolean = false;
  uploadFiles:any = []
  resumeCreate:boolean = false
  constructor(
    public cS:CreateResumeService,
    public dataService: DataService,
    public user: UserService,
    public helpers:HelpersService,
    private router: Router,
    public rS:ResumeService,
    private route: ActivatedRoute) {
       // this.photo = {path: this.user.profile.photo};


  }



  ngOnInit() {
    this.location = window.location.origin
    this.rand = this.helpers.randomString(5);
    this.rand1 = this.helpers.randomString(5);
    this.rand2 = this.helpers.randomString(5);
    this.cS.dublicate = this.route.snapshot.data['name']=='DUBLICATE'
    if (this.cS.dublicate){
        this.cS._id = undefined
        this.cS.resume._id = undefined
    }

    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
    this.uploadFiles = Object.assign([],this.cS.resume.uploadFiles)
    this.cS.verifyResume()
    console.log('ПАРАМЕТР:',this.cS._id)
    //console.log(this.route.snapshot.data['name'])
  }
  asdasd(sd){
    console.log(sd)
  }
  setFiles(e){
    if (this.uploadFiles&&this.uploadFiles.length<11){
      this.uploadFiles.push(e)
      this.cS.resume.uploadFiles = this.uploadFiles
    }

  }
  chFile(e){


    this.uploadFiles = Object.assign([],e)
    if (this.uploadFiles.length>10){
      this.uploadFiles.splice(9,100000)
    }

    this.cS.resume.uploadFiles = this.uploadFiles
  }
  setSinglePhoto(e){

    this.photo = e
  }

  CreateResume(){

    var create = true
    if (!this.cS.resume.hasOwnProperty('name')&&this.cS.resume.name!=''){
      create = false
    }
    if (!this.cS.resume.hasOwnProperty('language')){
      create = false
    }

   if (create){

      if (this.cS.resume._id){
        this.dataService.sendws('Resume/update',this.cS.resume).then(data=>{
          this.cS.resume = data['resume']
          this.cS.updateResume(this.cS.resume._id)
          console.log("DATA update",data)

        })
      }else{
        this.cS.verifyResume()
        this.dataService.sendws('Resume/create',this.cS.resume).then(data=>{

          this.cS.resume = data['resume']
          this.cS._id = data['resume']._id
          this.cS.saveResume()
           this.cS.updateResume(this.cS.resume._id)
           this.router.navigate(['/resume/create/',data['resume']._id]);
          console.log("DATA create",data)
        })
      }

   }
  }

}
