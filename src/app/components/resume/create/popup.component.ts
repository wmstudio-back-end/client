import {Component, Input, OnInit} from "@angular/core";
import {CreateResumeService} from "./create.service";
@Component({
    selector: 'popup-component',
    template: `
                <div class="popupBs" style="display:block;" >
                      <button class="button shaded" (click)="cS.saveResume()">Save</button>
        
                      <div class="checkbox" [routerLink]="['/resume/create/']">
                          <i class='icon black-list' style="margin-right: 5px;"></i>Back to CV
                      </div>
        
                      <div *ngIf="cS.popupUrl!=''" class="popupBs__link">
                        <p  [routerLink]="['/resume/create/'+cS.popupUrl]">{{cS.popupName}} <i class="material-icons">&#xE315;</i> </p>
                      </div>
                      <div *ngIf="cS.popupUrl=='index'" class="popupBs__link">
                      
                      </div>
                    
                  </div>
        

                
        
              `,
    styleUrls: ['popup.component.scss']

})
export class PopupComponent implements OnInit {
  name = ''
  url = ''
  constructor(public cS:CreateResumeService) {
    this.name = cS.popupName
    this.url = cS.popupUrl
    }

    ngOnInit() {
    
    }

 
}
