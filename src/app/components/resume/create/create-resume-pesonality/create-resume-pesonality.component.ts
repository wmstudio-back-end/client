import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "../create.service";
import {DataService} from "../../../../_services/data.service";

@Component({
  selector: 'create-resume-pesonality',
  templateUrl: './create-resume-pesonality.component.html',
  styleUrls: ['./create-resume-pesonality.component.scss']
})
export class CreateResumePesonalityComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    public cS:CreateResumeService,
    public dS:DataService,

  ) { }

  ngOnInit() {
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
  }
}
