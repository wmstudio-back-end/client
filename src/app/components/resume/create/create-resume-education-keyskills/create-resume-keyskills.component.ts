import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DataService} from "../../../../_services/data.service";
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "app/components/resume/create/create.service";
import {HelpersService} from "../../../../_services/helpers.service";
import {Organization} from "../../../../_models/user";
import {UserService} from "../../../../_services/user.service";
import {DownPopupService} from "../../../../_services/down-popup.service";

@Component({
  selector: 'create-resume-keyskills',
  templateUrl: './create-resume-keyskills.component.html',
  styleUrls: ['./create-resume-keyskills.component.scss']
})
export class CreateResumeKeyskillsComponent implements OnInit {
  iaddSchool =  <Organization>{
    schoolLocation:{addr:''}

  };
  addNewSchool: boolean = false;
  @ViewChild('editSchool') private listContainer: ElementRef;
  arrPosition = []
  arrSpeciality = []
  rand:string;
  rand1:string;
  rand2:string;
  rand3:string;
  rand4:string;
  rand5:string;
  rand6:string;
  rand7:string;
  rand8:string;
  rand9:string;
  rand10:string;
  rand11:string;
  rand12:string;
  rand13:string;
  rand14:string;
  year: any;
  nowfullYear = new Date().getFullYear()
  startYear = this.nowfullYear-50
  currentlyStuding:boolean = false;
  y1 = this.helpers.yearGenerate(this.nowfullYear-50, this.nowfullYear).reverse()
  y2 = this.helpers.yearGenerate(this.startYear, this.nowfullYear).reverse()
  y3 = this.helpers.yearGenerate(this.nowfullYear, this.nowfullYear+15).reverse()

  constructor(
    private route: ActivatedRoute,
    public cS:CreateResumeService,
    public helpers:HelpersService,
    public dataService : DataService,
    public popS:DownPopupService,
    public userService: UserService,) { }

  sddssdsd(e){
    //console.log('ssssssssssssssssssssss',e)
  }
  getData(e,i){
    if (i==0){
      this.iaddSchool.speciality = e
      this.dataService.sendws('Resume/searchSpecialityFromName',{speciality:e}).then(data=>{
        this.arrSpeciality = data['speciality']
      })
    }
    if (i==1){
      this.iaddSchool.speciality = e
    }
  }
  startYearCH(y){
    this.startYear = y
    this.y2 = this.helpers.yearGenerate(y, this.nowfullYear).reverse()
  }
  sortingSchools(a,b){
    if (a.curently==true){
      return 1
    }
    if (b.curently==true){
      return 1
    }

    if (a.endDateYear==b.endDateYear){
      if (a.endDateMonth>b.endDateMonth){
        return -1
      }
      if (a.endDateMonth<b.endDateMonth){
        return 1
      }
      return 0
    }
    //expectedGraduationMonth
    //expectedGraduationYear


    if (parseInt(a.endDateYear)>parseInt(b.endDateYear)){
      return -1
    }
    if (parseInt(a.endDateYear)<parseInt(b.endDateYear)){
      return 1
    }
    //return a.startDateYear-b.startDateYear
    return 0
  }
  ngOnInit() {
    console.log("this.userService.profile.Student.schoolsthis.userService.profile.Student.schools",this.userService.profile.Student.schools)
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
    this.rand = this.helpers.randomString(5);
    this.rand1 = this.helpers.randomString(5);
    this.rand2 = this.helpers.randomString(5);
    this.rand3 = this.helpers.randomString(5);
    this.rand4 = this.helpers.randomString(5);
    this.rand5 = this.helpers.randomString(5);
    this.rand6 = this.helpers.randomString(5);
    this.rand7 = this.helpers.randomString(5);
    this.rand8 = this.helpers.randomString(5);
    this.rand9 = this.helpers.randomString(5);
    this.rand10 = this.helpers.randomString(5);
    this.rand11 = this.helpers.randomString(5);
    this.rand12 = this.helpers.randomString(5);
    this.rand13 = this.helpers.randomString(5);
    this.rand14 = this.helpers.randomString(5);
    this.year = this.helpers.yearGenerate(1950, 2020);
    this.userService.users[this.cS.resume.uid].Student.schools.sort(this.sortingSchools)
  }
  schoolLocation = ''
  searchSchoolresult =[]

  deleteSchool(i) {
    if (this.helpers.confirm(0)){
      //this.userService.profile.Student.schools.splice(i,1)


      this.dataService.sendws('Schools/deleteSchool',{_id:this.userService.profile.Student.schools[i]._id}).then(data=>{
        this.userService.profile.Student.schools.splice(i,1)
        this.checkEdu()
        this.cS.updateAllResume()
      })
    }

  }
  changecurrentlyStuding(){
    this.currentlyStuding =!this.currentlyStuding
    if(this.currentlyStuding){
      delete this.iaddSchool.endDateMonth
      delete this.iaddSchool.endDateYear
    }else{
      delete this.iaddSchool.expectedGraduationMonth
      delete this.iaddSchool.expectedGraduationYear
    }
  }
  editSchool(id){
    window.scrollTo(0, document.getElementById('editSchool').offsetTop)
    this.addNewSchool = false;
    setTimeout(function(){
      for(var i=0;i<this.userService.profile.Student.schools.length;i++){
        if (this.userService.profile.Student.schools[i].schoolId==id){

          this.iaddSchool = <Organization>Object.assign({},this.userService.profile.Student.schools[i]);

          this.startYearCH(this.iaddSchool.startDateYear)
          break;
        }
      }
      this.addNewSchool = true;
    }.bind(this),1)




  }
  setschool(school){
    //this.iaddSchool.schoolId=school._id
    //this.searchSchoolresult = []
    //this.schoolLocation = school.name
    //this.iaddSchool.schoolName = school.name
  }
  searchSchool(e){
    if (e.length>2){
      console.log(e)
      this.dataService.sendws('Schools/searchSchool',{text:e}).then(data=>{
        this.searchSchoolresult = data['result']
        for(var s=0;s<this.userService.profile.Student.schools.length;s++){
          for(var i=0;i<this.searchSchoolresult.length;i++){
            if (this.searchSchoolresult[i]._id==this.userService.profile.Student.schools[s].schoolId){
              this.searchSchoolresult.splice(i,1)
            }
          }
        }
        //this.userService.profile.Student.schools.push(<Organization>data);
      })
    }else{
      this.searchSchoolresult = []
    }


  }
  clearIaddSchool(){
    this.startYear = null
    this.iaddSchool = <Organization>{

    };
    console.log(this.iaddSchool.schoolLocation)
  }
  checkEdu(){
    var cfg = [0,0,1,1,2,2,3,3,3,3,3,3,3,3]//образование
    var cfg2 = [1,3,7,13]//получено
    var cfg3 = [0,2,4,7]//в получении
    var cfgdegreeEnd = [7,9,11,13]//тип высшее образование получено
    var cfgdegreeNoEnd = [6,8,10,12]//тип высшее образование в получении
    var maxLevel = 0
    var noend = true
    var degreeEnd = 0
    var degreeNoEnd = 0

    for (var i = 0; i < this.userService.profile.Student.schools.length; i++) {
      if (this.userService.profile.Student.schools[i].educationLevel>=maxLevel){
        var t = this.userService.profile.Student.schools[i]

        maxLevel = t.educationLevel

        //max degreeEnd
        if (t.degree&&!t.curently&&t.degree>degreeEnd){
          degreeEnd=t.degree
        }
        if (t.degree&&t.curently&&t.degree>degreeNoEnd){
          degreeNoEnd=t.degree
        }
        if (t.curently&&!noend){//если существует законченное

        }else{
          noend = t.curently

        }


      }
    }

    if (noend){
      if (maxLevel==3){
        this.cS.resume.currentLevelOfEducation = cfgdegreeNoEnd[degreeNoEnd]
      }else{
        this.cS.resume.currentLevelOfEducation = cfg3[maxLevel]
      }

    }else{

      if (maxLevel==3){
        this.cS.resume.currentLevelOfEducation = cfgdegreeEnd[degreeEnd]
      }else{
        this.cS.resume.currentLevelOfEducation = cfg2[maxLevel]
      }

    }

    console.log("maxLevel = ",maxLevel)
    console.log("noend = ",noend)
    console.log("cfg3 = ",cfg3)
    console.log("cfg2 = ",cfg2)
    console.log("cfg3[maxLevel] = ",cfg3[maxLevel])
    console.log("cfg2 = ",cfg2[maxLevel])
    console.log("result = ",this.cS.resume.currentLevelOfEducation)
    console.log("degreeEnd = ",degreeNoEnd)
    console.log("degreeEnd = ",degreeNoEnd)
    console.log("----",cfg[this.cS.resume.currentLevelOfEducation])


  }
  addSchool() {


    var valid = true
    this.iaddSchool.hasOwnProperty('educationLevel')?null:valid=false
    this.iaddSchool.hasOwnProperty('schoolLocation')?null:valid=false
    this.iaddSchool.hasOwnProperty('schoolName')?null:valid=false

    this.iaddSchool.hasOwnProperty('startDateMonth')?null:valid=false
    this.iaddSchool.hasOwnProperty('startDateYear')?null:valid=false

    if (
      (this.iaddSchool.hasOwnProperty('expectedGraduationMonth')&&this.iaddSchool.hasOwnProperty('expectedGraduationYear'))
      ||
      (this.iaddSchool.hasOwnProperty('endDateMonth')&&this.iaddSchool.hasOwnProperty('endDateYear'))
      ||
      (this.iaddSchool.hasOwnProperty('expectedGraduationMonth')&&this.iaddSchool.hasOwnProperty('expectedGraduationYear'))
    ){
      if (this.iaddSchool.educationLevel==3&&this.iaddSchool.degree==null){
        valid=false
      }

    }else{
      valid=false
    }
    this.iaddSchool.schoolName?null:valid=false
      if (valid){
      var q = 'Schools/saveSchool'
      if (this.iaddSchool._id){
        q = 'Schools/editSchool'
        for(var i=0;i<this.userService.profile.Student.schools.length;i++){
          if (this.iaddSchool._id==this.userService.profile.Student.schools[i]._id){
            this.userService.profile.Student.schools[i] = this.iaddSchool
          }
        }
      }

      this.dataService.sendws(q,this.iaddSchool).then(data=>{

        if (data.hasOwnProperty('school')&&!this.iaddSchool._id){
          this.userService.profile.Student.schools.push(data['school'][0])
        }
        this.cS.updateAllResume()
        this.clearIaddSchool()




        this.checkEdu()
        this.userService.users[this.cS.resume.uid].Student.schools.sort(this.sortingSchools)
        //  = this.userService.profile.Student.schools.concat(data['schools'].Student.schools)
      })

      this.addNewSchool = false
      }else{
        //this.popS.show_warning('incorrect school')
        this.popS.show_warning(this.dataService.getOptionsFromId('errors',26).text);
      }


  }



}
