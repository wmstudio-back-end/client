import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "../create.service";
import {ModalDynamic} from "../../../modal/modal-dynamic";
import {ModalModule} from "../../../modal/modal.module";
import {ModalService} from "../../../../_services/modal.service";
import {UserService} from "../../../../_services/user.service";
import {IRequestRecommendation} from "../../../../_models/request-recommendation";
import {DataService} from "../../../../_services/data.service";
import {HelpersService} from "../../../../_services/helpers.service";



@Component({
  selector: 'create-resume-recommendation',
  templateUrl: './create-resume-recommendation.component.html',
  styleUrls: ['./create-resume-recommendation.component.scss']
})
export class CreateResumeRecommendationComponent implements OnInit {
  mRequest_educators = false
  mRequest_company = false
  mRequest_other = false
  reqommendationData: [IRequestRecommendation[],IRequestRecommendation[],IRequestRecommendation[]] = [[],[],[]]
  openModal = false
  modalRecomm
  constructor(
    private route: ActivatedRoute,
    public cS:CreateResumeService,
    public modalService:ModalService,
    public uS:UserService,
    private helpers:HelpersService,
    public dS:DataService
  ) {
    dS.messages["EVENT/newReqRecommendation"].subscribe(data => {
      this.ngOnInit()
    })
  }
  checkState : boolean = false;

  ngOnInit() {
    this.reqommendationData  = [[],[],[]]
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
    for(var i=0;i<this.uS.profile.recommendations.length;i++){

      this.reqommendationData[this.uS.profile.recommendations[i].type].push(this.uS.profile.recommendations[i])

    }
    var ids = []
    for (var i = 0; i < this.reqommendationData[0].length; i++) {
      ids.push(this.reqommendationData[0][i].educatorId)
    }
    this.uS.getUsers(ids).then(data2=>{
      for (var i = 0; i < this.reqommendationData[0].length; i++) {
        this.reqommendationData[0][i].name = this.uS.getUserName(this.reqommendationData[0][i].educatorId,0)

      }

    })

  }
  calcRating(i){
    var t = 0
    t+=i.rating_dilligance
    t+=i.rating_poise
    t+=i.rating_sociability
return t/3
  }
  getOrgName(_id){
    if (this.cS.resume.workExp)
    {
      for(var i=0;i<this.cS.resume.workExp.length;i++){
        if (this.cS.resume.workExp[i]._id==_id){
          return this.cS.resume.workExp[i].companyName
        }
      }
    }

    return false
  }
  deleteRec(id,type){
    if (this.helpers.confirm(0)){

      for (var j = 0; j < this.uS.profile.recommendations.length; j++) {
        if (this.uS.profile.recommendations[j]._id==id){
          this.uS.profile.recommendations.splice(j,1)
          break
        }
      }
      for (var i = 0; i < this.reqommendationData[type].length; i++) {
        if (this.reqommendationData[type][i]._id==id){
          this.reqommendationData[type].splice(i,1)
          break
        }

      }

      this.dS.sendws('Recommendation/deleteRecommendation',{_id:id}).then(data=>{
        console.log(data)
        this.cS.updateAllResume()
      })
    }

  }
  getSchName(_id){
    for(var i=0;i<this.uS.profile.Student.schools.length;i++){
      if (this.uS.profile.Student.schools[i].schoolId==_id){
        return this.uS.profile.Student.schools[i].schoolLocation.addr
      }
    }
    // this.dS.sendws('Schools/getSchoolFromId',{schoolId:_id}).then(data=>{
    //   console.log(data)
    // })

    return ''
  }
  setRec(e){
    this.reqommendationData[e.type].push(e)
    this.uS.profile.recommendations.push(e)
  }
  checkVis(id,type,e){

    for (var i = 0; i < this.uS.profile.recommendations.length; i++) {
      if (id==this.uS.profile.recommendations[i]._id){
        this.uS.profile.recommendations[i].visibility = e
         this.reqommendationData[type]['visibility'] = e
        this.dS.sendws('Recommendation/visibilityEdit',{_id:id,visibility:e}).then(data=>{
          console.log(data)
        })
      }

    }


  }
  showModal(i) {

  }

  hideModal(comp) {

  }

}
