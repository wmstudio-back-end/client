import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "../create.service";
import {DataService} from "../../../../_services/data.service";
import {HelpersService} from "../../../../_services/helpers.service";
import {UserService} from "../../../../_services/user.service";
import {IOfficeTools,IOtherSoftware,ICertificationsAndLicences,IDrivingLicense} from "../../../../_models/resume";



@Component({
  selector: 'create-resume-computer-skills',
  templateUrl: './create-resume-computer-skills.component.html',
  styleUrls: ['./create-resume-computer-skills.component.scss']
})
export class CreateResumeComputerSkillsComponent implements OnInit {
  iaddCert =  <ICertificationsAndLicences>{};
  iaddDriving =  <IDrivingLicense>{drivingStatus:false};
  iaddOfficeTools =  <IOfficeTools>{use:false,level:null};
  iaddOtherSoftware =  <IOtherSoftware>{use:false};
  searchSoftwareArr:string[] = []

  rand:string;
  rand1:string;
  rand2:string;
  rand3:string;
  rand4:string;
  rand5:string;
  rand6:string;
  rand7:string;
  rand8:string;
  rand9:string;
  rand10:string;
  addOfficeToolsRef = false
  addOtherSoftRef = false
  addCertRef = false
  addDrivingRef = false
  differenceIdsSft:number[] = []
  softwareArr = []
  othersoftwareArr = []
  drivingCategoryArr = []
  constructor(
    private route: ActivatedRoute,
    public cS:CreateResumeService,
    public dataService: DataService,
    public helpers:HelpersService,
    public userService: UserService
  ) { }

  ngOnInit() {
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
    this.rand = this.helpers.randomString(5);
    this.rand1 = this.helpers.randomString(5);
    this.rand2 = this.helpers.randomString(5);
    this.rand3 = this.helpers.randomString(5);
    this.rand4 = this.helpers.randomString(5);
    this.rand5 = this.helpers.randomString(5);
    this.rand6 = this.helpers.randomString(5);
    this.rand7 = this.helpers.randomString(5);
    this.rand8 = this.helpers.randomString(5);
    this.rand9 = this.helpers.randomString(5);
    this.rand10 = this.helpers.randomString(5);
    this.difference()
  }
  getSoftAttData(e){
    console.log(e)
    //this.dataService.sendws('System/getoptions',{software:e}).then(data=>{
    //  this.searchSoftwareArr = data['software']
    //})

  }
  setSoftAttData(e){
    console.log(e)
  }
  OthSoftData(e){

    this.iaddOtherSoftware.otherSoft = e
    console.log(this.iaddOtherSoftware.otherSoft)
  }
  difference(){

    var t = Object.assign([],this.dataService.options.drivingCategory)
    if (this.cS.resume.drivingLicense) {
      for (var s = 0; s < this.cS.resume.drivingLicense.length; s++) {
        for (var i = 0; i < t.length; i++) {
          if (t[i].id == this.cS.resume.drivingLicense[s].category) {
            t.splice(i, 1)
            break;
          }
        }
      }
    }
    this.drivingCategoryArr = t
    var t = Object.assign([],this.dataService.options.software)
    if (this.cS.resume.officeTools){
      for(var s=0;s<this.cS.resume.officeTools.length;s++){
        for(var i=0;i<t.length;i++){

          if (t[i].id==this.cS.resume.officeTools[s].officeTool){
            t.splice(i,1)
            break;
          }
        }
      }
    }

    this.softwareArr = t
    var m = Object.assign([],this.dataService.options.software)
    if (this.cS.resume.otherSoftware){
      for(var s=0;s<this.cS.resume.otherSoftware.length;s++){
        for(var i=0;i<m.length;i++){

          if (m[i].id==this.cS.resume.otherSoftware[s].otherSoft){
            m.splice(i,1)
            break;
          }
        }
      }
    }

    this.othersoftwareArr = m

    //if(this.editSoftCh&&this.iaddLanguage.langId){
    //  this.arrLang.push(this.dataService.options.languages[this.iaddLanguage.langId])
    //}

  }
  spliceArr(item : any, arr) {

    let index = arr.indexOf(item);
    console.log(index);
    if(index > -1) {
     arr.splice(index, 1)
    }
  }
  addCert() {
    if (!this.cS.resume.certificationsAndLicences){
      this.cS.resume.certificationsAndLicences = []
    }
    if (this.iaddCert.hasOwnProperty('name')&&this.iaddCert.hasOwnProperty('year')&&this.iaddCert.hasOwnProperty('issuer')){
      this.addCertRef = true
      this.cS.resume.certificationsAndLicences.push(this.iaddCert);
      setTimeout(function(){
        this.addCertRef = false
        this.iaddCert =  <ICertificationsAndLicences>{};

      }.bind(this),10)

    }

  }
  excludeIdOfficeTools(){
    var r = []
    for(var i=0;i<this.cS.resume.officeTools.length;i++){

      r.push(this.cS.resume.officeTools[i].officeTool)
    }
    console.log("ssasdasd",r)
    return r
  }
  deleteCert(item,i) {
    if (this.helpers.confirm(0)){
      this.cS.resume.certificationsAndLicences.splice(i, 1)
      setTimeout(function () {
        this.iaddCert = <ICertificationsAndLicences>{};
      }.bind(this), 10)
    }
  }
  deleteDriving(item,i) {
    if (this.helpers.confirm(0)){
      this.cS.resume.drivingLicense.splice(i, 1)
      this.addDrivingRef = true
      setTimeout(function () {
        this.addDrivingRef = false
        this.difference()
        this.iaddDriving = <IDrivingLicense>{};
      }.bind(this), 10)
    }

  }
  addDriving() {
    if (!this.cS.resume.drivingLicense){
      this.cS.resume.drivingLicense = []
    }
      this.addDrivingRef = true
      this.cS.resume.drivingLicense.push(this.iaddDriving);
    setTimeout(function(){
        this.addDrivingRef = false
        this.difference()
        this.iaddDriving =  <IDrivingLicense>{};
      }.bind(this),10)


  }

  deleteotherSof(id,i){
    if (this.helpers.confirm(0)){
      this.addOtherSoftRef = true
      this.cS.resume.otherSoftware.splice(i, 1)
      setTimeout(function () {
        this.difference()
        this.addOtherSoftRef = false
        this.iaddOtherSoftware = <IOtherSoftware>{};
      }.bind(this), 10)
    }
  }
  addOfficeTools() {
    if (!this.cS.resume.officeTools){
      this.cS.resume.officeTools = []
    }
    if (this.iaddOfficeTools.hasOwnProperty('officeTool')&&this.iaddOfficeTools.hasOwnProperty('level')){
      this.addOfficeToolsRef = true
      this.cS.resume.officeTools.push(this.iaddOfficeTools);
      setTimeout(function(){
        this.difference()
        this.addOfficeToolsRef = false
        this.iaddOfficeTools =  <IOfficeTools>{};
      }.bind(this),10)
    }
  }
  deleteSoftware(id,i){
    if (this.helpers.confirm(0)){
      this.cS.resume.officeTools.splice(i,1)
      this.addOfficeToolsRef = true
      setTimeout(function(){
        this.iaddOfficeTools =  <IOfficeTools>{};
        this.difference()
        this.addOfficeToolsRef = false
      }.bind(this),10)
    }



  }

  addOtherSoft() {
    if (!this.cS.resume.otherSoftware){
      this.cS.resume.otherSoftware = []
    }
    if (this.iaddOtherSoftware.hasOwnProperty('otherSoft')&&this.iaddOtherSoftware.hasOwnProperty('level')){
      this.addOtherSoftRef = true
      this.cS.resume.otherSoftware.push(this.iaddOtherSoftware);
      setTimeout(function(){
        this.difference()
        this.addOtherSoftRef = false
        this.iaddOtherSoftware =  <IOtherSoftware>{};
      }.bind(this),10)
    }

  }

}
