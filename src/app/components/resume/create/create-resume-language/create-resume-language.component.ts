import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "app/components/resume/create/create.service";
import {HelpersService} from "../../../../_services/helpers.service";

import {UserService} from "../../../../_services/user.service";
import {DataService} from "../../../../_services/data.service";
import {forEach} from "@angular/router/src/utils/collection";
import {isUndefined} from "util";
import {IlanguageSkills} from "../../../../_models/resume";

@Component({
  selector: 'create-resume-language',
  templateUrl: './create-resume-language.component.html',
  styleUrls: ['./create-resume-language.component.scss']
})
export class CreateResumeLanguageComponent implements OnInit {
  iaddLanguage =  <IlanguageSkills>{
    useEveryday : false,
    certificate : false
      };
  assign = Object.assign
  rand:string
  rand1:string
  rand2:string
  rand3:string
  arrLang = []
  editLangCh = false
  editLangI = null
  addTableSkills: boolean = false;
  langLevels = []
  constructor(private route: ActivatedRoute,public cS:CreateResumeService,public helpers:HelpersService,public userService: UserService, public dataService: DataService) {


    this.langLevels = Object.assign([],dataService.options.levels)
    console.log('================___================',this.langLevels)
    // let i0 = this.langLevels[0]
    // let i6 = this.langLevels[6]
    // this.langLevels.splice(0,1)
    // this.langLevels.splice(5,1)
    this.langLevels.sort((a,b)=>{
      if (a.id==6){return 1}
      if (b.id==6){return 1}
      return 0
    })
    console.log('9999999999999999999999999999',this.langLevels)
  }
  getLang(){

   // return this.dataService.options.languages

  }
  getDataLang(e,i){
    console.log("TYPE - "+i,e)
  }
  setArrLang(){


    // this.arrLang = this.dataService.options.languages.filter((e,i,a)=>{
    //   if (!this.cS.resume.languageSkills) return true;
    //   let id = e.id
    //   if (typeof this.cS.resume.languageSkills === 'object')
    //     return (this.cS.resume.languageSkills.findIndex((e,i,a)=>{return (e.langId==id)?true:false;})==-1)?true:false;
    //   else return true;
    // })
    // if(this.editLangCh&&this.iaddLanguage.langId){
    //   this.arrLang.push(this.dataService.options.languages[this.iaddLanguage.langId])
    // }
    var curLang = this.dataService.getOptionsFromId('languages',this.userService.profile.currentLang).value
    console.log(curLang)
     console.log(this.dataService.options.translate[curLang].languages)
    this.arrLang = []
    for (var key in this.dataService.options.translate[curLang].languages) {
      if ((this.cS.resume.languageSkills.findIndex((e,i,a)=>{return (e.langId==parseInt(key))?true:false;})==-1)?true:false)
      {
        this.arrLang.push({
          _id:key,
          text:this.dataService.options.translate[curLang].languages[key]
        })
      }



    }


  }

  ngOnInit() {
    if (!this.cS.resume.languageSkills){
      this.cS.resume.languageSkills = []
    }
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
    this.rand = this.helpers.randomString(5);
    this.rand1 = this.helpers.randomString(5);
    this.rand2 = this.helpers.randomString(5);
    this.rand3 = this.helpers.randomString(5);
    this.setArrLang()
  }

  spliceArr(item : any, arr) {
    let index = arr.indexOf(item);
    if(index > -1) {
      arr.splice(index, 1)
    }
  }
  deleteLang(i){
    let conf = this.helpers.confirm(0)
    if(conf) {
      this.cS.resume.languageSkills.splice(i,1)
      this.setArrLang()
    }

  }
  setLang(id){
    this.iaddLanguage.langId =id
    this.setArrLang()
  }
  editLang(i){
    this.editLangCh = true
    this.editLangI = i
    this.addTableSkills = false
    setTimeout(function(){
      this.iaddLanguage = <IlanguageSkills>Object.assign({},this.cS.resume.languageSkills[i])
      this.addTableSkills = true
      this.setArrLang()
    }.bind(this),1)

  }
  addTableSkillsCh(){
    this.editLangI = null
    this.editLangCh = false
    this.iaddLanguage = <IlanguageSkills>Object.assign({},{})
    this.addTableSkills = !this.addTableSkills
  }
  addLang() {
    console.log(this.iaddLanguage)
    if (
      this.iaddLanguage.hasOwnProperty('langLevel')
      &&this.iaddLanguage.hasOwnProperty('langId')){
      if (this.editLangCh){
        this.cS.resume.languageSkills[this.editLangI] = this.iaddLanguage
      }else{
        this.cS.resume.languageSkills.push(this.iaddLanguage);
      }
      this.iaddLanguage =  <IlanguageSkills>{};
      this.setArrLang()


    }else{
      this.helpers.alert(18)
        // alert("Выбери язык")
    }


  }

}
