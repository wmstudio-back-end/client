import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute}                                   from "@angular/router";
import {CreateResumeService}                              from "../create.service";
import {HelpersService}                                   from "../../../../_services/helpers.service";
import {IWorkExp}                                         from "../../../../_models/resume";
import {UserService}                                      from "../../../../_services/user.service";
import {DataService}                                              from "../../../../_services/data.service";
import {PageScrollConfig, PageScrollService, PageScrollInstance } from 'ng2-page-scroll';
import { DOCUMENT}                                                from '@angular/common';
import {DownPopupService} from "../../../../_services/down-popup.service";
@Component({
  selector: 'create-resume-work-exp',
  templateUrl: './create-resume-work-exp.component.html',
  styleUrls: ['./create-resume-work-exp.component.scss']
})
export class CreateResumeWorkExpComponent implements OnInit {
  addedForm: boolean = false;
  rand:string;
  rand1:string;
  rand2:string;
  needExp: boolean = true;
  locationForeign: boolean = false;
  curWork: boolean = false;
  maxWidth: number = 0;
  year : any
  @ViewChild('resumeWorkExp') resumeWorkExp:ElementRef;

  iaddWorkExp = <IWorkExp>{};
  edit:number = null
  arrPosition:string[] = []
  constructor(
    private pageScrollService: PageScrollService,
    @Inject(DOCUMENT) private document: any,
    private route: ActivatedRoute,
    public cS:CreateResumeService,
    public helpers:HelpersService,
    public userService : UserService,
    public popS : DownPopupService,
    public dataService : DataService
  ) { }

  getData(e){

      this.iaddWorkExp.position = e
      this.dataService.sendws('Resume/searchPositionFromName',{position:e}).then(data=>{
        this.arrPosition = data['position']
      })


  }
  clearWorkExp() {
    this.iaddWorkExp = <IWorkExp>{
      currentlyWorking: false,

    }
  };
  editWorkExp(item,i){

    this.iaddWorkExp = Object.assign({},item)
    this.edit = i
    this.addedForm = true
    let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '.sideContent');
    this.pageScrollService.start(pageScrollInstance);


  }
  ngOnInit() {
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
    this.rand = this.helpers.randomString(5);
    this.rand1 = this.helpers.randomString(5);
    this.rand2 = this.helpers.randomString(5);
    this.year = this.helpers.yearGenerate(1950, new Date().getFullYear());
    this.clearWorkExp()
    if (!this.cS.resume.workExp){
      this.cS.resume.workExp = []
    }
   this.cS.resume.workExp.sort((a,b)=>{
     if (b.currentlyWorking){return 99999999999}
     return b.endDateYear-a.endDateYear
   })

  }

  //   ngAfterViewInit() {
  //
  //       var self = this;
  //       (function () {
  //         let elemenstToWidthSimiliar = self.resumeWorkExp.nativeElement.querySelectorAll('.addedWorkExp-top .addedWorkExp-char .listChar-nameChar');
  //         let maxWidth = 0;
  //
  //        elemenstToWidthSimiliar.forEach((item)=>{
  //            if(item.offsetWidth > maxWidth) maxWidth = item.offsetWidth
  //         })
  //
  //
  //           self.maxWidth = maxWidth
  //        // elemenstToWidthSimiliar.forEach((item)=>{
  //        //     item.style.width = maxWidth
  //        // })
  //
  //       })();
  // }

  spliceArr(item : any, arr) {

    let index = arr.indexOf(item);

    if(index > -1) {
      arr.splice(index, 1)
    }
  }
  deleteWorkExp(item,i){
    if (this.helpers.confirm(0)){

      this.cS.resume.workExp.splice(i,1)
    }

  }
  newForm(){
    this.addedForm = !this.addedForm;
    this.edit = null
    this.clearWorkExp()
  }
  addWorkExp() {
    setTimeout(function () {
      try {
        this.resumeWorkExp.nativeElement.scrollTop = this.resumeWorkExp.nativeElement.scrollHeight;
      } catch (err) {
        //console.log(err)
      }
    }.bind(this), 1)
    if (this.edit==null){
      var add = true
      if (!this.iaddWorkExp.companyName||this.iaddWorkExp.companyName==''){add = false}
      if (!this.iaddWorkExp.position||this.iaddWorkExp.position==''){add = false}
      // if (this.iaddWorkExp.startDateMonth==undefined){add = false}
      if (this.iaddWorkExp.startDateYear==undefined){add = false}
      if (this.iaddWorkExp.endDateYear==undefined){add = false}
      if (this.iaddWorkExp.occupationField==undefined){add = false}
      if (!this.iaddWorkExp.currentlyWorking){
        // if (this.iaddWorkExp.endDateMonth==undefined){add = false}

      }

      if (add){
        this.cS.resume.workExp.push(this.iaddWorkExp);
        this.cS.resume.workExp.sort((a,b)=>{
          if (b.currentlyWorking){return 99999999999}
          return b.endDateYear-a.endDateYear
        })
        this.clearWorkExp()
        this.edit = null
        this.addedForm = false;

      }else{
        this.popS.show_warning(this.dataService.getOptionsFromId('errors',23).text)

      }


    }else{


      this.cS.resume.workExp[this.edit] = this.iaddWorkExp
      this.clearWorkExp()
      this.edit = null
      this.addedForm = false;


    }

    return true
  }


}
