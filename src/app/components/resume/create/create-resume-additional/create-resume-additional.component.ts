import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "../create.service";



@Component({
  selector: 'app-create-resume-additional',
  templateUrl: './create-resume-additional.component.html',
  styleUrls: ['./create-resume-additional.component.scss']
})
export class CreateResumeAdditionalComponent implements OnInit {
  @Input() name:string;
  @Input() url:string;


  
  constructor(private route: ActivatedRoute,public cS:CreateResumeService) {



  }
  
  ngOnInit() {
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])

  }

}
