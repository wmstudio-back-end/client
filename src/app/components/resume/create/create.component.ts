import {Component, OnInit} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {DataService} from "../../../_services/data.service";


import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "./create.service";
import {ModalService} from "../../../_services/modal.service";
import {UserService} from "../../../_services/user.service";
import {isUndefined} from "util";

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  constructor(
    public component: ComponentService,
    public dataService: DataService,
    public cS:CreateResumeService,
    public modalService:ModalService,
    public user:UserService,
    private route: ActivatedRoute
  ) {
    var initId = false

    for(var i=0;i<user.profile.Student.resumes.length;i++){
      if (user.profile.Student.resumes[i]._id==route.snapshot.params['id']){
        cS._id = route.snapshot.params['id']
        //cS.resume = Object.assign({},user.profile.Student.resumes[i])
        cS.resume = JSON.parse(JSON.stringify(user.profile.Student.resumes[i]));
        initId = true
      }
    }

    if (!initId){
      cS._id = undefined
      cS.clearResume()
    }
    }



  ngOnInit() {
    this.cS.setDataForPopup('','')

  }
  getRatingResumes(){
    this.dataService.sendws('User/getRatingResumes',{uids:["5a0ad77568bcc528028ea26e","5a0566dcc2819b7e03fa1063"]}).then(data=>{
      console.log("getRatingResumes",data)
    })
  }
  onDeactivate() {
    document.body.scrollTop = 0;
  }

    requestRecommendation() {
        //this.modalService.setModal('requestRecommendation');
    }

    viewerRecommendation() {
        //this.modalService.setModal('dddComponent')
    }





    checkedState2222(e) {
        console.log(e)
    }
}
