import {
  Component,
  EventEmitter,
  Injectable,
  Injector,
  Input,
  OnInit,
  Output
}                               from '@angular/core';
import {DataService}            from "../../../../../_services/data.service";
import {IRequestRecommendation} from "app/_models/request-recommendation";
import {UserService}            from "../../../../../_services/user.service";
import {CreateResumeService}    from "../../create.service";
import {HelpersService}         from "../../../../../_services/helpers.service";


@Component({
  selector:    "popup_recomendation",
  templateUrl: './request-recommendation.component.html',
  styleUrls:   ['./request-recommendation.component.scss']
})

@Injectable()
export class RequestRecommendationComponent implements OnInit {
  @Input('show') show: boolean = false
  @Input('type') type: number
  @Output('form') change       = new EventEmitter<any>();
  @Output('rshow') rshow       = new EventEmitter<boolean>();
  @Output('req') req           = new EventEmitter<IRequestRecommendation>();
  public recommendation        = <IRequestRecommendation>{
    type: this.type
  };
  public load                  = false;
                 companys      = []
                 schools       = []
                 educators     = []
                 option: any

  constructor(public dS: DataService, private uS: UserService, public cS: CreateResumeService, public helpers: HelpersService) {

  }

  ngOnChanges(changes: any) {
    if (changes.show) {
      this.ngOnInit()
    }
  }

  ngOnInit() {
    this.recommendation = <IRequestRecommendation>{
      type: this.type
    };
    this.companys       = []
    this.schools        = []
    this.educators      = []
    this.option         = null
    for (var i = 0; i < this.uS.profile.Student.schools.length; i++) {
      this.schools.push(
        {
          text: this.uS.profile.Student.schools[i].schoolName,
          id:   this.uS.profile.Student.schools[i].schoolId
        }
      )
    }
    //for(var i=0;i<this.cS.resume.workExp.length;i++){
    //  this.companys.push(
    //    {
    //      text:this.cS.resume.workExp[i].companyName,
    //      id:i
    //    }
    //  )
    //}
  }

  setGetEducatorFromSchoolID(schoolId) {
    this.recommendation.schoolId = schoolId
    this.dS.sendws('Schools/setGetEducatorFromSchoolID', {schoolId: schoolId}).then(data => {
      var ids = []
      for (var i = 0; i < data['result'].length; i++) {
        ids.push(data['result'][i].uid)

      }
      this.uS.getUsers(ids).then(data2 => {
        this.educators = []
        for (var i = 0; i < data['result'].length; i++) {
          this.educators.push({
            text: this.uS.getUserName(data['result'][i].uid, 0),
            id:   data['result'][i].uid
          })

        }
      })


      console.log(data)
    })
  }

  closeModal() {
    this.rshow.emit(false)
  }

  SendRequest(t) {
    var send                 = true
    this.recommendation.type = t
    // for (var i = 0; i < this.uS.profile.recommendations.length; i++) {
    //   if (
    //     (this.recommendation.educatorId&&(this.recommendation.educatorId==this.uS.profile.recommendations[i].educatorId&&this.recommendation.schoolId==this.uS.profile.recommendations[i].schoolId))||
    //     (this.recommendation.email&&(this.recommendation.email==this.uS.profile.recommendations[i].email))
    //   ){
    //     send = false
    //     console.log(this.recommendation)
    //     this.helpers.alert(12)
    //     // alert('рекомендация уже существует')
    //   }
    // }
    switch (this.recommendation.type) {
      case 0:
        if (
          (this.recommendation.educatorId && this.recommendation.schoolId && !this.recommendation.email)
          || (this.recommendation.email)
        ) {
        } else {
          send = false
        }

        //this.recommendation.educatorId?null:send = false
        //this.recommendation.schoolId?null:send = false

        break;
      case 1:
        this.recommendation.workExpId ? null : send = false
        this.recommendation.name ? null : send = false
        this.recommendation.position ? null : send = false
        this.recommendation.email ? null : send = false

        break;
      case 2:
        this.recommendation.name ? null : send = false
        this.recommendation.relation != null ? null : send = false
        this.recommendation.email ? null : send = false

        if (this.recommendation.relation == 0 || this.recommendation.relation == 1 || this.recommendation.relation == 2) {
          this.recommendation.position ? null : send = false
        }
        //this.recommendation.orgName||this.recommendation.position || this.recommendation.relation == 4 ?null:send = false
        break;
    }
    console.log(this.recommendation)
    this.recommendation.req = 0
    if (send) {
      this.load = true
      this.dS.sendws('Recommendation/createRecommendation', this.recommendation).then(data => {
        this.load = false
        if (data['res']['schoolId'] && !data['res']['school']) {
          this.dS.sendws('Schools/getSchoolFromId', {schoolId: data['res']['schoolId']}).then(r => {

            data['res']['school'] = r['result']
            this.req.emit(data['res'])
          })
        } else {
          this.req.emit(data['res'])
        }


        this.closeModal()
      })
    } else {
      this.helpers.alert(17)
      // alert("Не все обязательные поля заполнены")
    }


  }

}
