
import { Injectable } from '@angular/core';

import {DataService} from "../../../_services/data.service";
import {isNull} from "util";
import {IpreferredPeriod, IResume, Isalary, IWorkExp} from "../../../_models/resume";
import {UserService} from "../../../_services/user.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ResumeService} from "../../../_services/resume.service";




@Injectable()
export class CreateResumeService {
  pay = false;

  _id:string
  toSave = false
  public resume = <IResume>{

  };
  dublicate = false
  public popupName = ''
  public popupUrl = ''

  public resumeStatus = {
    objective:true,
    keyskills:false,
    language:false,
    computer:false,
    work_exp:false,
    personality:false,
    interest:false,
    expectations:false,
    additional:false,
    education_progress:false,
    recommendations:false,

  }
  constructor(
    private dataService:DataService,
    private userService:UserService,
    private router: Router,
    private rS: ResumeService,
    private route: ActivatedRoute
  ) {

    var t:IResume
    let a = {} as IResume
    this.toSave = true
    setInterval(()=>{

      for(var i=0;i<this.userService.profile.Student.resumes.length;i++){

        if (this.userService.profile.Student.resumes[i]._id==this._id){

          this.resume.prc = this.userService.profile.Student.resumes[i].prc
          if(JSON.stringify(this.userService.profile.Student.resumes[i])===JSON.stringify(this.resume)){

            this.toSave = false
            console.log("this.toSave ",this.toSave)
          }
          else
            {
            this.toSave = true
            console.log("this.toSave",this.toSave)
            // console.log(this.userService.profile.Student.resumes[i],this.resume)
          }
          break;
        }

      }


    },1000)
    this.clearResume()
    if (this.userService.profile.Student){
      for (var i = 0; i < this.userService.profile.Student.resumes.length; i++) {
        this.userService.profile.Student.resumes[i].workExp.sort(this.sortingWorkExp)
      }
    }




  }
  ngDoCheck(){
    console.log('========================')
  }
  sortingWorkExp(a:IWorkExp,b:IWorkExp){
    if (a.currentlyWorking==true){
      return 1
    }
    if (b.currentlyWorking==true){
      return 1
    }
    if (a.endDateYear==b.endDateYear){
      if (a.endDateMonth>b.endDateMonth){
        return -1
      }
      if (a.endDateMonth<b.endDateMonth){
        return 1
      }
      return 0
    }
    if (a.endDateYear>b.endDateYear){
      return -1
    }
    if (a.endDateYear<b.endDateYear){
      return 1
    }
    //return a.startDateYear-b.startDateYear
    return 0
  }
  printPdf(){
    this.router.navigate(['/resume/details/',this._id]);

    //const elementToPrint = window.document.getElementById('s');
    //const pdf = new jsPDF('p', 'pt', 'a4');
    //pdf.addHTML(elementToPrint, () => {
    //  pdf.save('web.pdf');
    //});
  }

  clearResume(){

  this.resume = <IResume>{
    additional:'',
    objective:'',
    informationAboutComputerSkills:'',
    otherSpecificSkills:[],
    interestsAndHobbies:'',
    yourMainValues:'',
    participationInCommunities: '',
    accomplishmentsInformation: '',
    characterTraits:[],
    officeTools:[],
    otherSoftware:[],
    certificationsAndLicences:[],
    drivingLicense:[],
    name:'',
    languageSkills:[],
    strenghs:[],
    idditonalInfo:'',
    publish:false,
    prc:0,
    deleted_at:0,
    pickedSkills:[],
    workExp:[],
    jobOptions:{
      jobType:[],
      possible_position:[],
      locations:[],
      preferredWorkTime:[],
      additonalInformation:'',
      remote_work:false,
      preferredPeriod:<IpreferredPeriod>{},
      salary:{
        monthly:null,
        hourly:null
      },
    },
    internshipOptions:{
      internshipType:[],
      init:false,
      possible_internship:[],
      locations:[],
      preferredPeriod:{
        type:0
      },
      additonalInformation:'',
      preferredWorkTime:[],
      scholarshipOpt:{
        returnWork:false,
        returnComWork:false,
        anyKind:false,
        sizescholarship:0
      }
    }



    };
  }

  setDataForPopup(name,url){

    this.popupName = name
    this.popupUrl = url
  }
  optionsChange(id,opt){
    var i = opt.indexOf(id);
    i>-1?opt.splice(i,1):
      opt.push(id)
  }
  verifyResume(){
    this.resumeStatus.objective = this.isValid('objective');
    this.resumeStatus.keyskills =
                                this.userService.profile.Student.schools.length>0
                                // &&this.isValid('accomplishmentsInformation')
                                &&this.isValid('pickedSkills')
                                &&this.isValid('currentLevelOfEducation')
    this.resumeStatus.language = this.isValid('languageSkills')
                                &&function(){
                                if (this.resume.hasOwnProperty('languageSkills')){

                                  for (var i = 0; i < this.resume.languageSkills.length; i++) {

                                    if (this.resume.languageSkills[i].hasOwnProperty('langLevel')&&this.resume.languageSkills[i].langLevel==6){
                                      return true
                                    }

                                  }
                                }
                                return false
                                }.bind(this)()

    this.resumeStatus.computer = this.isValid('officeTools')
                                &&this.isValid('otherSoftware')
                                &&this.isValid('informationAboutComputerSkills')
    this.resumeStatus.work_exp = this.isValid('workExp')
                                &&this.isValid('otherSpecificSkills');
    this.resumeStatus.personality = this.isValid('strenghs')&&this.isValid('characterTraits')
    this.resumeStatus.interest = this.isValid('interestsAndHobbies')&&this.isValid('yourMainValues')
    this.resumeStatus.additional = this.isValid('additional')
    this.resumeStatus.expectations =
                                  (
                                    this.resume.internshipOptions.init
                                      ||(
                                        // this.isValid('internshipOptions.possible_internship')
                                        this.isValid('internshipOptions.internshipType')
                                        &&this.isValid('internshipOptions.occupationField')
                                        &&(
                                          this.resume.internshipOptions.remote_work==true||
                                          (this.isValid('internshipOptions.locations')&&this.resume.internshipOptions.remote_work==false)

                                        )
                                        &&this.isValid('internshipOptions.preferredWorkTime')
                                        &&this.isValid('internshipOptions.additonalInformation')
                                        )
                                  )&&(
                                    this.isValid('jobOptions.jobType')
                                    &&this.isValid('jobOptions.occupationField')
                                    &&this.isValid('jobOptions.possible_position')
                                    &&(
                                      this.resume.jobOptions.remote_work==true||
                                      (this.isValid('jobOptions.locations')&&this.resume.jobOptions.remote_work==false)

                                    )
                                    &&this.isValid('jobOptions.preferredWorkTime')
                                  )
    this.resumeStatus.education_progress = function(){
                                  if (this.userService.profile.Student.schools){
                                    var max = 0
                                    var maxSch = null
                                    for (var i = 0; i < this.userService.profile.Student.schools.length; i++) {
                                      if (this.userService.profile.Student.schools[i].educationLevel>=max){
                                        max = this.userService.profile.Student.schools[i].educationLevel
                                        maxSch = i
                                      }
                                    }

                                    return this.userService.profile.Student.schools[maxSch]
                                          &&this.userService.profile.Student.schools[maxSch].hasOwnProperty('SchoolProgressEducation')
                                          &&this.userService.profile.Student.schools[maxSch].SchoolProgressEducation.hasOwnProperty('avarage')

                                  }
                                   return false
                                }.bind(this)()
    this.resumeStatus.recommendations = function(){
                                  if (this.userService.profile.recommendations&&this.userService.profile.recommendations.length>0){
                                    for (var i = 0; i < this.userService.profile.recommendations.length; i++) {
                                      if (this.userService.profile.recommendations[i].status==1){
                                        return true
                                      }

                                    }
                                  }
                                  return false

    }.bind(this)()
    // this.resume.prc = 0
    // for (var key in Object.keys(this.resumeStatus)) {
    //   var k =Object.keys(this.resumeStatus)[key]
    //
    //   if (typeof k=='string'){
    //     k=='objective'&&this.resumeStatus[k]?this.resume.prc+=5:null;
    //     k=='keyskills'&&this.resumeStatus[k]?this.resume.prc+=10:null;
    //     k=='language'&&this.resumeStatus[k]?this.resume.prc+=5:null;
    //     k=='computer'&&this.resumeStatus[k]?this.resume.prc+=5:null;
    //     k=='work_exp'&&this.resumeStatus[k]?this.resume.prc+=10:null;
    //     k=='personality'&&this.resumeStatus[k]?this.resume.prc+=5:null;
    //     k=='interest'&&this.resumeStatus[k]?this.resume.prc+=5:null;
    //     k=='expectations'&&this.resumeStatus[k]?this.resume.prc+=25:null;
    //     k=='additional'&&this.resumeStatus[k]?this.resume.prc+=2.5:null;
    //     k=='education_progress'&&this.resumeStatus[k]?this.resume.prc+=10:null;
    //
    //77.5
    //
    //   }
    //
    //
    // }
    // this.resume.prc+=this.userService.userDataComplited


  }
  isValid(str){

    var r  = false
    if (str.indexOf('.')>-1){
      var t = str.split('.')

      if (this.resume.hasOwnProperty(t[0])&&this.resume[t[0]].hasOwnProperty(t[1])&&Object.keys(this.resume[t[0]][t[1]]).length>0){
        r  =  true
      }
    }
    switch (typeof this.resume[str]){
      case 'string':
        if (this.resume.hasOwnProperty(str)&&this.resume[str]!=""){
          r  =  true
        }
        break;

      case 'boolean':
        if (this.resume.hasOwnProperty(str)){
          r  =  true
        }
        break;
      case 'number':
        if (this.resume.hasOwnProperty(str)){
          r  =  true
        }
        break;
      case 'object':

          if (this.resume.hasOwnProperty(str)&&Object.keys(this.resume[str]).length>0){
            r  =  true
          }



        break;


    }
    return r

  }
  updateAllResume(){
    for(var i=0;i<this.userService.profile.Student.resumes.length;i++){
      this.dataService.sendws('Resume/update',this.userService.profile.Student.resumes[i]).then(data=>{
        for(var i=0;i<this.userService.profile.Student.resumes.length;i++){
          if (this.userService.profile.Student.resumes[i]._id==data['resume']._id){

            this.userService.profile.Student.resumes[i] = <IResume>JSON.parse(JSON.stringify(data['resume']))
            this.rS.setResume([this.userService.profile.Student.resumes[i]])

          }
        }
      })



    }
  }
  saveResume(){
    let valid = true

    this.verifyResume()
    this.resume['resumeStatus'] = this.resumeStatus
    this.dataService.sendws('Resume/update',this.resume).then(data=>{
        //for(var i=0;i<this.userService.profile.Student.resumes.length;i++){
        //  if (this.userService.profile.Student.resumes[i]._id==this._id){
        //    //this.resume = data['resume']
        //    //this.resume.rating = resume['resume']
        //    this.userService.profile.Student.resumes[i] = JSON.parse(JSON.stringify(this.resume))
        //
        //  }
        //}

      for(var i=0;i<this.userService.profile.Student.resumes.length;i++){
        if (this.userService.profile.Student.resumes[i]._id==this._id){
          this.resume = <IResume>data['resume']

          this.userService.profile.Student.resumes[i] = <IResume>JSON.parse(JSON.stringify(this.resume))

        }
      }
      console.log('=======================')
      console.log( this.resume)
      // this.dataService.sendws('Resume/complitedResumeRatingCalculate',{resume:this.resume}).then(resume=>{
      //
      //
      // })
      this.updateAllResume()
    })


  }

  updateResume(_id){
    var update = false;
    //this.resume.prc = this.dataService.ObjectKeysLength(this.resume.prc)
    for(var i=0;i<this.userService.profile.Student.resumes.length;i++){
      if (this.userService.profile.Student.resumes[i]._id==_id){
        this.userService.profile.Student.resumes[i] = Object.assign({},this.resume)

        update = true;
        break;
      }
    }
    if (!update){
      this.userService.profile.Student.resumes.push(Object.assign({},this.resume))
    }
  }
}
