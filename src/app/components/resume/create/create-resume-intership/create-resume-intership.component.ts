import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "../create.service";
import {HelpersService} from "../../../../_services/helpers.service";
import {DataService} from "../../../../_services/data.service";
import {IMyDpOptions} from "mydatepicker";
import {IinternshipOptions, Ilocations, IpreferredPeriod, IscholarshipOpt} from "../../../../_models/resume";

@Component({
  selector: 'create-resume-intership',
  templateUrl: './create-resume-intership.component.html',
  styleUrls: ['./create-resume-intership.component.scss']
})
export class CreateResumeIntershipComponent implements OnInit {
  rand:string;
  rand1:string;
  rand2:string;
  rand3:string;
  rand4:string;
  rand5:string;
  rand6:string;
  rand7:string;
  rand8:string;
  rand9:string;
  rand10:string;
  rand11:string;
  rand12:string;
  rand13:string;
  rand14:string;
  arrPosition = []
  possible_internship:string
  Address: string
  Location: any;
  preferredPeriod_start:any
  preferredPeriod_end:any
  public scholarshipMask: Array<string | RegExp>= [  /[0-9]/,/[0-9]/,',',/[0-9]/,/[0-9]/,/[0-9]/]
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
    minYear:new Date().getFullYear(),
    width:'150px'
  };
  show_preferred = false
  constructor(
    private route: ActivatedRoute,
    public dS:DataService,
    public cS:CreateResumeService,
    public helpers:HelpersService
  ) { }

  ngOnInit() {


    if (this.cS.resume.internshipOptions.preferredPeriod&&this.cS.resume.internshipOptions.preferredPeriod.start){
      this.preferredPeriod_start = this.dS.convertPickeDate(this.cS.resume.internshipOptions.preferredPeriod.start)
    }else{}
    if (this.cS.resume.internshipOptions.preferredPeriod&&this.cS.resume.internshipOptions.preferredPeriod.end){
      this.preferredPeriod_end = this.dS.convertPickeDate(this.cS.resume.internshipOptions.preferredPeriod.end)
    }


    if (Object.keys(this.cS.resume.internshipOptions.preferredPeriod).length>0){
      this.show_preferred = true
    }
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
    this.rand = this.helpers.randomString(5);
    this.rand1 = this.helpers.randomString(5);
    this.rand2 = this.helpers.randomString(5);
    this.rand3 = this.helpers.randomString(5);
    this.rand4 = this.helpers.randomString(5);
    this.rand5 = this.helpers.randomString(5);
    this.rand6 = this.helpers.randomString(5);
    this.rand7 = this.helpers.randomString(5);
    this.rand8 = this.helpers.randomString(5);
    this.rand9 = this.helpers.randomString(5);
    this.rand10 = this.helpers.randomString(5);
    this.rand11 = this.helpers.randomString(5);
    this.rand12 = this.helpers.randomString(5);
    this.rand13 = this.helpers.randomString(5);
    this.rand14 = this.helpers.randomString(5);
  }

  getData(e,i){
    if (i==0){
      this.possible_internship = e
      this.dS.sendws('Resume/searchPositionFromName',{position:e}).then(data=>{
        this.arrPosition = data['position']
      })
    }
    if (i==1){
      this.possible_internship = e
    }
  }
  delPeriodPreferred(ee){
    if (!ee){
      this.cS.resume.internshipOptions.preferredPeriod = <IpreferredPeriod>{}

    }
    this.preferredPeriod_start = undefined
    this.preferredPeriod_end = undefined

    //this.cS.resume.jobOptions.preferredPeriod = <IpreferredPeriod>{}
  }
  initOptions(e){
    if (e) {
      //this.cS.resume.internshipOptions = <IinternshipOptions>{
      //  init:e,
      //  internshipType:{},
      //  locations:[],
      //  preferredPeriod:<IpreferredPeriod>{},
      //  scholarshipOpt:<IscholarshipOpt>{},
      //}
    }
    if (!e) {
      //this.cS.resume.internshipOptions = <IinternshipOptions>{
      //  internshipType:      {},
      //  init:                false,
      //  possible_internship: [],
      //  locations:           [],
      //  preferredPeriod:     {
      //    type:0
      //  },
      //  additonalInformation:'',
      //  preferredWorkTime:   {},
      //  scholarshipOpt:      {
      //    returnWork:     false,
      //    returnComWork:  false,
      //    anyKind:        false,
      //    sizescholarship:0
      //  }
      //}
    }

  }
  addPossible_internship(){
    if (this.possible_internship&&this.possible_internship!=''){
      this.cS.resume.internshipOptions.possible_internship.push(this.possible_internship)
      this.possible_internship = ''
    }




  }
  delPossible_internship(i){
    this.cS.resume.internshipOptions.possible_internship.splice(i,1)
  }
  deleteLocate(i){
    this.cS.resume.internshipOptions.locations.splice(i,1)
  }
  setLocate(){
    if (!this.cS.resume.internshipOptions.locations){
      this.cS.resume.internshipOptions.locations=[]
    }
    if (this.Location){
      this.cS.resume.internshipOptions.locations.push(this.Location)
      this.Address = ''
      this.Location = null
    }

  }
}
