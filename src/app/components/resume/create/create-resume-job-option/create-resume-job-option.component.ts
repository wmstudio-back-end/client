import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "app/components/resume/create/create.service";
import {HelpersService} from "../../../../_services/helpers.service";
import {DataService} from "../../../../_services/data.service";
import {IMyDpOptions} from "mydatepicker";
import {UserService} from "../../../../_services/user.service";
import {IpreferredPeriod} from "../../../../_models/resume";

@Component({
  selector: 'create-resume-job-option',
  templateUrl: './create-resume-job-option.component.html',
  styleUrls: ['./create-resume-job-option.component.scss']
})
export class CreateResumeJobOptionComponent implements OnInit {
  rand:string;
  rand1:string;
  rand2:string;
  rand3:string;
  rand4:string;
  rand5:string;
  rand6:string;
  rand7:string;
  rand8:string;
  rand9:string;
  rand10:string;
  rand11:string;
  rand12:string;
  rand13:string;
  rand14:string;
  possible_position:string
  Address: string
  Location: any;
  preferredPeriod_start:any
  preferredPeriod_end:any
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
    minYear:new Date().getFullYear(),
    width:'150px'
  };
  public salaryMask: Array<string | RegExp>= [  /[0-9]/,/[0-9]/,',',/[0-9]/,/[0-9]/,/[0-9]/]
  public salaryMask2: Array<string | RegExp>= [  /[0-9]/,/[0-9]/,/[0-9]/,',',/[0-9]/,/[0-9]/]
  arrPosition = []
  show_preferred = false
  constructor(
    private route: ActivatedRoute,
    public cS:CreateResumeService,
    public helpers:HelpersService,
    public dS:DataService,
    private uS:UserService
  ) { }
  getData(e,i){
    if (i==0){
      this.possible_position = e
      this.dS.sendws('Resume/searchPositionFromName',{position:e}).then(data=>{
        this.arrPosition = data['position']
      })
    }
    if (i==1){
      this.possible_position = e
    }
  }
  delPeriodPreferred(ee){
    if (!ee){
      this.cS.resume.jobOptions.preferredPeriod = <IpreferredPeriod>{}

    }
    this.preferredPeriod_start = undefined
    this.preferredPeriod_end = undefined

    //this.cS.resume.jobOptions.preferredPeriod = <IpreferredPeriod>{}
  }
  ngOnInit() {
    if (this.cS.resume.jobOptions.preferredPeriod&&this.cS.resume.jobOptions.preferredPeriod.start){
      this.preferredPeriod_start = this.dS.convertPickeDate(this.cS.resume.jobOptions.preferredPeriod.start)
    }
    if (this.cS.resume.jobOptions.preferredPeriod&&this.cS.resume.jobOptions.preferredPeriod.end){
      this.preferredPeriod_end = this.dS.convertPickeDate(this.cS.resume.jobOptions.preferredPeriod.end)
    }

    if (Object.keys(this.cS.resume.jobOptions.preferredPeriod).length>0){
      this.show_preferred = true
    }

    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
    this.rand = this.helpers.randomString(5);
    this.rand1 = this.helpers.randomString(5);
    this.rand2 = this.helpers.randomString(5);
    this.rand3 = this.helpers.randomString(5);
    this.rand4 = this.helpers.randomString(5);
    this.rand5 = this.helpers.randomString(5);
    this.rand6 = this.helpers.randomString(5);
    this.rand7 = this.helpers.randomString(5);
    this.rand8 = this.helpers.randomString(5);
    this.rand9 = this.helpers.randomString(5);
    this.rand10 = this.helpers.randomString(5);
    this.rand11 = this.helpers.randomString(5);
    this.rand12 = this.helpers.randomString(5);
    this.rand13 = this.helpers.randomString(5);
    this.rand14 = this.helpers.randomString(5);
  }
  //optionsChange(id,opt){
  //
  //      var i = opt.indexOf(id);
  //      i>-1?opt.splice(i,1):
  //        opt.push(id)
  //
  //}
  deleteLocate(i){
    this.cS.resume.jobOptions.locations.splice(i,1)
  }
  setLocate(){
    if (!this.cS.resume.jobOptions.locations){
      this.cS.resume.jobOptions.locations=[]
    }

  if (this.Location){
    this.cS.resume.jobOptions.locations.push(this.Location)
    this.Address = ''
    this.Location = null
  }

  }
  addPossible_position(){
    if (this.possible_position&&this.possible_position!=''&&this.possible_position.length<=50&&this.cS.resume.jobOptions.possible_position.length<=50){
      this.cS.resume.jobOptions.possible_position.push(this.possible_position)
      this.possible_position = ''
    }


  }
  delPossible_position(i){
    this.cS.resume.jobOptions.possible_position.splice(i,1)
  }
  fieldActivity(items){

    console.log(items)
  }
}
