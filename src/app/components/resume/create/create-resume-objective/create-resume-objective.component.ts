import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "../create.service";
import {HelpersService} from "../../../../_services/helpers.service";

@Component({
  selector: 'create-resume-objective',
  templateUrl: './create-resume-objective.component.html',
  styleUrls: ['./create-resume-objective.component.scss'],


})
export class CreateResumeObjectiveComponent implements OnInit {
  rand:string;
  constructor(private route: ActivatedRoute,public cS:CreateResumeService, public helpers:HelpersService) { }

  ngOnInit() {
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])

    this.rand = this.helpers.randomString(5);
  }

}
