import {Component, Input, OnInit} from '@angular/core';
import {CreateResumeService} from "../create.service";

@Component({
  selector: 'next-link-resume',
  templateUrl: './next-link-resume.component.html',
  styleUrls: ['./next-link-resume.component.scss']
})
export class NextLinkResumeComponent implements OnInit {
  @Input() name:string
  @Input() url:string
  constructor(private cS:CreateResumeService) {

  }

  ngOnInit() {

  }

}
