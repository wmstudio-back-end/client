import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CreateResumeService} from "../create.service";

@Component({
  selector: 'create-resume-interest',
  templateUrl: './create-resume-interest.component.html',
  styleUrls: ['./create-resume-interest.component.scss']
})
export class CreateResumeInterestComponent implements OnInit {
  
  constructor(private route: ActivatedRoute,public cS:CreateResumeService) { }
  
  ngOnInit() {
    this.cS.setDataForPopup(this.route.snapshot.data['name'],this.route.snapshot.data['url'])
  }

}
