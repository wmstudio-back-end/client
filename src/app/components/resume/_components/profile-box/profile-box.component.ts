import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../../../../_services/data.service";
import {UserService} from "../../../../_services/user.service";

@Component({
    selector: 'profile-resumebox',
    templateUrl: './profile-box.component.html',
    styleUrls: ['./profile-box.component.scss']
})
export class ProfileBoxComponent implements OnInit {
    @Input() items: any = [];
    @Input() title: string;
    @Input() limit: number = 0;
    localItems: any = [];
    constructor(public dS:DataService,public uS:UserService) {
    }

    ngOnInit() {

      this.items.sort((a:any,b:any)=>{
        return b.updated_at-a.updated_at
      })

      if (this.items){
        for(var i=0;i<this.items.length;i++){
          if (i>=this.limit){break;}
          this.localItems.push(this.items[i])
        }
      }
      this.localItems.sort((a:any,b:any)=>{
        return a.updated_at<b.updated_at
      })

    }

}
