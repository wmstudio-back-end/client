import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RightSidecontentComponent} from "./right-sidecontent/right-sidecontent.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [RightSidecontentComponent],
  exports:[RightSidecontentComponent]
})
export class BannersModule { }
