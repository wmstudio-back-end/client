
import {Component, OnInit} from "@angular/core";
import {ResumeService} from "../../../_services/resume.service";
import {DataService} from "../../../_services/data.service";
import {HelpersService} from "../../../_services/helpers.service";
import {LocationService} from "../../../_services/location.service";
import {UserService} from "../../../_services/user.service";
import {DownPopupService} from "../../../_services/down-popup.service";
@Component({
    selector: 'app-sidebar-candidates-filter',
    templateUrl: './sidebar-candidates-filter.component.html',
    styleUrls: ['./sidebar-candidates-filter.component.scss']
})
export class SidebarCandidatesFilterComponent implements OnInit {
    expYear: any                  = []
    typeVacOptions                = []
    private filterPlace: string[] = [
        'country',
        'city'
    ];

    constructor(public rs: ResumeService,
                public ds: DataService,
                public uS: UserService,
                private popS: DownPopupService,
                public ls: LocationService,
                public help: HelpersService) {
        this.expYear = help.getYears(0, 15,true)
        for (let i of this.ds.options.internshipType) {
            // i.id = parseInt('1'+i.id)
            this.typeVacOptions.push(i)
        }
        for (let i of this.ds.options.jobType) {
            //i.id = parseInt('2'+i.id)
            this.typeVacOptions.push(i)
        }
    }

    ngOnInit() {
    }

    setFilter(key, val, i, op) {
        // this.rs.changeFilterElem = undefined
        this.rs.setFilterItem(this.rs.getFilterItem(key, op, val, i))
    }

    setRating(n) {
        this.rs.setFilterOption('profileScore', n)
        this.setFilter('rating', n, 'resume', '$gte')
    }

    setExpectGrad(val, t) {
        let y, m;

        if (t == 'm') {
            m = val
            y = this.rs.filter.options.expectedGraduationYear
            y = (Object.keys(y).length) ? parseInt(Object.keys(y)[0]) : null
        } else {
            y = val
            m = this.rs.filter.options.expectedGraduationMonth
            m = (Object.keys(m).length) ? parseInt(this.help.month[Object.keys(m)[0]].text) : null
        }

        let year: number  = (y) ? y : this.help.getYear(),
            month: number = (m) ? m : 12
        let value         = [
            {
                $and: [
                    {'schools.expectedGraduationMonth': {$lte: month}},
                    {'schools.expectedGraduationYear': {$lte: year}},
                ]
            },
            {
                'schools.expectedGraduationYear': {$lte: year}
            },
        ]

        this.rs.unserFilter('$or', 'school')
        this.rs.setFilter('$or', value, 'school')
    }

    changeCheckbox(key: string, e: any) {
        let val          = e.target.checked
        let type: string = (key == 'recommends' || key == 'workExp') ? 'resume' : 'user'
        if (val && (key == 'workExp' || key == 'recommends'))
            val = {
                $exists: true,
                $not: {$size: 0}
            }
        if (key == 'male' || key == 'female') {
            let key = 'Student.gender',
                val = [];
            if (this.rs.filter.options.sex[0]) val.push(0)
            if (this.rs.filter.options.sex[1]) val.push(1)
            if (!val.length)
                this.rs.unserFilter(key, 'user', '$in')
            else
                this.rs.setFilter(key, val, 'user', '$in')
        } else if (!val) {
            this.rs.unserFilter(key, type)
        } else
            this.rs.setFilter(key, val, type)
    }

    setAge(e, from?: boolean) {
        let operand: string = '$lte',
            val             = e.target.value,
            y               = 0
        if (from) {
            operand = '$gte'
            y       = 1
        }
        this.rs.unserFilter('birthday', 'user', operand)
        if (val) {
            let year = (new Date()).getFullYear() - parseInt(val) - y
            let date = new Date()
            date.setFullYear(year)
            let D = Math.ceil(date.getTime() / 1000)
            console.log(new Date(D * 1000))
            this.rs.setFilter('birthday', D, 'user', operand)
        }
        // this.rs.changeFilterElem = undefined;
    }

    searchResume() {
        if (!this.uS.accessPackage.projBmark) {
            this.popS.show_warning(this.ds.getOptionsFromId('errors', 10).text)
            return
        }
        this.rs.filter.skip = 0
        this.rs.getResume()
    }

    setTypeVacFilter(event) {
        for (let i of this.ds.options.internshipType) {
            this.rs.unserFilter('internshipOptions.internshipType.' + i.id, 'resume', '$or', true)
        }
        for (let i of this.ds.options.jobType) {
            this.rs.unserFilter('jobOptions.jobType.' + i.id, 'resume', '$or', true)
        }
        if (event.length)
            for (let i of event) {
                let id = i.id.toString()
                if (id[0] == 1) {
                    if (this.rs.job)
                        this.rs.setFilter('internshipOptions.internshipType.' + id.slice(1), true, 'resume', '$or', true)
                } else {
                    if (this.rs.intern)
                        this.rs.setFilter('jobOptions.jobType.' + id.slice(1), true, 'resume', '$or', true)
                }
            }
        this.rs.getCountResumes(true)
    }

    setWorkTimeFilter(event) {
        for (let i of this.ds.options.workTime)
            this.rs.unserFilter('jobOptions.preferredWorkTime.' + i.id, 'resume', '$or', true)
        this.rs.unserFilter('$and', 'resume', '$or', true)
        let intern = [];
        if (event.length)
            for (let i of event) {
                let obj                                            = {}
                obj['internshipOptions.preferredWorkTime.' + i.id] = true
                if (this.rs.intern)
                    intern.push(obj)
                if (this.rs.job)
                    this.rs.setFilter('jobOptions.preferredWorkTime.' + i.id, true, 'resume', '$or', true)
            }
        if (intern.length)
            this.rs.setFilter('$and', [
                {'internshipOptions.init': false},
                {'$or': intern}
            ], 'resume', '$or', true)
        this.rs.getCountResumes(true)
    }

    setLastElem(event) {
        this.rs.changeFilterElem = event;
    }
  setAddress2(address) {
    console.log('999999999',address)
  }
    setAddress(address) {
        // if (typeof this.ls.location['resumeSearchCity'] === "undefined" || !address.trim()) {
        //     this.rs.unserFilter('city', 'location')
        //     this.rs.unserFilter('country', 'location')
        //     return
        // }
        // console.log('address', !address.trim())
        // let location = this.ls.location['resumeSearchCity']
        // if (location.hasOwnProperty('city'))
        //     this.rs.setFilter('city', location.city, 'location')
        // if (location.hasOwnProperty('country'))
        //     this.rs.setFilter('country', location.country, 'location')

        if (typeof this.ls.location['resumeSearchCity'] === "undefined" || !address.trim()) {
            this.rs.unserFilter('Location.addr', 'user')
            return
        }

        let location = this.ls.location['resumeSearchCity']
        console.log('address', !address.trim(),location)
        let locations = []
        if (typeof location === "undefined") return;
        let words  = [];
        let string = address.replace('/\s/g', '===')
                            .replace('/[,.]/g', '===')
                            .split('===')
                            .filter((e, i, a) => {
                                return (e) ? true : false;
                            });
        let keys   = Object.keys(location);
        for (var i = 0; i < keys.length; i++) {
            let k = keys[i]
            if (this.filterPlace.indexOf(k) > -1 && location[k])
                words.push(location[k])
        }

        if (words.length) {
          let text = [
            this.help.getPatternInString(words),
            this.help.getPatternInString(string),
          ]


          this.rs.setFilterItem(this.rs.getFilterItem('Location.addr', '$in', text, 'user'))
        }
    }

}
