import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {SidebarCandidatesFilterComponent} from "./sidebar-candidates-filter.component";
describe('SidebarCandidatesFilterComponent', () => {
  let component: SidebarCandidatesFilterComponent;
  let fixture: ComponentFixture<SidebarCandidatesFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations:[SidebarCandidatesFilterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarCandidatesFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
