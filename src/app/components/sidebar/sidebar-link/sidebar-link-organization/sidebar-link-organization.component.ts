import {Component, ComponentFactoryResolver, EventEmitter, Input, Output, ViewContainerRef} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../../../../_models/user";
import {MessageService} from "../../../../_services/message.service";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {DataService} from "../../../../_services/data.service";
import {UserService} from "../../../../_services/user.service";
import {DownPopupService} from "../../../../_services/down-popup.service";

@Component({
  //selector: 'sidebar-link-company',
  templateUrl:'sidebar-link-organization.component.html',
  styleUrls:  ['sidebar-link-organization.component.css']


})
export class SidebarLinkOrganizationComponent {
  @Input() type_profile: string;
  prc = 0
  profile: User;
  name = "Евгений";
  @Output('hide') hide = new EventEmitter<boolean>()
  constructor(private router: Router,
              public mS:MessageService,
              public authenticationService: AuthenticationService,
              public dataService: DataService,
              public user: UserService,
              public popS: DownPopupService,
              private resolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef) {
    //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.profile = this.user.profile
  }

  ready_to_work(val: number) {
    this.dataService.send("User/editProfile", {ready_to_work:val});
    // this.snackBar.open(this.profile.ready_to_work, "X", {
    //     duration: 1000,
    // });
  }


    toggleAccordion($event) {
        let parent = $event.toElement.parentNode;

        if(!parent.classList.contains('active')) {
            parent.classList.add('active');
        } else {
            parent.classList.remove('active');
        }

    }

  // remoteTemplateFactory: IDynamicRemoteTemplateFactory = {
  //     // This is an optional method
  //     buildRequestOptions (): RequestOptionsArgs {
  //         const headers = new Headers();
  //         headers.append('Token', '100500');
  //
  //         return {
  //             withCredentials: true,
  //             headers: headers
  //         };
  //     },
  //     // This is an optional method
  //     parseResponse (response: Response): string {
  //         return response.json().headers['User-Agent'];
  //     }
  // };
}
