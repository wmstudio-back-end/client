"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var SidebarLinkCompanyComponent = (function () {
  function SidebarLinkCompanyComponent(router, authenticationService, dataService, user, resolver, viewContainerRef) {
    this.router = router;
    this.authenticationService = authenticationService;
    this.dataService = dataService;
    this.user = user;
    this.resolver = resolver;
    this.viewContainerRef = viewContainerRef;
    this.prc = 0;
    this.name = "Евгений";
    //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    this.profile = this.user.profile;
  }

  SidebarLinkCompanyComponent.prototype.ready_to_work = function (val) {
    this.dataService.send("User/editProfile", {ready_to_work: val});
    // this.snackBar.open(this.profile.ready_to_work, "X", {
    //     duration: 1000,
    // });
  };
  return SidebarLinkCompanyComponent;
}());
__decorate([
  core_1.Input()
], SidebarLinkCompanyComponent.prototype, "type_profile", void 0);
SidebarLinkCompanyComponent = __decorate([
  core_1.Component({
    //selector: 'sidebar-link-company',
    templateUrl: 'sidebar-link-company.component.html',
    styleUrls: ['sidebar-link-company.component.css'],
  })
], SidebarLinkCompanyComponent);
exports.SidebarLinkCompanyComponent = SidebarLinkCompanyComponent;
