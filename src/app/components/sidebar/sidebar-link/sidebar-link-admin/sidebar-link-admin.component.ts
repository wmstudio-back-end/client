import {Component, ComponentFactoryResolver, EventEmitter, Input, Output, ViewContainerRef} from "@angular/core";
import {Router} from "@angular/router";

import {AuthenticationService} from "../../../../_services/authentication.service";
import {DataService} from "../../../../_services/data.service";

@Component({
  //selector: 'sidebar-link-admin',
  templateUrl:'sidebar-link-admin.component.html',
  styleUrls:  ['sidebar-link-admin.component.css']


})
export class SidebarLinkAdminComponent {
  @Input() type_profile: string;
  countVacancy = 0
  countVacancyProof = 0
  @Output('hide') hide = new EventEmitter<boolean>()
  constructor(public dataService:DataService) {
    this.dataService.sendws('Admin/getCountVacancy',{}).then((data)=>{
      this.countVacancy = data['countVacancy']
    })
    this.dataService.sendws('Admin/getCountVacancyProof',{query:{proof:1}}).then((data)=>{
      this.countVacancyProof = data['countVacancyProof']
    })
  }

}
