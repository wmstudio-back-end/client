import {Component, EventEmitter, OnInit, Output} from "@angular/core";
import {MessageService} from "../../../../_services/message.service";
import {DownPopupService} from "../../../../_services/down-popup.service";
import {UserService} from "../../../../_services/user.service";

@Component({
  selector: 'app-sidebar-link-educator',
  templateUrl: './sidebar-link-educator.component.html',
  styleUrls: ['./sidebar-link-educator.component.scss']
})
export class SidebarLinkEducatorComponent implements OnInit {
  @Output('hide') hide = new EventEmitter<boolean>()
  constructor(public mS:MessageService,public popS: DownPopupService, public user: UserService) {
  }

  ngOnInit() {
  }



    toggleAccordion($event) {
        let parent = $event.toElement.parentNode;

        if(!parent.classList.contains('active')) {
            parent.classList.add('active');
        } else {
            parent.classList.remove('active');
        }

    }


}
