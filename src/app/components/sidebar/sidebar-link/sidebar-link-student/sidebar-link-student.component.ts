import {
  Component, ComponentFactoryResolver, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild,
  ViewContainerRef
} from "@angular/core";
import {Router} from "@angular/router";
import {MessageService} from "../../../../_services/message.service";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {DataService} from "../../../../_services/data.service";
import {DownPopupService} from "../../../../_services/down-popup.service";
import {UserService} from "../../../../_services/user.service";


@Component({
  // selector: 'sidebar-link-student',
  templateUrl:'sidebar-link-student.component.html',
  styleUrls:  ['sidebar-link-student.component.scss']


})
export class SidebarLinkStudentComponent implements OnInit {
  @Input() type_profile: string;
  prc = 0

  accItems = {
    account: false,
    resume: false,
    tools: false
  }




  constructor(private router: Router,
              public mS:MessageService,
              public authenticationService: AuthenticationService,
              public dataService: DataService,
              public uS: UserService,
              public popS: DownPopupService,
              private resolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef) {
    //this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
  }

  getPrc(f = [],ud = this.uS.userDataComplited){


    var max = 0;
    var t = null
    if (this.uS.profile.Student.resumes.length>0){
      for (var i = 0; i < this.uS.profile.Student.resumes.length; i++) {
        if (max<this.uS.profile.Student.resumes[i].prc){
          t= i
          max = this.uS.profile.Student.resumes[i].prc
        }
      }

    }

    let res = max>0?(max+this.uS.userDataComplited):this.uS.userDataComplited;

    return res

  }
  ngOnInit() {
   // this.profile = this.user.profile
   //  this.uS.complitedUserdata()
    this.getPrc()


  }

  ready_to_work(val) {
    if (val){
      var p = false
      for (var i = 0; i < this.uS.profile.Student.resumes.length; i++) {
        if (this.uS.profile.Student.resumes[i].publish){p=true;}

      }
      if (!p){
        this.popS.show_success(this.dataService.getOptionsFromId('errors', 29).text)
      }

    }
    this.dataService.send("User/editProfile", {"Student.ready_to_work":val});
  }


  toggleAccordion($event) {
   let parent = $event.toElement.parentNode;

    if(!parent.classList.contains('active')) {
        parent.classList.add('active');
    } else {
        parent.classList.remove('active');
    }

  }

  // remoteTemplateFactory: IDynamicRemoteTemplateFactory = {
  //     // This is an optional method
  //     buildRequestOptions (): RequestOptionsArgs {
  //         const headers = new Headers();
  //         headers.append('Token', '100500');
  //
  //         return {
  //             withCredentials: true,
  //             headers: headers
  //         };
  //     },
  //     // This is an optional method
  //     parseResponse (response: Response): string {
  //         return response.json().headers['User-Agent'];
  //     }
  // };
}
