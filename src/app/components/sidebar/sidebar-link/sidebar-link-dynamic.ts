import {
  Component,
  ComponentFactoryResolver, EventEmitter,
  Input, Output,
  ReflectiveInjector,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import {SidebarLinkStudentComponent} from "./sidebar-link-student/sidebar-link-student.component";
import {SidebarLinkCompanyComponent} from "./sidebar-link-company/sidebar-link-company.component";
import {SidebarLinkAdminComponent} from "./sidebar-link-admin/sidebar-link-admin.component";
import {SidebarLinkEducatorComponent} from "./sidebar-link-educator/sidebar-link-educator.component";
import {SidebarLinkOrganizationComponent} from "./sidebar-link-organization/sidebar-link-organization.component";
import {isBoolean} from "util";
import {HelpersService} from "../../../_services/helpers.service";
@Component({
  selector:'sidebar-link',
  entryComponents:[
    SidebarLinkStudentComponent,
    SidebarLinkCompanyComponent,
    SidebarLinkAdminComponent,
    SidebarLinkEducatorComponent,
    SidebarLinkOrganizationComponent
  ], // Reference to the components must be here in order to dynamically create them
  template:`
             <div #dynamicComponentContainer></div>`
})
export class SidebarLinkDynamic {
  currentComponent = null;
  @ViewChild('dynamicComponentContainer', {read:ViewContainerRef}) dynamicComponentContainer: ViewContainerRef;
  // component: Class for the component you want to create
  // inputs: An object with key/value pairs mapped to input name/input value
  @Output('hide') hide = new EventEmitter<Boolean>()
  @Input() set componentData(data: {component: any, inputs: any}) {
    if (!data) {
      return;
    }

// Inputs need to be in the following format to be resolved properly
    let inputProviders = Object.keys(data.inputs).map((inputName) => {
      return {provide:inputName, useValue:data.inputs[inputName]};
    });
    let resolvedInputs = ReflectiveInjector.resolve(inputProviders);
    // We create an injector out of the data we want to pass down and this components injector
    let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.dynamicComponentContainer.parentInjector);
    // We create a factory out of the component we want to create
    let factory = this.resolver.resolveComponentFactory(data.component);
    // We create the component using the factory and the injector
    let component = factory.create(injector);
    // We insert the component into the dom container
    this.dynamicComponentContainer.insert(component.hostView);
    // We can destroy the old component is we like by calling destroy
    if (this.currentComponent) {
      this.currentComponent.destroy();
    }
    this.currentComponent = component;
  }


  constructor(private resolver: ComponentFactoryResolver,public help:HelpersService) {

  }

}
