import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ClickOutsideModule} from "ng-click-outside";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {ModulesModule} from "../../modules/modules.module";
import {SidebarVacancyFilterComponent} from "./sidebar-vacancy-filter/sidebar-vacancy-filter.component";
import {SidebarLinkStudentComponent} from "./sidebar-link/sidebar-link-student/sidebar-link-student.component";
import {SidebarLinkCompanyComponent} from "./sidebar-link/sidebar-link-company/sidebar-link-company.component";
import {SidebarLinkAdminComponent} from "./sidebar-link/sidebar-link-admin/sidebar-link-admin.component";
import {SidebarLinkDynamic} from "./sidebar-link/sidebar-link-dynamic";
import {SidebarCandidatesFilterComponent} from "./sidebar-candidates-filter/sidebar-candidates-filter.component";
import {SidebarTopCandidatesComponent} from "./sidebar-top-candidates/sidebar-top-candidates.component";
import {SidebarLinkEducatorComponent} from "./sidebar-link/sidebar-link-educator/sidebar-link-educator.component";
import {SidebarLinkOrganizationComponent} from "./sidebar-link/sidebar-link-organization/sidebar-link-organization.component";

@NgModule({
  imports:     [
    CommonModule,
    ClickOutsideModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    ModulesModule,


  ],
  declarations:[

    SidebarVacancyFilterComponent,
    SidebarLinkStudentComponent,
    SidebarLinkCompanyComponent,
    SidebarLinkAdminComponent,
    SidebarLinkDynamic,
    SidebarCandidatesFilterComponent,
    SidebarTopCandidatesComponent,
    SidebarLinkEducatorComponent,
    SidebarLinkOrganizationComponent,

  ],
  exports:     [

    SidebarVacancyFilterComponent,
    SidebarLinkStudentComponent,
    SidebarLinkCompanyComponent,
    SidebarLinkAdminComponent,
    SidebarLinkDynamic,
    SidebarCandidatesFilterComponent,
    SidebarTopCandidatesComponent,
    SidebarLinkOrganizationComponent,

  ]
})
export class SidebarModule {
}

