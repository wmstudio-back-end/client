import {Component, OnInit} from "@angular/core";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
@Component({
  selector: 'app-sidebar-top-candidates',
  templateUrl: './sidebar-top-candidates.component.html',
  styleUrls: ['./sidebar-top-candidates.component.scss']
})
export class SidebarTopCandidatesComponent implements OnInit {
  resumes = []
  objectKeys = Object.keys;
  constructor(public dS:DataService,private uS:UserService) {
  }

  ngOnInit() {
    this.dS.sendws('Resume/getRandomResume',{limit:3}).then(data=>{
      var ids = []
      for (var i = 0; i < data['res'].length; i++) {
        ids.push(data['res'].uid)

      }

      this.uS.getUsers(ids).then(users=>{
        this.resumes = data['res']
        console.log(this.resumes)
      })


    })
  }

}
