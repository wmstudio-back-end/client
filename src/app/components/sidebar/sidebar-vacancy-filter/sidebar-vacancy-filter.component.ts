import {Component, ElementRef, Input, OnInit, ViewChild} from "@angular/core";
import {DataService} from "../../../_services/data.service";
import {VacancyService} from "../../../_services/vacancy.service";
import {HelpersService} from "../../../_services/helpers.service";
@Component({
    selector: 'sidebar-vacancy-filter',
    templateUrl: './sidebar-vacancy-filter.component.html',
    styleUrls: ['./sidebar-vacancy-filter.component.scss']
})
export class SidebarVacancyFilterComponent implements OnInit {
    @Input('refresh') refresh: boolean
    @ViewChild('salary') salary: ElementRef
                      jobTypeSplice = []

    constructor(public dataService: DataService, public vacancyService: VacancyService, public help: HelpersService) {
    }


    ngOnChanges(changes: any) {
        if (changes.refresh && (changes.refresh.currentValue != changes.refresh.previousValue))
            this.salary.nativeElement.value = ''
    }

    ngOnInit() {
        this.jobTypeSplice = this.dataService.options.vacancyType.slice(2, this.dataService.options.vacancyType.length)
    }

    setFilter(key, val, op) {
        this.vacancyService.setFilterItem(this.help.getFilterItem(key, op, val))
    }

    setLastElem(event) {
        this.vacancyService.changeFilterElem = event;
    }

    setSalaryType(key, val) {
        this.vacancyService.unserFilter(key)
        this.vacancyService.setFilter(key, val, false)
        this.vacancyService.changeFilterElem = undefined;
    }

    setSalarySumm(e) {
        this.vacancyService.unserFilter('salary.summ')
        if (e.target.value)
            this.vacancyService.setFilter('salary.summ', parseInt(e.target.value), '$gte')
        this.vacancyService.changeFilterElem = undefined;
    }


    searchVacancy() {
        this.vacancyService.filter.skip = 0
        this.vacancyService.getVacancy()
    }

}
