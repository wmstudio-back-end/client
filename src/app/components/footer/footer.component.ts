import {Component, OnInit} from "@angular/core";
import {DataService} from "../../_services/data.service";
import {HelpersService} from "../../_services/helpers.service";
@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['footer.component.scss']
})
export class FooterComponent implements OnInit {
    clientProtocol: string = 'http'
    currYear               = 2018
    menu                   = {}
    menuColumns            = []

    constructor(public dataService: DataService,
                hs: HelpersService) {
        this.currYear = hs.getYear()
    }

    ngOnInit() {
        this.clientProtocol = window.location.protocol.replace(/:/g, '')
      if (this.dataService.options.hasOwnProperty('footerMenu'))
      {
        this.menu = this.dataService.options.footerMenu
        this.menuColumns    = Object.keys(this.dataService.options.footerMenu)
      }

    }

}
