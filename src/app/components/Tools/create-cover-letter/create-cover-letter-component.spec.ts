import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {CreateCoverLetterComponent} from "./create-cover-letter-component";

describe('CreateCoverLetterComponent', () => {
    let component: CreateCoverLetterComponent;
    let fixture: ComponentFixture<CreateCoverLetterComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ CreateCoverLetterComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CreateCoverLetterComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
