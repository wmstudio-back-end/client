import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {DataService} from "../../../_services/data.service";
import {ICoverLetter} from "../../../_models/cover-letter";
import {noUndefined} from "@angular/compiler/src/util";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../_services/user.service";

@Component({
    selector:   'app-create-cover-letter',
    templateUrl:'./create-cover-letter.html',
    styleUrls:  ['./create-cover-letter.scss']
})
export class CreateCoverLetterComponent implements OnInit {
    _id = null
    coverLetter:ICoverLetter = <ICoverLetter>{}
    constructor(
      public component:ComponentService,
      public dS:DataService,
      private router: Router,
      private route: ActivatedRoute,
      public uS:UserService
    ) {

    }

    ngOnInit() {

      if (this.route.snapshot.params['id']){
        for(var i = 0;i<this.uS.profile.CoverLetter.length;i++){
          if (this.uS.profile.CoverLetter[i]._id==this.route.snapshot.params['id']){
            this.coverLetter = this.uS.profile.CoverLetter[i]
            this._id = this.route.snapshot.params['id']
            break;
          }
        }
      }
    }

  saveCoverLetter(){


    var save = true
    this.coverLetter.text?null:save=false
    this.coverLetter.title?null:save=false
    this.coverLetter.language!=undefined?null:save=false
    if (save){
      if (this._id){
        this.dS.sendws('CoverLetter/updateCoverLetter',this.coverLetter).then((data)=>{
          for(var i = 0;i<this.uS.profile.CoverLetter.length;i++){
            if (this.uS.profile.CoverLetter[i]._id==this._id){
              if (data['coverLetter']){
                this.uS.profile.CoverLetter[i] = data['coverLetter']
              }

            }
          }
          this.router.navigate(['cover-letters']);
          console.log(data)
        })
      }else{
        this.dS.sendws('CoverLetter/create',this.coverLetter).then((data)=>{


          this.uS.profile.CoverLetter.push(data['coverLetter'])
          this.router.navigate(['cover-letters']);
        })
      }

    }

  }

}
