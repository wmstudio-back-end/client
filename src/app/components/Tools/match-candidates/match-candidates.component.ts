import {Component, OnInit} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";
import {DataService} from "../../../_services/data.service";
import {Imatch} from "../../../_models/vacancy";
import {Router} from "@angular/router";
import {DownPopupService} from "../../../_services/down-popup.service";
import {HelpersService} from "../../../_services/helpers.service";
import {TranslateService} from "@ngx-translate/core";
@Component({
  selector: 'app-match-candidates',
  templateUrl: './match-candidates.component.html',
  styleUrls: ['./match-candidates.component.scss']
})
export class MatchCandidatesComponent implements OnInit {
  viwe = []
  vac = []
  constructor(public component: ComponentService,
              public uS:UserService,
              public dS:DataService,
              public helpers: HelpersService,
              public tr:TranslateService,
              private popUp:DownPopupService,
              private router: Router,

) {
  }

  chVal(e,val){
    if (val){e=1}
    else{e=0}
    console.log(e,val)
  }
  getProjectName(id){
    for (var i = 0; i < this.uS.profile.Company.projects.length; i++) {
      if (id==this.uS.profile.Company.projects[i]._id){
        return this.uS.profile.Company.projects[i].name
      }
    }
  }
  saveMatch(vac,i){

    if (!this.uS.accessPackage.match){
      this.popUp.show_warning(this.dS.getOptionsFromId('errors', 10).text);
      return
    }
    this.dS.sendws('Vacancy/editMatch',{_id:vac._id,match:vac.match}).then(data=>{
        console.log("!!!!!!!!!!!!!",data)
      if (Object.keys(data['resumes']).length==0){
        this.popUp.show_success(this.dS.getOptionsFromId('errors', 25).text);
      }else{
        this.router.navigate(['/resume/all-resumes', {ids: data['resumes']}]);
      }

    })
  }
  ngOnInit() {

    if (confirm(this.tr.get('errors.' + 47)['value'])){


    }
    //this.popUp.show_warning(this.dS.getOptionsFromId('errors', 47).text)
    this.vac = []
    for (var i = 0; i < this.uS.profile.Company.vacancy.length; i++) {
   if (this.uS.profile.Company.vacancy[i].publish){
     this.vac.push(this.uS.profile.Company.vacancy[i])
   }

    }
    this.viwe = []
    for (var i = 0; i < this.uS.profile.Company.vacancy.length; i++) {
      this.viwe[i] = false
      if (!this.uS.profile.Company.vacancy[i].match){
        this.uS.profile.Company.vacancy[i].match = <Imatch>{
          e1:2,
          e2:2,
          e3:2,
          cand:0,
          work:0
        }
      }


    }
  }

}
