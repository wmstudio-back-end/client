import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import {PublicCompanyRatingsComponent} from "./public-company-ratings.component";

describe('PublicCompanyRatingsComponent', () => {
    let component: PublicCompanyRatingsComponent;
    let fixture: ComponentFixture<PublicCompanyRatingsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ PublicCompanyRatingsComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PublicCompanyRatingsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});
