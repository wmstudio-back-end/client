import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";

@Component({
    selector: 'app-public-company-ratings.component',
    templateUrl: './public-company-ratings.component.html',
    styleUrls: ['./public-company-ratings.component.scss']
})
export class PublicCompanyRatingsComponent implements OnInit {

    constructor(public component:ComponentService) { }

    ngOnInit() {
    }

}
