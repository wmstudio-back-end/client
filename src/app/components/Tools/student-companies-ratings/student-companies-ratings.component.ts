import {
  Component,
  OnInit
}                         from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {DataService}      from "../../../_services/data.service";
import {UserService}      from "../../../_services/user.service";

@Component({
  selector:    'app-student-companies-ratings',
  templateUrl: './student-companies-ratings.html',
  styleUrls:   ['./student-companies-ratings.scss']
})
export class StudentCompaniesRatings implements OnInit {
  show                   = false;
  companyId;
  modalTemplate: string;
  arrCompanies: string[] = [];
  arrFullCompanies       = [];
  arrCompanyId           = [];
  openModal2             = false

  constructor(public component: ComponentService, public dS: DataService, public uS: UserService) {
  }

  shadowBody() {
    let body = document.getElementsByTagName('body')[0];
    if (this.show)
      body.classList.add('blackFon');
    else
      body.classList.remove('blackFon');
  }


  setComment(e) {

    for (let i = 0; i < this.arrFullCompanies.length; i++) {
      if (e.companyId == this.arrFullCompanies[i]._id) {
        this.arrFullCompanies[i].MY = e


      }
    }
    this.sortCompany()
  }

  changeratingCompany(e) {
    for (let i = 0; i < this.arrFullCompanies.length; i++) {
      if (e.companyId == this.arrFullCompanies[i]._id) {

        this.arrFullCompanies[i].rating = e.rating

      }
    }
  }

  sortCompany() {
    this.uS.setUsers(this.arrFullCompanies)
    for (let i = 0; i < this.arrFullCompanies.length; i++) {
      // this.arrFullCompanies[i] = this.uS.prepareUser(this.arrFullCompanies[i])
      if (!this.arrFullCompanies[i].hasOwnProperty('rating')) {
        this.arrFullCompanies[i].rating = 0
      }
      if (this.arrFullCompanies[i].hasOwnProperty('comments') && this.arrFullCompanies[i].comments.length > 0) {

        for (var j = 0; j < this.arrFullCompanies[i].comments.length; j++) {
          var inArray = false;
          if (this.arrFullCompanies[i]['comments'][j].uid == this.uS.profile._id) {
            //выводим свой отзыв
            inArray = true
            if (!this.arrFullCompanies[i].MY) {
              this.arrFullCompanies[i].MY = this.arrFullCompanies[i]['comments'][j]
            } else {
              this.arrFullCompanies[i]['comments'][j] = this.arrFullCompanies[i].MY
            }

          }


        }
        if (!inArray && this.arrFullCompanies[i].MY) {
          this.arrFullCompanies[i]['comments'].push(this.arrFullCompanies[i].MY)
        }

        if (!(this.arrFullCompanies[i].Company.hasOwnProperty('public') && this.arrFullCompanies[i].Company.public.hasOwnProperty('nameCompany'))) {
          this.arrFullCompanies[i].Company.public.nameCompany = this.arrFullCompanies[i].Company.nameCompany
        }

      }

      this.arrCompanyId.push(this.arrFullCompanies[i]);
    }
  }

  getData(queryString, type) {
    if (queryString.length > 0) {

      this.dS.sendws('Company/searchCompaniesFromName', {
        nameCompany: queryString,
        skip:        0,
        limit:       10,
        type:        type
      }).then((data) => {

        if (type == 0) {
          this.arrCompanies = data['company'];
        } else if (type == 3) {
          this.arrFullCompanies = (data['company']);

          this.sortCompany()


        }

      });


      console.log(this.arrCompanyId);
    }
  }


  ngOnInit() {
  }

}
