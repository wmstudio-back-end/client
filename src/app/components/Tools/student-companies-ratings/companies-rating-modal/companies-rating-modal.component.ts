import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataService} from "../../../../_services/data.service";
import {UserService} from "../../../../_services/user.service";


@Component({
    selector: 'companies-rating-modal',
    templateUrl: './companies-rating-modal.component.html',
    styleUrls: ['./companies-rating-modal.component.scss']
})
export class CompaniesRatingModalComponent implements OnInit {
    @Input() openModal: boolean = false;
    @Input() template: string = 'send';

    @Input() cId: any;
    @Output() ropenModal = new EventEmitter<boolean>();
    @Output() comment = new EventEmitter<any>();
    @Output() changeratingCompany = new EventEmitter<any>();
    rateObj = {};

    constructor(public ds: DataService, public uS: UserService) {
    }

    clearObj() {
        this.rateObj = {
            workedForThisCompany: false,
            comment_recommend: true,
            comment_text: '',
            comment_anonim: false,
            rating_culture: 0,
            rating_work: 0,
            rating_salary: 0,
            rating_managment: 0,
            rating_career: 0

        }
    }

    ngOnChanges(e) {
      if (e.template && e.template.hasOwnProperty('currentValue')) {
        this.clearObj()
        this.rateObj = Object.assign(this.rateObj, this.cId)


      }
        if (e.cId && e.cId.hasOwnProperty('currentValue')) {
            this.clearObj()
            this.rateObj = Object.assign(this.rateObj, this.cId)


        }


    }

    ngOnInit() {
        this.clearObj()
    }

    closeModal() {
        this.ropenModal.emit(false)
        // this.template = 'get'
        this.shadowBodyRemove()

    }


    shadowBodyRemove() {
        let body = document.getElementsByTagName('body')[0];

            body.classList.remove('blackFon');
    }

    createNewRate() {
        this.ds.sendws("Public/newReview", this.rateObj).then((data) => {
            this.comment.emit(this.rateObj)
            this.changeratingCompany.emit(data)

          console.log("===========",this.rateObj)
          //this.uS.users[this.cId].rating = data['rating']
            this.closeModal()
        });


    }


    getReview() {
        this.ds.sendws("Public/getReviews", {companyid: this.cId, uid: true}).then((data) => {
            if (data['res'].length > 0) {
                console.log("neeeeeeee", data);
                //this.rateObj = <Reviews>data['res'][0]
            }

        });


    }


}
