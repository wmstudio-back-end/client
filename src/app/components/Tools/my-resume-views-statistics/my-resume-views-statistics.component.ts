import {Component, OnInit, ViewChild} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
import { GoogleChartComponent } from 'angular-google-charts';
@Component({
  selector: 'app-my-resume-views-statistics',
  templateUrl: './my-resume-views-statistics.component.html',
  styleUrls: ['./my-resume-views-statistics.component.scss']
})


export class MyResumeViewsStatisticsComponent implements OnInit {
  @ViewChild('chart')
  chart: GoogleChartComponent;
  filterSearch = true
  filterViwe = true
  filterBook = true
  resumes = []
  periodId = 7*86400
  resumeId = ""
  period = [
    {
      id:7*86400,
      text:'period_last.week'
    },
    {
      id:30*86400,
      text:'period_last.month'
    },
    {
      id:365*86400,
      text:'period_last.year'
    },
    {
      id:7*86400,
      text:'period_last.lastweek'
    },
    {
      id:30*86400,
      text:'period_last.lastmonth'
    },
    {
      id:90*86400,
      text:'period_last.last3month'
    },
    {
      id:180*86400,
      text:'period_last.last6month'
    },
    {
      id:365*86400,
      text:'period_last.lastyear'
    },
  ]
  constructor(
    public component:ComponentService,
    public dataService:DataService,
    public uS:UserService
  ) { }
  myData = [
    ['London', 8136000],
    ['New York', 8538000],
    ['Paris', 2244000],
    ['Berlin', 3470000],
    ['Kairo', 19500000]

  ];
  ngOnInit() {

    this.getResumes()
  }
  setfilterTime(e){
    this.periodId = e
    console.log(e)
  }
  setfilterResume(e){
    this.resumeId = e
    console.log(e)
  }
  getResumes(){
    var t = [{id:'',text:'All'}]
    for (var i = 0; i < this.uS.profile.Student.resumes.length; i++) {
      t.push({
        id:this.uS.profile.Student.resumes[i]._id,
        text:this.uS.profile.Student.resumes[i].name
      })
    }
    this.resumes = t

  }
  getPeriod(){


    return this.period
  }
}
