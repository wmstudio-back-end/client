import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {DataService} from "../../../_services/data.service";

@Component({
    selector: 'app-organization-statistics.component',
    templateUrl: './organization-statistics.component.html',
    styleUrls: ['./organization-statistics.component.scss']
})
export class OrganizationStatisticsComponent implements OnInit {


    constructor(public component:ComponentService, public dataService:DataService) { }

    ngOnInit() {
    }

}
