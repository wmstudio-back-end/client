import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {OrganizationStatisticsComponent} from "./organization-statistics.component";

describe('OrganizationStatisticsComponent', () => {
    let component: OrganizationStatisticsComponent;
    let fixture: ComponentFixture<OrganizationStatisticsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ OrganizationStatisticsComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(OrganizationStatisticsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});