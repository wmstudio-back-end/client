import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentVacancyListComponent } from './student-vacancy-list.component';

describe('StudentVacancyListComponent', () => {
  let component: StudentVacancyListComponent;
  let fixture: ComponentFixture<StudentVacancyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentVacancyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentVacancyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
