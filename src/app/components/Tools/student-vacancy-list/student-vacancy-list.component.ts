import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ComponentService} from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";
import {DataService} from "../../../_services/data.service";
import {VacancyService} from "../../../_services/vacancy.service";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector: 'app-student-vacancy-list',
  templateUrl: './student-vacancy-list.component.html',
  styleUrls: ['./student-vacancy-list.component.scss']
})
export class StudentVacancyListComponent implements OnInit {
  vacancyIds = []
  vacancy = []
  constructor( public dS:DataService,
               public uS:UserService,
               public component:ComponentService,
               public vS:VacancyService,
               public helpers:HelpersService,
               private route: ActivatedRoute) { }


  delete_count_unread(id){


    for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {
      if (this.uS.profile.savedSearches[i]._id==this.route.snapshot.params['id']){
        this.uS.profile.savedSearches[i].count_total.push(id)
        this.uS.profile.savedSearches[i].count_unread.splice(this.uS.profile.savedSearches[i].count_unread.indexOf(id),1)
        this.uS.profile.savedSearches[i].count_total = this.array_unique(this.uS.profile.savedSearches[i].count_total)

        this.dS.sendws('User/updateSearch',this.uS.profile.savedSearches[i]).then(data=>{


        })
      }

    }




  }
  array_unique(arr) {
    var tmp_arr = new Array();
    for (var i = 0; i < arr.length; i++) {
      if (tmp_arr.indexOf(arr[i]) == -1) {
        tmp_arr.push(arr[i]);
      }
    }
    return tmp_arr;
  }
  ngOnInit() {


    switch (this.route.snapshot.params['type']){


      case '4':
        for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {
          if (this.uS.profile.savedSearches[i]._id==this.route.snapshot.params['id']){

            for (var j = 0; j < this.uS.profile.savedSearches[i].ids.length; j++) {
              this.vacancyIds.push(this.uS.profile.savedSearches[i].ids[j])
            }
            break;
          }

        }
        break;
      case '5':

        for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {
          if (this.uS.profile.savedSearches[i]._id==this.route.snapshot.params['id']){

            for (var j = 0; j < this.uS.profile.savedSearches[i].count_new.length; j++) {
              this.vacancyIds.push(this.uS.profile.savedSearches[i].count_new[j])
              this.uS.profile.savedSearches[i].count_total.push(this.uS.profile.savedSearches[i].count_new[j])
            }

             // this.uS.profile.savedSearches[i].count_total = Object.assign(this.uS.profile.savedSearches[i].count_total,this.uS.profile.savedSearches[i].count_new)
            this.uS.profile.savedSearches[i].count_total = this.array_unique(this.uS.profile.savedSearches[i].count_total)
            this.uS.profile.savedSearches[i].count_new = []
            this.dS.sendws('User/updateSearch',this.uS.profile.savedSearches[i]).then(data=>{


            })
            break;
          }

        }
        break;
      case '6':
        for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {
          if (this.uS.profile.savedSearches[i]._id == this.route.snapshot.params['id']) {
            for (var j = 0; j < this.uS.profile.savedSearches[i].count_unread.length; j++) {
              this.vacancyIds.push(this.uS.profile.savedSearches[i].count_unread[j])
            }
            break;
          }

        }
        break;
      default:
        for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {
          this.vacancyIds.push(this.uS.profile.subscribeCandidate[i].resumeId)
        }
        break
    }

      this.vS.getVacancyFromIds(this.vacancyIds).then(data=>{
        this.vacancy = data['vacancy']
        console.log('===================================',data)
      })




  }


}
