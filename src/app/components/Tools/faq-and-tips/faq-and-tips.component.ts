import {Component, OnInit} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
@Component({
  selector: 'app-faq-and-tips',
  templateUrl: './faq-and-tips.component.html',
  styleUrls: ['./faq-and-tips.component.scss']
})
export class FAQAndTipsComponent implements OnInit {
  constructor(public component:ComponentService) {
  }

  ngOnInit() {
  }

}
