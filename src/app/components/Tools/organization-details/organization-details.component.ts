import {Component, OnInit} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
@Component({
    selector: 'app-organization-details',
    templateUrl: './organization-details.component.html',
    styleUrls: ['./organization-details.component.scss']
})
export class OrganizationDetailsComponent implements OnInit {
    constructor(public component: ComponentService) {
    }

    ngOnInit() {
    }

}
