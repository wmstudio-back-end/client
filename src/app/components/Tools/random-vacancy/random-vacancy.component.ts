import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";

@Component({
  selector: 'app-random-vacancy',
  templateUrl: './random-vacancy.component.html',
  styleUrls: ['./random-vacancy.component.scss']
})
export class RandomVacancyComponent implements OnInit {
  vacancy = []
  constructor(public dS: DataService,public uS:UserService) { }

  ngOnInit() {
    this.dS.sendws('Vacancy/getRandomVacancies',{limit:2}).then(data=>{
      var ids = []
      for (var i = 0; i < data['res'].length; i++) {
        ids.push(data['res'][i].uid)

      }
      this.uS.getUsers(ids).then(users=>{
        for(let i = 0; i < data['res'].length; i++){
          let rating = 0;
          let nameCompany = '';
          let user = this.uS.users[data['res'][i]['uid']]
          if (user) {
            if (user.hasOwnProperty('Company'))
              nameCompany = user.Company.nameCompany
            if (user.hasOwnProperty('rating'))
              rating = user.rating
          }
          data['res'][i]['rating'] = rating
          data['res'][i]['nameCompany'] = nameCompany
        }
        this.vacancy = data['res']
          console.log(this.vacancy)
      })

    })
  }

}
