import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";
import {DataService} from "../../../_services/data.service";
import {User} from "../../../_models/user";
import {MessageService} from "../../../_services/message.service";
import {Router} from "@angular/router";
import {DatePipe} from "@angular/common";
import {HelpersService} from "../../../_services/helpers.service";
interface UsersRecomm {
  uid:string
  letter:string
  iniciator:string
  created_at:string
  type:number
  status:number
  _id:string
  schoolId:string
  schoolName:string
  viwe:boolean
  comment_text:string
  req:number
}
@Component({
    selector: 'app-educator-recommendations',
    templateUrl: './educator-recommendations.html',
    styleUrls: ['./educator-recommendations.scss']
})
export class EducatorRecommendationsComponent implements OnInit {
  openModal = false
  req = []
  usersRecomm:UsersRecomm[] = []
  f=-1
  cId
  sId
  activeRec
  template = 'set'
  neb:number=0
  filText = ''
    constructor(
      public component:ComponentService,
      public uS:UserService,
      public dS:DataService,
      public mS:MessageService,
      public router:Router,
      private helpers:HelpersService
    ) {
      dS.messages["EVENT/newReqRecommendation"].subscribe(data => {
       this.ngOnInit()
      })


    }
  getSchoolID(uid){
    if (this.uS.users[uid]&&this.uS.users[uid].Student.schools){

      for (var i = 0; i < this.uS.users[uid].Student.schools.length; i++) {
        for (var j = 0; j < this.uS.profile.Educator.schools.length; j++) {
          if (this.uS.users[uid].Student.schools[i].schoolId==this.uS.profile.Educator.schools[j]._id){
            return this.uS.profile.Educator.schools[j]._id
          }

        }

      }
    }
  }
  updateRec(e){
    var added = true
    for (var i = 0; i < this.uS.profile.recommendations.length; i++) {
      if (this.uS.profile.recommendations[i]._id==e._id){
        console.log('-------------------------------------')
        this.uS.profile.recommendations[i] = e
        added = false
      }
    }
    if (added){
      this.uS.profile.recommendations.push(e)
    }
  }
  setFilter(s){
    this.f=s;
    this.req = this.filter();
  }
  getSchoolName(schoolId){

        for (var j = 0; j < this.uS.profile.Educator.schools.length; j++) {
          if (schoolId==this.uS.profile.Educator.schools[j]._id){
            return this.uS.profile.Educator.schools[j].addr
          }

        }

  }
  Decline(_id,i){
    if (this.helpers.confirm(0)){
      this.dS.sendws('Recommendation/decline',{_id:_id}).then(data=>{
        this.usersRecomm[i].status = 2
        for (var j = 0; j < this.uS.profile.recommendations.length; j++) {
          if (this.uS.profile.recommendations[j]._id==this.usersRecomm[i]._id){
            this.uS.profile.recommendations[j].status=this.usersRecomm[i].status
          }

        }
      })
    }

  }
  Revoke(_id,i){
    if (this.helpers.confirm(1)){
      this.dS.sendws('Recommendation/revoke',{_id:_id}).then(data=>{
        this.usersRecomm[i].status = 0
        for (var j = 0; j < this.uS.profile.recommendations.length; j++) {
         if (this.uS.profile.recommendations[j]._id==this.usersRecomm[i]._id){
           this.uS.profile.recommendations[j].status=this.usersRecomm[i].status
         }

        }
      })
    }

  }
  filter(){
    console.log('filter')
    var req = []
    this.neb = 0
    for (var i = 0; i < this.usersRecomm.length; i++) {
      this.usersRecomm[i].status==0?this.neb++:null;
      var t = false
      if (this.uS.users[this.usersRecomm[i].uid]){
        t=this.uS.getUserName(this.usersRecomm[i].uid,1).toLowerCase().indexOf(this.filText.toLowerCase())>-1
      }
      (this.usersRecomm[i].status==this.f||this.f<0)&&t?req.push(this.usersRecomm[i]):null
    }
    return req
  }
    ngOnInit() {
      this.neb = 0
      this.usersRecomm = []
      var ids = []
      let date = new DatePipe('en-US');
      for (var i = 0; i < this.uS.profile.recommendations.length; i++) {
        if (this.uS.profile.recommendations[i].uuid==this.uS.profile._id){
          console.log('000000000000000000000',this.uS.profile.recommendations[i])
          continue
        }
        console.log('=============')
        ids.push(this.uS.profile.recommendations[i].uuid)
        this.uS.profile.recommendations[i].status==0?this.neb++:null
        this.usersRecomm.push(<UsersRecomm>{
          uid:this.uS.profile.recommendations[i].uuid,
          created_at: date.transform(this.uS.profile.recommendations[i].created_at * 1000, 'dd.MM.yyyy HH:mm'),
          letter:this.uS.profile.recommendations[i].letter,
          type:this.uS.profile.recommendations[i].type,
          schoolName: this.getSchoolName(this.uS.profile.recommendations[i].schoolId),
          status:this.uS.profile.recommendations[i].status,
          schoolId:this.uS.profile.recommendations[i].schoolId,
          _id:this.uS.profile.recommendations[i]._id,
          comment_text:this.uS.profile.recommendations[i].comment_text,
          viwe:false,
          req:this.uS.profile.recommendations[i].req
        })
      }
      this.uS.getUsers(ids).then(us=>{
        this.dS.sendws('Schools/getSchoolUDFromUids',{ids:ids}).then(data=>{
          var sh = {}
          for (var i = 0; i < data['result'].length; i++) {
            this.uS.users[data['result'][i].uid].Student.schools = []
            if (!this.uS.users[data['result'][i].uid].Student.schools){this.uS.users[data['result'][i].uid].Student.schools = []}
            this.uS.users[data['result'][i].uid].Student.schools.push(data['result'][i])

          }
          this.f = -1
          this.req = this.filter()
          this.setFilter(-1)
        })
      })




    }

  SetRec(s){

    for (var i = 0; i < this.usersRecomm.length; i++) {
      if (this.usersRecomm[i].uid==s){
        this.usersRecomm[i].status=1
        break
      }

    }
    for (var j = 0; j < this.uS.profile.recommendations.length; j++) {
      if (this.uS.profile.recommendations[j]._id==this.usersRecomm[i]._id){
        this.uS.profile.recommendations[j].status=this.usersRecomm[i].status
      }

    }

  }

}
