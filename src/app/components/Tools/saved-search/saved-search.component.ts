import {
  Component,
  OnInit
}                         from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {DataService}      from "../../../_services/data.service";
import {UserService}      from "../../../_services/user.service";
import {TranslateService} from "@ngx-translate/core";
import {HelpersService}   from "../../../_services/helpers.service";
import {DownPopupService} from "../../../_services/down-popup.service";

@Component({
  selector:    'saved-search',
  templateUrl: './saved-search.html',
  styleUrls:   ['./saved-search.scss']
})
export class SavedSearchComponent implements OnInit {

  optionsVal       = []
  CompanyProjects  = []
  CompanyProjectsF = []
  sortname         = false
  sortdate         = false
  mvFilter         = false
  sirttype         = -1
  sortingArr       = []

  constructor(
    public dataService: DataService,
    public component: ComponentService,
    public uS: UserService,
    public help: HelpersService,
    public popS: DownPopupService,
    public translate: TranslateService
  ) {
    for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {
      !this.uS.profile.savedSearches[i].count_total ? this.uS.profile.savedSearches[i].count_total = [] : null;
      !this.uS.profile.savedSearches[i].count_unread ? this.uS.profile.savedSearches[i].count_unread = [] : null;
      !this.uS.profile.savedSearches[i].count_new ? this.uS.profile.savedSearches[i].count_new = [] : null;
      //var ob = Object.keys(this.uS.profile.savedSearches[i].options)
      //var opt = {
      //  name:'asd',
      //  val:"asd"
      //}
      //for (var j = 0; j < ob.length; j++) {
      //  if (this.dataService.options.hasOwnProperty(ob[j])){
      //    console.log(ob[j])
      //    opt.name = ob[j]
      //    opt.val = ob[j]
      //  }
      //}
      this.optionsVal[i] = {
        name:   "ASD",
        values: "sssssssss"
      }
    }
  }

  getFilterSave() {
    var t = []
    this.sorting();
    t = this.uS.profile.savedSearches

    return t
  }

  sorting() {
    if (this.sortname) {
      this.uS.profile.savedSearches.sort(function (a, b) {
        a.name < b.name
      })
    }
    if (this.sortdate) {
      this.uS.profile.savedSearches.sort(function (a, b) {
        a.updated_at - b.updated_at
      })
    }

  }

  ngOnInit() {

    if (this.uS.profile.type_profile == 'Company') {
      this.CompanyProjectsF.push({
        id:   -1,
        text: 'all_projects'
      })
      for (var i = 0; i < this.uS.profile.Company.projects.length; i++) {
        this.CompanyProjects.push({
          id:   this.uS.profile.Company.projects[i]._id,
          text: this.uS.profile.Company.projects[i].name
        })
        this.CompanyProjectsF.push({
          id:   this.uS.profile.Company.projects[i]._id,
          text: this.uS.profile.Company.projects[i].name
        })
      }
    }
    this.filter()

  }

  arr_diff(a1, a2) {

    var a    = [],
        diff = [];

    for (var i = 0; i < a1.length; i++) {
      a[a1[i]] = true;
    }

    for (var i = 0; i < a2.length; i++) {
      if (a[a2[i]]) {
        delete a[a2[i]];
      } else {
        a[a2[i]] = true;
      }
    }

    for (var k in a) {
      diff.push(k);
    }

    return diff;
  }

  updateSS(q, i) {
    if (this.uS.profile.type_profile == 'Company' && !this.uS.accessPackage.search) {
      this.popS.show_success(this.dataService.getOptionsFromId('errors', 1).text)
      return
    }
    if (this.uS.profile.type_profile == 'Company' && this.uS.accessRight == 3) {
      this.popS.show_info(this.dataService.getOptionsFromId('errors', 28).text)
      return
    }
    var m     = this.uS.profile.type_profile == 'Company' ? 'Resume/getCountResume' : 'Vacancy/getCountVacancy'
    var query = {}
    if (typeof q.query == 'string') {
      query = JSON.parse(q.query)
    } else {
      query = q.query
    }
    this.dataService.sendws(m, {
      query: query,
      value: '_id'
    }).then(data => {
      var ids = []
      for (var j = 0; j < data['count'].length; j++) {
        ids.push(data['count'][j])

      }

      // var count_new = this.uS.profile.savedSearches[i].ids.indexOf(ids)
      var count_new    = ids
      var count_unread = ids
      var count_total  = ids
      // if (this.uS.profile.savedSearches[i].ids){
      //   count_new = ids.filter(x => this.uS.profile.savedSearches[i].ids.indexOf(x) == -1);
      // }

      console.log("IDS", ids)
      // console.log("count_total", this.uS.profile.savedSearches[i].count_total)
      // console.log("count_unread", this.uS.profile.savedSearches[i].count_unread)
      // console.log("count_new", this.uS.profile.savedSearches[i].count_new)
      for (var s = 0;s<this.uS.profile.savedSearches[i].count_total.length;s++){

      }
      for (var g = 0;g<ids.length;g++){
        if (this.uS.profile.savedSearches[i].count_total.indexOf(ids[g])==-1){
          console.log('!!!!!!!!!!!!!!!!!!!!!',ids[g])
          this.uS.profile.savedSearches[i].count_new.push(ids[g])
          this.uS.profile.savedSearches[i].ids.push(ids[g])
          this.uS.profile.savedSearches[i].count_unread.push(ids[g])
        }else{

        }
      }
      q.count_new = this.uS.profile.savedSearches[i].count_new = this.array_unique(this.uS.profile.savedSearches[i].count_new)
      q.ids = this.uS.profile.savedSearches[i].ids = this.array_unique(this.uS.profile.savedSearches[i].ids)
      q.count_total = this.uS.profile.savedSearches[i].count_total = this.array_unique(this.uS.profile.savedSearches[i].count_total)
      q.count_unread = this.uS.profile.savedSearches[i].count_unread = this.array_unique(this.uS.profile.savedSearches[i].count_unread)

      // q.count_new = this.uS.profile.savedSearches[i].count_new = this.array_unique(Object.assign(this.uS.profile.savedSearches[i].count_new, ids.filter(x => !this.uS.profile.savedSearches[i].count_new.includes(x))));
      // q.ids = this.uS.profile.savedSearches[i].ids = this.array_unique(Object.assign(this.uS.profile.savedSearches[i].ids, ids))
      // q.count_total = this.uS.profile.savedSearches[i].count_total = this.array_unique(Object.assign(this.uS.profile.savedSearches[i].count_total, ids))

      // q.count_new = this.uS.profile.savedSearches[i].count_new  = this.array_unique(this.uS.profile.savedSearches[i].count_total.filter(x => !this.uS.profile.savedSearches[i].count_new .includes(x))
      //   .concat(this.uS.profile.savedSearches[i].count_new .filter(x => !this.uS.profile.savedSearches[i].count_total.includes(x))))


      // q.count_unread = this.uS.profile.savedSearches[i].count_unread = ids
      //
       console.log("2count_total", this.uS.profile.savedSearches[i].count_total)
      // console.log("2count_unread", this.uS.profile.savedSearches[i].count_unread)
      // console.log("2count_new", this.uS.profile.savedSearches[i].count_new)
      this.dataService.sendws('User/updateSearch', q).then(data => {
        this.uS.profile.savedSearches[i] = data['res']
        this.ngOnInit()
      })
    })

  }

  array_unique(arr) {
    var tmp_arr = new Array();
    for (var i = 0; i < arr.length; i++) {
      if (tmp_arr.indexOf(arr[i]) == -1) {
        tmp_arr.push(arr[i]);
      }
    }
    return tmp_arr;
  }

  dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property  = property.substr(1);
    }
    return function (a, b) {
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    }
  }

  filter() {
    var t = []

    this.uS.profile.savedSearches.sort(this.dynamicSort((this.sortname ? '-' : '') + 'name'))
    for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {

      if (this.uS.profile.savedSearches[i].project == this.sirttype) {
        t.push(this.uS.profile.savedSearches[i])
      } else if (this.sirttype == -1) {
        t.push(this.uS.profile.savedSearches[i])
      }


    }

    if (this.sortdate) {
      t.sort((a, b) => {
        return b.created_at - a.created_at
      })
    }
    if (this.sortname) {
      t.sort((a, b) => {
        var x = a.name.toLowerCase();
        var y = b.name.toLowerCase();
        return x > y ? -1 : x > y ? 1 : 0;
      })
    }
    console.log("=======11======",t)
    t.map((e) => {
      e.optText = this.Keys(e.options)
      return e;
    })
    console.log("=======22======",t)
    this.sortingArr = t

  }

  deleteSS(_id, i) {
    if (this.uS.profile.type_profile == 'Company' && !this.uS.accessPackage.search) {
      this.popS.show_success(this.dataService.getOptionsFromId('errors', 1).text)
      return
    }
    if (this.uS.profile.type_profile == 'Company' && this.uS.accessRight == 3) {
      this.popS.show_info(this.dataService.getOptionsFromId('errors', 28).text)
      return
    }
    this.dataService.sendws('User/deleteSearch', {_id: _id}).then(data => {
      this.uS.profile.savedSearches.splice(i, 1)
      this.ngOnInit()
    })

  }

  updateSearch(item) {
    if (this.uS.profile.type_profile == 'Company' && !this.uS.accessPackage.search) {
      this.popS.show_success(this.dataService.getOptionsFromId('errors', 1).text)
      return
    }
    if (this.uS.profile.type_profile == 'Company' && this.uS.accessRight == 3) {
      this.popS.show_info(this.dataService.getOptionsFromId('errors', 28).text)
      return
    }
    this.dataService.sendws('User/updateSearch', item).then(data => {

    })
  }

  updateProject(i, id) {
    if (this.uS.profile.type_profile == 'Company' && !this.uS.accessPackage.search) {
      this.popS.show_success(this.dataService.getOptionsFromId('errors', 1).text)
      return
    }
    if (this.uS.profile.type_profile == 'Company' && this.uS.accessRight == 3) {
      this.popS.show_info(this.dataService.getOptionsFromId('errors', 28).text)
      return
    }
    this.dataService.sendws('User/reProjectSearch', {
      _id:     i,
      project: id
    }).then(data => {
      console.log(i, id)
    })

  }

  getOptionVal(ob, name) {

    var str
    switch (typeof ob) {
      case 'string':
        str = ob
        break;
      case 'number':

        if (!this.dataService.getOptionsFromId(name, ob).error) {
          str = this.dataService.getOptionsFromId(name, ob).text

        } else {
          str = ob
        }

        break;
      case 'object':
        str = ''
        console.log("Type " + name + "   OF: " + typeof ob, Object.keys(ob))
        for (var i = 0; i < Object.keys(ob).length; i++) {
          if (!this.dataService.getOptionsFromId(name, Object.keys(ob)[i]).error) {
            //str += this.translate.get(this.dataService.getOptionsFromId(name,Object.keys(ob)[i]).text)['value']
            str += this.dataService.getOptionsFromId(name, Object.keys(ob)[i]).text
          }

        }
        for (var key in ob) {


        }

        //if (!this.dataService.getOptionsFromId(name,ob).error){
        //
        //  str = this.dataService.getOptionsFromId(name,ob).text
        //
        //}
        break;
    }
    return str
  }

  Keys(options) {
    var str: string = ''
    var req         = []
    var strl_salary = ''
    var sal_1       = false
    var sal_2       = false

    for (var key in options) {


      if (key == 'olderThan') {
        continue
      }
      //var name = this.help.getTranslate('savSea.'+key);
      var name = 'savSea.' + key;

      if (key == 'salary' || key == 'minSalary') {
        if (key == 'minSalary') {
          options[key] > 0 ? sal_1 = true : null
          strl_salary = options[key] + ' ' + strl_salary
        }
        if (key == 'salary') {
          sal_2 = true
          str   = ''
          for (var k in options[key]) {
            if (!this.dataService.getOptionsFromId(key, k).error) {
              strl_salary = strl_salary + ' ' + this.translate.get(this.dataService.getOptionsFromId(key, k).text)['value']
            }
          }
        }
        if (sal_1 && sal_2) {
          req.push({
            name: name,
            text: strl_salary
          })
        }

        continue;
      }
      switch (typeof options[key]) {
        case 'string':
          if (options[key] != '') {

            req.push({
              name: name,
              text: options[key]
            })
          }
          break;
        case 'number':
          options[key] > 0 ? req.push({
            name: name,
            text: options[key]
          }) : null
          break;
        case 'object':
          let values = []
          let keyOption = ''
          keyOption = key=='educationLevel'?'educationLevelCurrent':key
          keyOption = key=='languagesSkills'?'languages':key


          if (keyOption=='city'){
            values.push(options[key].addr)
          }
          if (keyOption=='expectedGraduationMonth'&&Object.keys(options[key]).length>0){
            // values.push(options[key].addr) expectedGraduationYear
            values.push(Object.keys(options[key])[0])
          }
          if (keyOption=='expectedGraduationYear'&&Object.keys(options[key]).length>0){

            values.push(Object.keys(options[key])[0])
          }
          for (var k in options[key]) {

            if (!this.dataService.getOptionsFromId(keyOption, k).error) {
              if (options[key][k]) {
                values.push(this.dataService.getOptionsFromId(keyOption, k).text)
              }
            }
          }

          if (values.length) {
            req.push({
              name:   name,
              values: values
            })
          }
          break;
      }
    }
    return req
  }

  getOptions(item, name) {
    //var type = typeof item
    //var str:string = ''
    //switch (type){
    //  case 'object':
    //    console.log(item)
    //    for (var key in item) {
    //      this.dataService.getOptionsFromId('vacancyType',key).error
    //        ?null
    //        :str+=', '+this.dataService.getOptionsFromId('vacancyType',key).text
    //
    //    }
    //
    //    break;
    //  default:
    //
    //}
    //return str.substring(2,str.length)
    //var ob = Object.keys(item)
    //for (var i = 0; i < ob.length; i++) {
    //  if (this.dataService.options.hasOwnProperty(ob[i])){
    //    switch (typeof ob[i]){
    //      //case 'string':
    //      //  options.push({
    //      //
    //      //  })
    //      //  break;
    //      //case 'number':
    //      //
    //      //  if (!this.dataService.getOptionsFromId(name,ob).error){
    //      //    str = this.dataService.getOptionsFromId(name,ob).text
    //      //
    //      //  }else{
    //      //    str =  ob
    //      //  }
    //      //
    //      //  break;
    //      //case 'object':
    //      //  str = ''
    //      //  console.log("Type "+name+"   OF: "+typeof ob,Object.keys(ob))
    //      //  for (var i = 0; i < Object.keys(ob).length; i++) {
    //      //    if (!this.dataService.getOptionsFromId(name,Object.keys(ob)[i]).error){
    //      //      str += this.dataService.getOptionsFromId(name,Object.keys(ob)[i]).text
    //      //    }
    //      //
    //      //  }
    //      //  for (var key in ob) {
    //      //
    //      //
    //      //
    //      //  }
    //      //
    //      //  //if (!this.dataService.getOptionsFromId(name,ob).error){
    //      //  //
    //      //  //  str = this.dataService.getOptionsFromId(name,ob).text
    //      //  //
    //      //  //}
    //      //  break;
    //    }
    //  }
    //
    //
    //}
  }


}
