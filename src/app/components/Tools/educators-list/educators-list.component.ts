import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";
import {DataService} from "../../../_services/data.service";
import {MessageService} from "../../../_services/message.service";
import {Router} from "@angular/router";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
    selector: 'app-educators-list.component',
    templateUrl: './educators-list.component.html',
    styleUrls: ['./educators-list.component.scss']
})
export class EducatorsListComponent implements OnInit {

    boxSide:boolean = false;
    users:any = []
    usersCopy:any = []
    countSearch = this.usersCopy.length
    inviteEmail:string = ''
  surname:string = ''
  employed:boolean = false
  not_employed:boolean = false
  unknow:boolean = false
    constructor(
      public component:ComponentService,
      public uS:UserService,
      public dS:DataService,
      public mS:MessageService,
      public helpers:HelpersService,
      public router: Router,
    ) { }

    ngOnInit() {

      var ids = []
      if (!this.uS.profile.Organization.schools_edu){
        this.uS.profile.Organization.schools_edu = []
      }
        for (var i = 0; i < this.uS.profile.Organization.schools_edu.length; i++) {
          ids.push(this.uS.profile.Organization.schools_edu[i].uid)

        }


      this.uS.getUsers(ids).then((data)=>{
        //this.users = Object.assign(this.users,data)
        //console.log("data",data)
        for (var i = 0; i < this.uS.profile.Organization.schools_edu.length; i++) {
          var set = false
          this.users[i] = {
            school : this.uS.profile.Organization.schools_edu[i],
            edit : false
          }
          for (var k in data) {
            if (this.uS.profile.Organization.schools_edu[i].uid==data[k]._id){
              Object.assign(this.users[i],data[k])
            }
          }
        }
        this.usersCopy = this.users
        //this.ready = true
      })

    }

  search_name(name){
  if (name.indexOf(this.surname)>-1){
    return true
  }
}
  clearSearch(){
    this.surname = ''
    this.employed = false
    this.not_employed = false
    this.unknow = false
    this.filterEdu(false)
  }
  filterEdu(init) {
    var itemList = Object.assign([],this.users)
      var newList = []
    for (var i = 0; i < itemList.length; i++) {
        var add = false
      if (itemList[i].school.status==0&&this.unknow){
        this.surname!=''?add = this.search_name(itemList[i].first_name+' '+itemList[i].last_name):add=true
      }
      if (itemList[i].school.status==1&&this.not_employed){
        this.surname!=''?add = this.search_name(itemList[i].first_name+' '+itemList[i].last_name):add=true
      }
      if (itemList[i].school.status==2&&this.employed){
        this.surname!=''?add = this.search_name(itemList[i].first_name+' '+itemList[i].last_name):add=true
      }
      if (!this.unknow&&!this.not_employed&&!this.employed){
        this.surname!=''?add = this.search_name(itemList[i].first_name+' '+itemList[i].last_name):add=true
      }
      if (add){
        newList.push(itemList[i])
      }

    }
  this.countSearch = newList.length
  if (init){
    this.usersCopy = newList
  }

  }
  deleteInvite(id,i){
    if (this.helpers.confirm(0)){

        this.dS.sendws('Schools/deleteInviteEdu',{_id:id}).then((data)=>{
          console.log("------------",data)
          this.users.splice(i,1)
        })
      }

  }
  seveStatus(user,i){
    if (user.school.hasOwnProperty('statusT')){
      //this.uS.users[user._id]
      this.uS.profile.Organization.schools_edu[i].status = user.school.statusT
      this.uS.profile.Organization.schools_edu[i].edit = false
      this.dS.sendws('Schools/proofEduStatus',{status:user.school.statusT,_id:user.school._id}).then((data)=>{
        console.log("------------",data)
      })
    }

  }
  inviteSchoolEdu(){
    this.dS.sendws('Schools/inviteSchoolEdu',{inviteEmail:this.inviteEmail,school:this.uS.profile.Organization._id}).then((data)=>{
      this.users.push({school:data['result']})
      this.inviteEmail = ''
      console.log("------------",data)
    })
  }
  setStatus(user,o){

      switch (o){
        case 0:
          user.school.statusT = 2
          break;
        case 1:
          user.school.statusT = 1
          break;
      }
      console.log(user)
    console.log(o)
  }

}
