import {Component, OnInit} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";
import {DataService} from "../../../_services/data.service";
import {ShopService} from "../../../_services/shop.service";
@Component({
    selector: 'app-invoices',
    templateUrl: './invoices.component.html',
    styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit {
  p: number = 1;
  collection: any[]
  pageChange:any
    constructor(
      public component: ComponentService,
      public uS:UserService,
      public dS:DataService,
      public shopService:ShopService
    ) {
    }

    ngOnInit() {
      console.log(this.uS.profile.service.orders)
    }

}
