import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {CoverLetterService} from "../../../_services/cover-letter.service";
import {UserService} from "../../../_services/user.service";
import {DataService} from "../../../_services/data.service";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
    selector:   'app-cover-letters',
    templateUrl:'./cover-letters.html',
    styleUrls:  ['./cover-letters.scss']
})
export class CoverLettersComponent implements OnInit {

    coverModal:boolean[] = []



    constructor(
      public component:ComponentService,
      public Cl:CoverLetterService,
      public uS:UserService,
      public dS:DataService,
      private helpers:HelpersService
    ) { }


    ngOnInit() {
      for(var i = 0;i<this.uS.profile.CoverLetter.length;i++){
        this.coverModal[i] = false
      }
    }
  setDef(_id,i){

      this.dS.sendws('CoverLetter/CoverLetterChdefLng',{_id:_id,val:i}).then((data)=>{

      })


  }
  deleteCoverLetter(_id,i){
    if (this.helpers.confirm(0)){
        this.dS.sendws('CoverLetter/deleteCoverLetter',{_id:_id}).then((data)=>{
          this.uS.profile.CoverLetter.splice(i,1)
        })
      }

  }

}
