import {Component, OnInit} from '@angular/core';
import {ComponentService} from "../../../_services/component.service";


@Component({
    selector: 'app-organization-profile',
    templateUrl: './organization-profile.html',
    styleUrls: ['./organization-profile.scss']
})
export class OrganizationProfileComponent implements OnInit {


    inputStyle: string

    constructor(public component: ComponentService) {
    }

    ngOnInit() {
    }

}
