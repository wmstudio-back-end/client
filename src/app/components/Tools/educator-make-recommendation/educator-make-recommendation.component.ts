import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";
import {HelpersService} from "../../../_services/helpers.service";
import {DataService} from "../../../_services/data.service";
import {DownPopupService} from "../../../_services/down-popup.service";
export interface iSearchUser {
  surname : string
  exGrad : string
  schoolId : string[]
  educat : any
  curst : boolean
  grad : boolean
}
@Component({
    selector: 'app-educator-make-recommendation',
    templateUrl: './educator-make-recommendation.html',
    styleUrls: ['./educator-make-recommendation.scss']
})

export class EducatorMakeRecommendationComponent implements OnInit {
  searchUser =  <iSearchUser>{}
  openModal = false
  activeRec
  template = 'send'
  errorForm = true
  uuid
  schoolIds = []
  searchIds = []
  userShow  = []
  userShData  = {}
  years = []
  clear = false
    cId
  sId
  count = 0
  schName = ''
    constructor(
      public component:ComponentService,
      public uS:UserService,
      public hS:HelpersService,
      public dS:DataService,
      public popUp: DownPopupService,
    ) { }

    ngOnInit() {

      this.clearSearch()
      this.years  = this.hS.getYears(0,8,true)
      console.log('=====================',this.years)

    }
  getSchoolID(uid){
    if (this.uS.users[uid]&&this.uS.users[uid].Student.schools){

      for (var i = 0; i < this.uS.users[uid].Student.schools.length; i++) {
        for (var j = 0; j < this.uS.profile.Educator.schools.length; j++) {
          if (this.uS.users[uid].Student.schools[i].schoolId==this.uS.profile.Educator.schools[j]._id){

            return this.uS.profile.Educator.schools[j]._id
          }

        }

      }
    }
  }
  getSchoolName(uid){
    if (this.uS.users[uid]&&this.uS.users[uid].Student.schools){
    var names = []
      for (var i = 0; i < this.uS.users[uid].Student.schools.length; i++) {
        for (var j = 0; j < this.uS.profile.Educator.schools.length; j++) {
          if (this.uS.users[uid].Student.schools[i].schoolId==this.uS.profile.Educator.schools[j]._id){
            names.push(this.uS.profile.Educator.schools[j].addr)
          }

        }

      }
    }
    return names


  }
  setYear(){
    return new Date().getFullYear()
  }
  Revoke(uid){
   var ID
    for (var i = 0; i < this.uS.profile.recommendations.length; i++) {
      if (this.uS.profile.recommendations[i].uuid==uid){
        ID = this.uS.profile.recommendations[i]._id
        break;
      }
    }

      if (this.hS.confirm(1)&&ID){

        this.uS.profile.recommendations[i].status = 0
        this.dS.sendws('Recommendation/revoke',{_id:ID}).then(data=>{
          //this.usersRecomm[i].status = 0
        })
      }


  }
  getReccomendation(uid){
    for (var i = 0; i < this.uS.profile.recommendations.length; i++) {
      if (this.uS.profile.recommendations[i].uuid==uid){
        return this.uS.profile.recommendations[i]
      }
    }
    return null
  }
  isRec(uid){
    for (var i = 0; i < this.uS.profile.recommendations.length; i++) {
      if (this.uS.profile.recommendations[i].uuid==uid){
        return this.uS.profile.recommendations[i].status
      }
    }
    return 0
  }
  countSearch(){

    if (!this.searchUser.surname||!this.searchUser.schoolId){
      this.errorForm = false
      return
    }

    if (this.searchUser.schoolId&&this.searchUser.surname){
      this.dS.sendws('User/searchUserEdu',this.searchUser).then((data)=>{
        this.count = data['users'].length
        var ids = []
        for (var i = 0; i < data['users'].length; i++) {
          ids.push(data['users'][i])
        }
        for (var i = 0; i < data['ud'].length; i++) {
          this.userShData[data['ud'][i].uid] = data['ud'][i]

        }
        this.searchIds = ids

      })
    }

  }
  updateRec(e){
    console.log('============',e)
    var added = true
    for (var i = 0; i < this.uS.profile.recommendations.length; i++) {
      if (this.uS.profile.recommendations[i]._id==e._id){
        console.log('-------------------------------------')
        this.uS.profile.recommendations[i] = e
        added = false
      }
    }
    if (added){
      this.uS.profile.recommendations.push(e)
    }
  }
  clearSearch(){
    this.clear = true
    setTimeout(function(){this.clear = false}.bind(this),11)
    this.searchUser =  <iSearchUser>{
      schoolId:[]
    }
    for (var i = 0; i < this.uS.profile.Educator.schools.length; i++) {
      this.searchUser.schoolId.push(this.uS.profile.Educator.schools[i].schoolId)
      this.schoolIds.push({
        id:this.uS.profile.Educator.schools[i].schoolId,
        text:this.uS.profile.Educator.schools[i].addr
      })

    }
  }
  showSearch(){
    if (!this.searchUser.surname||!this.searchUser.schoolId){
      this.errorForm = false
      this.popUp.show_success(this.dS.getOptionsFromId('errors', 17).text);
      return
    }

    this.userShow = []
    this.uS.getUsers(this.searchIds).then((data)=>{
      this.dS.sendws('Schools/getSchoolUDFromUids',{ids:this.searchIds}).then(data=>{
        var sh = {}
        for (var i = 0; i < data['result'].length; i++) {
          this.uS.users[data['result'][i].uid].Student.schools = []
        }
        for (var i = 0; i < data['result'].length; i++) {

            this.uS.users[data['result'][i].uid].Student.schools.push(data['result'][i])



        }
      })
      for (var key in data) {
        if (data.hasOwnProperty(key)) {
          this.userShow.push(data[key])
        }
      }


    })
  }


}
