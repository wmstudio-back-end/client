import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {HiddenVacancyComponent} from "./hidden-vacancy.component";

describe('HiddenVacancyComponent', () => {
    let component: HiddenVacancyComponent;
    let fixture: ComponentFixture<HiddenVacancyComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ HiddenVacancyComponent ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(HiddenVacancyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should be created', () => {
        expect(component).toBeTruthy();
    });
});