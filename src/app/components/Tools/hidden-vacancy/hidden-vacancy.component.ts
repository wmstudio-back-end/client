import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";

@Component({
    selector: 'app-hidden-vacancy.component',
    templateUrl: './hidden-vacancy.component.html',
    styleUrls: ['./hidden-vacancy.component.scss']
})
export class HiddenVacancyComponent implements OnInit {

    constructor(public component:ComponentService) { }

    ngOnInit() {
    }

}
