import {
  Component,
  OnInit
}                          from '@angular/core';
import {ComponentService}  from "../../../_services/component.service";
import {
  ActivatedRoute,
  Router
}                          from "@angular/router";
import {MessageService}    from "../../../_services/message.service";
import {DataService}       from "../../../_services/data.service";
import {ResumeService}     from "../../../_services/resume.service";
import {UserService}       from "../../../_services/user.service";
import {HelpersService}    from "../../../_services/helpers.service";
import {DomSanitizer}      from "@angular/platform-browser";
import {User}              from "app/_models";
import {EmbedVideoService} from "../../../_services";

@Component({
  selector:    'app-company-details',
  templateUrl: './company-details.html',
  styleUrls:   ['./company-details.scss']
})
export class CompanyDetailsComponent implements OnInit {
  public company             = <User>{};
         frameVideoUrl
         openVac             = []
         initv               = false
         iframe_html: any;
         companyVacancyCount = 0

  constructor(public component: ComponentService,
              public router: Router,
              public route: ActivatedRoute,
              public mS: MessageService,
              public dS: DataService,
              public rS: ResumeService,
              public embedService: EmbedVideoService,
              public uS: UserService,
              public sanitizer: DomSanitizer,
              public hS: HelpersService) {
  }

  ngOnInit() {
    this.dS.sendws('Vacancy/getRandomVacancies', {
      limit: 3,
      uid:   this.route.snapshot.params['id']
    }).then(data => {
      this.openVac = data['res']

    })
    this.uS.getUsers([this.route.snapshot.params['id']]).then(data => {
      this.company = this.uS.users[this.route.snapshot.params['id']]

      this.initv = true

      if (this.company && this.company.Company.videoUrl != '') {
        try {
          this.frameVideoUrl = this.company.Company.videoUrl
          if (this.company.Company.videoUrl.indexOf('youtube') > -1){
            let videoId = this.hS.getParameterByName('v',this.company.Company.videoUrl)
            if (videoId)
              this.company.Company.videoUrl = 'https://youtu.be/'+videoId
          }
          this.iframe_html = this.embedService.embed(this.company.Company.videoUrl,{query:{rel:0}, attr: { width: '100%', height: '100%' } });
          console.log(this.iframe_html);
        } catch (err) {
          console.log('error parse video',err);
        }
      }

    })
    this.getCountVacancyFromUid(this.route.snapshot.params['id'])
  }

  getCountVacancyFromUid(uid) {
    this.dS.sendws('Vacancy/getCountVacancyFromUid', {
      uid:    uid,
      filter: {
        proof:      2,
        publish:    true,
        deleted_at: 0,
        showInfo:   1
      }
    }).then(data => {
      this.companyVacancyCount = data['count']
    })
  }
}
