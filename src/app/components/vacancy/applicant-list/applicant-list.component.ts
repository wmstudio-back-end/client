import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../_services/user.service";
import {DataService} from "../../../_services/data.service";
import {ComponentService} from "../../../_services/component.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector:   'app-applicant-list',
  templateUrl:'./applicant-list.component.html',
  styleUrls:  ['./applicant-list.component.scss']
})
export class ApplicantListComponent implements OnInit {
  resumeIds = []
    objectKeys = Object.keys;
    constructor(
      public dS:DataService,
      public uS:UserService,
      public component:ComponentService,
      private route: ActivatedRoute
    ) {

    }
  routeDetail(_id){
    for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {
      if (this.uS.profile.savedSearches[i]._id==this.route.snapshot.params['id']){
        console.log('count_unread',this.uS.profile.savedSearches[i].count_unread)
        this.uS.profile.savedSearches[i].count_unread.splice(this.uS.profile.savedSearches[i].count_unread.indexOf(_id),1)
        // this.uS.profile.savedSearches[i].count_new.splice(this.uS.profile.savedSearches[i].count_new.indexOf(_id),1)
        console.log('resumeID',_id)
        console.log('searchID',this.route.snapshot.params['id'])
        console.log('count_unread',this.uS.profile.savedSearches[i].count_unread)
        this.dS.sendws('User/updateSearch',this.uS.profile.savedSearches[i]).then(data=>{


        })
      }

    }
    //console.log('00000000000000000000000',_id,this.route.snapshot.params['id'])
  }
    ngOnInit() {

      switch (this.route.snapshot.params['type']){
        case '0':
          for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {
            if (this.uS.profile.subscribeCandidate[i].vacancyId==this.route.snapshot.params['id']
              &&this.uS.profile.subscribeCandidate[i].isnew==0
            ){
              this.uS.profile.subscribeCandidate[i].isnew = 1
              this.resumeIds.push(this.uS.profile.subscribeCandidate[i].resumeId)
            }
          }

          this.dS.sendws("Vacancy/setNewSubscritionVacancy", {vacancyId:this.route.snapshot.params['id']}).then((data) => {

          })
          break;
        case '1':
          for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {
            if (this.uS.profile.subscribeCandidate[i].vacancyId==this.route.snapshot.params['id']
              &&this.uS.profile.subscribeCandidate[i].isview==0){

              this.resumeIds.push(this.uS.profile.subscribeCandidate[i].resumeId)
            }
          }


          break;
        case '2':
          for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {
          if (this.uS.profile.subscribeCandidate[i].vacancyId==this.route.snapshot.params['id']
            ){
            this.resumeIds.push(this.uS.profile.subscribeCandidate[i].resumeId)
          }
          }
          console.log("))))))))))))))))))")
          console.log(this.resumeIds)
          break;
        case '3':
          for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {
            this.resumeIds.push(this.uS.profile.subscribeCandidate[i].resumeId)
          }
          break;
        case '4':
          for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {
            if (this.uS.profile.savedSearches[i]._id==this.route.snapshot.params['id']){
              console.log(this.uS.profile.savedSearches[i])
              for (var j = 0; j < this.uS.profile.savedSearches[i].ids.length; j++) {
                this.resumeIds.push(this.uS.profile.savedSearches[i].ids[j])
              }
              break;
            }

          }
          break;
        case '5':
          for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {
            if (this.uS.profile.savedSearches[i]._id==this.route.snapshot.params['id']){
              console.log(this.uS.profile.savedSearches[i])
              for (var j = 0; j < this.uS.profile.savedSearches[i].count_new.length; j++) {
                this.resumeIds.push(this.uS.profile.savedSearches[i].count_new[j])
              }
              this.uS.profile.savedSearches[i].count_new = []
              this.dS.sendws('User/updateSearch',this.uS.profile.savedSearches[i]).then(data=>{


              })
              break;
            }

          }
          break;
        case '6':
          for (var i = 0; i < this.uS.profile.savedSearches.length; i++) {
            if (this.uS.profile.savedSearches[i]._id==this.route.snapshot.params['id']){
              console.log(this.uS.profile.savedSearches[i])
              for (var j = 0; j < this.uS.profile.savedSearches[i].count_unread.length; j++) {
                this.resumeIds.push(this.uS.profile.savedSearches[i].count_unread[j])
              }
              break;
            }

          }
          break;
        default:
          for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {
            this.resumeIds.push(this.uS.profile.subscribeCandidate[i].resumeId)
          }
            break
      }


    }

}
