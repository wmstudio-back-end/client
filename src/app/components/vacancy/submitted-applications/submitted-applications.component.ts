import {Component, OnInit} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";
import {IVacancy} from "../../../_models/vacancy";
import {DataService} from "../../../_services/data.service";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector:   'app-submitted-applications',
  templateUrl:'./submitted-applications.component.html',
  styleUrls:  ['./submitted-applications.component.scss']
})
export class SubmittedApplicationsComponent implements OnInit {
  vacancy:IVacancy[] = []
  subscribeCount = []
  p: number = 1;
  collection: any[]
  pageChange:any
  coverModal
  coverModalVac
  content = ''
  constructor(
    public component:ComponentService,
    public uS:UserService,
    public dS:DataService,
    public helpers:HelpersService

  ) {

  }




  pagin(e){
    console.log(e)
  }
  ngOnInit() {
    var ids = []
    for (var i = 0; i < this.uS.profile.subscribeVacancy.length; i++) {
      ids.push(this.uS.profile.subscribeVacancy[i].vacancyId)
    }
    this.dS.sendws('Vacancy/getVacancyFromIds',{ids:ids}).then((data)=>{

      data['vacancy'].sort(function(a,b){
        return b.created_at-a.created_at
      })
      this.vacancy = data['vacancy']
    })
    this.dS.sendws('Vacancy/getCountSubscribeToVacancys',{ids:ids}).then((data)=>{
      this.subscribeCount = data['vacancy']

    })

  }

}
