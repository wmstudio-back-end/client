import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {ModulesModule} from "../../modules/modules.module";
import {SidebarModule} from "../sidebar/sidebar.module";
import {CreateComponent} from "./create/create.component";
import {ManualComponent} from "./create/manual/manual.component";
import {SwitchComponent} from "./create/switch/switch.component";
import {UploadComponent} from "./create/upload/upload.component";
import {MyDatePickerModule} from "mydatepicker";
import {MyDateRangePickerModule} from "mydaterangepicker";
import {SearchVacanciesComponent} from "./vacancy-search/vacancy-search.component";
import {BannersModule} from "../banners/banners.module";
import {DetailsComponent} from "./details/details.component";
import {Step1Component} from "./create/components/step-1/step-1.component";
import {ModalModule} from "../modal/modal.module";
import {CompanyListComponent} from "./list/list.component";
import {ClipboardModule} from "ngx-clipboard";
import {StepPromotionComponent} from "./create/components/step-promotion/step-promotion.component";
import {RandomVacancyComponent} from "../Tools/random-vacancy/random-vacancy.component";
import {PdfViewerModule} from "ng2-pdf-viewer";
import {ApplicantListComponent} from "./applicant-list/applicant-list.component";
import {TextMaskModule} from "angular2-text-mask";
import { CompanyVacanciesAllComponent } from './company-vacancies-all/company-vacancies-all.component';

import { VacancyCardComponent } from '../../modules/vacancy-card/vacancy-card.component';
import {SubmittedApplicationsComponent} from "./submitted-applications/submitted-applications.component";
import {NgxPaginationModule} from "ngx-pagination";
@NgModule({
  imports:     [
    PdfViewerModule,
    CommonModule,
    RouterModule,
    TextMaskModule,
    TranslateModule,
    FormsModule,
    ModulesModule,
    NgxPaginationModule,
    ModalModule,
    SidebarModule,
    MyDatePickerModule,
    MyDateRangePickerModule,
    ClipboardModule,
    BannersModule,
    RouterModule.forChild([
      {
        path:     'applist/:id/:type',
        component:ApplicantListComponent
      },
      {
        path:     'create',
        component:CreateComponent,
        data:     {
          breadcrumb:''
        },
        children: [
          {
            path:     '',
            component:SwitchComponent
          },
          {
            path:     'manual',
            component:ManualComponent
          },
          {
            path:     'upload',
            component:UploadComponent
          },
          {
            path:     'manual/:id',
            component:ManualComponent
          },
          {
            path:     'upload/:id',
            component:UploadComponent
          },
          {
            path:     'manual/:id/:step',
            component:ManualComponent
          },
          {
            path:     'upload/:id/:step',
            component:UploadComponent
          },
          {
            path:     'new',
            component:SwitchComponent,
            data:     {
              clear:true
            }
          }
        ]
      },

         {
        path:     'details/:id',
        component:DetailsComponent,
        data:     {
          breadcrumb:''
        },
        children: []
      },
      {
        path:     'search',
        component:SearchVacanciesComponent
      },
      {
        path:     'search/:id',
        component:SearchVacanciesComponent
      },
      {
        path:     'list',
        component:CompanyListComponent
      },
      {
        path:     'all-vacancies/:id',
        component:CompanyVacanciesAllComponent
      },
      {
        path:     'Submitted-applications',
        component:SubmittedApplicationsComponent
      }
    ])
  ],
  declarations:[

    ManualComponent,
    CreateComponent,
    ApplicantListComponent,
    SwitchComponent,
    UploadComponent,
    CompanyListComponent,
    SearchVacanciesComponent,
    RandomVacancyComponent,
    DetailsComponent,
    Step1Component,
    StepPromotionComponent,
    CompanyVacanciesAllComponent,

    SubmittedApplicationsComponent,

  ],
  exports:     []
  //schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  //bootstrap: [ CreateComponent]
})
export class VacancyModule {
}
