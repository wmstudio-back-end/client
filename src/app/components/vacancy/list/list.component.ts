import {Component, Directive, OnInit} from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
import {Router} from "@angular/router";
import {CreatVacancyService} from "../create/create.service";
import {IVacancy} from "../../../_models/vacancy";
import {VacancyService} from "../../../_services/vacancy.service";
import {DownPopupService} from "../../../_services/down-popup.service";
import {HelpersService} from "../../../_services/helpers.service";

@Component({

    templateUrl:'./list.html',
    styleUrls:  ['./list.scss']
})
export class CompanyListComponent implements OnInit {
    coverModal = [];
    CompanyProjects = []
    mobFilter = false
    vacancyTypes = []
    vacancyProjects = []
    filters = {
      project:[],
      vacancyType:[],

    }
    sort = {
      Posted:2,
      Deadline:2,
      Company_rating:2,
      Salary:2,
    }
  sortOlder:number = 100
  modalPromo={}
  constructor(
    public component:ComponentService,
    public dS:DataService,
    public uS:UserService,
    public hS:HelpersService,
    public router: Router,
    public cS:CreatVacancyService,
    public vS:VacancyService,
    public popUp:DownPopupService,
  ) {

    this.uS.profile.Company.vacancy.sort(function(a,b){
      return b.created_at-a.created_at
    })
    for (var i = 0; i < this.uS.profile.Company.vacancy.length; i++) {

      this.modalPromo[this.uS.profile.Company.vacancy[i]._id] = false

    }

  }
  view_varible = false
  view_var(){

  }
  asasas(v){
    console.log(v)
    v.__proto__.show = true
  }
  getClass(p){
      var t = {}
      t['status'+p] = true
      return t
  }
  isPromot(vacancy){
    var isActiv = false

    vacancy.PROMpremiumListing&&vacancy.PROMpremiumListing > this.hS.getUnixTimestamp()?isActiv = true:null
    vacancy.PROMhightFrame&&vacancy.PROMhightFrame > this.hS.getUnixTimestamp()?isActiv = true:null

      // if (vacancy.logInstruction){
      //   for (var i = 0; i < vacancy.logInstruction.length; i++) {
      //     switch (vacancy.logInstruction[i].type){
      //       case 'premiumListing':
      //
      //         vacancy.PROMpremiumListing&&vacancy.PROMpremiumListing > this.hS.getUnixTimestamp()?isActiv = true:null
      //         break;
      //       case 'hightFrame':
      //         vacancy.PROMhightFrame&&vacancy.PROMhightFrame > this.hS.getUnixTimestamp()?isActiv = true:null
      //         break;
      //       case 'insocialMedia':
      //         break;
      //
      //       case 'bannerofEmail':
      //         break;
      //       case 'targetGroup':
      //         break;
      //     }
      //
      //   }
      // }

      return isActiv
  }
  sortinc(p){
    console.log(this.sortOlder)
    for (var key in Object.keys(this.sort)) {

      if(Object.keys(this.sort)[key]==p){

      }else{
        this.sort[Object.keys(this.sort)[key]]=2
      }

    }

    this.sort[p]++

    if(this.sort[p]>4){this.sort[p]=2}
  }





    ngOnInit() {
      console.log('--------------------')
      console.log(this.uS.profile.subscribeCandidate)


      var ids = []
      for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {

        ids.push(this.uS.profile.subscribeCandidate[i].resumeId)
      }


    if (this.uS.profile.Company.projects){
      for(var i=0;i<this.uS.profile.Company.projects.length;i++){
        this.CompanyProjects.push({
          id:this.uS.profile.Company.projects[i]._id,
          text:this.uS.profile.Company.projects[i].name
        })
      }
    }
      this.vacancyTypes = Object.assign(this.vacancyTypes,this.dS.options.vacancyType)
      // this.vacancyTypes.unshift({id:-1,text:'AllVacanciesType'})
      console.log("this.vacancyTypes",this.vacancyTypes)

    }
    checkRout(address, id, type){
      if(this.uS.accessPackage.applicants) {
          this.router.navigate([address,id,type]);
      } else {
          this.popUp.show_warning(this.dS.getOptionsFromId('errors',10).text);
      }

    }

  setEditVacancy(vacancy){

    this.cS.vacancy = JSON.parse(JSON.stringify(vacancy));
    this.cS._id = vacancy._id

    if (vacancy.type_temp==1){
      this.router.navigate(['/vacancy/create/manual',vacancy._id]);
    }
    else if (vacancy.type_temp==2){
      this.router.navigate(['/vacancy/create/upload',vacancy._id]);
    }
    else {
      this.router.navigate(['/vacancy/create/']);
    }
  }
  setDublicatVacancy(vacancy){

    //this.cS._id = vacancy._id
    this.cS.vacancy = JSON.parse(JSON.stringify(vacancy));
    this.cS.vacancy.proof = 0
    this.cS.vacancy.publish = false
    this.cS.vacancy.edit = true
    this.cS._id = undefined

    delete this.cS.vacancy._id
    if (vacancy.type_temp==1){

      this.router.navigate(['/vacancy/create/manual','dublicat']);
    }
    if (vacancy.type_temp==2){
      this.router.navigate(['/vacancy/create/upload','dublicat']);
    }
  }
  setProject(vid,pid,i){
   console.log("vid",vid)
    console.log('pid',pid)
    this.dS.sendws('Vacancy/changeVacancyProject',{vid:vid,pid:pid}).then((data)=>{

      this.uS.profile.Company.vacancy[i].projectID = pid

    })

  }
  changeNotify(e,_id,i){
      console.log('assal;dka;lskd;laskd;',e);
      console.log('vac ackd;',i);
    this.dS.sendws('Vacancy/changeNotify',{_id:_id,notify:e}).then((data)=>{
      // this.uS.profile.Company.vacancy[i].notify = e
    })
      console.log(e)
  }
  editVacancy(vacancy,i){
      console.log("CAVA",vacancy)
      console.log("CAVA__i",i)
  }

  setFilters(){

    var vac:IVacancy[] = []
    var time = this.dS.options.server_time
    for(var i=0;i<this.uS.profile.Company.vacancy.length;i++){
      this.coverModal.push(false)
      var add = true;
      this.filters.vacancyType.length>0
        &&this.filters.vacancyType.indexOf(this.uS.profile.Company.vacancy[i].type)<0
          ?add=false:null
      this.filters.project.length>0
        &&this.filters.project.indexOf(this.uS.profile.Company.vacancy[i].projectID)<0
        ?add=false:null
  if (this.sortOlder<100){
    this.sortOlder==0&&this.uS.profile.Company.vacancy[i].created_at<(time-86400*3)?add=false:null
    this.sortOlder==25&&this.uS.profile.Company.vacancy[i].created_at<(time-86400*7)?add=false:null
    this.sortOlder==50&&this.uS.profile.Company.vacancy[i].created_at<(time-86400*14)?add=false:null
    this.sortOlder==75&&this.uS.profile.Company.vacancy[i].created_at<(time-86400*21)?add=false:null
  }


      add?vac.push(this.uS.profile.Company.vacancy[i]):null
      //add?this.coverModal.push(false):null

    }
    if (this.sort.Posted>2){
      vac.sort((a,b)=>{
        if (this.sort.Posted==4){return a.created_at-b.created_at}
        if (this.sort.Posted==3){return b.created_at-a.created_at}

      })
    }
    if (this.sort.Deadline>2){
      vac.sort((a,b)=>{
        if (this.sort.Deadline==4){return a.deadLine-b.deadLine}
        if (this.sort.Deadline==3){return b.deadLine-a.deadLine}

      })
    }

    if (this.sort.Salary>2){

      vac.sort((a,b)=>{

        var ka = 0
        var kb = 0
        a.salary.type==0?ka=180:null
        b.salary.type==0?kb=180:null

        a.salary.type==1?ka=40:null
        b.salary.type==1?kb=40:null

        a.salary.type==2?ka=1:null
        b.salary.type==2?kb=1:null
        if (this.sort.Salary==4){return (ka*a.salary.summ)-(kb*b.salary.summ)}
        if (this.sort.Salary==3){return (ka*b.salary.summ)-(kb*a.salary.summ)}

      })
    }
    if (this.sort.Company_rating>2){
      vac.sort((a,b)=>{
        if (this.sort.Company_rating==4){return this.uS.users[a.uid].rating - this.uS.users[b.uid].rating}
        if (this.sort.Company_rating==3){return this.uS.users[b.uid].rating - this.uS.users[a.uid].rating}

      })
    }
    //sort = {
    //  Posted:2,
    //  Deadline:2,
    //  Company_rating:2,
    //  Salary:2,
    //}
    this.uS.profile.Company.vacancy.sort(function(a,b){
      return b.created_at-a.created_at
    })
    return vac
  }

}
