import {
  Component,
  HostListener,
  OnInit
}                         from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {DataService}      from "../../../_services/data.service";
import {VacancyService}   from "../../../_services/vacancy.service";
import {IVacancy}         from "../../../_models/vacancy";
import {User}             from "../../../_models/user";
import {Location}         from '@angular/common';
import {UserService}      from "../../../_services/user.service";
import {
  ActivatedRoute,
  Router
}                         from "@angular/router";
import {HelpersService}   from "../../../_services/helpers.service";
import {DownPopupService} from "../../../_services/down-popup.service";

@Component({
  templateUrl: './vacancy-search.component.html',
  styleUrls:   ['vacancy-search.component.scss']
})
export class SearchVacanciesComponent implements OnInit {
  private message             = {}
          vacancy: IVacancy[] = []
          vacArr: IVacancy[]
          users: User[]       = []
          usersArr: User[]
          skip                = 0;
          limit               = 8;
          count               = 0;
          pages               = [];
          curPage             = 1
          maxPage             = 1
          saveModal: boolean  = false
          compact: boolean    = false
          sortBtns            = {
            'published_at': 2,
            deadLine:       0,
            'user.rating':  0,
            "salary.summ":  0
          }
          _id: string         = ''


  public stopApply: boolean = false;

  @HostListener("window:scroll", [])
  onWindowScroll() {


    let number  = document.body.scrollTop;
    let numberr = document.getElementById('stopScroll').getBoundingClientRect().bottom;
    if (number > numberr) {
      this.stopApply = true;


    } else if (this.stopApply && number < numberr) {
      this.stopApply = false;
    }

  }


  constructor(private router: Router,
              private route: ActivatedRoute,
              public dataService: DataService,
              public vacancyService: VacancyService,
              public component: ComponentService,
              public location: Location,
              private popS: DownPopupService,
              public helpers: HelpersService,
              public userService: UserService) {
    if (typeof route.snapshot.params['id'] !== "undefined")
      this._id = route.snapshot.params['id']
    this.compact = this.userService.preferCompactView
  }

  ngOnInit() {
    // this.vacancyService.clearFilter()
    if (this._id)
      this.vacancyService.setSaveSearch(this._id)
    else
      this.vacancyService.setSaveSearch()
    this.getVacancy()
    this.vacancyService.vacancy.subscribe((data) => {
      this.vacancy  = data['vacancy']
      this.count    = data['count']
      let pages = [];
      this.pages    = []
      let activPage = this.skip / this.limit
      this.curPage = activPage + 1;
      this.maxPage = Math.ceil(this.count / this.limit)
      for (var i = 0; i < this.maxPage; i++) {
        let status   = activPage == i ? 3 : 1
        let num: any = i + 1
        let pushPage = false
        if (i === 0) {
          pushPage = true
        }
        if (activPage === 0 && i === 3){
          pushPage = true
        }
        if (activPage > 3 && i === 1) {
          pushPage = true
          num      = '...'
        }
        if (i < activPage + 3 && i > activPage - 3 && i != this.maxPage - 1) {
          pushPage = true
        }
        if (activPage < this.maxPage - 4 && i == this.maxPage - 2) {
          pushPage = true
          num      = '...'
        }
        if (i == this.maxPage - 1) {
          pushPage = true
        }

        if (pushPage) {
          pages.push({
            status: status,
            num:    num,
            style:  0
          })
        }
      }
      this.pages = pages
    })

  }

  setPage(n) {
    if (n == '...') return;
    this.skip = (n - 1) * this.limit
    this.getVacancy()
  }

  getVacancy() {
    let sort = {}
    for (var key in this.sortBtns) {
      if (this.sortBtns[key] == 1) {
        sort[key] = 1
      }
      if (this.sortBtns[key] == 2) {
        sort[key] = -1
        if (key == 'published_at') {
          sort['activInstruction.premiumListing'] = -1
        }
      }
    }
    this.vacancyService.filter.skip  = this.skip
    this.vacancyService.filter.limit = this.limit
    this.vacancyService.filter.sort  = sort
    this.vacancyService.getVacancy()


  }

  formatData(time) {
    return new Date(time * 1000);
  }

  setSortVacancy(sort) {
    let {key, val} = sort
    for (var k in Object.keys(this.sortBtns)) {
      if (key != Object.keys(this.sortBtns)[k]) {
        this.sortBtns[Object.keys(this.sortBtns)[k]] = 0
      }
    }
    this.vacancyService.filter.sort = {}
    this.sortBtns[key]              = val
    this.getVacancy()
  }

  sortVacancy(key) {
    for (var k in Object.keys(this.sortBtns)) {
      if (key != Object.keys(this.sortBtns)[k]) {
        this.sortBtns[Object.keys(this.sortBtns)[k]] = 0
      }

    }
    this.vacancyService.filter.sort = {}
    switch (this.sortBtns[key]) {
      case 0:
        this.sortBtns[key] = 1
        break;
      case 1:
        this.sortBtns[key] = 2
        break;
      case 2:
        this.sortBtns[key] = 1
        break;
    }

    this.getVacancy()
  }

  prevPage() {
    this.skip -= this.limit
    if (this.skip < 0) {
      this.skip = 0
    }
    this.getVacancy()
  }

  nextPage() {
    this.skip += this.limit
    this.getVacancy()
  }

  setBookmark(id, i) {
    this.popS.show_info(this.dataService.getOptionsFromId('errors', 15).text);
    this.userService.setBookmark(id)
    this.vacancy[i].bookmark = true

  }

  unsetBookmark(id, i) {
    this.popS.show_info(this.dataService.getOptionsFromId('errors', 16).text);
    this.userService.unsetBookmark(id)
    this.vacancy[i].bookmark = false

  }

  isSubscribe(ids) {
    console.log("===============")
    console.log(ids)
    console.log(this.userService.profile._id)
    if (ids && ids.indexOf(this.userService.profile._id) > -1) {
      return true
    }
    return false
  }


}
