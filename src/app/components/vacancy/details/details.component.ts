import {Component, ElementRef, OnInit, Pipe, HostListener, ViewChild} from '@angular/core';
import {ActivatedRoute, Params, Router}                               from "@angular/router";
import {IVacancy}                                                     from "../../../_models/vacancy";
import {DataService}                                                  from "../../../_services/data.service";
import {VacancyService}                                               from "../../../_services/vacancy.service";
import {MessageService}                                               from "../../../_services/message.service";
import {UserService}                                                  from "../../../_services/user.service";
import {User}                                                         from "../../../_models/user";

import {DomSanitizer}                                                 from "@angular/platform-browser";
import {DownPopupService}                                             from "../../../_services/down-popup.service";
import {HelpersService} from "../../../_services/helpers.service";

@Pipe({name: 'safe'})
@Component({
    selector:    'app-details',
    templateUrl: './details.component.html',
    styleUrls:   ['./details.component.scss']
})

export class DetailsComponent implements OnInit {
    vacancy: IVacancy
    iframe;
    _id: string
    isDataAvailable: boolean = false;
    companyVacancyCount = 0
    typeFile = ''
    user: User;
    port: string;
    host: string;
    uid: string;
    isApply = true
    resumes = []
    coverLetter = []
    apply = {
        resumeId:      '',
        coverLetterId: '',
        content:       '',
        vacancyId:     '',
        _id:           '',
        created_at:     null,
    }
    modal: boolean = false
    photo: string = '/assets/img/ic_business_grey.svg';
    @ViewChild('boxSd') private boxSd: ElementRef;


    constructor(private router: Router,
                private route: ActivatedRoute,
                private hS: HelpersService,
                public dataService: DataService,
                public popS: DownPopupService,
                public vacancyService: VacancyService,
                public mS: MessageService,
                public userService: UserService,
                private domSanitizer: DomSanitizer,
                public element: ElementRef) {

        this._id = route.snapshot.params['id']
        this.port = dataService.PORT_STATIC;
        this.host = dataService.HOST_STATIC;

        this.route.params.subscribe(params => {
            this._id = route.snapshot.params['id']

            this.ngOnInit()
        });
        //window.addEventListener("scroll", (e) => {
        //
        //  console.log(this.boxSd.nativeElement.offsetTop)
        //
        //  //element.nativeElement.
        //  //if (window.pageYOffset > 100) {
        //  //  console.log(window.pageYOffset)
        //  //  element.nativeElement.classList.add('stick');
        //  //} else {
        //  //  console.log(window.pageYOffset)
        //  //  element.nativeElement.classList.remove('stick');
        //  //}
        //});

    }
    sadasd(e){

      console.log(e)
    }
    printPdf() {

        //
        // const elementToPrint = window.document.getElementById('vacancyDet');
        // const pdf = new jsPDF('p', 'pt', 'a4');
        // var specialElementHandlers = {
        //     '#editor': function (element, renderer) {
        //         return true;
        //     }
        // };
        // var options = {
        //     margin:            {top: 10, right: 10, bottom: 10, left: 10, useFor: 'content'},
        //     'pagesplit':       true,
        //     'elementHandlers': specialElementHandlers
        // };
        //
        // let margins = {
        //     top:    100,
        //     bottom: 100,
        //     left:   40,
        //     width:  750
        // };
        // pdf.addHTML(elementToPrint, 10, 10, options, () => {
        //     pdf.save('web.pdf');
        // }, margins);
    }

    setVacancy() {

    }

    applyToVacancy(_id) {
        this.apply.vacancyId = _id


        var save = true
        this.apply.resumeId ? null : save = false
        //this.apply.content ? null : save = false
        if (save) {
            this.dataService.sendws('Vacancy/subscribeToVacancy', this.apply).then((data) => {
                if (data['vacancy'].upserted) {
                    console.log(this.userService.profile.subscribeVacancy)
                    console.log(data['vacancy'].upserted[0])
                    this.apply.created_at = Math.round(+new Date() / 1000)
                    this.apply._id = data['vacancy'].upserted[0]._id
                    this.userService.profile.subscribeVacancy.push(this.apply)
                }
                console.log("applyToVacancy", data)
                this.modal = false
            })
        } else {
            this.popS.show_success(this.dataService.getOptionsFromId('errors', 23).text)

        }


    }

    getCountVacancyFromUid(uid) {
        this.dataService.sendws('Vacancy/getCountVacancyFromUid', {uid: uid,filter:{proof:2,publish:true,deleted_at:0,showInfo:1}}).then(data => {
            this.companyVacancyCount = data['count']

          this.userService.getUsers([uid])
        })
    }
    b1 = {
      y:0,
      height:0,
      offset:0,
      top:0
    }
    b2 = {
      y:0,
      height:0,
      offset:0,
      top:0
    }
    wscroll = 0
  sideContent = {
    y:0,
    height:0,
    offset:0,
    top:0
  }
  clientHeight = 0
  scroll = (): void => {
    //handle your scroll here
    //notice the 'odd' function assignment to a class field
    //this is used to be able to remove the event listener
    var scrollTop = window.pageYOffset ? window.pageYOffset : (document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop);
    this.wscroll = scrollTop
    // console.log('scrollTop',scrollTop)
    var b1 = window.document.getElementById('bottomline1')
    var b2 = window.document.getElementById('bottomline2')
    var sideContent = window.document.getElementById('sideContent')
    // console.log(sideContent.offsetHeight)
    if (b1){
      if (this.b1.height==0&&b1.getBoundingClientRect().height>0){
        this.b1.height = b1.getBoundingClientRect().height
      }

      this.b1.y = b1.getBoundingClientRect()['y']||0
      this.b1.top = b1.getBoundingClientRect().top
      this.b1.offset = b1.offsetTop
    }
    if (b2) {
      this.b2.height = b2.getBoundingClientRect().height
      this.b2.y = b2.getBoundingClientRect()['y'] || 0
      this.b2.top = b2.getBoundingClientRect().top
      this.b2.offset = b2.offsetTop
    }
    if (sideContent) {
      this.sideContent.height = sideContent.getBoundingClientRect().height
      this.sideContent.y = sideContent.getBoundingClientRect()['y'] || 0
      this.sideContent.top = sideContent.getBoundingClientRect().top
      this.sideContent.offset = sideContent.offsetTop
      this.clientHeight = document.documentElement.clientHeight
    }
var start = false
      if (b1&&this.b1.height+this.b1.offset+this.wscroll>this.sideContent.height+this.sideContent.offset){
        start = true
        b1.style.position = 'absolute'
        b1.style.bottom = '0px'

      }
      if (b1&&this.clientHeight+this.wscroll-this.b1.height<this.b1.height+this.b1.offset+(b2?52:0)){
        start = false
          b1.style.position = 'fixed'
          b1.style.bottom = b2?'52px':'0px'
      }

    b2?b2.style.top = (this.b1.top+this.b1.height+(start?1:0))+"px":null



  };
    ngOnInit() {


      window.addEventListener('scroll', this.scroll, true); //third parameter

        this.dataService.sendws('Vacancy/getVacancyFromIds', {ids: [this._id]}).then(data => {
            this.vacancy = data['vacancy'][0]

          if (this.vacancy.uid!=this.userService.profile._id){
            // this.vacancyService.setViweVacancy(this._id)
          //

          }
            if (this.vacancy.vacancyUrl && this.vacancy.vacancyUrl != '') {
                this.iframe = this.domSanitizer.bypassSecurityTrustResourceUrl(this.vacancy.vacancyUrl);
            }
            this.getCountVacancyFromUid(this.vacancy.uid)
            if (this.vacancy.LanguagesRequired) {
                if (this.vacancy.LanguagesRequired.length == 1 && this.vacancy.LanguagesRequired[0].languageID == null) {
                    this.vacancy.LanguagesRequired = null
                }
            }
          if (this.vacancy.uploadFiles&&this.vacancy.uploadFiles.length&&this.vacancy.descriptionStep==2){

            var m  = this.vacancy.uploadFiles[0].path.split(".")
            m.reverse()
            this.typeFile = m[0]
          }

        })
        //this.vacancyService.getVacancyFromId(this._id).then((data: IVacancy) => {
        //    this.vacancy = data
        //    this.isDataAvailable = true
        //    this.uid = this.vacancy.uid
        //})
        this.resumes = []
        if (this.userService.profile.type_profile == 'Student') {
            for (var i = 0; i < this.userService.profile.Student.resumes.length; i++) {
                this.resumes.push(
                    {
                        text: this.userService.profile.Student.resumes[i].name,
                        id:   this.userService.profile.Student.resumes[i]._id
                    }
                )
            }
            this.coverLetter = []
            for (var i = 0; i < this.userService.profile.CoverLetter.length; i++) {
                this.coverLetter.push(
                    {
                        text: this.userService.profile.CoverLetter[i].title,
                        id:   this.userService.profile.CoverLetter[i]._id
                    }
                )
            }
        }

    }
  setTextCL(id){
    for (var i = 0; i < this.userService.profile.CoverLetter.length; i++) {
      if (id==this.userService.profile.CoverLetter[i]._id){
        this.apply.content =  this.userService.profile.CoverLetter[i].text
        break;
      }
    }

  }
    checkCSize() {
        if (!this.user) return false;
        if (this.user.Company.companySize) {
            if (typeof this.dataService.options.companySize[this.user.Company.companySize] !== "undefined")
                return true;
        }
        return false;
    }

    checkUser() {
        if (this.user) return true;
        if (typeof this.userService.users[this.uid] !== "undefined") {
            this.user = this.userService.users[this.uid]
            if (this.user.hasOwnProperty('photo')) {
                if (this.user.photo.length) {
                    this.photo = '//' + this.host + ':' + this.port + this.user.photo[0]['url']
                }
            }
            return true;
        } else {

          return false
        };
    }



}
