import { Component, OnInit }    from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {DataService}      from "../../../_services/data.service";
import {VacancyService}   from "../../../_services/vacancy.service";
import {DownPopupService} from "../../../_services/down-popup.service";
import {ComponentService} from "../../../_services/component.service";
import {HelpersService}   from "../../../_services/helpers.service";
import {UserService}      from "../../../_services/user.service";
import {Location}         from "@angular/common";
import {IVacancy}         from "../../../_models/vacancy";

@Component({
  selector: 'app-company-vacancies-all',
  templateUrl: './company-vacancies-all.component.html',
  styleUrls: ['./company-vacancies-all.component.scss']
})
export class CompanyVacanciesAllComponent implements OnInit {
  vacanciesArr = []
  vacancy: IVacancy[] = []
  compact: boolean = false

  constructor(private router: Router,
              private route: ActivatedRoute,
              public dS: DataService,
              public vacancyService: VacancyService,
              public component: ComponentService,
              public location: Location,
              public popS:DownPopupService,
              public helpers: HelpersService,
              public userService: UserService) { }

  ngOnInit() {
      this.dS.sendws('Vacancy/getVacancyFromUid', {companyId:this.route.snapshot.params['id'],filter:{proof:2,publish:true,deleted_at:0,showInfo:1} })
          .then((data)=>{
              this.vacanciesArr = data['vacancies']
          })


  }

    setBookmark(id,i) {
        this.popS.show_info(this.dS.getOptionsFromId('errors',15).text);
        this.userService.setBookmark(id)
        this.vacanciesArr[i].bookmark = true

    }

    unsetBookmark(id,i) {
        this.popS.show_info(this.dS.getOptionsFromId('errors',16).text);
        this.userService.unsetBookmark(id)
        this.vacanciesArr[i].bookmark = false


    }


}
