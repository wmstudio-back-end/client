import {
  Component,
  OnInit
}                            from "@angular/core";
import {DataService}         from "../../../../_services/data.service";
import {CreatVacancyService} from "../create.service";
import {TranslateService}    from "@ngx-translate/core";
import {UserService}         from "../../../../_services/user.service";
import {ActivatedRoute}      from "@angular/router";
import {IMyDpOptions} from "mydatepicker";
import {HelpersService} from "../../../../_services/helpers.service";

@Component({

  templateUrl: './upload.component.html',
  styleUrls:   ['./upload.component.scss']
})
export class UploadComponent implements OnInit {

  dropFiles: any      = [];
  aplicatCont: number = 1
  deadLine                                  = null
  private myDatePickerOptionsDeadline: IMyDpOptions = <IMyDpOptions>{};
  constructor(
    public dataService: DataService,
    public cS: CreatVacancyService,
    public translate: TranslateService,
    public helpers: HelpersService,
    public user: UserService,
    private route: ActivatedRoute
  ) {
    var initId = false

    for (var i = 0; i < user.profile.Company.vacancy.length; i++) {
      if (user.profile.Company.vacancy[i]._id == route.snapshot.params['id']) {
        cS._id     = route.snapshot.params['id']
        //cS.resume = Object.assign({},user.profile.Student.resumes[i])
        cS.vacancy = JSON.parse(JSON.stringify(user.profile.Company.vacancy[i]));
        initId     = true
        if (cS.vacancy.proofUrl) {
          this.aplicatCont = 0
        }
      }
    }

    if (route.snapshot.params['id'] == 'dublicat') {
      cS._id = undefined
      initId = true
    }
    if (!initId) {
      cS._id = undefined
      cS.clearVacancy()

    }


    if (!user.accessPackage.projBmark) {
      this.steps.splice(0, 1)
      this.stepActive = this.stepActive + 1
    }
    if (route.snapshot.params['step']) {
      this.stepActive = parseInt(route.snapshot.params['step'])
    }
    this.setStatus(user.accessPackage.projBmark ? this.stepActive : this.stepActive + 1)

  }
  changeErr(){
    if (this.aplicatCont==0&&this.cS.validationForms&&!this.cS.vacancy.proofUrl){
      return {'border-color':'red'}
    }else{
      return {}
    }
  }
  ngOnInit() {
    this.cS.isManual          = false
    this.cS.vacancy.type_temp = 2

    for (var s = 0; s < this.steps.length; s++) {
      this.verifyStep(s)
    }
    if (this.cS.vacancy.projectID && this.cS.pay && this.stepActive < 2) {

      this.pageFlip(1)
    }
    var end_deadline = this.dataService.convertPickeDate(this.helpers.getUnixTimestamp() + 86400 * 30)
    if (this.cS.vacancy.published_at > this.helpers.getUnixTimestamp()) {
      end_deadline = this.dataService.convertPickeDate(this.cS.vacancy.published_at)
    }
    this.deadLine                 = this.dataService.convertPickeDate(this.cS.vacancy.deadLine)
    this.myDatePickerOptionsDeadline = <IMyDpOptions>{
      dateFormat:   'dd.mm.yyyy',
      width:        '150px',
      disableSince: end_deadline, //end
      disableUntil: this.dataService.convertPickeDate(this.helpers.getUnixTimestamp() - 86400), //start

    };
  }

  setDeadline(u) {
    this.deadLine = this.dataService.convertPickeDate(u)
  }
  renderList(event) {
    return event;
  }

  /*------STEP PAGING ------*/

  stepActive = 0
  steps      = [
    {
      name:     "CVstep.d0",
      status:   1,
      complete: true,
      step:     1
    },
    {
      name:     "CVstep.d1",
      status:   0,
      complete: false,
      step:     2
    },
    {
      name:     "CVstep.d2",
      status:   0,
      complete: false,
      step:     3
    },
    {
      name:     "CVstep.d3",
      status:   0,
      complete: true,
      step:     4
    }
  ]

  pageFlip(i) {
    this.stepActive += i
    this.setStatus(this.cS.pay ? this.stepActive : this.stepActive + 1)
  }

  setStep(i) {
    this.steps[i].step = i + 1
  }

  setStatus(i) {
    console.log('setStatus', i)
    for (var key in this.steps) {
      if (this.steps[key].status < 2 && key <= i) {
        this.steps[key].status = 1
      }
      else if (this.steps[key].status == 2) {

      } else {
        this.steps[key].status = 0
      }
    }
    if (i != 0) {
      for (var s = 0; s < this.steps.length; s++) {
        this.verifyStep(s)
      }

    }
  }

  verifyStep(step) {
    var verStep = step
    if (!this.user.accessPackage.projBmark) {
      verStep += 1
    }
    var r = false
    var s = true

    switch (verStep) {
      case 0:

        s = true

        break;
      case 1:


        typeof this.cS.vacancy.languageID == 'number' ? null : s = false
        this.cS.vacancy.positionTitle && this.cS.vacancy.positionTitle != '' ? null : s = false;
        typeof this.cS.vacancy.fieldActivity == 'number' ? null : s = false;
        typeof this.cS.vacancy.type == 'number' ? null : s = false;
        typeof this.cS.vacancy.occupationField == 'number' ? null : s = false
        this.cS.vacancy.deadLine > 0 ? null : s = false;

        break;
      case 2:

        this.cS.vacancy.proofUploadFiles.length > 0 || this.cS.vacancy.proofUrl ? null : s = false

        break;
      case 3:


        break;
      case 4:


        break;
    }
    if (r && s == false && this.steps.length >= step) {
      this.steps[step].status = 1
    }
    if (r && s) {
      this.steps[step].status = 2
    }
    this.steps[step].complete = s
    return r;
  }

  /*------STEP 2 ------*/
  descriptionStep: number = 1
  uploadFiles             = []

  changeStep(i) {
    this.descriptionStep = i
    this.uploadFiles     = []
  }

  setFiles(e) {
    this.cS.vacancy.uploadFiles.push(e)
  }

}
