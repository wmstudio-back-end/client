import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../../../_services/data.service";

import {UserService} from "../../../../../_services/user.service";
import {ShopService} from "../../../../../_services/shop.service";
import {CreatVacancyService} from "../../create.service";
import {HelpersService} from "../../../../../_services/helpers.service";
import {ActivInstruction} from "../../../../../_models/vacancy";

@Component({
  selector: 'app-step-promotion',
  templateUrl: './step-promotion.component.html',
  styleUrls: ['./step-promotion.component.scss']
})
export class StepPromotionComponent implements OnInit {

  constructor(
    public dataService:DataService,
    public cS:CreatVacancyService,
    public uS:UserService,
    public hS:HelpersService,
    public shopService:ShopService,

  ) { }
  activInstruction = null
  modalValues = {
    text:'',
    files:[]
  }
  modalAdd = false
  modalView = false
  modalViewContent = {
    text:'',
    files:[]
  }

  // PROMpremiumListing=false
  // PROMtargetGroup=false
  // PROMinsocialMedia=false
  // PROMhightFrame=false
  // PROMbannerofEmail=false

  PROMpremiumListing=0
  PROMtargetGroup=0
  PROMinsocialMedia=0
  PROMhightFrame=0
  PROMbannerofEmail=0
  table_1 = []
  table_2 = []

  promotion = [
    {
      type:'premiumListing',
      text:'promotions.prem_listing',
      text_h:'promotions.prem_listing_h',
      active:false,
      applied:0,
      price:0,
      available:0,
      available_i:'promodays',
      difference:[],
      until:'',
      addinstructions:false,
      detailsShow:false,

      details:[]
    },
    {
      type:'hightFrame',
      text:'promotions.frame',
      text_h:'promotions.frame_h',
      active:false,
      applied:0,
      price:0,
      available:0,
      available_i:'promodays',
      difference:[],
      until:'',
      addinstructions:false,
      detailsShow:false,

      details:[]
    },
    // {
    //   type:'insocialMedia',
    //   text:'promotions.isSocialMedia',
    //   text_h:'promotions.isSocialMedia_h',
    //   active:false,
    //   applied:0,
    //   price:0,
    //   available:0,
    //   available_i:'ps',
    //   difference:[],
    //   until:'',
    //   addinstructions:true,
    //   detailsShow:false,
    //
    //   details:[]
    // },{
    //   type:'targetGroup',
    //   text:'promotions.edvtarg',
    //   text_h:'promotions.edvtarg_h',
    //   active:false,
    //   applied:0,
    //   price:0,
    //   available:0,
    //   available_i:'ps',
    //   difference:[],
    //   until:'',
    //   addinstructions:true,
    //   detailsShow:false,
    //
    //   details:[]
    // },{
    //   type:'bannerofEmail',
    //   text:'promotions.banner',
    //   text_h:'promotions.banner_h',
    //   active:false,
    //   applied:0,
    //   price:0,
    //   available:0,
    //   available_i:'ps',
    //   difference:[],
    //   until:'',
    //   addinstructions:true,
    //   detailsShow:false,
    //   details:[
    //
    //   ]
    // },
  ]
  clearModalValues(){
    this.modalValues = {
      text:'',
      files:[]
    }
  }
  setAddModal(type){

    if (!this.cS.vacancy.activInstruction){this.cS.vacancy.activInstruction = <ActivInstruction>{}}
    if (!this.cS.vacancy.activInstruction[type]){this.cS.vacancy.activInstruction[type] = {}}
    console.log(this.cS.vacancy.activInstruction)

    this.activInstruction = type
    Object.keys(this.cS.vacancy.activInstruction[type]).length>0?this.modalValues = this.cS.vacancy.activInstruction[type]:this.clearModalValues();
    this.modalAdd=true;

    console.log(Object.keys(this.cS.vacancy.activInstruction[type]).length>0)
    console.log("modalValues",this.modalValues)
  }
  saveInstruction(data){


    if (!this.cS.vacancy.activInstruction){this.cS.vacancy.activInstruction = <ActivInstruction>{}}
    if (!this.cS.vacancy.activInstruction[data['outValue']]){this.cS.vacancy.activInstruction[data['outValue']] = {}}
    this.cS.vacancy.activInstruction[data['outValue']] = {text:data['text'],files:data['files']}
  }
  convertUntil(time){
    var t = new Date(time*1000)
    var m = t.getMonth()<10?"0"+(t.getMonth()+1).toString():(t.getMonth()+1).toString()
    var d = t.getDate()<10?"0"+t.getDate().toString():t.getDate().toString()
    return d+'/'+m
  }
  summValues(kl){
    var t=0
    for (var i = 0; i < kl.length; i++) {
      t+=kl[i].instruction.values

    }

    return t
  }
  applyInstr(id,type){

  }
  ngOnInit() {
    console.log('===================================================')
    if (!this.cS.vacancy.logInstruction){
      this.cS.vacancy.logInstruction = []
    }
  if (!this.cS.vacancy.activInstruction){
    this.cS.vacancy.activInstruction = {
      premiumListing:{values:0},
      hightFrame:{values:0},
      insocialMedia:{values:0},
      targetGroup:{values:0},
      bannerofEmail:{values:0}
    }
  }
    !this.cS.vacancy.activInstruction.premiumListing?this.cS.vacancy.activInstruction.premiumListing = {values:0}:null
    !this.cS.vacancy.activInstruction.hightFrame?this.cS.vacancy.activInstruction.hightFrame = {values:0}:null
    !this.cS.vacancy.activInstruction.insocialMedia?this.cS.vacancy.activInstruction.insocialMedia = {values:0}:null
    !this.cS.vacancy.activInstruction.targetGroup?this.cS.vacancy.activInstruction.targetGroup = {values:0}:null
    !this.cS.vacancy.activInstruction.bannerofEmail?this.cS.vacancy.activInstruction.bannerofEmail = {values:0}:null

    for (var i = 0; i < this.promotion.length; i++) {

    this.promotion[i].available = this.uS.profile.service.active[this.promotion[i]['type']]||0;
     this.promotion[i].applied = this.cS.vacancy['PROM'+this.promotion[i]['type']+'Val']||0

      var minDur = []
      var cof = 1
      this.promotion[i].available_i=='ps'?cof=-10:cof=1
      minDur.push(Math.floor((this.cS.vacancy.published_at-this.hS.getUnixTimestamp()+(86400*cof))/86400))
      minDur.push(Math.floor((this.cS.vacancy.deadLine-this.hS.getUnixTimestamp()+(86400*cof))/86400))
      minDur.push(this.uS.profile.service.active[this.promotion[i]['type']])
      console.log('===',minDur)
      minDur.sort(function(a, b) {
        return a - b;
      })
      minDur = minDur.filter(date=>date>0)
      console.log('999999999',this.promotion[i]['type'],minDur)
      var value = 1
      for (var s = 0;  s<minDur[0]&&s<30; s++) {
        this.promotion[i].difference.push({
          id:s+1,
          text:s+1,
        });
        // this.duration.push({
        //   id:s+1,
        //   text:s+1,
        // })

      }

     //  for (var j = 0; j < this.promotion[i].available; j++) {
     //      var dayTodown = 1
     //    this.promotion[i].available_i=='ps'?dayTodown=10:dayTodown=1;
     //    if (
     //      (this.hS.getUnixTimestamp()+(86400*(j))<this.cS.vacancy.deadLine
     //      // &&this.hS.getUnixTimestamp()+(86400*(j))<this.cS.vacancy.published_at
     //      &&this.promotion[i].available_i!='ps')
     //      ||(
     //        (
     //          // this.hS.getUnixTimestamp()+(86400*(j))<this.cS.vacancy.deadLine
     //          // &&this.hS.getUnixTimestamp()+(86400*(j))<this.cS.vacancy.published_at
     //          this.promotion[i].available_i=='ps'
     //
     //          &&this.cS.vacancy.deadLine-(86400*10)>this.hS.getUnixTimestamp()
     //          &&this.cS.vacancy.published_at-(86400*10)>this.hS.getUnixTimestamp()
     //        )
     //      )
     //      //
     //      )
     //    {
     //      this.promotion[i].difference.push({id:j+1,text:j+1});
     //    }
     //   // this.promotion[i].difference.push({id:j+1,text:j+1});
     //  }
      for (var j = 0; j < this.cS.vacancy.logInstruction.length; j++) {
        if (this.cS.vacancy.logInstruction[j].type==this.promotion[i]['type']){
          this.promotion[i].details.push(this.cS.vacancy.logInstruction[j])
        }

      }
     // console.log(this.promotion[i])
      if (this.cS.vacancy.proof==2){
        switch (this.promotion[i]['type']){
          case 'premiumListing':
            // this.promotion[i].applied = Math.ceil((this.hS.getUnixTimestamp()-this.promotion[i].applied)/86400)*-1||0
            this.cS.vacancy['PROM'+this.promotion[i]['type']]?this.promotion[i].until = this.convertUntil(this.cS.vacancy['PROM'+this.promotion[i]['type']]):null
            break;
          case 'hightFrame':
            this.cS.vacancy['PROM'+this.promotion[i]['type']]?this.promotion[i].until = this.convertUntil(this.cS.vacancy['PROM'+this.promotion[i]['type']]):null
            break;
          default:
            break
        }
      }

      // this.promotion[i].applied>0?this.promotion[i].active=true:null
      //if (this.uS.profile.service&&this.uS.profile.service.active&&this.uS.profile.service.active.orderId) {
        this.promotion[i].price = this.shopService.getPackage(this.shopService.cart.subscriptions.packageId).settings['price' + (this.promotion[i]['type'].charAt(0).toUpperCase()) + this.promotion[i]['type'].substr(1)]
     // }
    }
    for (var i = 0; i < this.promotion.length; i++) {
      if (this.promotion[i].available>0){
        this.table_1.push(this.promotion[i])
      }else{
        this.table_2.push(this.promotion[i])
      }
    }



  }
  setPROM(){
    // this.PROMpremiumListing?this.cS.vacancy.PROMpremiumListing =this.PROMpremiumListing:this.cS.vacancy.PROMpremiumListing=0;
    // this.PROMtargetGroup?this.cS.vacancy.PROMtargetGroup =this.PROMtargetGroup:this.cS.vacancy.PROMtargetGroup=0;
    // this.PROMinsocialMedia?this.cS.vacancy.PROMinsocialMedia =this.PROMinsocialMedia:this.cS.vacancy.PROMinsocialMedia=0;
    // this.PROMhightFrame?this.cS.vacancy.PROMhightFrame =this.PROMhightFrame:this.cS.vacancy.PROMhightFrame=0;
    // this.PROMbannerofEmail?this.cS.vacancy.PROMbannerofEmail =this.PROMbannerofEmail:this.cS.vacancy.PROMbannerofEmail=0;

  }
  getCountpromot(val){
    if (!this.uS.profile.service){return [];}
    if (!this.uS.profile.service.active){return [];}
    if (!this.uS.profile.service.active[val]){return [];}
    var t = []
    for (var i = 0; i < this.uS.profile.service.active[val]; i++) {
      t.push({
        id:i+1,
        text:i+1
      })

    }
    return t
  }


}
