import { Component, OnInit } from '@angular/core';
import {CreatVacancyService} from "../../create.service";
import {CompanyProjects} from "../../../../../_models/user";
import {DataService} from "../../../../../_services/data.service";
import {UserService} from "../../../../../_services/user.service";
import {HelpersService} from "../../../../../_services/helpers.service";

@Component({
  selector: 'create-step-1',
  templateUrl: './step-1.component.html',
  styleUrls: ['./step-1.component.scss']
})
export class Step1Component implements OnInit {
  iProject=<CompanyProjects>{}
  constructor(public dataService:DataService,public user: UserService,public cS:CreatVacancyService,public helpers:HelpersService) {
    this.setProjectList()
    if (cS.vacancy.projectID){
      for (var i=0;i<this.user.profile.Company.projects.length;i++){
        if (this.user.profile.Company.projects[i]['_id']==cS.vacancy.projectID){
          this.projectText = this.user.profile.Company.projects[i]['name']
        }
      }
    }
  }

  ngOnInit() {
  }
  /*------STEP 0 ------*/
  projectText:string = 'vacancy.Choose_project'
  projectList:any[]
  nameNewProject:string;
  PLdefauiltVal:any
  salary_not_viwe:number = 0
  renderNewProject(data){
    this.user.profile.Company.projects.push(data)
    this.cS.vacancy.projectID = data._id
    this.setProjectList()
  }
  createNewProject(){
    this.dataService.sendws("Vacancy/createNewProject", this.iProject).then((data)=>{
      console.log(data)
        this.renderNewProject(data['project'])
        this.setProjectList()
        this.nameNewProject = ''
    });


  }

  setProjectList(){

    this.projectList = new Array()
    for(var i=0;i<this.user.profile.Company.projects.length;i++){
      this.projectList.push({
        text:this.user.profile.Company.projects[i].name,
        id:this.user.profile.Company.projects[i]._id
      })
      this.PLdefauiltVal = this.projectList[i]
    }
  }

}
