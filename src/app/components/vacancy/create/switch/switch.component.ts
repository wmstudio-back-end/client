import {Component, OnInit} from "@angular/core";
import {DataService} from "../../../../_services/data.service";
import {CreatVacancyService} from "../create.service";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../../_services/user.service";
@Component({

  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent implements OnInit {
  constructor(
    public dataService: DataService,
    public cS:CreatVacancyService,
    private route: ActivatedRoute,
    private router: Router,
    private uS:UserService
  ) {
  }

  ngOnInit() {
    if (this.route.snapshot.data['clear']){
      this.cS._id = null
      this.cS.clearVacancy()
      console.log('CLEAR SWITCH')
    }
    this.cS.dublicate = this.route.snapshot.data['name']=='DUBLICATE'
    if (this.cS.dublicate){
      for(var i=0;i<this.uS.profile.Company.vacancy.length;i++){
        if (this.uS.profile.Company.vacancy[i]._id==this.route.snapshot.params['id']){

          this.cS._id = this.route.snapshot.params['id']
          this.cS.vacancy = JSON.parse(JSON.stringify(this.uS.profile.Company.vacancy[i]));
          break;
        }
      }


      //this.router.navigate(['/vacancy/create/'+this.cS._id+'/manual']);
      //this.router.navigate(['/vacancy/create/'+this.cS._id+'/manual']);
      //this.cS._id = undefined
      //this.cS.vacancy._id = undefined
    }
  }

}
