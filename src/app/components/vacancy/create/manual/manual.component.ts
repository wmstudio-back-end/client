import {
  Component,
  Inject,
  Injectable,
  OnInit,
  ViewChild,
  ElementRef
}                            from "@angular/core";
import {DOCUMENT}            from '@angular/common';
import {DataService}         from "../../../../_services/data.service";
import {UserService}         from "../../../../_services/user.service";
import {IMyDpOptions}        from "mydatepicker";
import {
  IMyDrpOptions,
  IMyDateRangeModel
}                            from 'mydaterangepicker';
import {CreatVacancyService} from "../create.service";
import {TranslateService}    from "@ngx-translate/core";
import {
  PageScrollConfig,
  PageScrollService,
  PageScrollInstance
}                            from 'ng2-page-scroll';
import emailMask             from 'text-mask-addons/dist/emailMask'
import {HelpersService}      from "../../../../_services/helpers.service";
import {ActivatedRoute}      from "@angular/router";
import {VacancyService}      from "../../../../_services/vacancy.service";

@Component({

  templateUrl: './manual.component.html',
  styleUrls:   ['./manual.component.scss']
})


@Injectable()
export class ManualComponent implements OnInit {
  private myDatePickerOptions: IMyDpOptions
  private myDatePickerOptionsstart: IMyDpOptions
  private myDatePickerOptionsend: IMyDpOptions
  private myDatePickerOptionsDeadline: IMyDpOptions = <IMyDpOptions>{};
          deadLine                                  = null
          optEnd                                    = null
          optStart                                  = null
  @ViewChild('creatDemo') creatDemo;
          emailMask                                 = emailMask;

  private myDateRangePickerOptions: IMyDrpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
    minYear:    new Date().getFullYear(),
    width:      '250px'
  };

  private model: Object                      = {
    date: {
      year:  new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day:   new Date().getDate()
    }
  };
  private rangeModel: any                    = {
    beginDate: {
      year:  new Date().getFullYear(),
      month: new Date().getMonth() + 1,
      day:   new Date().getDate()
    }
  };
  public salaryMask: Array<string | RegExp>  = [
    /[0-9]/,
    /[0-9]/,
    ',',
    /[0-9]/,
    /[0-9]/,
    /[0-9]/
  ]
  public salaryMask2: Array<string | RegExp> = [
    /[0-9]/,
    /[0-9]/,
    /[0-9]/,
    ',',
    /[0-9]/,
    /[0-9]/
  ]
  private message                            = {}
  public oldStep: any;
          arrLang                            = []
  @ViewChild('box')
  private box: ElementRef;
  public dates: any                          = {
    'contractDef':   '',
    'contractStart': '',
    'contractEnd':   '',
    'deadLine':      ''
  };

  constructor(private pageScrollService: PageScrollService,
              @Inject(DOCUMENT) private document: any,
              public dataService: DataService,
              public user: UserService,
              public cS: CreatVacancyService,
              public helpers: HelpersService,
              private route: ActivatedRoute,
              public vS: VacancyService,
              public translate: TranslateService) {
    var initId = false

    for (var i = 0; i < user.profile.Company.vacancy.length; i++) {
      if (user.profile.Company.vacancy[i]._id == route.snapshot.params['id']) {
        cS._id     = route.snapshot.params['id']
        //cS.resume = Object.assign({},user.profile.Student.resumes[i])
        cS.vacancy = JSON.parse(JSON.stringify(user.profile.Company.vacancy[i]));
        initId     = true

      }
    }
    if (route.snapshot.params['id'] == 'dublicat') {
      cS._id = undefined
      initId = true
    }

    if (!initId) {

      cS._id = undefined
      cS.clearVacancy()

    }
    PageScrollConfig.defaultScrollOffset = 150;
    PageScrollConfig.defaultDuration     = 500;
    if (!user.accessPackage.projBmark) {
      this.steps.splice(0, 1)
      this.stepActive = this.stepActive + 1
    }
    if (route.snapshot.params['step']) {

      this.stepActive = parseInt(route.snapshot.params['step'])

    }
    this.setStatus(user.accessPackage.projBmark ? this.stepActive : this.stepActive + 1)

    this.setDates()
  }

  setLang(id) {

    this.setArrLang()

  }

  setArrLang() {
    let curLang  = this.dataService.getOptionsFromId('languages', this.user.profile.currentLang).value
    this.arrLang = []
    for (let key in this.dataService.options.translate[curLang].languages) {
      this.arrLang.push({
        _id:  key,
        text: this.dataService.options.translate[curLang].languages[key]
      })
    }
  }

  parseFloatSalary(i) {
    let t = i.replace(/,/gi, '.')
    console.log("1= ", t)
    t = t.match(/[0-9\.]{0,6}[0-9]{0,2}/)
    console.log("2= ", t)
    t = parseFloat(t)
    console.log("3= ", t)
    return t


  }

  ngOnInit() {
    this.setDates();
    this.setArrLang();
    for (let i = 0; i < this.steps.length; i++) {
      this.verifyStep(i)
    }
    this.cS.isManual          = true
    this.cS.vacancy.type_temp = 1
    if (this.cS.vacancy.contract.start) {
      this.contract_start = 0
    }
    if (this.cS.vacancy.contract.end) {
      this.contract_end = 0
    }
    if (this.cS.vacancy.contract.def) {
      this.contract_date = 0
    }
    if (this.cS.vacancy.projectID && this.cS.pay && this.stepActive<2) {

      this.pageFlip(1)
    }
    if (this.cS.vacancy.aplicatCont) {
      this.aplicatCont = 0
    }
    if (this.cS.vacancy.vacancyUrl) {
      this.descriptionStep = 3
    }
    if (this.cS.vacancy.uploadFiles) {
      this.descriptionStep = 2
    }
    console.log("cS.vacancy.uploadFiles", this.cS.vacancy.uploadFiles)
  }

  setDatech(unix) {
    // this.cS.vacancy.deadLine=unix
  }

  onDateRangeChanged(event: IMyDateRangeModel) {
    console.log('onDateRangeChanged(): Begin date: ', event.beginDate, ' End date: ', event.endDate);
    console.log('onDateRangeChanged(): Formatted: ', event.formatted);
    this.cS.vacancy.contract.start = null
    this.cS.vacancy.contract.end   = null
    this.cS.vacancy.contract.def   = event.beginEpoc + '-' + event.endEpoc;
  }

  public goToStartStep(): void {
    console.log('set step');
    if (this.box && window.scrollY > this.box.nativeElement.offsetTop)
      setTimeout(() => {
        let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '.stepBox');
        this.pageScrollService.start(pageScrollInstance);
      }, 300, this)
  };

  setDate() {
    console.log('setDate');
  }

  submitForm(val) {
    console.log("FORM", val)
  }

  /*------STEP PAGING ------*/

  stepActive: number = 0
  steps              = [
    {
      name:     "CVstep.m0",
      status:   1,
      complete: false,
      step:     1
    },
    {
      name:     "CVstep.m1",
      status:   0,
      complete: false,
      step:     2
    },
    {
      name:     "CVstep.m2",
      status:   0,
      complete: false,
      step:     3
    },
    {
      name:     "CVstep.m3",
      status:   0,
      complete: false,
      step:     4
    },
    {
      name:     "CVstep.m4",
      status:   0,
      complete: false,
      step:     5
    },
    {
      name:     "CVstep.m5",
      status:   0,
      complete: false,
      step:     6
    },
    // {
    //   name:"CVstep.m6",
    //   status:0,
    //   complete:false,
    //   step:7
    // }
  ]

  pageFlip(i) {

    this.stepActive += i
    this.setStatus(this.cS.pay ? this.stepActive : this.stepActive + 1)
  }


  setStatus(i) {
    for (var key in this.steps) {
      if (this.steps[key].status < 2 && key <= i) {
        this.steps[key].status = 1
      }
      else if (this.steps[key].status == 2) {

      } else {
        this.steps[key].status = 0
      }
    }
    if (i != 0) {
      for (var s = 0; s < this.steps.length; s++) {
        this.verifyStep(s)
      }
      this.goToStartStep()
    }

  }


  /*------STEP 0 ------*/
  //projectList:any[]
  //nameNewProject:string;
  //PLdefauiltVal:any
  //salary_not_viwe:number = 0
  //renderNewProject(data){
  //  this.user.profile.Company.projects.push(data)
  //  this.setProjectList()
  //}
  //createNewProject(){
  //  this.dataService.sendws("User/projectInsert", {name:this.nameNewProject}).then(this.renderNewProject.bind(this));
  //  this.setProjectList()
  //  this.nameNewProject = ''
  //}
  //setProjectList(){
  //  this.projectList = new Array()
  //  for(var i=0;i<this.user.profile.Company.projects.length;i++){
  //    this.projectList.push({
  //      text:this.user.profile.Company.projects[i].name,
  //      id:this.user.profile.Company.projects[i]._id
  //    })
  //    this.PLdefauiltVal = this.projectList[i]
  //  }
  //}


  /*------STEP 1 ------*/

  Languages_required = [
    {
      languageID: undefined,
      levelID:    undefined
    }
  ]
  contract_date      = 1
  contract_start     = 1
  contract_end       = 1

  Languages_requiredPush() {
    if (!this.cS.vacancy.LanguagesRequired) {
      this.cS.vacancy.LanguagesRequired = []
    }
    if (this.cS.vacancy.LanguagesRequired.length) {
      let cansel = false;
      for (let i = 0; this.cS.vacancy.LanguagesRequired.length > i; i++) {
        let lang = this.cS.vacancy.LanguagesRequired[i];
        if (typeof lang.levelID === "undefined" ||
          typeof lang.languageID === "undefined" ||
          (!lang.levelID && lang.levelID != 0) ||
          (!lang.languageID && lang.languageID)
        ) {
          cansel = true;
          return true;
        }
      }
      if (cansel) return;
    }
    this.cS.vacancy.LanguagesRequired.push({
      languageID: undefined,
      levelID:    undefined
    })
  }

  Languages_requiredSplice(index) {
    this.cS.vacancy.LanguagesRequired.splice(index, 1)
  }

  many_years() {
    let ar = []
    for (var i = 0; i < 10; i++) {
      ar.push({
        id:   1,
        text: i.toString()
      })
    }
    return ar
  }

  /*------STEP 2 ------*/
  descriptionStep: number = 1
  uploadFiles             = []

  changeStep(i) {
    this.cS.vacancy.descriptionStep = i
    //this.uploadFiles = []
    //switch (i){
    //  case 1:
    //    this.cS.vacancy.uploadFiles = null
    //    this.cS.vacancy.vacancyUrl = null
    //    break;
    //  case 2:
    //
    //    this.cS.vacancy.vacancyUrl = null
    //
    //    break;
    //  case 3:
    //
    //    this.cS.vacancy.uploadFiles = null
    //    break;
    //}


  }

  renderList(e) {
    console.log(e)
  }

  setFiles(e) {
    //console.log("@@@@@@@@@@@@@@@@@@@@@@@@@")
    //console.log(e)
    //this.uploadFiles.push(e)
    //console.log(this.uploadFiles)
  }

  /*------STEP 3 ------*/
  aplicatCont     = 1
  salary_not_viwe = 0

  /*------STEP 4 ------*/
  showInfo = 1

  setDates() {
    if (this.cS.vacancy.contract.def) {
      this.dates.contractDef = this.dataService.convertPickeRangeDate(this.cS.vacancy.contract.def)
    } else {
      this.dates.contractDef = ''
    }

    if (this.cS.vacancy.contract.start) {
      this.dates.contractStart = this.dataService.convertPickeDate(this.cS.vacancy.contract.start)
    } else {
      this.dates.contractStart = ''
    }

    if (this.cS.vacancy.contract.end) {
      this.dates.contractEnd = this.dataService.convertPickeDate(this.cS.vacancy.contract.end)
    } else {
      this.dates.contractEnd = ''
    }

    if (this.cS.vacancy.deadLine) {
      this.dates.deadLine = this.dataService.convertPickeDate(this.cS.vacancy.deadLine)
    } else {
      this.dates.deadLine = ''
    }
    //let server_time = this.dataService.options.server_time
    var end_deadline = this.dataService.convertPickeDate(this.helpers.getUnixTimestamp() + 86400 * 30)
    if (this.cS.vacancy.published_at > this.helpers.getUnixTimestamp()) {
      end_deadline = this.dataService.convertPickeDate(this.cS.vacancy.published_at)
    }

    this.myDatePickerOptionsDeadline = <IMyDpOptions>{
      dateFormat:   'dd.mm.yyyy',
      width:        '150px',
      disableSince: end_deadline, //end
      disableUntil: this.dataService.convertPickeDate(this.helpers.getUnixTimestamp() - 86400), //start

    };

    this.deadLine                 = this.dataService.convertPickeDate(this.cS.vacancy.deadLine)
    this.myDatePickerOptions      = <IMyDpOptions>{
      // other options...
      dateFormat:   'dd.mm.yyyy',
      disableUntil: this.dataService.convertPickeDate(this.helpers.getUnixTimestamp() - 86400), //start
      width:        '150px'
    };
    this.myDatePickerOptionsstart = <IMyDpOptions>{
      // other options...
      dateFormat:   'dd.mm.yyyy',
      disableUntil: this.dataService.convertPickeDate(this.helpers.getUnixTimestamp() - 86400), //start
      width:        '150px'
    };
    this.myDatePickerOptionsend   = <IMyDpOptions>{
      // other options...
      dateFormat:   'dd.mm.yyyy',
      disableUntil: this.dataService.convertPickeDate(this.helpers.getUnixTimestamp() - 86400), //end
      width:        '150px'
    };

  }

  setDeadline(u) {
    this.deadLine = this.dataService.convertPickeDate(u)
  }

  setOptStart(u) {
    this.dates.contractStart = this.dataService.convertPickeDate(u)
    this.cS.vacancy.contract.start = u
  }

  setOptEnd(u) {
    this.dates.contractEnd = this.dataService.convertPickeDate(u)

    this.cS.vacancy.contract.end = u
  }

  addFile(e) {
    console.log(e)
  }

  parseInt(n) {
    return parseInt(n)
  }

  verifyStep(step) {
    var verStep = step
    if (!this.user.accessPackage.projBmark) {
      verStep += 1
    }
    var r = false
    var s = true

    switch (verStep) {
      case 0:

        s = true

        break;
      case 1:

        let valid = true


        s ? s = this.helpers.isValidObject('languageID',this.cS.vacancy) : null
        s ? s = this.helpers.isValidObject('positionTitle',this.cS.vacancy) : null
        s ? s = this.helpers.isValidObject('fieldActivity',this.cS.vacancy) : null
        s ? s = this.helpers.isValidObject('type',this.cS.vacancy) : null
        s ? s = this.helpers.isValidObject('occupationField',this.cS.vacancy) : null
        s ? s = this.helpers.isValidObject('Worktime',this.cS.vacancy) : null
        s ? s = this.helpers.isValidObject('deadLine',this.cS.vacancy) : null




        /**
         * В создании вакансии убрать из числа обязательных к заполнению данных следующие:
         * 1. Поле требуемые языки. Сейчас требуется ввести минимум один язык. http://joxi.ru/eAOEMXRfxaNKKm
         */
        // this.Languages_required.length > 0 ? null : s = false;

        break;
      case 2:


        (this.cS.vacancy.descriptionStep == 1 && this.cS.vacancy.roleTasks && this.cS.vacancy.roleTasks != '')
        || (this.cS.vacancy.descriptionStep == 2 && this.cS.vacancy.uploadFiles && Object.keys(this.cS.vacancy.uploadFiles).length > 0)
        || (this.cS.vacancy.descriptionStep == 3 && this.cS.vacancy.vacancyUrl && this.cS.vacancy.vacancyUrl != '') ? null : s = false

        break;
      case 3:
        (this.aplicatCont == 0 && this.cS.vacancy.aplicatCont)
        || (this.aplicatCont == 1 && this.cS.vacancy.aplicatCont == null) ? null : s = false


        break;
      case 4:
        // this.cS.vacancy.showInfo == 0
        // || (this.cS.vacancy.showInfo == 1 && this.cS.vacancy.contactEmail) ? null : s = false


        break;
    }
    if (r && s == false && this.steps.length >= step) {
      this.steps[step].status = 1
    }
    if (r && s) {
      this.steps[step].status = 2
    }
    this.steps[step].complete = s
    return r;
  }

}
