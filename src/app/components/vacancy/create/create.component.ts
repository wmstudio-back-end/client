import {Component, OnInit} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {CreatVacancyService} from "./create.service";
import {UserService} from "../../../_services/user.service";
import {ActivatedRoute} from "@angular/router";
@Component({
  selector: 'app-add-vacancy',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  constructor(
    public component: ComponentService,
    public cS: CreatVacancyService,
    public uS:UserService,
    private route: ActivatedRoute,

  ) {
    var initId = false

    for(var i=0;i<uS.profile.Company.vacancy.length;i++){
     if (uS.profile.Company.vacancy[i]._id==route.snapshot.params['id']){
       cS._id = route.snapshot.params['id']
       //cS.resume = Object.assign({},user.profile.Student.resumes[i])
       cS.vacancy = JSON.parse(JSON.stringify(uS.profile.Company.vacancy[i]));
       initId = true
     }
    }
    if (route.snapshot.params['id']=='dublicat'){
      initId = false
    }
    if (!initId){
     cS._id = undefined

    }
  }

  ngOnInit() {
    var initId = false



  }

}
