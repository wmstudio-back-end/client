import {Injectable} from "@angular/core";
import {ActivInstruction, IVacancy} from "../../../_models/vacancy";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
import {LocationService} from "../../../_services/location.service";
import {Router} from "@angular/router";
import {VacancyService} from "../../../_services/vacancy.service";
import {HelpersService} from "../../../_services/helpers.service";
import {DownPopupService} from "../../../_services/down-popup.service";

@Injectable()
export class CreatVacancyService {
  public pay = true
  isManual = false
  _id = null
  validationForms = false
  invalidFormVal = true
  vacancy = <IVacancy>{};
  dublicate = false
  arrPosition = []

  constructor(public dataService: DataService,
              public userService: UserService,
              public locationService: LocationService,
              public router: Router,
              public popUp: DownPopupService,
              public vS: VacancyService,
              public helpers: HelpersService) {
    this.pay = userService.accessPackage.projBmark
    this.clearVacancy()


  }

  getData(e, i) {
    e = this.helpers.verifyLength(50, e)
    if (i == 0) {
      this.vacancy.positionTitle = e
      this.dataService.sendws('Resume/searchPositionFromName', {position: e}).then(data => {
        this.arrPosition = data['position']
      })
    }
    if (i == 1) {

      this.vacancy.positionTitle = e
    }

  }


  creatDemo() {
    this.vacancy = Object.assign(this.vacancy,{
      type_temp: 1,
      proof: 0,

      published_at: 0,
      edit: true,
      Previous_work: false,
      notify: false,
      LanguagesRequired: [
        {
          languageID: 0,
          levelID: 0
        }
      ],
      keySkills: [],
      deleted_at: 0,
      type: 0,
      contract: {},
      salary: {},
      proofUploadFiles: [],
      descriptionStep: 1,
      showInfo: 1,
      publish: false,
      messages: true,
      positionTitle: "Биржевой брокер",
      languageID: 24,
      fieldActivity: 0,
      occupationField: 0,
      minEducation: 0,
      Worktime: [
        0,
        1
      ],
      logInstruction:[],
      activInstruction:{
        premiumListing:{values:0},
        hightFrame:{values:0},
        insocialMedia:{values:0},
        targetGroup:{values:0},
        bannerofEmail:{values:0}},
      address: "Москва, Россия",
      scholarshipOffer: [0],
      deadLine: this.dataService.options.server_time,
      shotDescription: "прпрпрпрп",
      contactEmail: "poc@poc.ru",
      rating: 0,
      submitted_at: this.dataService.options.server_time
    })
  }

  public clearVacancy() {

    this.vacancy = <IVacancy>{
      type_temp: 0,
      proof: 0,
      published_at: 0,
      edit: true,
      Previous_work: false,
      notify: true,
      LanguagesRequired: [{languageID: null, levelID: null}],
      keySkills: [],
      deleted_at: 0,
      Desired_personality_traits:[],
      type: null,
      contract: {},
      salary: {
        type: undefined,
        summ: undefined,
        summ2: undefined
      },

      proofUploadFiles: [],
      descriptionStep: 1,
      showInfo: 1,
      Worktime:[],
      publish: false,
      messages: true,
      logInstruction:[],
      activInstruction:{
        premiumListing:{values:0},
        hightFrame:{values:0},
        insocialMedia:{values:0},
        targetGroup:{values:0},
        bannerofEmail:{values:0}
      }

    };
    //this.vacancy.LanguagesRequired = [{languageID:null,levelID:null}]

  }

  invalidForm(e) {
    this.invalidFormVal = e
    console.log("invalidForm", e)
  }

  deleteVacancy(_id, i) {
    if (this.userService.accessRight > 1) {
      this.helpers.alert(2)
      // alert('NOT access')
      return
    }
    if (this.helpers.confirm(0)){
      this.dataService.sendws('Vacancy/deleteVacancy', {_id: _id}).then((data) => {
        for (var j = 0; j < this.userService.profile.Company.vacancy.length; j++) {
          if (this.userService.profile.Company.vacancy[j]._id == _id) {
            this.userService.profile.Company.vacancy.splice(j, 1)
            break
          }


        }

      })
    }
  }

  isValid(str) {

    var r = false
    if (str.indexOf('.') > -1) {
      var t = str.split('.')

      if (this.vacancy.hasOwnProperty(t[0]) && this.vacancy[t[0]].hasOwnProperty(t[1]) && Object.keys(this.vacancy[t[0]][t[1]]).length > 0) {
        r = true
      }
    }
    switch (typeof this.vacancy[str]) {
      case 'string':
        if (this.vacancy.hasOwnProperty(str) && this.vacancy[str] != "") {
          r = true
        }
        break;
      case 'number':
        if (this.vacancy.hasOwnProperty(str)) {
          r = true
        }
        break;
      case 'object':

        if (this.vacancy.hasOwnProperty(str) && this.vacancy[str] && Object.keys(this.vacancy[str]).length > 0) {
          r = true
        }


        break;


    }
    return r

  }
  saveVacancy() {

    this.validationForms = true
    this.vS.changePublish(this.vacancy.publish,this.vacancy,function(data){
      console.log('================',data)
      if (data){
        this.validationForms = false
        this.clearVacancy()
        this.router.navigate(['/vacancy/list']);
      }

    }.bind(this))

  }
  saveVacancy2() {

    if (this.userService.accessRight==3){
      this.popUp.show_info(this.dataService.getOptionsFromId('errors',28).text)
      return
    }
    if (
      (this.vacancy.publish)
        &&(!this.userService.profile.service
        ||!this.userService.profile.service.active
        ||!this.userService.profile.service.active.packageId)
    ){

      this.popUp.show_info(this.dataService.getOptionsFromId('errors',49).text)
      return
    }
    this.validationForms = true
    let valid = true
    valid ? valid = this.isValid('languageID') : null
    valid ? valid = this.isValid('positionTitle') : null
    valid ? valid = this.isValid('fieldActivity') : null
    valid ? valid = this.isValid('type') : null
    valid ? valid = this.isValid('occupationField') : null
    if (this.isManual) {
      valid ? valid = this.isValid('LanguagesRequired') : null
      valid ? valid = this.isValid('deadLine') : null
      valid ? valid = this.vacancy.showInfo == 1 ? this.isValid('contactEmail') : true : null
      valid ? valid =
        (this.vacancy.descriptionStep == 1 && this.isValid('roleTasks'))
        || (this.vacancy.descriptionStep == 2 && this.isValid('uploadFiles'))
        || (this.vacancy.descriptionStep == 3 && this.isValid('vacancyUrl') )
        : null
    }


    var pub = this.vacancy.publish
  if (!this.vacancy.publish&&this.vacancy.proof==1){
    this.vacancy.proof = 0
  }
    if (this.vacancy.publish){
      if (this.vacancy.deadLine<new Date().getTime()){
        valid = false
        this.popUp.show_success(this.dataService.getOptionsFromId('errors', 21).text);
      }
    }

    if (valid) {
      this.validationForms = false
      //if (this._id){
      this.dataService.sendws('Vacancy/updateVacancy', {vacancy:this.vacancy}).then((data) => {


        if (this.userService.profile.type_profile == 'Admin') {

          this.clearVacancy()
          this.router.navigate(['/admin/vacancy/proof']);
          return
        }
        var isNewVacancy = true
        if (data['vacancy']) {
          for (var i = 0; i < this.userService.profile.Company.vacancy.length; i++) {
            if (this.vacancy._id == this.userService.profile.Company.vacancy[i]._id) {
              isNewVacancy = false
              if (data['vacancy'].proof == 1 && data['vacancy'].publish == true&& data['vacancy'].type_temp == 1) {
                this.popUp.show_success(this.dataService.getOptionsFromId('errors', 17).text);
              }
              if (data['vacancy'].proof == 2 && data['vacancy'].publish == true) {
                this.popUp.show_success(this.dataService.getOptionsFromId('errors', 18).text);
              }
              if (data['vacancy'].proof == 2 && data['vacancy'].publish == false) {
                this.popUp.show_success(this.dataService.getOptionsFromId('errors', 19).text);
              }
              if (data['vacancy'].proof == 1 && data['vacancy'].publish == true && data['vacancy'].type_temp == 2) {
                this.popUp.show_success(this.dataService.getOptionsFromId('errors', 52).text);
              }
              this.userService.profile.Company.vacancy[i] = data['vacancy']

              break;
            }
          }
          if (isNewVacancy) {
            this.userService.profile.Company.vacancy.push(data['vacancy'])

          }
          this.clearVacancy()
          this.router.navigate(['/vacancy/list']);
        }
        if (data['error']) {
          alert(data['error'].message)
          console.log(data['error'])
        }
        if (data['profile']) {
          this.userService.profile.service.active = data['profile']
        }
      })
      //}
      //else{
      //
      //  this.dataService.sendws('Vacancy/createNew',this.vacancy).then((data)=>{
      //    if(this.userService.profile.type_profile=='Admin'){
      //
      //      this.clearVacancy()
      //      this.router.navigate(['/admin/vacancy/proof']);
      //      return
      //    }
      //    if (data['vacancy']){
      //      if (data['vacancy'].proof==1){
      //        this.popUp.show_success(this.dataService.getOptionsFromId('errors',17).text);
      //      }
      //      this.userService.profile.Company.vacancy.push(data['vacancy'])
      //      //if (needPublish&&this.userService.profile.type_profile=='Company'){
      //      //  this.vS.changePublish(true,this.vacancy._id)
      //      //}
      //      this.clearVacancy()
      //      this.router.navigate(['/vacancy/list']);
      //    }
      //
      //
      //
      //  })
      //}

    } else {
      this.popUp.show_info(this.dataService.getOptionsFromId('errors', 11).text);
    }


  }
}
