import {AfterViewChecked, Component, ElementRef, OnInit, Pipe, ViewChild} from "@angular/core";
import {ComponentService} from "../../_services/component.service";
import {UserService} from "../../_services/user.service";
import {DataService} from "../../_services/data.service";
import {MessageService} from "../../_services/message.service";

@Component({
    selector: 'app-messages',
    templateUrl: './messages.component.html',
    styleUrls: ['./messages.component.scss'],

})
@Pipe({name: 'keys'})
export class MessagesComponent implements OnInit, AfterViewChecked {
    message: string = ''
    dialogId: string = '591aeb2ac080fb6cbff0c022'
    messages = []
    initPage: boolean = false
    searchMessages: string = ''
    keysGetter = Object.keys
    dialogs = []
    toggleChat: boolean = false;
    @ViewChild('chatContnet') private listContainer: ElementRef;
    uploadFile: any = []
    typeView = [{text:'Show ALL',id:0},{text:'Show unread',id:1},{text:'Show blocked',id:2}]
    constructor(public component: ComponentService,
                public uS: UserService,
                public dS: DataService,
                public mS: MessageService,) {
    }

    transform(value, args: string[]): any {
        let keys = [];
        for (let key in value) {
            keys.push({key: key, value: value[key]});
        }
        return keys;
    }

    findText(e) {
        if (e.keyCode == 13) {
            // this.mS.findMessageText(this.searchMessages).then(data => {
            //  })
        }
    }

    setSingleFile(e) {

        this.uploadFile[0] = {
            name: e.name,
            url: '//' + this.dS.HOST_STATIC + ':' + this.dS.PORT_STATIC + e.path
        }

    }

    onScrollEvent($event) {
        if ($event) {
            //let tracker = event.target;
            //let limit = tracker['scrollHeight'] - tracker['clientHeight'];
            //console.log(this.listContainer.nativeElement.scrollTop);
            // if (this.listContainer.nativeElement.scrollTop == 0) {
            //     var t = this.listContainer.nativeElement.scrollHeight
            //
            // }
        }
        return true
    }
  getDialogs(){
      return Object.keys(this.mS.dialogs)
  }
    ngOnInit() {

        //this.dialogs = Object.keys(this.mS.dialogs)

      //this.dialogs.sort()

        //this.initDialog(this.mS.dialogActive,0,20)
        //this.scrollToBottom();
    }

    ngAfterViewChecked() {
        if (this.mS.dialogs[this.mS.dialogActive] && !this.initPage) {
            this.initPage = true
            this.scrollToBottom();
        }
        //if (this.listContainer.nativeElement){
        //  this.scrollToBottom();
        //  console.log("+++++++++++++++++++++++",this.listContainer.nativeElement.scrollTop )
        //}
        //this.scrollToBottom();
    }

    initDialog(id, skip, limit) {
        console.log(id)
        //this.mS.initDialog(id,skip,limit).then(data=>{
        //  console.log("this.mS.dialogs[id].user",this.mS.dialogs[id].user)
        //  //this.countNew
        //  //this.messages = this.mS.dialogs[id].messages
        //  //this.messages.splice(0,skip)
        //  //this.messages.splice(20,limit)
        //
        //})
        //this.dialogId = id
        //console.log("DIALOG ID" , id)
        //this.DialogList = []
        //this.dS.sendws('Message/getMessage',{dialog_id:id}).then(data=>{
        //  console.log('Message/getMessage',data)
        //  this.DialogList = data['messages']
        //  this.scrollToBottom()
        //})
    }

    send(e) {
        if (!this.mS.dialogs[this.mS.dialogActive]) {
            return
        }

        if (!e || (e && e.keyCode == 13)) {
            this.dS.sendws('Message/setMessage', {
                receiver: this.mS.dialogActive,
                text: this.message,
                file: this.uploadFile
            }).then(data => {
                this.mS.setMessage(data)
                this.scrollToBottom()
                this.uploadFile = []


            })
            this.message = ''
        }


    }

    private scrollToBottom(): void {

        setTimeout(function () {
            try {
                this.listContainer.nativeElement.scrollTop = this.listContainer.nativeElement.scrollHeight;
            } catch (err) {
                //console.log(err)
            }
        }.bind(this), 1)
        //this.listContainer.nativeElement.scrollTop = this.listContainer.nativeElement.scrollHeight;

    }

}
