import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import {HelpersService} from "../../../_services/helpers.service";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
import {CreatVacancyService} from "../../vacancy/create/create.service";
import {ShopService} from "../../../_services/shop.service";

@Component({
  selector: 'app-services-instructions-promo',
  templateUrl: './services-instructions-promo.component.html',
  styleUrls: ['./services-instructions-promo.component.css']
})
export class ServicesInstructionsPromoComponent implements OnInit {
  @Input() vacancy
  @Output() onclose = new EventEmitter<boolean>()
  constructor(
    public dataService:DataService,
    public cS:CreatVacancyService,
    public uS:UserService,
    public hS:HelpersService,
    public shopService:ShopService,
  ) { }
  promotion = [
    {
      type:'premiumListing',
      text:'promotions.prem_listing',
      text_h:'promotions.prem_listing_h',
      active:false,
      applied:0,
      price:0,
      available:0,
      available_i:'promodays',
      difference:[],
      until:'',
      addinstructions:false,
      detailsShow:false,

      details:[]
    },
    {
      type:'hightFrame',
      text:'promotions.frame',
      text_h:'promotions.frame_h',
      active:false,
      applied:0,
      price:0,
      available:0,
      available_i:'promodays',
      difference:[],
      until:'',
      addinstructions:false,
      detailsShow:false,

      details:[]
    },
    // {
    //   type:'insocialMedia',
    //   text:'promotions.isSocialMedia',
    //   text_h:'promotions.isSocialMedia_h',
    //   active:false,
    //   applied:0,
    //   price:0,
    //   available:0,
    //   available_i:'ps',
    //   difference:[],
    //   until:'',
    //   addinstructions:true,
    //   detailsShow:false,
    //
    //   details:[]
    // },{
    //   type:'targetGroup',
    //   text:'promotions.edvtarg',
    //   text_h:'promotions.edvtarg_h',
    //   active:false,
    //   applied:0,
    //   price:0,
    //   available:0,
    //   available_i:'ps',
    //   difference:[],
    //   until:'',
    //   addinstructions:true,
    //   detailsShow:false,
    //
    //   details:[]
    // },{
    //   type:'bannerofEmail',
    //   text:'promotions.banner',
    //   text_h:'promotions.banner_h',
    //   active:false,
    //   applied:0,
    //   price:0,
    //   available:0,
    //   available_i:'ps',
    //   difference:[],
    //   until:'',
    //   addinstructions:true,
    //   detailsShow:false,
    //   details:[
    //
    //   ]
    // },
  ]
  table_1 = []
  table_2 = []
  ngOnInit() {
    //if (this.uS.profile.service&&this.uS.profile.service.active&&this.uS.profile.service.active.orderId){
      for (var i = 0; i < this.promotion.length; i++) {
        this.promotion[i].available = this.uS.profile.service.active[this.promotion[i]['type']]||0;

        // this.promotion[i].applied = this.vacancy['PROM'+this.promotion[i]['type']+'Val']||0
        for (var j = 0; j < this.promotion[i].available; j++) {
          var dayTodown = 1
          this.promotion[i].available_i=='ps'?dayTodown=10:dayTodown=1;
          if (
            (this.hS.getUnixTimestamp()+(86400*(j))<this.vacancy.deadLine
              &&this.hS.getUnixTimestamp()+(86400*(j))<this.vacancy.published_at
              &&this.promotion[i].available_i!='ps')
            ||(
              (
                // this.hS.getUnixTimestamp()+(86400*(j))<this.vacancy.deadLine
                // &&this.hS.getUnixTimestamp()+(86400*(j))<this.vacancy.published_at
                this.promotion[i].available_i=='ps'

                &&this.vacancy.deadLine-(86400*10)>this.hS.getUnixTimestamp()
                &&this.vacancy.published_at-(86400*10)>this.hS.getUnixTimestamp()
              )
            )
          //
          )
          {
            this.promotion[i].difference.push({_id:j+1,text:j+1});
          }
          // this.promotion[i].difference.push({id:j+1,text:j+1});
        }
        for (var j = 0; j < this.vacancy.logInstruction.length; j++) {
          if (this.vacancy.logInstruction[j].type==this.promotion[i]['type']){
            this.promotion[i].details.push(this.vacancy.logInstruction[j])
          }

        }
        console.log(this.promotion[i])
        if (this.vacancy.proof==2) {
          switch (this.promotion[i]['type']) {
            case 'premiumListing':
              // this.promotion[i].applied = Math.ceil((this.hS.getUnixTimestamp()-this.promotion[i].applied)/86400)*-1||0
              this.vacancy['PROM' + this.promotion[i]['type']] ? this.promotion[i].until = this.convertUntil(this.vacancy['PROM' + this.promotion[i]['type']]) : null
              break;
            case 'hightFrame':
              this.vacancy['PROM' + this.promotion[i]['type']] ? this.promotion[i].until = this.convertUntil(this.vacancy['PROM' + this.promotion[i]['type']]) : null
              break;
            default:
              break
          }
        }
        // this.promotion[i].applied>0?this.promotion[i].active=true:null
        this.promotion[i].price = this.shopService.getPackage(this.shopService.cart.subscriptions.packageId).settings['price'+(this.promotion[i]['type'].charAt(0).toUpperCase())+this.promotion[i]['type'].substr(1)]
      }
      for (var i = 0; i < this.promotion.length; i++) {
        if (this.promotion[i].available>0){
          this.table_1.push(this.promotion[i])
        }else{
          this.table_2.push(this.promotion[i])
        }
      }
    //}
    console.log(this.vacancy)
  }
  summValues(kl){
    var t=0
    for (var i = 0; i < kl.length; i++) {
      t+=kl[i].instruction.values

    }

    return t
  }
  convertUntil(time){
    var t = new Date(time*1000)
    var m = t.getMonth()<10?"0"+(t.getMonth()+1).toString():(t.getMonth()+1).toString()
    var d = t.getDate()<10?"0"+t.getDate().toString():t.getDate().toString()
    return d+'/'+m
  }
}
