import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesInstructionsPromoComponent } from './services-instructions-promo.component';

describe('ServicesInstructionsPromoComponent', () => {
  let component: ServicesInstructionsPromoComponent;
  let fixture: ComponentFixture<ServicesInstructionsPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesInstructionsPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesInstructionsPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
