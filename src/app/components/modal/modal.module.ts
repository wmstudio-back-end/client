import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestRecommendationComponent } from './request-recommendation/request-recommendation.component';
import {ModalDynamic} from "./modal-dynamic";
import { DddComponent } from './ddd/ddd.component';

import {ModulesModule} from "../../modules/modules.module";
import {ModalService} from "../../_services/modal.service";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import { SaveSearchComponent } from './save-search/save-search.component';
import { ServicesAddInstructionsComponent } from './services-add-instructions/services-add-instructions.component';
import { ServicesInstructionsPromoComponent } from './services-instructions-promo/services-instructions-promo.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule,
    FormsModule,
    ModulesModule,
    CommonModule,



  ],
  declarations: [
    ModalDynamic,
    RequestRecommendationComponent,
    DddComponent,
    SaveSearchComponent,
    ServicesAddInstructionsComponent,
    ServicesInstructionsPromoComponent,



  ],
  exports: [
    ModalDynamic,
    RequestRecommendationComponent,
    DddComponent,
    SaveSearchComponent,
    ServicesAddInstructionsComponent,
    ServicesInstructionsPromoComponent,


  ],

})
export class ModalModule { }
