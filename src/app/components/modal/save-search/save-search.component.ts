import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SaveSearch} from "../../../_models/saveSearch";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
import {CompanyProjects} from "../../../_models/user";
import {DownPopupService} from "../../../_services/down-popup.service";
import {VacancyService} from "../../../_services/vacancy.service";

@Component({
  selector: 'modal-save-search',
  templateUrl: './save-search.component.html',
  styleUrls: ['./save-search.component.scss']
})

export class SaveSearchComponent implements OnInit {
  @Input() show:boolean
  @Input() type:string
  @Input() options:SaveSearch
  public errorName:boolean=false
  @Output('out') out = new EventEmitter<any>()
  @Output('showModal') showModal = new EventEmitter<boolean>()
  period:any={}
  projects:any=[]
  curProject:any={}
  newProject=<CompanyProjects>{}
  change:any={0:'project'}
  projectId=null

  constructor(public dataService:DataService,public uS:UserService,public popUp:DownPopupService,public vS:VacancyService) {
  }

  getProjects(){
    if (this.type!='resume') return
    if (!this.uS.profile.Company.projects.length) return
    let projects = []
    for (let i = 0; i < this.uS.profile.Company.projects.length; i++) {
      let project = this.uS.profile.Company.projects[i]
      projects.push({id:projects.length,value:project._id,text:project.name})
    }
    this.projects = projects
    if (!this.options.project) return
    let projectIndex = this.projects.findIndex((e,i,a)=>{return e.value==this.options.project?true:false})
    console.log('projectIndex',projectIndex)
    if (projectIndex == -1) return
    this.projectId = projectIndex
  }

  shadowBody() {
    let body = document.getElementsByTagName('body')[0]
    if(this.show)
      body.classList.add('blackFon')
    else
      body.classList.remove('blackFon')
  }

  ngOnInit() {
    // this.vS.saveSearch = <SaveSearch>{}
    this.getProjects()
    this.period[this.options.period] = true
  }

  ngOnChanges(changes:any){
    this.getProjects()
    this.period[this.options.period] = true
    if (!changes.show) return;
      if (changes.show.currentValue == true) {
        this.shadowBody()
      }
  }

  checkName(){
    if (!this.options.name.trim()) {
      this.errorName = true
      return false;
    }
    this.errorName = false
    return true;
  }

  save(){

    if (!this.checkName()) return false
    if (this.newProject.name)
      return this.createProject()
    this.out.emit(this.options)
    this.show = false
    this.shadowBody()
    this.showModal.emit(false)
  }

  createProject(){
    console.log(this.newProject)
    this.dataService.sendws("Vacancy/createNewProject", this.newProject).then((data)=>{
      console.log(data)
      let project:CompanyProjects = data['project']
      this.uS.profile.Company.projects.push(project)
      this.options.project = project._id
      this.getProjects()
      this.newProject = <CompanyProjects>{}
      this.out.emit(this.options)
      this.show = false
      this.shadowBody()
      this.showModal.emit(false)
      this.change = {0:'project'}
    })
  }

  closePopup(){
    this.show = false
    this.shadowBody()
    this.showModal.emit(false)
  }

}
