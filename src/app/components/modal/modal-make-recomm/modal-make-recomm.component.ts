import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
import {Reviews} from "../../../_models/reviews";
import {IRequestRecommendation} from "../../../_models/request-recommendation";
import {DownPopupService} from "../../../_services/down-popup.service";


@Component({
  selector:   'modal-make-recomm',
  templateUrl:'./modal-make-recomm.component.html',
  styleUrls:  ['./modal-make-recomm.component.scss']
})
export class ModalMakeRecommComponent implements OnInit {
  @Input() openModal:boolean = false;
  @Input() template:string = 'send';
  @Input() schoolId:string;
  @Input() studentId:string;


  @Input() activeRec:any;
  @Output() ropenModal = new EventEmitter<boolean>();
  @Output() updateRec = new EventEmitter<any>();
  @Output() status = new EventEmitter<string>();
  rateObj = <IRequestRecommendation>{
    comment_recommend:true
}
  constructor(public ds:DataService, public uS:UserService,public popS:DownPopupService) { }
  updateAll(){
    this.rateObj = <IRequestRecommendation>{
      comment_recommend:true
    }
  }
  ngOnChanges(e){

    if (e.openModal){
      this.updateAll()
    }

    if (e.activeRec){

      // this.rateObj.comment_recommend = true

       this.getRecommendation()

    }
  }
  ngOnInit() {

    this.rateObj.comment_recommend = true

  }

  closeModal() {
    this.ropenModal.emit(false)
  }


  createNewRate() {
    var add = true
    if(this.schoolId){
      this.rateObj.schoolId = this.schoolId
    }
    if(this.studentId){
      this.rateObj.uuid = this.studentId
    }
    this.rateObj['comment_recommend']==true||this.rateObj['comment_recommend']==false?null:add = false
    this.rateObj['rating_poise']?null:add = false
    this.rateObj['rating_dilligance']?null:add = false
    this.rateObj['rating_sociability']?null:add = false
    if (!add){
      this.popS.show_success(this.ds.getOptionsFromId('errors',23).text)

      return
    }
    this.rateObj.req = 1
    if (this.rateObj._id){
      this.rateObj.status = 1
      this.activeRec.status = 1
      this.activeRec = this.rateObj
      this.ds.sendws("Recommendation/verifyRecommendation", this.rateObj).then( (data)=> {
        this.updateRec.emit(this.activeRec)

      });


    }else{

      this.ds.sendws("Recommendation/createVerifyRecommendation", this.rateObj).then( (data)=> {
        this.uS.profile.recommendations.push(data['res'])
        this.updateRec.emit(data['res'])
        if (data['res'].uuid){
          this.studentId = data['res'].uuid
        }
        this.rateObj = <IRequestRecommendation>data['res']
      });
    }
    this.closeModal()

  }


  getRecommendation() {
    if (!this.activeRec||!this.activeRec._id){return}
    this.updateAll()
  var q = {
    _id:this.activeRec._id
  }
  // if (this.sId){
  //   q['schoolId'] = this.sId
  // }
    this.ds.sendws("Recommendation/getRecommendation", q).then((data)=> {
      if (data['res']){
        this.rateObj = <IRequestRecommendation>data['res']
        if (!data['res'].hasOwnProperty('comment_recommend')){

          this.rateObj.comment_recommend = true
        }
        if (data['res'].uuid){
          console.log('!!!!!!!!!!!!!!!!!')
          this.studentId = data['res'].uuid
        }
      }


      //if (data['res'].length>0){
      //  console.log("neeeeeeee", data);
      //  this.rateObj = <Reviews>data['res'][0]
      //}

    });




  }

}
