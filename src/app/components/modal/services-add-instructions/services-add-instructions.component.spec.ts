import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicesAddInstructionsComponent } from './services-add-instructions.component';

describe('ServicesAddInstructionsComponent', () => {
  let component: ServicesAddInstructionsComponent;
  let fixture: ComponentFixture<ServicesAddInstructionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicesAddInstructionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicesAddInstructionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
