import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Items} from "../../../modules/multiselect-autocopmplete-area/multiselect-autocopmplete-area.component";

@Component({
  selector: 'app-services-add-instructions',
  templateUrl: './services-add-instructions.component.html',
  styleUrls: ['./services-add-instructions.component.css']
})
export class ServicesAddInstructionsComponent implements OnInit {
  @Input() show:boolean
  @Input() outValue:any
  @Input() text:string
  @Input() files:any[]

  @Output() close = new EventEmitter<boolean>();
  @Output() output = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    if (!this.text){this.text = ''}
    if (!this.files){this.files = []}
    console.log("TEXT",this.text)
    console.log("files",this.files)
  }
  ngOnChanges(changes: any) {


    if (changes.show) {
      this.ngOnInit()
    }


  }
  setMultiFile(data){
    this.files.unshift(data)

  }
  save(){
    this.output.emit({
      outValue:this.outValue,
      text:this.text,
      files:this.files
    })
    this.close.emit(false)
  }
}
