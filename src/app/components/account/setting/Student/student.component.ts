import {Component, HostListener, OnInit} from '@angular/core';
import {Router} from "@angular/router";


import {UserService} from '../../../../_services/user.service';
import {DataService} from "app/_services";



@Component({
    moduleId: module.id,
    styleUrls: ['../setting.component.scss', './student.component.scss'],
    templateUrl: './student.component.html'

})
export class SettingStudentComponent implements OnInit {
    openModal:boolean = false;
  model = {
    options:true
  }
  yourPass:string
    constructor(public user: UserService,public dS:DataService) { }
  @HostListener('change', ['$event'])
  elementClicked(event) {
    if (!this.user.profile.Settings[event.target.name]){
      this.user.profile.Settings[event.target.name] = true
    }
    event.target.value=='true'?this.user.profile.Settings[event.target.name] = true:this.user.profile.Settings[event.target.name] = false
    this.user.editProfile({Settings:this.user.profile.Settings})
  }
  deactivateAcc(){
    if (this.user.profile.password==this.yourPass){
      this.dS.sendws('Admin/deactivateAccount',{yourPass:this.yourPass}).then(data=>{

      })
    }
    }

    ngOnInit() {

    }


}
