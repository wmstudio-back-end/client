import {Component, HostListener, OnInit} from "@angular/core";
import {UserService} from '../../../../_services/user.service';
import {Settings} from "../../../../_models/user";


@Component({
  moduleId: module.id,
  styleUrls: ['../setting.component.scss', './educator.component.scss'],
  templateUrl: './educator.component.html',

})
export class SettingEducatorComponent implements OnInit {
  openModal:boolean = false;

  constructor(public user: UserService) { }
  @HostListener('change', ['$event'])
  elementClicked(event) {
    if (!this.user.profile.Settings[event.target.name]){
      this.user.profile.Settings[event.target.name] = true
    }
    event.target.value=='true'?this.user.profile.Settings[event.target.name] = true:this.user.profile.Settings[event.target.name] = false
    this.user.editProfile({Settings:this.user.profile.Settings})
  }
  ngOnInit() {

    if (!this.user.profile.Settings){
      this.user.profile.Settings =  <Settings> {
        contact_info:false,
        news:true
      }
    }

  }


}
