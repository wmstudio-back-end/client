import {Component, HostListener, OnInit} from "@angular/core";


import {UserService} from '../../../../_services/user.service';
import {DataService} from "../../../../_services/data.service";


@Component({
  moduleId: module.id,

  styleUrls: ['../setting.component.scss', './company.component.scss'],
  templateUrl: './company.component.html',
})
export class SettingCompanyComponent implements OnInit {
  openModal:boolean = false;
  yourPass:string
  constructor(public user: UserService,public dS:DataService) { }
  @HostListener('change', ['$event'])
  elementClicked(event) {
    if (!this.user.profile.Settings[event.target.name]){
      this.user.profile.Settings[event.target.name] = true
    }
    event.target.value=='true'?this.user.profile.Settings[event.target.name] = true:this.user.profile.Settings[event.target.name] = false
    this.user.editProfile({Settings:this.user.profile.Settings})
  }

  ngOnInit() {

  }


}
