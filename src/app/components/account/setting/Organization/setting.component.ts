import {Component, HostListener, OnInit} from "@angular/core";


import {UserService} from '../../../../_services/user.service';

@Component({
  moduleId: module.id,

  styleUrls:  ['../setting.component.scss', './setting.component.scss'],
  templateUrl:'./setting.component.html',
})
export class SettingOrganizationComponent implements OnInit {
  readonly:boolean =false
  constructor(public user: UserService) { }
  @HostListener('change', ['$event'])
  elementClicked(event) {

    if (!this.user.profile.Settings[event.target.name]){
      this.user.profile.Settings[event.target.name] = true
    }
    event.target.value=='true'?this.user.profile.Settings[event.target.name] = true:this.user.profile.Settings[event.target.name] = false
    this.user.editProfile({Settings:this.user.profile.Settings})
  }
  ngOnInit() {
    if (this.user.profile.access==3){
      this.readonly = true
    }

  }


}
