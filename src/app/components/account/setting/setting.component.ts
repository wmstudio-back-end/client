﻿import {Component, ComponentFactoryResolver} from "@angular/core";
import {UserService} from "../../../_services/user.service";
import {ComponentService} from "../../../_services/component.service";
@Component({
  moduleId: module.id,
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent {
  types: string = 'student'

  constructor(private resolver: ComponentFactoryResolver,
              public component: ComponentService,
              private userService: UserService) {
  }

}
