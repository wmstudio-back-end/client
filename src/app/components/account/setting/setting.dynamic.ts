import {
  Component,
  ComponentFactoryResolver,
  Input,
  ReflectiveInjector,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import {SettingStudentComponent} from "./Student/student.component";
import {SettingCompanyComponent} from "./Company/company.component";
import {SettingAdminComponent} from "./Admin/admin.component";
import {SettingEducatorComponent} from "./Educator/educator.component";
import {SettingOrganizationComponent} from "./Organization/setting.component";
@Component({
  selector:'account-setting',
  entryComponents:[
    SettingStudentComponent,
    SettingCompanyComponent,
    SettingAdminComponent,
    SettingEducatorComponent,
    SettingOrganizationComponent
  ], // Reference to the components must be here in order to dynamically create them
  template:`
             <div #setting_dynamic></div>`
})
export class SettingDynamic {
  currentComponent = null;
  @ViewChild('setting_dynamic', {read:ViewContainerRef}) setting_dynamic: ViewContainerRef;
  // component: Class for the component you want to create
  // inputs: An object with key/value pairs mapped to input name/input value
  @Input() set componentData(data: {component: any, inputs: any}) {
    if (!data) {
      return;
    }
// Inputs need to be in the following format to be resolved properly
    let inputProviders = Object.keys(data.inputs).map((inputName) => {
      return {provide:inputName, useValue:data.inputs[inputName]};
    });
    let resolvedInputs = ReflectiveInjector.resolve(inputProviders);
    // We create an injector out of the data we want to pass down and this components injector
    let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.setting_dynamic.parentInjector);
    // We create a factory out of the component we want to create
    let factory = this.resolver.resolveComponentFactory(data.component);
    // We create the component using the factory and the injector
    let component = factory.create(injector);
    // We insert the component into the dom container
    this.setting_dynamic.insert(component.hostView);
    // We can destroy the old component is we like by calling destroy
    if (this.currentComponent) {
      this.currentComponent.destroy();
    }
    this.currentComponent = component;
  }
  
  constructor(private resolver: ComponentFactoryResolver) {
  }

}
