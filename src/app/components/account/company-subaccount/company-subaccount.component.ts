
import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector:   'app-company-subaccount',
  templateUrl:'./company-subaccount.component.html',
  styleUrls:  ['./company-subaccount.component.scss']
})
export class CompanySubaccountComponent implements OnInit {


  email:string
  access:any
  Uaccess:any
  typeAccess:any
  Ublocked:number
  constructor(
    public component:ComponentService,
    public dataService:DataService,
    public helpers:HelpersService,

    public userService:UserService
  ) { }

  ngOnInit() {
    this.init()
    //console.log('ssssssssssss',this.dataService.options.access)
  }
  clearEdit(user){
    this.Uaccess=user.access
    this.Ublocked=user.blocked


  }
  init(){
    for(var i=0;i<this.userService.profile.subaccounts.length;i++){
      this.userService.profile.subaccounts[i].edit = false
    }
  }
  deleteInvite(_id,i){
    if (this.helpers.confirm(0)){
      this.dataService.sendws('User/deleteInvite',{_id:_id}).then((data)=>{
        this.userService.profile.subaccounts.splice(i,1)

      })
    }

  }
  deleteEmployee(_id,i){
    if (this.helpers.confirm(0)){
      this.dataService.sendws('User/deleteEmployee',{_id:_id}).then((data)=>{
        this.userService.profile.employee.splice(i,1)

      })
    }
  }
  editEmployee(v,i){
    console.log(this.Ublocked)

    //if (!this.Ustatus){
    //  this.Ustatus = 0
    //}else{
    //  this.Ustatus = 1
    //}
    this.dataService.sendws('User/editEmployee',{_id:v._id,access:this.Uaccess}).then((data)=>{
      this.userService.profile.employee[i].access = this.Uaccess
      console.log("this.userService.profile.subaccounts[i]",this.userService.profile.employee[i])
      this.Uaccess=null
      this.Ublocked=null


    })
  }
  editInvite(v,i){
    console.log(this.Ublocked)

    //if (!this.Ustatus){
    //  this.Ustatus = 0
    //}else{
    //  this.Ustatus = 1
    //}
    this.dataService.sendws('User/editInvite',{invite:v.invite,email:v.email,access:this.Uaccess,blocked:this.Ublocked}).then((data)=>{
      this.userService.profile.subaccounts[i].blocked = this.Ublocked
      this.userService.profile.subaccounts[i].invite = v.invite
      this.userService.profile.subaccounts[i].access = this.Uaccess
      console.log("this.userService.profile.subaccounts[i]",this.userService.profile.subaccounts[i])
      this.Uaccess=null
      this.Ublocked=null


    })

  }
  suspendChange(v){
    console.log(v)
  }
  createSubaccount(){
    if (!this.email||this.access==null){return}
    if (this.email==this.userService.profile.email){
      // alert('Вы не можете добавить сами себя')
      this.helpers.alert(13)
      return
    }
    if (!this.isInvite('email',this.email)) {
      this.dataService.sendws('User/invite', {typeAccess:this.typeAccess,email:this.email, access:this.access.id}).then((data) => {
        if (data.hasOwnProperty('invite')&&!data.hasOwnProperty('_id')){
        // alert('Invited email')
          this.helpers.alert(14)
          this.email = ''
        }
        if (data.hasOwnProperty('_id')){
          this.userService.profile.subaccounts.push(data)
          this.init()
        }
        if (data.hasOwnProperty('sendmail')){
          this.userService.profile.subaccounts.push(data['sendmail'])
          this.init()
        }


      })
    }else{
      this.helpers.alert(15)
      // alert('Пользователь уже добавлен')
    }
  }
  isInvite(key,val){

    for(var i=0;i<this.userService.profile.subaccounts.length;i++){
      if(this.userService.profile.subaccounts[i][key]==val){
        return true
      }
    }

    return false
  }
}
