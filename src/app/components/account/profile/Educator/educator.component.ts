import {Compiler, Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {UserService} from "../../../../_services/user.service";
import {DataService} from "app/_services";
@Component({
  templateUrl: './educator.component.html',
  styleUrls: ['./educator.component.scss']
})
export class ProfileEducatorComponent implements OnInit {
  rmor = 200
  newRec = 0
  constructor(private router: Router,
              public authenticationService: AuthenticationService,
              public user: UserService,
              public dataService: DataService) {

  }

  ngOnInit() {
    this.newRec = 0
    for (var i = 0; i < this.user.profile.recommendations.length; i++) {
      if (this.user.profile.recommendations[i].uuid==this.user.profile._id){

        continue
      }
      if (this.user.profile.recommendations[i].status==0){
        this.newRec++
      }

    }
  }


}
