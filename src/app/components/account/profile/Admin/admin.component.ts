import {Component, OnInit} from "@angular/core";
import {UserService} from "../../../../_services/user.service";
@Component({
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class ProfileAdminComponent implements OnInit {
  constructor(public user:UserService) {
  }

  ngOnInit() {
  }

}
