import {Compiler, Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../../../../_models/user";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {UserService} from "../../../../_services/user.service";
import {DataService} from "../../../../_services/data.service";
@Component({
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.scss']
})
export class ProfileStudentComponent implements OnInit {
  birthday = new Date()
  // currentUser: User;
  profile: User;
  last_messages: any = []
  message = {}
  rmor = 200
  constructor(private router: Router,
              public authenticationService: AuthenticationService,
              public user: UserService,
              public dataService: DataService) {

    this.message = this.dataService.messages["User/getUsers"].subscribe(data => {
      console.log(data)
    })
  }

  ngOnInit() {

  }

  public lastMessage(i) {
    console.log(i)
    return this.user.profile.messages[i]
  }

  ready_to_work(val: number) {
    this.user.profile["ready_to_work"] = val
    this.dataService.send("User/editProfile", {ready_to_work:val});

  }


  private loadAllUsers() {
    //this.userService.getAll().subscribe(users => { this.users = users; });
  }

}
