import {
  Component,
  ComponentFactoryResolver,
  Input,
  ReflectiveInjector,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import {ProfileStudentComponent} from "./Student/student.component";
import {ProfileCompanyComponent} from "./Company/company.component";
import {ProfileAdminComponent} from "./Admin/admin.component";
import {ProfileEducatorComponent} from "./Educator/educator.component";
import {ProfileOrganizationComponent} from "./Organization/profile.component";

@Component({
  selector:'profile',
  entryComponents:[
    ProfileStudentComponent,
    ProfileCompanyComponent,
    ProfileAdminComponent,
    ProfileEducatorComponent,
    ProfileOrganizationComponent
  ], // Reference to the components must be here in order to dynamically create them
  template:`
             <div #profile_dynamic></div>`
})
export class ProfileDynamic {
  currentComponent = null;
  @ViewChild('profile_dynamic', {read:ViewContainerRef}) profile_dynamic: ViewContainerRef;
  // component: Class for the component you want to create
  // inputs: An object with key/value pairs mapped to input name/input value
  @Input() set componentData(data: {component: any, inputs: any}) {
    if (!data) {
      return;
    }
// Inputs need to be in the following format to be resolved properly
    let inputProviders = Object.keys(data.inputs).map((inputName) => {
      return {provide:inputName, useValue:data.inputs[inputName]};
    });
    let resolvedInputs = ReflectiveInjector.resolve(inputProviders);
    // We create an injector out of the data we want to pass down and this components injector
    let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.profile_dynamic.parentInjector);
    // We create a factory out of the component we want to create
    let factory = this.resolver.resolveComponentFactory(data.component);
    // We create the component using the factory and the injector
    let component = factory.create(injector);
    // We insert the component into the dom container
    this.profile_dynamic.insert(component.hostView);
    // We can destroy the old component is we like by calling destroy
    if (this.currentComponent) {
      this.currentComponent.destroy();
    }
    this.currentComponent = component;
  }
  
  constructor(private resolver: ComponentFactoryResolver) {
  }

}
