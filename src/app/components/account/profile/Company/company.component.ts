
import {
  Compiler,
  Component,
  OnInit
}                              from "@angular/core";
import {Router}                from "@angular/router";
import {User}                  from "../../../../_models/user";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {UserService}           from "../../../../_services/user.service";
import {DataService}           from "../../../../_services/data.service";
import {HelpersService}        from "../../../../_services/helpers.service";

@Component({
  templateUrl: './company.component.html',
  styleUrls:   ['./company.component.scss']
})
export class ProfileCompanyComponent implements OnInit {
  rmor               = 200
  birthday           = new Date()
  // currentUser: User;
  profile: User;
  last_messages: any = []
  message            = {}

  constructor(private router: Router,
              public authenticationService: AuthenticationService,
              public user: UserService,
              public hs: HelpersService,
              private compiler: Compiler,
              public dataService: DataService) {
    this.message = this.dataService.messages["User/getUsers"].subscribe(data => {

    })
    if (this.user.profile.Company.vacancy){
      this.user.profile.Company.vacancy.sort((a: any, b: any) => {
        return b.updated_at - a.updated_at
      })
    }

  }

  ngOnInit() {

  }

  public lastMessage(i) {
    console.log(i)
    return this.user.profile.messages[i]
  }

  ready_to_work(val: number) {

    this.user.profile["ready_to_work"] = val

    this.dataService.send("User/editProfile", {ready_to_work: val});

    // this.snackBar.open(this.profile.ready_to_work, "X", {
    //     duration: 1000,
    // });
  }
}
