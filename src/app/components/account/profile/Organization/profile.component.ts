import {Compiler, Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {User} from "../../../../_models/user";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {UserService} from "../../../../_services/user.service";
import {DataService} from "../../../../_services/data.service";
@Component({
  templateUrl:'./profile.component.html',
  styleUrls:  ['./profile.component.scss']
})
export class ProfileOrganizationComponent implements OnInit {

  birthday = new Date()
  // currentUser: User;
  profile: User;
  last_messages: any = []
  message = {}

  constructor(private router: Router,
              public authenticationService: AuthenticationService,
              public user: UserService,
              private compiler: Compiler,
              public dataService: DataService,) {
    this.message = this.dataService.messages["User/getUsers"].subscribe(data => {
      console.log(data)
    })
  }

  ngOnInit() {

  }

  public lastMessage(i) {
    console.log(i)
    return this.user.profile.messages[i]
  }

  ready_to_work(val: number) {

    this.user.profile["ready_to_work"] = val

    this.dataService.send("User/editProfile", {ready_to_work: val});

    // this.snackBar.open(this.profile.ready_to_work, "X", {
    //     duration: 1000,
    // });
  }
}
