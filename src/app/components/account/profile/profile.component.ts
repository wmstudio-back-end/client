﻿import {Component, ComponentFactoryResolver} from "@angular/core";
import {UserService} from "../../../_services/user.service";
import {ComponentService} from "../../../_services/component.service";
@Component({
  template:`
             <sidebar-link [componentData]="component.componentSidebarLink" class="sidebar">sidebar-link</sidebar-link>
             <profile class="sideContent" [componentData]="component.componentProfile"></profile>
           `

})
export class ProfileComponent {
  types: string = 'student'

  constructor(private resolver: ComponentFactoryResolver,
              public component: ComponentService,
              private userService: UserService) {
  }

}
