import {
  Component,
  ComponentFactoryResolver,
  Input,
  ReflectiveInjector,
  ViewChild,
  ViewContainerRef
} from "@angular/core";
import {UserDataStudentComponent} from "./Student/userdata.component";
import {UserDataCompanyComponent} from "./Company/userdata.component";
import {UserDataAdminComponent} from "./Admin/userdata.component";
import {UserDataEducatorComponent} from "./Educator/userdata.component";
import {UserDataOrganizationComponent} from "./Organization/userdata.component";
@Component({
  selector:'userdata',
  entryComponents:[
    UserDataStudentComponent,
    UserDataCompanyComponent,
    UserDataAdminComponent,
    UserDataEducatorComponent,
    UserDataOrganizationComponent,
  ], // Reference to the components must be here in order to dynamically create them
  template:`
             <div #userdata_dynamic></div>`
})
export class UserDataDynamic {
  currentComponent = null;
  @ViewChild('userdata_dynamic', {read:ViewContainerRef}) userdata_dynamic: ViewContainerRef;
  // component: Class for the component you want to create
  // inputs: An object with key/value pairs mapped to input name/input value
  @Input() set componentData(data: {component: any, inputs: any}) {
    if (!data) {
      return;
    }
// Inputs need to be in the following format to be resolved properly
    let inputProviders = Object.keys(data.inputs).map((inputName) => {
      return {provide:inputName, useValue:data.inputs[inputName]};
    });
    let resolvedInputs = ReflectiveInjector.resolve(inputProviders);
    // We create an injector out of the data we want to pass down and this components injector
    let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.userdata_dynamic.parentInjector);
    // We create a factory out of the component we want to create
    let factory = this.resolver.resolveComponentFactory(data.component);
    // We create the component using the factory and the injector
    let component = factory.create(injector);
    // We insert the component into the dom container
    this.userdata_dynamic.insert(component.hostView);
    // We can destroy the old component is we like by calling destroy
    if (this.currentComponent) {
      this.currentComponent.destroy();
    }
    this.currentComponent = component;
  }
  
  constructor(private resolver: ComponentFactoryResolver) {
  }

}
