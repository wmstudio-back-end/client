
import {
  Compiler,
  Component,
  OnInit
}                              from "@angular/core";
import {Router}                from "@angular/router";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {
  DataService,
  UserService
}                              from "app/_services";
import {
  ElementRef,
  ViewChild
}                              from '@angular/core';
import {LocationService}       from "../../../../_services/location.service";
import {IMyDpOptions}          from "mydatepicker";
import {ILocation}             from "../../../../_models/user";
import {noUndefined}           from "@angular/compiler/src/util";
import {HelpersService}        from "../../../../_services/helpers.service";
import {DownPopupService}      from "../../../../_services/down-popup.service";

export interface Student {

  gender: string

}

export interface FormUserdata {
  student: Student,
  genddd: string
}

@Component({
  selector:    'app-account-student',
  templateUrl: './userdata.component.html',
  styleUrls:   ['./userdata.component.scss']
})


export class UserDataStudentComponent implements OnInit {
  first_name: string;
  last_name: string;
  displayAs: number;
  phone: number;
  email: string;
  prefLang: any
  currentPass: string
  newPass: string
  repeatPass: string
  personalId: number
  gender: number
  ready_to_work: number
  Address: string
  birthday: any
  seldate                                  = null
  Location: ILocation;
  update: boolean                          = false;
  oAuth: boolean                           = false
  disconnectFbModal: boolean               = false
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
    minYear:    new Date(1900, 1, 1).getFullYear(),
    width:      '150px'
  };

  constructor(public router: Router,
              public authenticationService: AuthenticationService,
              public user: UserService,
              public compiler: Compiler,
              public locationService: LocationService,
              public helpers: HelpersService,
              public popS: DownPopupService,
              public dataService: DataService) {

    this.user.oAuth.subscribe((status) => {
      this.oAuth = status
    })
    this.user.checkOAuth()
  }
ch(e){
  console.log('EVENT',e)
}
  ngOnChanges(changes: any) {
    console.log(changes);
  }

  ChangeItem(e) {
    return e
  }

  ngOnInit() {
    if (this.user.profile.smartId) {
      this.oAuth = true
    }


    this.gender        = this.user.profile.Student.gender
    this.ready_to_work = this.user.profile.Student.ready_to_work
    this.first_name    = this.user.profile.first_name
    this.last_name     = this.user.profile.last_name
    this.phone         = this.user.profile.phone
    this.email         = this.user.profile.email
    this.personalId    = this.user.profile.personalId
    this.Address       = this.user.profile.Address
    this.birthday      = this.user.profile.birthday
    this.displayAs     = this.user.profile.displayAs
    this.Location      = this.user.profile.Location


    if (this.birthday) {

      var yyyy = new Date(this.birthday * 1000).getFullYear();
      var mm   = new Date(this.birthday * 1000).getMonth() + 1;
      var dd   = new Date(this.birthday * 1000).getDate();

      this.seldate = {
        year:  yyyy,
        month: mm,
        day:   dd
      }

    }
  }

  basd(e) {
    console.log(e.epoc)
  }

  changeAddress(e) {
    if (e == '') {
      console.log("EVENT", e)
      this.Location = null
    }

  }

  public save() {



    //this.locationService.setLocation('profile');
    var data = {}

    this.gender != undefined ? data["Student.gender"] = this.user.profile.Student.gender = this.gender : null
    this.ready_to_work != undefined ? data["Student.ready_to_work"] = this.user.profile.Student.ready_to_work = this.ready_to_work : null
    this.prefLang ? data["prefLang"] = this.user.profile.prefLang = this.prefLang : null
    data['personalId'] = this.user.profile.personalId = this.personalId
    data['phone'] = this.user.profile.phone = this.phone
    this.email ? data['newemail'] =  this.email : null;
    this.first_name ? data['first_name'] = this.user.profile.first_name = this.first_name : null
    this.last_name ? data['last_name'] = this.user.profile.last_name = this.last_name : null
    this.Address ? data['Address'] = this.user.profile.Address = this.Address : null
    data['birthday'] = this.user.profile.birthday = this.birthday
    this.displayAs != null ? data['displayAs'] = this.user.profile.displayAs = this.displayAs : null
    // this.Location?data['Location']=this.user.profile.Location = this.Location:null
    data['Location'] = this.user.profile.Location = this.Location

    console.log("this.Location!!!!", this.Location)

    //this.locationService.setLocation('profile')



    this.user.editProfile(data);
    this.update = false;


    // console.log(this.user.userDataComplited)
    // this.user.profile.hasOwnProperty('last_name')?this.uS.profile.Student.resumes[i].prc+=1:this.uS.profile.Student.resumes[i]-=1
    // this.user.profile.hasOwnProperty('phone')?this.uS.profile.Student.resumes[i].prc+=5:this.uS.profile.Student.resumes[i]-=5
    // this.user.profile.hasOwnProperty('email')?this.uS.profile.Student.resumes[i].prc+=1:this.uS.profile.Student.resumes[i]-=1
    // this.user.profile.hasOwnProperty('personalId')?this.uS.profile.Student.resumes[i].prc+=2.5:this.uS.profile.Student.resumes[i]-=2.5
    // this.user.profile.hasOwnProperty('birthday')?this.uS.profile.Student.resumes[i].prc+=1:this.uS.profile.Student.resumes[i]-=1
    // this.user.profile.Student&&this.uS.profile.Student.hasOwnProperty('gender')?this.uS.profile.Student.resumes[i].prc+=1:this.uS.profile.Student.resumes[i]-=1
    // this.user.profile.Location&&this.uS.profile.Location.hasOwnProperty('place_id')?this.uS.profile.Student.resumes[i].prc+=5:this.uS.profile.Student.resumes[i]-=5
    //

  }

  unsetOAuth() {
    this.user.checkPass(function (isset) {
      this.disconnectFbModal = false
      if (isset['pass'] && isset['email']) {
        this.disconnectFbModal = false
        this.user.unsetOAuth()
      } else {
        this.disconnectFbModal = true
        console.log('this.disconnectFbModal',this.disconnectFbModal);
      }
    }.bind(this))
  }


}
