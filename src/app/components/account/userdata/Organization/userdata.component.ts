import {Compiler, Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {UserService} from "../../../../_services/user.service";
import {DataService} from "../../../../_services/data.service";
import {HelpersService} from "../../../../_services/helpers.service";
@Component({
  selector: 'app-account-organization',
  templateUrl: './userdata.component.html',
  styleUrls: ['./userdata.component.scss']
})
export class UserDataOrganizationComponent implements OnInit {
  first_name:string
  last_name:string
  displayAs:any
  phone:number
  email:string
  prefLang:any
    currentPass:string
    newPass: string;
    repeatPass: string;
  company_name:string
  company_code:string
  Address:string
  readonly:boolean =false
  oAuth: boolean = false
  disconnectFbModal: boolean               = false

  constructor(public router: Router,
              public authenticationService: AuthenticationService,
              public user: UserService,
              public helpers: HelpersService,
              public compiler: Compiler,
              public dataService: DataService) {

    this.first_name = this.user.profile.first_name
    this.last_name = this.user.profile.last_name
    this.phone = this.user.profile.phone
    this.email = this.user.profile.email
    this.Address = this.user.profile.Address
    this.user.oAuth.subscribe((status)=>{
          this.oAuth = status
      })
    this.user.checkOAuth()

  }

  ngOnInit() {
    if (this.user.profile.smartId){this.oAuth = true}
    if (this.user.profile.access==3){
      this.readonly = true
    }
  }

  public save() {

    var data = {
      phone:this.phone,
      email:this.email,
      first_name:this.first_name,
      last_name:this.last_name,
      Address:this.Address,
    }

  }

  unsetOAuth() {
    this.user.checkPass(function (isset) {
      this.disconnectFbModal = false
      if (isset['pass'] && isset['email']) {
        this.disconnectFbModal = false
        this.user.unsetOAuth()
      } else {
        this.disconnectFbModal = true
        console.log('this.disconnectFbModal',this.disconnectFbModal);
      }
    }.bind(this))
  }
}
