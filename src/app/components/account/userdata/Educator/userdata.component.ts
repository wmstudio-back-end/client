import {Compiler, Component, OnInit} from "@angular/core";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {UserService} from "../../../../_services/user.service";
import {DataService} from "../../../../_services/data.service";
import {Router} from "@angular/router";
import {LocationService} from "../../../../_services/location.service";
import {ILocation} from "../../../../_models/user";
import {HelpersService} from "../../../../_services/helpers.service";
@Component({
  selector: 'app-educator',
  templateUrl: './userdata.component.html',
  styleUrls: ['./userdata.component.scss']
})
export class UserDataEducatorComponent implements OnInit {
  photo:any;
  first_name: string;
  last_name: string;
  displayAs: any;
  phone: number;
  email: string;
    currentPass:string
    newPass: string
    repeatPass: string
  Address: string;
  birthday = new Date();
  inputStyle:string
  prefLang:number
  Location: ILocation;
  Location2: ILocation;
  saveSchoolR = false
  oAuth: boolean = false
  disconnectFbModal: boolean               = false
  constructor(private router: Router,
              public authenticationService: AuthenticationService,
              public user: UserService,
              private compiler: Compiler,
              public dataService: DataService,
              public helpers: HelpersService,
              public locationService: LocationService) {
    this.first_name = this.user.profile.first_name
    this.last_name = this.user.profile.last_name
    this.phone = this.user.profile.phone
    this.email = this.user.profile.email
    this.Address = this.user.profile.Address;
    this.photo = {path: this.user.profile.photo};
    this.prefLang = this.user.profile.prefLang;

    this.user.oAuth.subscribe((status)=>{
          this.oAuth = status
    })
    this.user.checkOAuth()
  }

  ngOnInit() {
    if (this.user.profile.smartId){this.oAuth = true}
    if (!this.user.profile.Educator.schools){
      this.user.profile.Educator.schools = []

    }
    this.Location2 = this.user.profile.Location
  }
  saveSchool(){
    this.saveSchoolR = true

    var save =true
    for (var i = 0; i < this.user.profile.Educator.schools.length; i++) {
      if(this.Location.place_id==this.user.profile.Educator.schools[i].place_id){
        save = false
      }
    }
    if(save){
      this.dataService.sendws('Schools/connectEduSchool',this.Location).then((data)=>{
        this.user.profile.Educator.schools.push(data['school'])
        console.log(this.Location)
        console.log(this.Address)
        this.saveSchoolR = false
        this.Location = null
      })
    }else{
      this.helpers.alert(11)
      // alert('dublicate school')
    }


  }



  deleteSchool(index,id) {

    if (this.helpers.confirm(0)){
      this.dataService.sendws("User/deleteSchoolEducator",{id:id}).then(data => {
        data ? this.user.profile.Educator.schools.splice(index, 1) : null;
      })
    }





  }


  setPhoto(e){
    this.photo = e
    console.log(e)
  }

  public save() {
    var data = {}
    this.prefLang?data["Educator.prefLang"]  = this.user.profile.prefLang = this.prefLang:null
    data['phone'] = this.user.profile.phone= this.phone
    this.email?data['newemail'] = this.email:null
    this.first_name?data['first_name'] = this.user.profile.first_name = this.first_name:null
    this.last_name?data['last_name'] = this.user.profile.last_name=this.last_name:null
    this.Address?data['Address'] = this.user.profile.Address=this.Address:null
    this.displayAs?data['displayAs'] = this.user.profile.displayAs=this.displayAs:null
    this.Location2?data['Location'] = this.user.profile.Location= this.Location2:null
    //this.locationService.setLocation('profile');



    this.user.editProfile(data);

  }

  unsetOAuth() {
    this.user.checkPass(function (isset) {
      this.disconnectFbModal = false
      if (isset['pass'] && isset['email']) {
        this.disconnectFbModal = false
        this.user.unsetOAuth()
      } else {
        this.disconnectFbModal = true
        console.log('this.disconnectFbModal',this.disconnectFbModal);
      }
    }.bind(this))
  }
}
