﻿import {Component, ComponentFactoryResolver} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";
@Component({
  template:`
             <sidebar-link [componentData]="component.componentSidebarLink" class="sidebar">sidebar-link</sidebar-link>
             <userdata class="sideContent" [componentData]="component.componentUserData"></userdata>
           `

})
export class UserDataComponent {
  types: string = 'student'

  constructor(private resolver: ComponentFactoryResolver,
              public component: ComponentService,
              private userService: UserService) {
  }

}
