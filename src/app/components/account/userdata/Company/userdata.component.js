"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var AccountCompanyComponent = (function () {
  function AccountCompanyComponent(router, authenticationService, user, compiler, dataService) {
    this.router = router;
    this.authenticationService = authenticationService;
    this.user = user;
    this.compiler = compiler;
    this.dataService = dataService;
    this.birthday = new Date();
    this.last_messages = [];
    this.message = {};
    this.message = this.dataService.messages["User/getUsers"].subscribe(function (data) {
      console.log(data);
    });
  }

  AccountCompanyComponent.prototype.ngOnInit = function () {
  };
  AccountCompanyComponent.prototype.lastMessage = function (i) {
    console.log(i);
    return this.user.profile.messages[i];
  };
  AccountCompanyComponent.prototype.ready_to_work = function (val) {
    this.user.profile["ready_to_work"] = val;
    this.dataService.send("User/editProfile", {ready_to_work: val});
    // this.snackBar.open(this.profile.ready_to_work, "X", {
    //     duration: 1000,
    // });
  };
  return AccountCompanyComponent;
}());
AccountCompanyComponent = __decorate([
  core_1.Component({
    selector: 'app-account-company',
    templateUrl: './userdata.component.html',
    styleUrls: ['./userdata.component.scss']
  })
], AccountCompanyComponent);
exports.AccountCompanyComponent = AccountCompanyComponent;
