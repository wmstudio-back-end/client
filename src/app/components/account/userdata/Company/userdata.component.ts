import {Compiler, Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {UserService} from "../../../../_services/user.service";
import {DataService} from "../../../../_services/data.service";
import {LocationService} from "../../../../_services/location.service";
import {ILocation} from "../../../../_models/user";
import {HelpersService} from "../../../../_services/helpers.service";
import {DownPopupService} from "../../../../_services/down-popup.service";

@Component({
  selector: 'app-account-company',
  templateUrl: './userdata.component.html',
  styleUrls: ['./userdata.component.scss']
})
export class UserDataCompanyComponent implements OnInit {

  first_name:string
  last_name:string
  displayAs:any
  phone:number
  email:string
  prefLang:any
    currentPass:string
    newPass: string;
    repeatPass: string;
  nameCompany:string
  Address:string
  regCode:number
  Location: ILocation;
  update: boolean = false;
  checkerUpdate:any
  oAuth: boolean = false
  disconnectFbModal: boolean               = false

  constructor(public router: Router,
              public authenticationService: AuthenticationService,
              public user: UserService,
              public compiler: Compiler,
              public help: HelpersService,
              public locationService: LocationService,
              public popS: DownPopupService,
              public dataService: DataService) {

      this.user.oAuth.subscribe((status)=>{
          this.oAuth = status
      })
      this.user.checkOAuth()

  }

  ngOnInit() {
    // if (this.user.profile.smartId){this.oAuth = true}
    this.first_name = this.user.profile.first_name
    this.last_name = this.user.profile.last_name
    this.phone = this.user.profile.phone
    this.email = this.user.profile.email
    this.Address = this.user.profile.Address
    this.nameCompany = this.user.profile.Company.nameCompany
    this.regCode = this.user.profile.Company.regCode
    this.displayAs = this.user.profile.displayAs
    this.Location = this.user.profile.Location
    this.checkerUpdate = this.help.checkerInputs(this.getInputs())

  }

  public getInputs(){
    return {
      first_name: this.first_name,
      last_name: this.last_name,
      phone: this.phone,
      email: this.email,
      Address: this.Address,
      nameCompany: this.nameCompany,
      regCode: this.regCode,
      displayAs: this.displayAs,
      Location: this.Location,
      prefLang:this.user.profile.prefLang
    }
  }

  public checkChange(key,value){
    this.update = this.checkerUpdate.checkValues(this.getInputs(),{key:key,value:value})
  }

  public save() {



    //this.locationService.setLocation('profile');
    var data = {}

    this.prefLang?data["prefLang"]  = this.user.profile.prefLang = this.prefLang:null
    this.nameCompany?data["Company.nameCompany"] = this.user.profile.Company.nameCompany = this.nameCompany:null
    this.regCode?data["Company.codeCompany"] = this.user.profile.Company.regCode = this.regCode:null
    data['phone'] = this.user.profile.phone= this.phone
    data['newemail'] = this.email
    this.first_name?data['first_name'] = this.user.profile.first_name = this.first_name:null
    this.last_name?data['last_name'] = this.user.profile.last_name=this.last_name:null
    this.Address?data['Address'] = this.user.profile.Address=this.Address:null
    this.displayAs!=null ? data['displayAs'] = this.user.profile.displayAs = this.displayAs : null
    // this.Location?data['Location'] = this.user.profile.Location= this.Location:null
    data['Location'] = this.user.profile.Location = this.Location



      this.user.editProfile(data,function(data){
        this.email = this.user.profile.email
        this.user.profile.email = this.email

      }.bind(this));
    this.update = false;


  }

  unsetOAuth() {
    this.user.checkPass(function (isset) {
      this.disconnectFbModal = false
      if (isset['pass'] && isset['email']) {
        this.disconnectFbModal = false
        this.user.unsetOAuth()
      } else {
        this.disconnectFbModal = true
        console.log('this.disconnectFbModal',this.disconnectFbModal);
      }
    }.bind(this))
  }




}
