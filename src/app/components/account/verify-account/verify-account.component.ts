import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {HelpersService} from "../../../_services/helpers.service";
import {UserService} from "../../../_services";

@Component({
    selector:   'app-verify-account',
    templateUrl:'./verify-account.html',
    styleUrls:  ['./verify-account.scss']
})
export class VerifyAccountComponent implements OnInit {

    constructor(public component:ComponentService, public hS : HelpersService, public uS: UserService) { }

    ngOnInit() {
    }


}
