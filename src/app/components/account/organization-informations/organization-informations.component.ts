import { Component, OnInit } from '@angular/core';
import {ILocation, Organization} from "../../../_models/user";
import {ComponentService} from "../../../_services/component.service";
import {UserService} from "../../../_services/user.service";
import {LocationService} from "../../../_services/location.service";
import {DataService} from "../../../_services/data.service";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector: 'app-organization-informations',
  templateUrl: './organization-informations.component.html',
  styleUrls: ['./organization-informations.component.scss']
})
export class OrganizationInformationsComponent implements OnInit {
  iOrg:Organization
  //inputStyle: string
  //schoolName:string
  //regCode:string
  //mobilePhone:string
  //landlinePhone:string
  //aboutOrg:string
  //videoUrl:string
  //educationLevel:number
  //typeOfSchool:number
  //fieldActivity:number
  //webSite:string
  //Additional_information:string
  //photo:any

  AdditionalAddresses:any
  Location:ILocation
  Address:ILocation
  constructor(public component: ComponentService,
              public uS: UserService,
              public locationService: LocationService,
              public helpers: HelpersService,
              public dS: DataService) {


  }
  ngOnInit() {
    this.iOrg = this.uS.profile.Organization
  }
  addInput(){


    this.Location = <ILocation>{}
    if (!this.iOrg.AdditionalAddresses){
      this.iOrg.AdditionalAddresses = []
    }
    var add = true
    for (var i = 0; i < this.iOrg.AdditionalAddresses.length; i++) {
      if (!this.iOrg.AdditionalAddresses[i].addr){
        add = false
      }

    }
    if (add){
      this.iOrg.AdditionalAddresses.push(this.Location)
    }else{
      this.helpers.alert(19)
      // alert('enter Address')
    }

    //this.clearLocate()




  }
  save(){


    this.uS.profile.Organization = this.iOrg
    this.dS.sendws('User/editOrgInfo',this.iOrg)
  }
}
