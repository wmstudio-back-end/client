import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {RouterModule} from "@angular/router";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import {SidebarModule} from "../sidebar/sidebar.module";
import {ModulesModule} from "../../modules/modules.module";
import {MessagesComponent} from "./_components/messages/messages.component";
import {UserDataComponent} from "./userdata/userdata.component";
import {UserDataDynamic} from "./userdata/userdata.dynamic";
import {UserDataAdminComponent} from "./userdata/Admin/userdata.component";
import {UserDataStudentComponent} from "./userdata/Student/userdata.component";
import {UserDataCompanyComponent} from "./userdata/Company/userdata.component";
import {UserDataEducatorComponent} from "./userdata/Educator/userdata.component";
import {ProfileStudentComponent} from "./profile/Student/student.component";
import {ProfileCompanyComponent} from "app/components/account/profile/Company/company.component";
import {ProfileAdminComponent} from "./profile/Admin/admin.component";
import {ProfileEducatorComponent} from "app/components/account/profile/Educator/educator.component";
import {ProfileComponent} from "./profile/profile.component";
import {ProfileDynamic} from "./profile/profile.dynamic";
import {TitleComponent} from './_components/title/title.component';
import {UserLineComponent} from './_components/user-line/user-line.component';
import {ProfileBoxComponent} from '../resume/_components/profile-box/profile-box.component';
import {SaveSearchesBoxComponent} from './_components/save-searches-box/save-searches-box.component';
import {SettingStudentComponent} from "./setting/Student/student.component";
import {SettingCompanyComponent} from "./setting/Company/company.component";
import {SettingAdminComponent} from "app/components/account/setting/Admin/admin.component";
import {SettingEducatorComponent} from "app/components/account/setting/Educator/educator.component";
import {SettingComponent} from "./setting/setting.component";
import {SettingDynamic} from "./setting/setting.dynamic";
import {CompanySubaccountComponent} from "./company-subaccount/company-subaccount.component";
import {CompanyProjectsComponent} from "./company-projects/projects.component";
import {UserDataOrganizationComponent} from "./userdata/Organization/userdata.component";
import {SettingOrganizationComponent} from "./setting/Organization/setting.component";
import {ProfileOrganizationComponent} from "app/components/account/profile/Organization/profile.component";
import {OrganizationSubaccountComponent} from "./organization-subaccount/organization-subaccount.component";
import {VerifyAccountComponent} from "./verify-account/verify-account.component";
import { CompanyRatingComponent } from './company-rating/company-rating.component';
import {CompanyInformationsComponent} from "./company-informations/informations.component";

import {MyDatePickerModule} from "mydatepicker";
import {BookmarkedVacanciesComponent} from "./bookmarked/bookmarked-vacancies/bookmarked-vacancies.component";
import {NgPipesModule} from "ngx-pipes";
import { DeactivateAccountComponent } from './_components/deactivate-account/deactivate-account.component';
import { ServicesComponent } from './services/services.component';
import { ServiceModalComponent } from './_components/service-modal/service-modal.component';
import {ClickOutsideModule} from "ng-click-outside";
import { DetailResumeBookmarkComponent } from './_components/detail-resume-bookmark/detail-resume-bookmark.component';
import {ShopCartModalComponent} from "./services/shop-cart-modal/shop-cart-modal.component";
// import {SmoothScrollDirective, SmoothScrollToDirective} from "ng2-smooth-scroll";
import { OrganizationInformationsComponent } from './organization-informations/organization-informations.component';
import {TextMaskModule} from "angular2-text-mask";
import {BookmarkedResumeComponent} from "./bookmarked/bookmarked-resume/bookmarked-resume";
import {VacancyCardComponent} from "../../modules/vacancy-card/vacancy-card.component";
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
export var declarationsCmp = [

  SettingStudentComponent,
  SettingCompanyComponent,
  SettingAdminComponent,
  SettingEducatorComponent,
  SettingOrganizationComponent,

  ProfileStudentComponent,
  ProfileCompanyComponent,
  ProfileAdminComponent,
  ProfileEducatorComponent,
  ProfileOrganizationComponent,

  UserDataAdminComponent,
  UserDataStudentComponent,
  UserDataCompanyComponent,
  UserDataEducatorComponent,
  UserDataOrganizationComponent,

  CompanySubaccountComponent,
  OrganizationSubaccountComponent,
  CompanyProjectsComponent
]

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        FormsModule,
        ModulesModule,
        SidebarModule,
        MyDatePickerModule,
        NgPipesModule,
        ClickOutsideModule,
      TextMaskModule,
      ScrollToModule.forRoot(),
        RouterModule.forChild([
            {
                path: '',
                component: ProfileComponent,
                data: {
                    breadcrumb: ''
                }
            },
            {
              path: 'settings',
              component: SettingComponent,
              data: {
                breadcrumb: ''
              }
            },
          {
            path: 'services',
            component: ServicesComponent,
            data: {
              breadcrumb: ''
            }
          },
            {
                path: 'userdata',
                component: UserDataComponent,
                data: {
                    breadcrumb: ''
                }
            },
            {
            path: 'subaccounts',
            component: CompanySubaccountComponent,
            data: {
              breadcrumb: ''
              }
            },
          {
            path: 'orgsubaccounts',
            component: OrganizationSubaccountComponent,
            data: {
              breadcrumb: ''
            }
          },
            {
            path: 'projects',
            component: CompanyProjectsComponent,
            data: {
              breadcrumb: ''
            }
            },
          {
            path: 'projects/:id',
            component: CompanyProjectsComponent,
            data: {
              breadcrumb: ''
            }
          },

          {
            path: 'verify',
            component: VerifyAccountComponent,
            data: {
              breadcrumb: ''
            }
          },
          {
            path: 'rating',
            component: CompanyRatingComponent,
            data: {
              breadcrumb: ''
            }
          },
          {
            path: 'information',
            component: CompanyInformationsComponent,
            data: {
              breadcrumb: ''
            }
          },{
            path: 'organization',
            component: OrganizationInformationsComponent,
            data: {
              breadcrumb: ''
            }
          },
          {
            path: 'bookmarked-vacancy',
            component: BookmarkedVacanciesComponent,
            data: {
              breadcrumb: ''
            }
          },
          {
            path: 'bookmarked-resume',
            component: BookmarkedResumeComponent,
            data: {
              breadcrumb: ''
            }
          }

        ])
    ],
    declarations: [
      // SmoothScrollToDirective,
      // SmoothScrollDirective,
        ProfileDynamic,
        ProfileComponent,
        SettingComponent,
        SettingDynamic,
        UserDataDynamic,
        UserDataComponent,
        declarationsCmp,
        MessagesComponent,
        TitleComponent,
        UserLineComponent,
        ProfileBoxComponent,
        SaveSearchesBoxComponent,
        VerifyAccountComponent,
        CompanyRatingComponent,
        CompanyInformationsComponent,
      BookmarkedVacanciesComponent,
      BookmarkedResumeComponent,
      DeactivateAccountComponent,
      ServicesComponent,
      ServiceModalComponent,
      ShopCartModalComponent,
      DetailResumeBookmarkComponent,
      OrganizationInformationsComponent,






    ],
    exports: [],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    bootstrap: [ProfileComponent, UserDataComponent, SettingComponent]

})
export class AccountModule {
}
