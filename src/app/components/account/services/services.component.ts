import {
  Component,
  ElementRef,
  Inject,
  OnInit,
  Pipe
} from '@angular/core';

import {DOCUMENT}         from '@angular/common';
import {
  PageScrollConfig,
  PageScrollService,
  PageScrollInstance
}                         from 'ng2-page-scroll';
import {TranslateService} from "@ngx-translate/core";
import {ComponentService} from "../../../_services/component.service";
import {ShopService}      from "../../../_services/shop.service";
import {DataService}      from "../../../_services/data.service";
import {UserService}      from "../../../_services/user.service";
import {HelpersService}   from "../../../_services/helpers.service";
import {
  Router,
  ActivatedRoute,
  Params
}                         from "@angular/router";
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';
@Component({
  selector:    'app-services',
  templateUrl: './services.component.html',
  styleUrls:   ['./services.component.scss']
})
export class ServicesComponent implements OnInit {
  modal: number       = 0;
  fixedToggle         = false
  cartModal           = false
  openModal: boolean  = false;
  openModal2: boolean = false;
  openModal3: boolean = false;
  openModal4: boolean = false;
  havepromo           = false
  countVac            = 1

  constructor(public component: ComponentService,
              public shopService: ShopService,
              public dS: DataService,
              private activatedRoute: ActivatedRoute,
              public hs: HelpersService,
              public uS: UserService,
              public translate: TranslateService,
              private pageScrollService: PageScrollService,
              private _scrollToService: ScrollToService,
              @Inject(DOCUMENT) private document: any) {
    // PageScrollConfig.defaultScrollOffset = 150;
    // PageScrollConfig.defaultDuration     = 500;
  }
  scrollTo2(){
    const config: ScrollToConfigOptions = {
      target: 'destination'
    };

    this._scrollToService.scrollTo(config);
  }
  scrollTo(){
    window.scrollTo(0, 0)
    document.getElementById('addVacScroll').focus();
    var el = document.getElementById('addVacScroll').getBoundingClientRect()
    var el2 = document.getElementById('header-wrap').getBoundingClientRect()
    document.getElementById('inpaddVacScroll').focus()

    // window.scrollTo({
    //   top: el.top-50,
    //   behavior: 'smooth'
    // })
    var i = 10;
    var int = setInterval(function() {
      window.scrollTo(0, i);
      i += 10;
      if (i >= el.top-el2.height-50) clearInterval(int);
    }, 10);

  }
  outBasket() {
    if (this.fixedToggle) this.fixedToggle = !this.fixedToggle
  }

  inBasket() {
    if (window.innerWidth <= 640) {
      let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '.title-wrap');
      this.pageScrollService.start(pageScrollInstance);
    }

  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params.hasOwnProperty('package')) {
        this.modal = 1
      }
    });
  }

  filterOrderService(status) {
    var orders = []

    for (var i = 0; i < this.uS.profile.service.orders.length; i++) {

      var s = false
      this.uS.profile.service.orders[i].service.vacancy ? s = true : null
      this.uS.profile.service.orders[i].service.premiumListing ? s = true : null
      this.uS.profile.service.orders[i].service.hightFrame ? s = true : null
      this.uS.profile.service.orders[i].service.insocialMedia ? s = true : null
      this.uS.profile.service.orders[i].service.targetGroup ? s = true : null
      this.uS.profile.service.orders[i].service.bannerofEmail ? s = true : null
      if (s && this.uS.profile.service.orders[i].status == status) {
        orders.push(this.uS.profile.service.orders[i])
      }
    }


    return orders
  }

  getOrderDesc(order) {
    var str = ''
    this.translate.get('Additional vacancy')
    if (order.service.hasOwnProperty('vacancy') && order.service.vacancy > 0) str += 'AdditionalVacancy / ' + order.service.vacancy + '<br>'
    if (order.service.hasOwnProperty('premiumListing') && order.service.premiumListing > 0) str += 'Premium listing / ' + order.service.premiumListing + 'days<br>'
    if (order.service.hasOwnProperty('hightFrame') && order.service.hightFrame > 0) str += 'Highlighted frame / ' + order.service.hightFrame + 'days<br>'
    if (order.service.hasOwnProperty('insocialMedia') && order.service.insocialMedia > 0) str += 'Advertisement in Social media / ' + order.service.insocialMedia + 'pcs<br>'
    if (order.service.hasOwnProperty('targetGroup') && order.service.targetGroup > 0) str += 'e-Advertisement to a target group / ' + order.service.targetGroup + 'pcs<br>'
    if (order.service.hasOwnProperty('bannerofEmail') && order.service.bannerofEmail > 0) str += 'Banner inside of e-mail alerts / ' + order.service.bannerofEmail + 'pcs<br>'
    return str

  }

  addCart(t, c, event) {
    if (c==0){return}
    var order         = {
      services: {}
    }
    order.services[t] = c
    this.shopService.addCart(order)

    console.log(event);

  }

  sad(e) {
    if (parseInt(e) < 0) {
      e = 0
    }

  }

  changeText(el) {
    let thisContext = this;
    el.textContent == this.translate.get('btn.BuyServiceButton.Buy')['value']
      ? el.textContent = this.translate.get('btn.BuyServiceButton.Added')['value']
      : this.translate.get('btn.BuyServiceButton.Buy')['value'];

    setTimeout(function () {
      el.textContent = thisContext.translate.get('btn.BuyServiceButton.Buy')['value'];
    }, 3000);


  }

  checkCount(e) {
    if (e.value > 1) {
      return true
    }
    //console.log(e.value)
    return false
    //if (e && e.value && e.value == 0) {
    //    return true
    //}
    //else {
    //    return false
    //}

  }
}
