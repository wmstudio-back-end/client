import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ShopService} from "../../../../_services/shop.service";
import {DataService} from "../../../../_services/data.service";
import {Isubscriptions} from "../../../../_models/service";

@Component({
  selector:   'app-shop-cart-modal',
  templateUrl:'./shop-cart-modal.component.html',
  styleUrls:  ['./shop-cart-modal.component.scss']
})
export class ShopCartModalComponent implements OnInit {

  @Input("show") cartModal = false
  @Output("apply") apply:void
  @Output("cartModal") show = new EventEmitter<boolean>();
  havepromo = false
  priceSubscription = 0
  allPrice = 0
  promoError: boolean = false;

  constructor(public shopService: ShopService,public dS:DataService) { }
  removeCart(type){
    var order = {
      services:{}
    }
    order.services[type]=0
    this.shopService.cart.services[type]=0;
    this.shopService.addCart(order)
  }
  getAllPrice(){
    this.allPrice = 0
    this.allPrice+=this.shopService.cartPrice.subscription||0;
    this.allPrice+=this.shopService.cartPrice.service.bannerofEmail||0;
    this.allPrice+=this.shopService.cartPrice.service.hightFrame||0;
    this.allPrice+=this.shopService.cartPrice.service.insocialMedia||0;
    this.allPrice+=this.shopService.cartPrice.service.premiumListing||0;
    this.allPrice+=this.shopService.cartPrice.service.targetGroup||0;
    this.allPrice+=this.shopService.cartPrice.service.vacancy||0;
    return this.allPrice
  }
  removeSubscr(){
    var order = {
      subscriptions:{
        packageId:null,
        periodId:null
      }
    }

    this.shopService.cart.subscriptions = <Isubscriptions>{
      packageId:null,
      periodId:null
    }
    this.shopService.addCart({})
    this.shopService.getPriceCart()
  }
  clickOutside(e){
    this.shopService.cart.promocode.length>0?this.havepromo=true:this.havepromo=false
    if (e.target.className=='icon cancel-black'||e.target.classList.contains('delete-forever-black')){
      return
    } else {
      this.cartModal = false;
      this.show.emit(this.cartModal)
    }

  }
  rounded(i){
    return parseFloat(i.toFixed(2))>0?i.toFixed(2):0;
  }

  ngOnChanges(changes: any) {

    if (changes.cartModal.currentValue==false){this.cartModal = changes.cartModal.currentValue}
    if (changes.cartModal.currentValue==true){this.cartModal = changes.cartModal.currentValue}
    this.show.emit(this.cartModal)


  }
  ngOnInit() {
    this.shopService.cart.promocode.length>0?this.havepromo=true:this.havepromo=false


  }

  setErrorPromo(error) {
    this.promoError = error
  }
}
