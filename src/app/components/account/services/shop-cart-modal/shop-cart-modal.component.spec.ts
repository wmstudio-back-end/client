import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShopCartModalComponent } from './shop-cart-modal.component';

describe('ShopCartModalComponent', () => {
  let component: ShopCartModalComponent;
  let fixture: ComponentFixture<ShopCartModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShopCartModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShopCartModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
