import {
  Component,
  OnInit
}                         from "@angular/core";
import {DataService}      from "../../../../_services/data.service";
import {ComponentService} from "../../../../_services/component.service";
import {UserService}      from "../../../../_services/user.service";
import {IVacancy}         from "../../../../_models/vacancy";


@Component({
  selector:    'app-bookmarked-vacancies',
  templateUrl: './bookmarked-vacancies.component.html',
  styleUrls:   ['./bookmarked-vacancies.component.scss']
})
export class BookmarkedVacanciesComponent implements OnInit {
  sorted                  = {
    posted:        0,
    deadLine:      0,
    rating:        0,
    salary:        0,
    typeOfVacancy: 0
  }
  filterSpActive: boolean = false

  constructor(
    public dS: DataService,
    public component: ComponentService,
    public uS: UserService
  ) {
  }

  bookVacancy: any[]

  ngOnInit() {
    var t = []
    this.uS.profile.bookmarks.items.sort(function(a,b){
      a.created_at - b.created_at
    })
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      t.unshift(this.uS.profile.bookmarks.items[i].resumeId)

    }
    this.dS.sendws('Vacancy/getVacancyFromIds', {ids: t}).then((data) => {
      this.bookVacancy = data['vacancy']
      for (var i = 0; i < this.bookVacancy.length; i++) {
        this.bookVacancy[i].bookmark = false
        for (var j = 0; j < this.uS.profile.bookmarks.items.length; j++) {
          if (this.uS.profile.bookmarks.items[j].resumeId == this.bookVacancy[i]._id) {
            this.bookVacancy[i].bookmark = true
            this.bookVacancy[i].create_bookmark = this.uS.profile.bookmarks.items[j].created_at
            break;
          }

        }
      }
      console.log(this.bookVacancy)
    })
  }

  sortedF(t, d = 0) {
    if (t == 'posted') {
      this.sorted.deadLine = 0
      this.sorted.rating   = 0
      this.sorted.salary   = 0
    }
    if (t == 'deadLine') {
      this.sorted.posted = 0
      this.sorted.rating = 0
      this.sorted.salary = 0
    }
    if (t == 'rating') {
      this.sorted.posted   = 0
      this.sorted.deadLine = 0
      this.sorted.salary   = 0
    }
    if (t == 'salary') {
      this.sorted.posted   = 0
      this.sorted.deadLine = 0
      this.sorted.rating   = 0
    }

    if (d) {
      this.sorted[t] = d
    }else{
      this.sorted[t]++
      if (this.sorted[t] > 2) {
        this.sorted[t] = 0
      }
    }
  }

  getOcF() {
    var t = Object.assign([], this.dS.options.vacancyTypeShort)

    t.unshift({
      id:   -1,
      text: "btn.seeAll"
    });
    return t
  }


  getBookmarked() {
    var t = []

    if (this.bookVacancy) {
      this.bookVacancy.sort(function(a,b){
        return b.create_bookmark - a.create_bookmark
      })


      if (this.sorted.deadLine) {
        this.bookVacancy.sort((a: any, b: any) => {
          if (this.sorted.deadLine && b.deadLine && a.deadLine) {
            if (this.sorted.deadLine == 1) {
              return b.deadLine - a.deadLine
            }
            if (this.sorted.deadLine == 2 && b.deadLine && a.deadLine) {
              return a.deadLine - b.deadLine
            }
          }
          return 1
        })
      }
      if (this.sorted.salary) {
        this.bookVacancy.sort((a: any, b: any) => {
          if (this.sorted.salary && b.salary.summ && a.salary.summ) {
            if (this.sorted.salary == 1 && b.salary.summ && a.salary.summ) {
              return b.salary.summ * (b.salary.type + 3) - a.salary.summ * (a.salary.type + 3)
            }
            if (this.sorted.salary == 2 && b.salary.summ && a.salary.summ) {
              return a.salary.summ * (a.salary.type + 3) - b.salary.summ * (b.salary.type + 3)
            }
          }

          return 1
        })
      }
      if (this.sorted.rating) {
        this.bookVacancy.sort((a: any, b: any) => {
          if (this.sorted.rating == 1 && this.uS.users[b.uid] && this.uS.users[a.uid]) {
            return this.uS.users[b.uid].rating - this.uS.users[a.uid].rating
          }
          if (this.sorted.rating == 2 && this.uS.users[b.uid] && this.uS.users[a.uid]) {
            return this.uS.users[a.uid].rating - this.uS.users[b.uid].rating
          }
          return 1
        })
      }
      if (this.sorted.posted) {
        this.bookVacancy.sort((a: any, b: any) => {
          if (this.sorted.posted == 1) {
            return b.created_at - a.created_at
          }
          if (this.sorted.posted == 2) {
            return a.created_at - b.created_at
          }
        })
      }

      for (var i = 0; i < this.bookVacancy.length; i++) {
        if (this.sorted.typeOfVacancy > 0) {
          if (this.sorted.typeOfVacancy == 1 && (this.bookVacancy[i].type == 0 || this.bookVacancy[i].type == 1)) {
            t.push(this.bookVacancy[i])
          }
          if (this.sorted.typeOfVacancy == 2 && (this.bookVacancy[i].type == 2 || this.bookVacancy[i].type == 3 || this.bookVacancy[i].type == 4)) {
            t.push(this.bookVacancy[i])
          }
        } else {
          t.push(this.bookVacancy[i])
        }

      }
    }

    return t
  }

  setBookmark(id, i) {
    this.uS.setBookmark(id)
    this.bookVacancy[i].bookmark = true

  }

  unsetBookmark(id, i) {
    this.uS.unsetBookmark(id)
    this.bookVacancy[i].bookmark = false

  }
}
