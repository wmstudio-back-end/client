import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../../_services/component.service";
import {UserService} from "../../../../_services/user.service";
import {TranslateService} from "@ngx-translate/core";
import {DataService} from "../../../../_services/data.service";
import {IResume} from "../../../../_models/resume";
import {Router} from "@angular/router";
import {MessageService} from "../../../../_services/message.service";
import {ResumeService} from "../../../../_services/resume.service";
import {DownPopupService} from "../../../../_services/down-popup.service";


@Component({
    selector:   'app-bookmarked-candidates',
    templateUrl:'./bookmarked-resume.html',
    styleUrls:  ['./bookmarked-resume.scss']
})
export class BookmarkedResumeComponent implements OnInit {

    goTo(location: string): void {
        window.location.hash = location;
    }
  //str += this.translate.get(this.dataService.getOptionsFromId(name,Object.keys(ob)[i]).text)['value']
  objectKeys = Object.keys
  notesFromResume = []
  resumes = []
  progetsList = []
  init = false
  messCount = {}
  recommCount = {}
  filterVal = -1
  sortAdded = false
  sortratingProfile = false
  //bookmarked_status
    settings = [

    ]
    notesVb:boolean = false;
    CoverLetterVb :boolean = false;
    candidateMenu :boolean = false;

    constructor(
      public component:ComponentService,
      public uS:UserService,
      public translate: TranslateService,
      public dS:DataService,
      public router: Router,
      public mS:MessageService,
      public rS:ResumeService,
      public popS:DownPopupService
    ) {

    }
  ngOnInit() {
    this.initProjects()



  }

  getResumes(projectID,typeId){

      var t = []
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {

      if (
        this.uS.profile.bookmarks.items[i].type==typeId
        &&projectID=='default'&&
        !this.uS.profile.bookmarks.items[i].hasOwnProperty('project')
      ){

        t.push(this.uS.profile.bookmarks.items[i].resumeId)

      }else if (
        this.uS.profile.bookmarks.items[i].type==typeId
        &&this.uS.profile.bookmarks.items[i].project==projectID
      ){

        t.push(this.uS.profile.bookmarks.items[i].resumeId)

      }
    }

   return t
  }

  getProgectCount(project){
    var count = 0
    for (var i = 0; i < project.types.length; i++) {
      count+=project.types[i].resumes.length

    }
    return count
  }
  vallnotes(resumes){
    console.log('===-------@@@')
    for (var i = 0; i < resumes.length; i++) {
      console.log('===',resumes[i])
      document.getElementById('notes_'+resumes[i]).classList&&
      document.getElementById('notes_'+resumes[i]).classList.contains('active')?
        document.getElementById('notes_'+resumes[i]).classList.remove('active'):
        document.getElementById('notes_'+resumes[i]).classList.add("active");

    }
  }



  jsstr(u){
    return JSON.stringify(u)
  }



  settingsFilter(){
    var t = [{
      id:-1,
      text:'All'
    }]
    for (var i = 0; i < this.dS.options.bookmarked_status.length; i++) {
      t.push({
        id:this.dS.options.bookmarked_status[i].id,
        text:this.dS.options.bookmarked_status[i].text
      })

    }
    return t
  }
  setFilter(e){
  this.filterVal = e
    this.initProjects()
  }


  getAction(r){
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==r._id){
        return this.uS.profile.bookmarks.items[i].action
      }
    }
  }
  getProjects(){
    var t = []
    for (var i = 0; i < this.uS.profile.Company.projects.length; i++) {
      t.push({
        id:this.uS.profile.Company.projects[i]._id,
        text:this.uS.profile.Company.projects[i].name
      })
    }
    return t
  }
  checkProjectBookM(r,e,pr,sett,res){
    if (!this.uS.accessPackage.projBmark){
      this.popS.show_success(this.dS.getOptionsFromId('errors',1).text)
      return
    }
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==r._id){
        this.uS.profile.bookmarks.items[i].project=e.id
        this.innerProject(this.uS.profile.bookmarks.items[i],r)
      }
    }
    this.progetsList[pr].types[sett].resumes.splice(res,1)

    this.dS.sendws('User/updateBookmark',{id:r._id,project:e.id})
  }
  getStatus(_id){
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==_id){
        return this.uS.profile.bookmarks.items[i].type
      }
    }
  }
  getCreatedAtBook(r){
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==r._id){
        return this.uS.profile.bookmarks.items[i].created_at
      }
    }
  }

  getFromBookM(r){
    for (var i = 0; i < this.uS.profile.bookmarks.items.length; i++) {
      if (this.uS.profile.bookmarks.items[i].resumeId==r._id){
        return this.uS.profile.bookmarks.items[i].from
      }
    }
  }


  sort_resumes(resumes){


    if(this.sortAdded){
      resumes.sort((a,b)=>{
        return a.updated_at-b.updated_at
      })
    }else{
      resumes.sort(function(a,b){
        return b.updated_at-a.updated_at
      })
    }
    if(this.sortratingProfile){
      var u = this.uS.users

      //resumes.sort((a,b)=>{
      //  !u[b.uid].rating?u[b.uid].rating=0:null
      //  !u[a.uid].rating?u[a.uid].rating=0:null
      //  return u[a.uid].rating-u[b.uid].rating
      //})
    }else{
      var u = this.uS.users
      //resumes.sort(function(a,b){
      //  !u[b.uid].rating?u[b.uid].rating=0:null
      //  !u[a.uid].rating?u[a.uid].rating=0:null
      //  return u[b.uid].rating-u[a.uid].rating
      //})
    }

    //for (var i = 0; i < this.resumes.length; i++) {
    //
    //  for (var j = 0; j < this.uS.profile.bookmarks.items.length; j++) {
    //    if (this.resumes[i]._id==this.uS.profile.bookmarks.items[j].resumeId){
    //      this.settings[this.uS.profile.bookmarks.items[j].type].resumes.push(this.resumes[i])
    //    }
    //  }
    //
    //}
  }

  getMessCount(uid){
  if (this.messCount[uid]){
  return this.messCount[uid]
  }
  return 0
  }
  getRecommCount(uid){
    if (this.recommCount[uid]){
      return this.recommCount[uid]
    }
    return 0
  }

  innerProject(project,resume){
    var pId = project.project?project.project:'default'
    for (var i = 0; i < this.progetsList.length; i++) {
      if (this.progetsList[i]._id==pId){

        this.progetsList[i].types[project.type].resumes.push(resume)
      }
    }
  }

  initTypes(progectId,show = false){
    var sett = []
    for (var i = 0; i < this.dS.options.bookmarked_status.length; i++) {
      var resumeIds = []
      let items = this.uS.profile.bookmarks.items
      items.sort(function(a,b){
        return a.created_at - b.created_at
      })
      for (var j = 0; j < items.length; j++) {
        if (
          (items[j].project==progectId
          ||
            (
              progectId=='default'
              &&!items[j].project
            )
          )
          &&items[j].type==this.dS.options.bookmarked_status[i].id
        ){
          resumeIds.unshift(items[j].resumeId)
        }

      }
      var added = true
      if (this.filterVal>-1){
        added = false
        if (this.dS.options.bookmarked_status[i].id==this.filterVal){
          added = true
        }

      }else{
        added = true
      }
      if (added){
        sett.push({
          id:this.dS.options.bookmarked_status[i].id,
          text:this.dS.options.bookmarked_status[i].text,
          resumes:resumeIds,
          show:show
        })
      }


    }
    console.log(sett)
    return sett
  }


  initProjects(){
    this.progetsList= [{
      _id:"default",
      name:'default',
      created_at:0,
      types:this.initTypes('default')

    }]
    for (var i = 0; i < this.uS.profile.Company.projects.length; i++) {
      this.progetsList.push({
        _id:this.uS.profile.Company.projects[i]._id,
        name:this.uS.profile.Company.projects[i].name,
        created_at:this.uS.profile.Company.projects[i].created_at,
        show:false,
        types:this.initTypes(this.uS.profile.Company.projects[i]._id)
      })
    }
  }





}
