import {Component, Inject, OnInit, Pipe} from "@angular/core";
import {ComponentService} from "../../../_services/component.service";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
import {CompanyProjects} from "../../../_models/user";
import {ActivatedRoute} from "@angular/router";
import {PageScrollConfig, PageScrollService, PageScrollInstance } from 'ng2-page-scroll';
import { DOCUMENT} from '@angular/common';
import {HelpersService} from "../../../_services/helpers.service";
import {DownPopupService} from "../../../_services/down-popup.service";
@Component({
  selector:   'app-projects',
  templateUrl:'./projects.component.html',
  styleUrls:  ['./projects.component.scss']
})
export class CompanyProjectsComponent implements OnInit {
  formNewProject:boolean = false
  projectVac = {}
  projectSavSearch = {}
  projectBookm = {}
  iproject = <CompanyProjects>{}
  editProjectId = null
  constructor(
    private pageScrollService: PageScrollService,
    @Inject(DOCUMENT) private document: any,
    public component: ComponentService,
    public userService:UserService,
    public helpers:HelpersService,
    private dataService:DataService,
    public popS:DownPopupService,
    private route: ActivatedRoute,) {


  }
  getProgect(){

  }
  ngOnInit() {

    for (var i = 0; i < this.userService.profile.Company.projects.length; i++) {
      this.projectVac[this.userService.profile.Company.projects[i]._id] = {
        vacancy:[],
        savedSearches:[],
        vacallcand:0,
        vacnewcand:0,
        vacunreadcand:0,
        vacsavedcand:0,

        savallcand:0,
        savnewcand:0,
        savunreadcand:0,
        savsavedcand:0
      }
    }
    for(var i = 0;i<this.userService.profile.Company.vacancy.length;i++){
      if (this.userService.profile.Company.vacancy[i].projectID){
        var prId = this.userService.profile.Company.vacancy[i].projectID
        if (!this.projectVac[prId]){
          this.projectVac[prId] = {
            vacancy:[],
            savedSearches:[],
            vacallcand:0,
            vacnewcand:0,
            vacunreadcand:0,
            vacsavedcand:0,

            savallcand:0,
            savnewcand:0,
            savunreadcand:0,
            savsavedcand:0
          }
        }
        this.projectVac[prId].vacancy.push(this.userService.profile.Company.vacancy[i])

      }
    }
    //кол-во сохраненных поисков
    for (var i = 0; i < this.userService.profile.savedSearches.length; i++) {
      if(this.userService.profile.savedSearches[i].project&&this.projectVac[this.userService.profile.savedSearches[i].project]){
        this.projectVac[this.userService.profile.savedSearches[i].project].savedSearches.push(this.userService.profile.savedSearches[i])
        this.projectVac[this.userService.profile.savedSearches[i].project].savallcand+=this.userService.profile.savedSearches[i].ids?this.userService.profile.savedSearches[i].ids.length:0
        this.projectVac[this.userService.profile.savedSearches[i].project].savnewcand+=this.userService.profile.savedSearches[i].count_new?this.userService.profile.savedSearches[i].count_new.length:0
        this.projectVac[this.userService.profile.savedSearches[i].project].savunreadcand+=this.userService.profile.savedSearches[i].count_unread?this.userService.profile.savedSearches[i].count_unread.length:0
      }
    }
  //кол-во вакансий


    for(var s = 0;s<this.userService.profile.subscribeCandidate.length;s++){
      for(var i = 0;i<this.userService.profile.Company.vacancy.length;i++){
        if (
            this.userService.profile.subscribeCandidate[s].vacancyId==this.userService.profile.Company.vacancy[i]._id
            &&this.userService.profile.Company.vacancy[i].projectID
        ){
          this.projectVac[prId].vacallcand++;
          if (this.userService.profile.subscribeCandidate[s].isnew==0){this.projectVac[prId].vacnewcand++}
          if (this.userService.profile.subscribeCandidate[s].isview==0){this.projectVac[prId].vacunreadcand++}
        }
      }
    }
    for(var s = 0;s<this.userService.profile.bookmarks.items.length;s++){
      if (this.projectVac[this.userService.profile.bookmarks.items[s].project]){
        this.projectVac[this.userService.profile.bookmarks.items[s].project].vacsavedcand++
      }
    }
    //кол-во вакансий

    if (this.route.snapshot.params['id']){
      var s=0
      for (var i = 0; i < this.userService.profile.Company.projects.length; i++) {
        if (this.route.snapshot.params['id']==this.userService.profile.Company.projects[i]._id){
          this.editProject(this.route.snapshot.params['id'],i)
        }

      }

    }

  }

  deleteProject(_id,i){

    if (!this.userService.accessPackage.projBmark){
      this.popS.show_warning(this.dataService.getOptionsFromId('errors',10).text)
      return
    }
    if (this.userService.accessRight==3){
      this.popS.show_info(this.dataService.getOptionsFromId('errors',28).text)
      return
    }

    if (this.helpers.confirm(0)) {
        this.dataService.sendws('Vacancy/deleteProject', {_id: _id}).then((data) => {
          this.userService.profile.Company.projects.splice(i, 1)

        })
    }

  }
  editProject(_id,i){
    if (!this.userService.accessPackage.projBmark){
      this.popS.show_warning(this.dataService.getOptionsFromId('errors',10).text)
      return
    }
    if (this.userService.accessRight==3){
      this.popS.show_info(this.dataService.getOptionsFromId('errors',28).text)
      return
    }
    this.editProjectId = _id
    this.formNewProject = true
    this.iproject = <CompanyProjects>{

    }
    setTimeout(function(){
      this.iproject = <CompanyProjects>Object.assign({},this.userService.profile.Company.projects[i])
      let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(this.document, '.sideContent');
      this.pageScrollService.start(pageScrollInstance);
    }.bind(this),1)
  }
  addNewProject(){

    if (!this.userService.accessPackage.projBmark){
      this.popS.show_warning(this.dataService.getOptionsFromId('errors',10).text)
      return
    }
    if (this.userService.accessRight==3){
      this.popS.show_info(this.dataService.getOptionsFromId('errors',28).text)
      return
    }
    this.editProjectId = null
    this.formNewProject=!this.formNewProject
    if (this.formNewProject&&this.editProject){
      this.iproject = <CompanyProjects>{}
    }
  }
  createNewProject(){

    if (!this.userService.accessPackage.projBmark){
      this.popS.show_warning(this.dataService.getOptionsFromId('errors',10).text)
      return
    }
    if (this.userService.accessRight==3){
      this.popS.show_info(this.dataService.getOptionsFromId('errors',28).text)
      return
    }
    if (this.iproject.name!=""){
      if (this.editProjectId){
        this.dataService.sendws('Vacancy/editProject',this.iproject).then((data)=>{
          for(var i=0;i<this.userService.profile.Company.projects.length;i++){
            if (this.userService.profile.Company.projects[i]._id==this.editProjectId){
              this.userService.profile.Company.projects[i] = this.iproject
            }
          }
          this.formNewProject = false


        })
      }else{
        this.iproject.uid = this.userService.profile._id
        this.dataService.sendws('Vacancy/createNewProject',this.iproject).then((data)=>{

          this.userService.profile.Company.projects.push(data['project'])
          this.formNewProject = false
          this.ngOnInit()
        })
      }



    }else{
      this.popS.show_success(this.dataService.getOptionsFromId('errors',23).text)

    }

  }
}
