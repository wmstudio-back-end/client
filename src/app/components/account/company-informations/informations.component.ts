import {
  Component,
  OnInit
}                         from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {UserService}      from "../../../_services/user.service";
import {DataService}      from "../../../_services/data.service";
import {LocationService}  from "../../../_services/location.service";
import {ILocation}        from "../../../_models/user";
import {HelpersService}   from "../../../_services/helpers.service";
import {DownPopupService} from "../../../_services/down-popup.service";

@Component({
  selector:    'app-informations',
  templateUrl: './informations.component.html',
  styleUrls:   ['./informations.component.scss']
})
export class CompanyInformationsComponent implements OnInit {
  photo: any;
  Address: string
  Location               = <ILocation>{};
  addLocate              = false
  arrCompanies: string[] = []
  nameCompany: string
  validVideoURL: boolean = true
  addAddr                = false

  constructor(public component: ComponentService,
              public userService: UserService,
              public locationService: LocationService,
              public helpers: HelpersService,
              public popS: DownPopupService,
              public dataService: DataService) {
    this.photo = {path: this.userService.profile.Photo};
    //if (typeof this.userService.profile.Company['AdditionalAddresses'] === "undefined"){
    //    this.userService.profile.Company['AdditionalAddresses'] = [];
    //}
  }

  public vatnumberMask: Array<string | RegExp> = [
    /[0-9]/,
    /[0-9]/,
    /[0-9]/,
    /[0-9]/,
    /[0-9]/,
    /[0-9]/,
    /[0-9]/,
    /[0-9]/,
    /[0-9]/,
    /[0-9]/,
    /[0-9]/
  ]

  setPhoto(e) {
    this.photo = e
    console.log(e)
  }

  ngOnInit() {
    if (!this.userService.profile.Company.AdditionalAddresses) {
      this.userService.profile.Company.AdditionalAddresses = []
    }
    this.nameCompany = this.userService.profile.Company.nameCompany
    if (!this.userService.profile.Company.Established){
      this.userService.profile.Company.Established = ''
    }
  }

  addAddres(e) {

    this.userService.profile.Company.AdditionalAddresses.push(e)
  }

  delAddres(i) {
    this.userService.profile.Company.AdditionalAddresses.splice(i, 1)
  }

  activityField(e) {

    this.userService.profile.Company.activityField = []
    for (var i = 0; i < e.length; i++) {
      this.userService.profile.Company.activityField.push(e[i].id)
    }
  }

  save() {


    if (this.userService.accessRight != 0) {
      this.popS.show_info(this.dataService.getOptionsFromId('errors', 28).text)
      return
    }
    if (!this.userService.profile.Company.videoUrl) {
      this.userService.profile.Company.videoUrl = ''
    }
    var reg                                   = new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")
    this.userService.profile.Company.videoUrl = this.userService.profile.Company.videoUrl.trim()
    if (this.userService.profile.Company.videoUrl != '') {
      if (!reg.test(this.userService.profile.Company.videoUrl)) {
        this.validVideoURL = false
        return
      }
    }


    this.validVideoURL = true

    this.userService.profile.Company.nameCompany = this.nameCompany

    this.dataService.sendws('User/editCompanyInfo', {
      nameCompany:   this.nameCompany,
      regCode:       this.userService.profile.Company.regCode,
      vatnumber:     this.userService.profile.Company.vatnumber,
      mobilePhone:   this.userService.profile.Company.mobilePhone,
      landlinePhone: this.userService.profile.Company.landlinePhone,
      Address:       this.userService.profile.Company.Address,
      aboutCompany:  this.userService.profile.Company.aboutCompany,

      videoUrl: this.userService.profile.Company.videoUrl,

      activityField:       this.userService.profile.Company.activityField,
      webSite:             this.userService.profile.Company.webSite,
      skills:              this.userService.profile.Company.skills,
      Established:         this.userService.profile.Company.Established,
      companySize:         this.userService.profile.Company.companySize,
      legalFormBusiness:   this.userService.profile.Company.legalFormBusiness,
      AdditionalAddresses: this.userService.profile.Company.AdditionalAddresses,

    })
  }

  getData(e, t) {
    if (t == 0) {
      this.nameCompany = e
      this.dataService.sendws('Company/searchCompaniesFromName', {
        nameCompany: e,
        type:        t
      }).then(data => {
        this.arrCompanies = data['company']
      })
    } else {
      this.nameCompany = e
    }

  }

  addInput() {


    this.Location = <ILocation>{}
    if (!this.userService.profile.Company.AdditionalAddresses) {
      this.userService.profile.Company.AdditionalAddresses = []
    }
    var add = true
    for (var i = 0; i < this.userService.profile.Company.AdditionalAddresses.length; i++) {
      if (!this.userService.profile.Company.AdditionalAddresses[i].addr) {
        add = false
      }

    }
    if (add) {
      this.userService.profile.Company.AdditionalAddresses.push(this.Location)
    } else {
      // this.helpers.alert(16)
      // alert('enter Address')
    }

    //this.clearLocate()


  }

  clearLocate() {
    this.Location = <ILocation>{}
    this.Address  = ''
    //setTimeout(function(){
    //  this.Location = <ILocation>{}
    //  this.Address = ''
    //  this.addLocate = false
    //
    //}.bind(this),10)
  }
}

