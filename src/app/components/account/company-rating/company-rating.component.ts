import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";
import {MessageService} from "../../../_services/message.service";
import {Router} from "@angular/router";
import {DownPopupService} from "../../../_services/down-popup.service";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector: 'app-company-rating',
  templateUrl: './company-rating.component.html',
  styleUrls: ['./company-rating.component.scss']
})

export class CompanyRatingComponent implements OnInit {
  rewiews = []
  rating_undefuser:boolean = false
  rating_websitevis:boolean = false
  rating_career=0
  rating_culture=0
  rating_managment=0
  rating_salary=0
  rating_work=0
  limit = 3
  ratingPrc = 0
  constructor(
    public component:ComponentService,
    public dS:DataService,
    public uS:UserService,
    public mS:MessageService,
    public popS:DownPopupService,
    public hS:HelpersService,
    private router:Router
  ) { }
  setDialog(uid){
    var send = false
    for (var j = 0; j < this.uS.profile.service.orders.length; j++) {
      if (this.uS.profile.service.orders[j].status>0){
        send = true
      }
    }
    if (send){
      this.mS.setDialogs(uid);
      this.router.navigate(['/messages']);
    }
    if (!send){
      this.hS.alert(4)
      // alert('Сделайте хотя бы одну оплату для отправки сообщения')
      return
    }

  }
  sendMess(e,id,i){

    var send = false
    for (var j = 0; j < this.uS.profile.service.orders.length; j++) {
      if (this.uS.profile.service.orders[j].status>0){
        send = true
      }
    }
    if (!send){
      this.hS.alert(4)
      // alert('Сделайте хотя бы одну оплату для отправки сообщения')
      return
    }
    if ( this.uS.profile.proof>0){send = true;}
    if (!send){
      this.hS.alert(5)
      // alert('Подтвердите свой аккаунт для отправки сообщения ')
      return
    }
    this.dS.sendws('Public/newReviewComment',{cid:id,reply_text:e.value}).then(data=>{
      console.log(this.rewiews)
      for (var j = 0; j < this.rewiews.length; j++) {
        if (this.rewiews[j]._id==data['res'].cid){
          this.rewiews[j].replies.push(data['res'])
        }

      }
      //
      e.value = ''
    })

  }
  savePublSetting(){
    if (!this.uS.accessPackage.rating){
      this.popS.show_success(this.dS.getOptionsFromId('errors',1).text)

      return
    }


    this.dS.sendws('User/savePublSetting',{rating_undefuser:this.rating_undefuser,rating_websitevis:this.rating_websitevis}).then(data=>{
      this.rating_career=0
      this.rating_culture=0
      this.rating_managment=0
      this.rating_salary=0
      this.rating_work=0
      this.limit = 3
      this.ratingPrc = 0
      this.rewiews = []
      this.uS.profile.Company.rating_undefuser =   this.rating_undefuser
     this.uS.profile.Company.rating_websitevis = this.rating_websitevis
      this.ngOnInit()
    })
  }
  incLimit(i){
    i.limit+=999
  }
  ReportAbuse(_id,t){
    this.dS.sendws('Public/reportComment',{type:t,record_id:_id}

    ).then(data=>{
      if (data['success']){
        this.popS.show_info(this.dS.getOptionsFromId('errors',27).text)
      }
    })

  }
  ngOnInit() {

    this.rating_undefuser = this.uS.profile.Company.rating_undefuser
    this.rating_websitevis = this.uS.profile.Company.rating_websitevis

    this.dS.sendws('Public/getReviews',{companyId:this.uS.profile._id}).then(data=>{


      var ids = []
      var prc = 0
      var i = 0
      for (; i < data['res'].length; i++) {
        data['res'][i].limit = 3
        this.rating_career+=data['res'][i].rating_career
        this.rating_culture+=data['res'][i].rating_culture
        this.rating_managment+=data['res'][i].rating_managment
        this.rating_salary+=data['res'][i].rating_salary
        this.rating_work+=data['res'][i].rating_work
        ids.push(data['res'][i].uid)
        if (data['res'][i].replys){
          for (var j = 0; j < data['res'][i].replys.length; j++) {
            ids.push(data['res'][i].replys[j].uid)

          }
        }

        if (data['res'][i].comment_recommend==true){
          prc++
        }
      }

      this.ratingPrc = Math.floor(prc*100/i)
      this.rating_career=~~this.rating_career/i
      this.rating_culture=~~this.rating_culture/i
      this.rating_managment=~~this.rating_managment/i
      this.rating_salary=~~this.rating_salary/i
      this.rating_work=~~this.rating_work/i

      this.uS.getUsers(ids).then(users=>{
        this.rewiews = data['res']

      })

    })

  }
  calcRating(item){
    return ~~(item.rating_career+
    item.rating_culture+
    item.rating_managment+
    item.rating_salary+
    item.rating_work)/5

  }
}
