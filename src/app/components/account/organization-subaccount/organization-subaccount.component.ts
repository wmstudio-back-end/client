import { Component, OnInit } from '@angular/core';
import {ComponentService} from "../../../_services/component.service";
import {DataService} from "../../../_services/data.service";
import {UserService} from "../../../_services/user.service";

@Component({
  selector:   'app-company-subaccount',
  templateUrl:'./organization-subaccount.component.html',
  styleUrls:  ['./organization-subaccount.component.scss']
})
export class OrganizationSubaccountComponent implements OnInit {
Subaccount = {
  first_name:'',
  last_name: '',
  email:     '',
  password:  '',
  access:null

}
  readonly:boolean =false

  constructor(public component:ComponentService,public dataService:DataService,public userService:UserService) { }

  ngOnInit() {
    if (this.userService.profile.access==3){
      this.readonly = true
    }
  }
  createSubaccount(){

   this.dataService.sendws('User/createSubAccount',this.Subaccount).then(data=>{
     if (data.hasOwnProperty('_id')){
       this.userService.profile.subaccounts.push(data)
     }
   })
  }

}

