import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../../../../_services/data.service";

@Component({
  selector: 'app-detail-resume-bookmark',
  templateUrl: './detail-resume-bookmark.component.html',
  styleUrls: ['./detail-resume-bookmark.component.scss']
})
export class DetailResumeBookmarkComponent implements OnInit {
  @Input() resume:any
  constructor(public dS:DataService) { }

  ngOnInit() {
  }

}
