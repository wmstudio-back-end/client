import {Component, Input, OnInit} from "@angular/core";
import {UserService} from "../../../../_services/user.service";
import {AuthenticationService} from "../../../../_services/authentication.service";
import {DataService} from "../../../../_services/data.service";

@Component({
  selector: 'profile-userline',
  templateUrl: './user-line.component.html',
  styleUrls: ['./user-line.component.scss']
})
export class UserLineComponent implements OnInit {
  @Input() text: string;
  @Input() subtext: string;
  @Input() btn: string;
  bar:boolean = false;

  @Input() arrowUl: boolean = false;
  statusArr = [{class:'active',text:'Active'},{class:'suspended',text:'Suspended'},{class:'invited',text:'Invited'}]
  accessArr = [{id:0,text:'full access'},{id:1,text:'view and create'},{id:2,text:'view only'}]
  constructor(public user: UserService, public authenticationService: AuthenticationService,private dataService:DataService) {
  }

  ngOnInit() {
    if (this.user.profile.invite){
      //this.user.profile.invite.push()
    }

  }
  pushMe(){
    var t = this.user.profile.invite
   return t
  }
  setProfileInvite(_id,i){

    this.user.accessRight = i
    this.dataService.sendws('User/setProfileInvite',{_id:_id,i:i}).then((data)=>{
      this.user.getUsers([_id]).then(users=>{

        // this.user.setProfile(users['users'][0]['Company'])
        // if (data['setProfileInvite'].hasOwnProperty('service')){
        //    data['setProfileInvite'].service = this.user.profile.service
        // }

        this.user.setProfile(data['setProfileInvite'])
      })



      //this.user.profile = this.user.profile,data['setProfileInvite']
      //if (data['rights'].hasOwnProperty('vacancy')){
      //  this.user.profile.Company.vacancy = data['rights'].vacancy
      //}
      //console.log(data['rights'].vacancy)
      //this.user.profile.Company.vacancy = data['setProfileInvite'].vacancy
      //this.user.profile.Company = data['setProfileInvite'].Company
      //this.user.profile.messages = data['setProfileInvite'].messages


    })

  }
  logout() {
    this.authenticationService.logout()
  }

  rightbtn() {
    if (this.btn == 'logout') {
      return true
    }
    return false
  }
}
