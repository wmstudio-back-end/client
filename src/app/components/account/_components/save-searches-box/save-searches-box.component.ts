import {Component, Input, OnInit} from '@angular/core';
import {SaveSearch} from "../../../../_models/saveSearch";
import {VacancyService} from "../../../../_services/vacancy.service";
import {UserService} from "../../../../_services";

@Component({
    selector: 'profile-save-searches-box',
    templateUrl: './save-searches-box.component.html',
    styleUrls: ['./save-searches-box.component.scss']
})
export class SaveSearchesBoxComponent implements OnInit {
    @Input('searches') searchesIn: SaveSearch[]
    @Input() limit: number = 0
    @Input() type:string
    public searches: SaveSearch[] = []

    constructor(vacancyService:VacancyService, public uS: UserService ) {

    }

    ngOnInit() {
        this.limit = (this.searchesIn.length <=3)?this.searchesIn.length:this.limit
        this.checkSavedSearches()
    }

    checkSavedSearches(){
        if (!this.searchesIn) return false;
        for(let i:number=0;i<this.limit;i++){
            var search = this.searchesIn[i]
            if (typeof search['query'] === "undefined") continue
            search.discovered = search.count_new.length
            this.searches.push(search)
        }
    }

    getSavedSearchesNewCount(search){
      console.log('=============================')
      console.log(search.count_new)
        let count:number=0;

        return count;
    }

}
