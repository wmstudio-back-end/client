import {Component, Input, OnInit} from "@angular/core";
import {UserService} from "../../../../_services/user.service";
import {MessageService} from "../../../../_services/message.service";
import {ActivatedRoute, Router} from "@angular/router";
import {DataService} from "../../../../_services/data.service";
@Component({
  selector: 'account-messages_block',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  @Input() limit: number = 2;

  constructor(public user: UserService,
              public dS:DataService,
              public mS:MessageService,
              public router: Router,
              public route: ActivatedRoute,) {

  }

  ngOnInit() {

    this.mS.messages.sort(function(a,b){
      return b.date-a.date
    })

  }

}
