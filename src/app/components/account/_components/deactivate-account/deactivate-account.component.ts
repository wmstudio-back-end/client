import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
}                              from '@angular/core';
import {UserService}           from "../../../../_services/user.service";
import {DataService}           from "../../../../_services/data.service";
import {DownPopupService}      from "../../../../_services/down-popup.service";
import {AuthenticationService} from "../../../../_services/authentication.service";

@Component({
  selector:    'app-deactivate-account',
  templateUrl: './deactivate-account.component.html',
  styleUrls:   ['./deactivate-account.component.scss']
})
export class DeactivateAccountComponent implements OnInit {
  yourPass: string
  @Input() openModal: boolean = false;
  @Output() openModalB        = new EventEmitter<boolean>()
  @Output() closeModal        = new EventEmitter<boolean>()

  constructor(public uS: UserService, public dS: DataService, public popS: DownPopupService, private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
  }

  deactivateAcc() {
    this.dS.sendws('Admin/deactivateAccount', {yourPass: this.yourPass}).then(data => {

      if (data['result'] == false) {
        this.popS.show_success(this.dS.getOptionsFromId('errors', 7).text)
        this.closeModal.emit(true)
      } else {
        this.openModalB.emit(false)
        this.openModal = false
        this.closeModal.emit(true)
        this.authenticationService.logout()
      }

    })

  }
}
