import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
}                       from '@angular/core';
import {ShopService}    from "app/_services/shop.service";
import {noUndefined}    from "@angular/compiler/src/util";
import {DataService}    from "../../../../_services/data.service";
import {UserService}    from "../../../../_services/user.service";
import {HelpersService} from "../../../../_services/helpers.service";
import {
  Router,
  ActivatedRoute,
  Params
}                       from "@angular/router";


@Component({
  selector:    'app-service-modal',
  templateUrl: './service-modal.component.html',
  styleUrls:   ['./service-modal.component.scss']
})
export class ServiceModalComponent implements OnInit {
  /**
   * show type active modal
   *
   * @number 0  Close modal
   * @number 1  Modal order subscription
   * @number 2  Modal order additional vacancies
   * @number 3  Modal order service duration
   * @number 4  Modal order service document
   *
   *
   */

  @Input() active       = 0
  @Output() activeClose = new EventEmitter<number>()
  @Output() order       = new EventEmitter<any>()
  promocode             = false
  packageId: string
  periodId: number
  promocodeVal: string
  vacancyId: string
  periodListingDays: number
  Specify: string
  vacancy               = []
  duration              = []
  uploadFile            = []

  constructor(
    public dS: DataService,
    public uS: UserService,
    public shopService: ShopService,
    private activatedRoute: ActivatedRoute,
    private hS: HelpersService
  ) {


  }

  cdd(e) {
    console.log(e)
  }

  setSingleFile(file) {
    this.uploadFile = [file]

  }

  getVacancy() {
    for (var i = 0; i < this.uS.profile.Company.vacancy.length; i++) {
      if (
        this.uS.profile.Company.vacancy[i].publish == true
        && this.uS.profile.Company.vacancy[i].edit == false
        && this.uS.profile.Company.vacancy[i].proof == 2
        &&
        (
          (this.hS.getUnixTimestamp() + (86400) < this.uS.profile.Company.vacancy[i].deadLine
            && this.hS.getUnixTimestamp() + (86400) < this.uS.profile.Company.vacancy[i].published_at
            && (this.active == 0 || this.active == 1 || this.active == 2 || this.active == 3)
          )
          ||
          (
            this.uS.profile.Company.vacancy[i].deadLine - (86400 * 10) > this.hS.getUnixTimestamp()
            && this.uS.profile.Company.vacancy[i].published_at - (86400 * 10) > this.hS.getUnixTimestamp()

            && (this.active == 4 || this.active == 5 || this.active == 6)
          )
        )
      //this.uS.profile.Company.vacancy[i].published_at>this.hS.getUnixTimestamp()
      ) {


        this.vacancy.push({
          id:   this.uS.profile.Company.vacancy[i]._id,
          text: this.uS.profile.Company.vacancy[i].positionTitle,
        })
      }
    }
    return this.vacancy
  }

  getDuration(type) {

    this.duration = []
    var minDur    = []
    var vac       = ''
    this.periodId = null
    for (var i = 0; i < this.uS.profile.Company.vacancy.length; i++) {
      if (this.vacancyId == this.uS.profile.Company.vacancy[i]._id) {
        if (this.active == 0 || this.active == 1 || this.active == 2 || this.active == 3) {
          minDur.push(Math.floor((this.uS.profile.Company.vacancy[i].published_at - this.hS.getUnixTimestamp() + (86400)) / 86400))
          minDur.push(Math.floor((this.uS.profile.Company.vacancy[i].deadLine - this.hS.getUnixTimestamp() + (86400)) / 86400))
          minDur.push(this.uS.profile.service.active[type])
          minDur.sort(function (a, b) {
            return a - b;
          })
          console.log(minDur[0])
          for (var s = 0; s < minDur[0] && s < 30; s++) {

            this.duration.push({
              id:   s + 1,
              text: s + 1,
            })

          }
        }
      }
    }

  }

  updateDuration(type) {
    this.getDuration(type)
    // this.getDuration('hightFrame')
    // this.getDuration('targetGroup')
    // this.getDuration('bannerofEmail')
  }

  ngOnChanges(changes: any) {
    //console.log(changes.active.currentValue)
    if (changes.hasOwnProperty('active')) {

      this.getVacancy()
      this.vacancy           = []
      this.duration          = []
      this.uploadFile        = []
      this.periodId          = null
      this.vacancyId         = null
      this.periodListingDays = null
      this.Specify           = null
      this.getVacancy()

      //changes.active.currentValue==2?this.getDuration('premiumListing'):null
      //changes.active.currentValue==3?this.getDuration('hightFrame'):null
      //  changes.active.currentValue==5?this.getDuration('targetGroup'):null
      //    changes.active.currentValue==6?this.getDuration('bannerofEmail'):null
      this.active = changes.active.currentValue
    }

  }

  ngOnInit() {
    console.log(this.dS.packages)
    this.packageId = this.shopService.cart.subscriptions.packageId
    this.periodId  = this.shopService.cart.subscriptions.periodId

    this.activatedRoute.queryParams.subscribe((params: Params) => {
      if (params.hasOwnProperty('package')) {
        let selectPackage = params['package'];
        for(let i of this.dS.options.packages){
          if (i.text.toLowerCase() === selectPackage.toLowerCase())
            this.packageId = i.id
        }
      }
    });

  }

  closeModal() {
    this.activeClose.emit(0)
  }

  clickexclude(e) {

    if (
      !e.target.classList.contains('button') &&
      !e.target.classList.contains('text-ic__text') &&
      !e.target.classList.contains('modalbtn') &&
      !e.target.classList.contains('ic_close_birch') &&
      !e.target.classList.contains('ic_check_blue')
    ) {

      //console.log('====CLOSE======',e.target)
      this.closeModal()
    }
  }

  initOrder(type) {
    if (this.vacancyId) {
      var i = this.uS.profile.Company.vacancy.findIndex((e, i, a) => {
        return (e._id == this.vacancyId) ? true : false;
      })
      if (i > -1) {
        this.uS.profile.Company.vacancy[i].activInstruction[type].values = 1
        this.uS.profile.Company.vacancy[i].activInstruction[type].text   = this.Specify
        this.uS.profile.Company.vacancy[i].activInstruction[type].files  = this.uploadFile
        this.dS.sendws('Vacancy/updateVacancy', {
          vacancy: this.uS.profile.Company.vacancy[i],
          options: {setServices: [type]}
        }).then((data) => {
          if (data['vacancy']) {
            this.uS.profile.Company.vacancy[i] = data['vacancy']
          }
          if (data['profile']) {
            this.uS.profile.service.active = data['profile']
          }
          this.closeModal()
        })
        console.log(this.uS.profile.Company.vacancy)
      }


      //
      // this.dS.sendws('Vacancy/initOrder',{type:type,vacancyId:this.vacancyId,Specify:this.Specify,uploadFile:this.uploadFile}).then(data=>{
      //
      //   data&&data.hasOwnProperty('bannerofEmail')?this.uS.profile.service.active.bannerofEmail=data['bannerofEmail']:null;
      //   data&&data.hasOwnProperty('targetGroup')?this.uS.profile.service.active.targetGroup=data['targetGroup']:null;
      //   data&&data.hasOwnProperty('insocialMedia')?this.uS.profile.service.active.insocialMedia=data['insocialMedia']:null;
      //   data&&data.hasOwnProperty('error')?alert(data['error'].message):null;
      //   //this.uS.profile.service.active.insocialMedia =  data['premiumListing']
      //   //var i = this.hS.findObject(this.uS.profile.Company.vacancy,'_id',this.vacancyId)
      //   //if (i>-1){this.uS.profile.Company.vacancy[i].PROMinsocialMediaVal= data['PROMinsocialMediaVal']}
      //   this.closeModal()
      // })
    }
  }

  initPremiumlisting() {

    if (this.vacancyId && this.periodListingDays) {
      var i = this.uS.profile.Company.vacancy.findIndex((e, i, a) => {
        return (e._id == this.vacancyId) ? true : false;
      })
      if (i > -1) {
        this.uS.profile.Company.vacancy[i].activInstruction.premiumListing.values = this.periodListingDays
        this.dS.sendws('Vacancy/updateVacancy', {
          vacancy: this.uS.profile.Company.vacancy[i],
          options: {setServices: ['premiumListing']}
        }).then((data) => {
          if (data['vacancy']) {
            this.uS.profile.Company.vacancy[i] = data['vacancy']
          }
          if (data['profile']) {
            this.uS.profile.service.active = data['profile']
          }
          this.closeModal()
        })
        console.log(this.uS.profile.Company.vacancy)
      }
    }

  }

  initHighlihtedFrame() {
    if (this.vacancyId && this.periodListingDays) {
      var i = this.uS.profile.Company.vacancy.findIndex((e, i, a) => {
        return (e._id == this.vacancyId) ? true : false;
      })
      if (i > -1) {
        this.uS.profile.Company.vacancy[i].activInstruction.hightFrame.values = this.periodListingDays
        this.dS.sendws('Vacancy/updateVacancy', {
          vacancy: this.uS.profile.Company.vacancy[i],
          options: {setServices: ['hightFrame']}
        }).then((data) => {
          if (data['vacancy']) {
            this.uS.profile.Company.vacancy[i] = data['vacancy']
          }
          if (data['profile']) {
            this.uS.profile.service.active = data['profile']
          }
          this.closeModal()
        })
        console.log(this.uS.profile.Company.vacancy)
      }


    }
  }

  addtoCart() {
    if (this.packageId != undefined && this.periodId != undefined) {
      var ob = {
        subscriptions: {
          packageId: this.packageId,
          periodId:  this.periodId
        },
        promocode:     this.promocodeVal ? this.promocodeVal : null
      }
      this.shopService.addCart(ob)
      this.closeModal()
    }

  }

}
