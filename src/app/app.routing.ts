﻿import {RouterModule, Routes} from "@angular/router";
import {StyleguideComponent} from "./components/styleguide/styleguide.component";
import {AppComponent} from "./app.component";
import {ComponentService} from "./_services/component.service";
import {MessagesComponent} from "./components/messages/messages.component";
import {EducatorRecommendationsComponent} from "./components/Tools/educator-recommendations/educator-recommendations.component";
import {EducatorMakeRecommendationComponent} from "./components/Tools/educator-make-recommendation/educator-make-recommendation.component";
import {OrganizationProfileComponent} from "./components/Tools/organization-profile/organization-profile.component";
import {CompanySubaccountComponent} from "./components/account/company-subaccount/company-subaccount.component";
import {SavedSearchComponent} from "./components/Tools/saved-search/saved-search.component";
import {CompanyDetailsComponent} from "./components/Tools/company-details/company-details.component";
import {CoverLettersComponent} from "./components/Tools/cover-letter/cover-letters-component";
import {CreateCoverLetterComponent} from "./components/Tools/create-cover-letter/create-cover-letter-component";
import {FAQAndTipsComponent} from "./components/Tools/faq-and-tips/faq-and-tips.component";
import {StudentCompaniesRatings} from "./components/Tools/student-companies-ratings/student-companies-ratings.component";
import {PublicCompanyRatingsSearchComponent} from "./components/Tools/public-company-ratings-search/public-company-ratings-search.component";
import {PublicCompanyRatingsComponent} from "./components/Tools/public-company-ratings/public-company-ratings.component";
import {HiddenVacancyComponent} from "./components/Tools/hidden-vacancy/hidden-vacancy.component";
import {InvoicesComponent} from "./components/Tools/invoices/invoices.component";
import {OrganizationDetailsComponent} from "./components/Tools/organization-details/organization-details.component";
import {EducatorsListComponent} from "./components/Tools/educators-list/educators-list.component";
import {CompanyMyServicesComponent} from "./components/Tools/company-my-services/company-my-services.component";
import {OrganizationStatisticsComponent} from "./components/Tools/organization-statistics/organization-statistics.component";

import {MyResumeViewsStatisticsComponent} from "./components/Tools/my-resume-views-statistics/my-resume-views-statistics.component";
import {NewsDetailComponent} from "./components/news/news-detail/news-detail.component";
import {MatchCandidatesComponent} from "./components/Tools/match-candidates/match-candidates.component";
import {NewsListComponent} from "./components/news/news-list/news-list.component";
import {StudentVacancyListComponent} from "./components/Tools/student-vacancy-list/student-vacancy-list.component";

const appRoutes2: Routes = [
  {path:'', component:NewsListComponent},
  {path:'FAQ-and-tips', component:FAQAndTipsComponent},
  {path:'account',loadChildren:'./components/account/account.module#AccountModule'},
  {path:'vacancy',loadChildren:'./components/vacancy/vacancy.module#VacancyModule'},
  {path:'resume', loadChildren:'./components/resume/resume.module#ResumeModule'},
  {path:'admin',loadChildren:'./components/admin/admin.module#AdminModule'},
  {path:'messages', component:MessagesComponent},
  {path:'educator-recommendations', component:EducatorRecommendationsComponent},
  {path:'educator-make-recommendation', component:EducatorMakeRecommendationComponent,},
  {path:'organization-profile', component:OrganizationProfileComponent},
  {path:'company-subaccount', component:CompanySubaccountComponent},
  {path:'saved-search', component:SavedSearchComponent},
  {path:'company-details/:id', component:CompanyDetailsComponent},
  {path:'cover-letters', component:CoverLettersComponent},
  {path:'student-companies-ratings', component:StudentCompaniesRatings},
  {path:'public-company-ratings-search', component:PublicCompanyRatingsSearchComponent},
  {path:'public-company-ratings.component', component:PublicCompanyRatingsComponent},
  {path:'hidden-vacancy.component', component:HiddenVacancyComponent},
  {path:'invoices', component:InvoicesComponent},
  {path:'organization-details.component', component:OrganizationDetailsComponent},
  {path:'create-cover-letter', component:CreateCoverLetterComponent},
  {path:'create-cover-letter/:id', component:CreateCoverLetterComponent},
  {path:'educators-list', component:EducatorsListComponent},
  {path:'company-my-services.component', component:CompanyMyServicesComponent},
  {path:'organization-statistics.component', component:OrganizationStatisticsComponent},

  {path:'My-resume-views-statistics', component:  MyResumeViewsStatisticsComponent},
  {path:'StudentVacancyListComponent/:id/:type', component:  StudentVacancyListComponent},
  {path:'news-detail', component:NewsDetailComponent},
  {path:'match-candidates', component:MatchCandidatesComponent},
  {path:'styleguide', component:StyleguideComponent},
  {path:'**', component:NewsListComponent},
]

export const routing = RouterModule.forRoot(appRoutes2);
