import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {DataService} from "../../_services/data.service";
import {ResumeService} from "../../_services/resume.service";
import {UserService} from "../../_services/user.service";
import {DownPopupService} from "../../_services/down-popup.service";
@Component({
    selector: 'resume-sort-panel-filter-component',
    templateUrl: './resume-sort-panel-filter.component.html',
    styleUrls: ['./resume-sort-panel-filter.component.scss']
})
export class ResumeSortPanelFilterComponent implements OnInit {
    @Input() viewCompact: boolean            = false
    @Input() viewRange: boolean              = false
    @Input() viewSelect: boolean             = false
    @Input() savedSearchCompanyMenu: boolean = false
    @Input() btns: any
    @Output('btn') sortBtns                  = new EventEmitter<string>()
    @Output('sortbtns') setSortBtns          = new EventEmitter<any>()
    @Output('compact') compact               = new EventEmitter<boolean>()
             comp: boolean                   = false
             openFilter: boolean             = false

    constructor(public dataService: DataService,
                public rs: ResumeService,
                private userService: UserService,
                private popS: DownPopupService) {

    }

    ngOnInit() {
    }


    shadowBody(rm = false) {
      let body = document.getElementsByTagName('body')[0];
      if (rm)
        return body.classList.remove('blackFon');

      if (this.openFilter) {
        body.classList.add('blackFon');
      } else {
        body.classList.remove('blackFon');
      }
    }

    sortVacancy(key, val?) {
        if (val) this.setSortBtns.emit({key: key, val: val})
        else this.sortBtns.emit(key)
    }

    searchResume() {
        this.rs.filter.skip = 0
        if (!this.userService.accessPackage.search) {
            this.popS.show_warning(this.dataService.getOptionsFromId('errors', 10).text);
            return

        }
        this.rs.getResume()
    }

}
