import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {HelpersService} from "../../_services/helpers.service";
export interface Items {
  _id: string;
  text: string;


}
@Component({
  selector: 'input-search-autocomplete',
  templateUrl: './input-search-autocomplete.component.html',
  styleUrls: ['./input-search-autocomplete.component.scss']
})
export class InputSearchAutocompleteComponent implements OnInit {
  @Input() result: string[];
  @Input() arrIn: Items[]
  @Input() title: string
  @Input() readonly: boolean = false

  @Input() refresh;
  @Input() value: string
  @Input() valueId: string

  @Output('query') query = new EventEmitter<string>();
  @Output() arrOut = new EventEmitter<string>();
  @Output() refreshOut = new EventEmitter<any>();

  @Output('queryFull') queryFull = new EventEmitter<string>();
  queryString = '';
  possibleBox:boolean = false;

  arrInFinal: Items[] = []




  public filteredList = [];
  public elementRef;

    constructor(myElement: ElementRef,public helpers:HelpersService) {
        this.elementRef = myElement;
    }
  ngOnChanges(changes: any) {


    if (changes.refresh && changes.refresh.currentValue != changes.refresh.previousValue && typeof changes.refresh.previousValue !== "undefined") {
      this.result = []
      this.arrInFinal = []
      this.queryString = ''
      this.refreshOut.emit(this.refresh)
    }
    if (changes.value) {
      this.queryString = this.value
    }

    if (changes.valueId) {
      this.queryString = ''
      for (var i = 0; i < this.arrIn.length; i++) {
        if (this.arrIn[i]._id==this.valueId){
          this.queryString = this.arrIn[i].text
        }

      }
    }


  }


    filter(e) {

        if (this.queryString != ""){
            if(e.keyCode == 13 ) {
            this.queryFull.emit(this.queryString);
                this.result = [];

            } else {
              if (this.result){
                this.query.emit(this.queryString);
                this.result = this.result.filter(function(el){
                  console.log(this.queryString)
                  return el.toLowerCase().indexOf(this.queryString.toLowerCase()) > -1;
                }.bind(this));
              }
              if (this.arrIn){
                //this.query.emit(this.queryString);
                this.arrInFinal = []
                var regexp = new RegExp(this.queryString,'i');

                for (var i = 0; i < this.arrIn.length; i++) {
                  if (this.arrIn[i].text.match(regexp)){
                    this.arrInFinal.push(this.arrIn[i])
                  }

                }
                //this.result = this.result.filter(function(el){
                //  return el.toLowerCase().indexOf(this.queryString.toLowerCase()) > -1;
                //}.bind(this));
              }

            }



        } else {
          this.result = [];
          this.arrInFinal = [];
          this.queryFull.emit(this.queryString);

        }

    }

  selectStr(item){
        this.queryFull.emit(item);
        this.result = [];
        this.queryString = item;
    }
  selectAr(item){
        this.arrOut.emit(item);
        this.result = [];
        this.arrInFinal = [];
        this.queryString = item.text;
    }







    ngOnInit() {
    if (this.value){
      this.queryString = this.value
    }
    if (this.valueId){
      for (var i = 0; i < this.arrIn.length; i++) {
        if (this.arrIn[i]._id==this.valueId){
          this.queryString = this.arrIn[i].text
        }

      }
      }
    }

}
