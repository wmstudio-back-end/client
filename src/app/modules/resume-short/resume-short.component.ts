import {Component, Input, OnInit} from '@angular/core';
import {IResume} from "../../_models/resume";
import {User} from "../../_models/user";
import {UserService} from "../../_services/user.service";
import {DataService} from "../../_services/data.service";
import {ResumeService} from "../../_services/resume.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-resume-short',
  templateUrl: './resume-short.component.html',
  styleUrls: ['./resume-short.component.scss']
})
export class ResumeShortComponent implements OnInit {
  @Input() resume:IResume
  @Input() rating:number
  @Input() nameView:boolean = true
  obK = Object.keys
  User:User
  hightSchool:any
  ind = 0
  approved_at
  oninit = false
  constructor(
    public uS:UserService,
    public dS:DataService,
    public rS:ResumeService,
    private route: ActivatedRoute
  ) { }

  ngOnChanges(e: any) {

    if (e.resume && e.resume.hasOwnProperty('currentValue')) {

      }
    if (e.user && e.user.hasOwnProperty('currentValue')) {

    }
  }
  ngOnInit() {

  if (this.uS.profile.subscribeCandidate&&this.route.snapshot.params['id']){
    for (var i = 0; i < this.uS.profile.subscribeCandidate.length; i++) {
      if (this.uS.profile.subscribeCandidate[i].vacancyId==this.route.snapshot.params['id']
        &&this.resume._id==this.uS.profile.subscribeCandidate[i].resumeId
      ){
        this.approved_at = this.dS.dateConvert(this.uS.profile.subscribeCandidate[i].created_at)
      }

    }
  }
    this.uS.getUsers([this.resume.uid]).then(user=>{
      if (!this.uS.users[this.resume.uid]){return}
    this.uS.getSchools([this.resume.uid]).then(data=>{

      //this.User = this.uS.users[this.resume.uid]
      var max = 0

      for (var i = 0; i < this.uS.users[this.resume.uid].Student.schools.length; i++) {
        if (this.uS.users[this.resume.uid].Student.schools[i].educationLevel>max){
          max = this.uS.users[this.resume.uid].Student.schools[i].educationLevel
          this.ind = i
        }
      }
      this.oninit = true
      //this.hightSchool = this.uS.users[this.resume.uid].Student.schools[this.ind]
      //console.log(this.resume)
      //console.log('===========================================------------'+this.ind+'---------000000000000000000000000000000000000',this.uS.users[this.resume.uid].Student.schools)
    })
  })

  }

}
