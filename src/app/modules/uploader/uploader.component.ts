import {
  Component, Directive, ElementRef, EventEmitter, HostListener, Input, OnInit, Output,
  ViewChild
} from '@angular/core';
import {AuthenticationService} from "../../_services/authentication.service";
import {HttpService} from "../../_services/http.service";
import {UserService} from "../../_services/user.service";
import {DataService} from "../../_services/data.service";

@Component({
  selector: 'uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss']
})

export class UploaderComponent implements OnInit {
  @Input('inputStyle') inputStyle: string;
  @Input('fileTypes') fileTypes: string[] = [];
  @Input('alertUploader') alertUploader:boolean = false;
  @Input('multiple') multiple: boolean = false;
  @Input('profilePhoto') profilePhoto: boolean = false;
  @Output('file') change = new EventEmitter<any>();
  @ViewChild('fileInput') fileInput:ElementRef;
  @HostListener('dragover', ['$event']) public onDragOver(evt){
    evt.preventDefault();
    evt.stopPropagation();
    this.dragover=1
    console.log('dragover')
  }
  @HostListener('dragleave', ['$event']) public onDragLeave(evt){
    evt.preventDefault();
    evt.stopPropagation();
    this.dragover=0
    console.log('dragleaver')
  }

  @HostListener('drop', ['$event']) public onDrop(evt){
    evt.preventDefault();
    evt.stopPropagation();
    if (evt.dataTransfer.files.length>0){
      this.dragover=2
      this.checkFiles(evt.dataTransfer.files)
      this.inputStyle = "drop";
    }
    console.log('drop')


  }
  dragover = 0
  filesToUpload = [];
  fileBase64:string = ''
  accept:string
  port:string
  host:string


  constructor(public user: UserService, private authServ: AuthenticationService, private http: HttpService, private dataService: DataService) {
    this.port = dataService.PORT_STATIC
    this.host = dataService.HOST_STATIC
  }

  parsefiles(event){
    let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
    let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
    let files2: FileList = target.files;
    return files2
  }
  checkFiles(files){

    for(var i=0;i<files.length;i++){
      if (this.fileTypes.length==0){
        this.uploadFile(files[i])
      }else {
        if (this.fileTypes.indexOf(files[i].name.toLowerCase().split('.').reverse()[0])>-1||this.fileTypes.length==0){
          this.uploadFile(files[i])
        }else{
          console.log("false",files[i].name)
        }
      }
    }
  }
  uploadFile(file){
    console.log(this.inputStyle,this.profilePhoto);
    var reader = new FileReader();
    reader.onload = function (event) {
      file.resizeFile = event.target.result;
      if (this.inputStyle == 'singlePhoto')
        if (this.profilePhoto) {
          this.resizeUpload(file, 400, 400);
          return;
        }
      this.uploadToServer(file);
    }.bind(this)
    reader.readAsDataURL(file)
  }
  uploadToServer(file){
    var token = this.authServ.getToken();
    var type = file.name.split('.').reverse()[0];
    var profilePhoto = false;
    if (this.inputStyle == 'singlePhoto')
        type = 'photo';
    var data = {
      token: token,
      file: {
        name: file.name,
        content: file.resizeFile,
        mimeType: file.mimeType,
        size: file.size,
      },
      type: type
    }
    this.fileBase64 = data.file.content

    this.http.postData(data).subscribe(function (res) {
      if (!res){return;}
      if (!res.hasOwnProperty('success'))
        return;
      if (!res.hasOwnProperty('data'))
        return;
      if (this.inputStyle == 'singlePhoto' && !res.data.hasOwnProperty('path'))
        return;
      var data = res.data;
      console.log(this.inputStyle,this.profilePhoto);
      if (this.inputStyle == 'singlePhoto')
        if (this.profilePhoto) {
          if (data.hasOwnProperty('path')) {
            data.path = '//' + this.host + ':' + this.port + data.path;
            this.user.profile.Photo = data.path;
            this.user.profile.photo = data.path;
            console.log({Photo: data.id})
            this.user.setPhoto({id: data.id})
          }
        }

      this.change.emit(data);
    }.bind(this));
  }

  ngOnInit() {

    this.accept = ''
    let keys = Object.keys(this.fileTypes)
    for(let i = 0;keys.length>i;i++){
      this.accept+= '.'+this.fileTypes[keys[i]]+','
    }

  }

  resizeUpload(file,width, height) {
    var img = new Image();
    img.src = URL.createObjectURL(file);
    img.onload = function () {
      var resize = this.imageToDataUri(img, width, height);
      file.resizeFile = resize;

      file.mimeType = 'image/png';
      this.uploadToServer(file);
    }.bind(this);
  }

  imageToDataUri(img, width, height) {
    var canvas = document.createElement('canvas'),
        ctx = canvas.getContext('2d');
    var iw = img.width;
    var ih = img.height;
    var scale = Math.min((400 / iw), (400 / ih));
    var iwScaled = iw * scale;
    var ihScaled = ih * scale;

    canvas.width = iwScaled;
    canvas.height = ihScaled;
    ctx.drawImage(img, 0, 0, iwScaled, ihScaled);
    var t = canvas.toDataURL('image/png', 1);

    return t
  }

}
