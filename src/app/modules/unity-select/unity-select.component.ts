import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {isUndefined} from "util";
import {DataService} from "../../_services/data.service";
import {noUndefined} from "@angular/compiler/src/util";
import {TranslateService} from "@ngx-translate/core";
export interface Items {
    _id: string;
    text: string;
}
@Component({
    selector: 'app-unity-select',
    templateUrl: './unity-select.component.html',
    styleUrls: ['./unity-select.component.css']
})
export class UnitySelectComponent implements OnInit {
    @Input('title') title: string = ''
    @Input() type: string
    @Input() error: any
    @Input() valid: boolean
    @Input() differenceIds: number[]
    @Input() readonly: boolean
    @Input('arrIn')
    set arr(items: any) {
        this.arrInFinal = items
        this.arrIn = items
    }
    @Input('arrVal') arrVal: any = {}
    @Input() refresh: boolean
    @Input() default: string
    @Output('arrOut') change = new EventEmitter<Items>()
    @Output('errorOut') errorOut = new EventEmitter<boolean>()
    @Output('arrOutVal') arrOutVal = new EventEmitter<any>()
    random: string = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)
    @Input('value') selectedItem: Items
    @Input('valueId') valueId: number
    @Input('valueText') valueText: string
    @Input('change') lastChange: any
    @Input('changeName') changeName: string
    unitySelect: boolean = false
    errorClass: boolean = false
    arrIn: Items[]
    arrInFinal: Items[]

    constructor(private dS: DataService,
                private translate:TranslateService) {
        //if (!this.default) this.default = this.title

      console.log('===================',this.title)
    }

    Difference(A, B) {
        var M = A.length, N = B.length, C = [];

        for (var i = 0; i < M; i++) {
            var j = 0, k = 0;
            while (B[j] !== A[i] && j < N) j++;
            while (C[k] !== A[i] && k < C.length) k++;
            if (j == N && k == C.length) C[C.length] = A[i];
        }

        return C;
    }

    ngOnChanges(changes: any) {

        if (changes.lastChange) {
            if (typeof changes.lastChange.currentValue === "object") {
                if (changes.lastChange.currentValue[0] == this.changeName)
                    this.setSelected()
            }
        }
        if (changes.arr){

            this.selectedItem = {_id: "none", text: this.default?this.default:''}
        }
        if (changes.refresh && changes.refresh.currentValue != changes.refresh.previousValue && typeof changes.refresh.previousValue !== "undefined") {
            this.selectedItem = {_id: "none", text: this.default?this.default:''}
        }
      this.filterValue()
    }

    public clear() {

    }

    clickexclude(e) {
        if (this.random != e.target.id) {
            this.unitySelect = false
        }

    }

    changeItem(item) {
        this.selectedItem = item
        this.change.emit(this.selectedItem)
        this.unitySelect = false
        this.arrVal = {}
        this.arrVal[item.id] = true
        this.arrOutVal.emit(this.arrVal)

    }

    filterValue() {
        if (this.valid) {
            var v = false
            this.error === null ? v = true : null
            this.error === undefined ? v = true : null
            this.error === [] ? v = true : null
            this.error === {} ? v = true : null
            this.error === false ? v = true : null
            this.error === '' ? v = true : null

            this.errorClass = v
            this.errorOut.emit(this.errorClass)

        }


        if (this.valueText) {
            //console.log("this.valueText", this.valueText)
            for (var i = 0; i < this.arrIn.length; i++) {
                if (this.arrIn[i]['text'] + '' == this.valueText + '') {
                    this.selectedItem = this.arrIn[i]
                    //this.title = this.selectedItem.text
                }
            }
        }
      if (this.valueId != undefined) {

        for (var i = 0; i < this.arrIn.length; i++) {
          if (this.arrIn[i]['id'] == this.valueId) {
            this.selectedItem = this.arrIn[i]
            //this.title = this.selectedItem.text
          }
        }

      }
        this.arrInFinal = this.arrIn
    }

    ngOnInit() {
        this.filterValue()
        this.setSelected()

    }

    setSelected() {
        if (typeof this.arrVal === "undefined")
            this.arrVal = {}
        else {
            if (this.arrVal)
                for (var key in this.arrIn) {
                    var id = this.arrIn[key][id]
                    if (typeof this.arrVal[id] !== "undefined")
                        if (this.arrVal[id])
                            this.selectedItem = this.arrIn[key]
                }
        }
        //console.log('this.selectedItem',this.selectedItem)
        if (!this.selectedItem)
        {
          this.selectedItem = {_id: "none", text: ''}
        }
        else
        {

        }
    }
}
