"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var UnitySelectComponent = (function () {
  function UnitySelectComponent() {
    this.change = new core_1.EventEmitter();
    this.unitySelect = false;
  }

  UnitySelectComponent.prototype.changeItem = function (item) {
    this.selectedItem = item;
    this.change.emit(this.selectedItem);
    this.unitySelect = false;
  };
  UnitySelectComponent.prototype.ngOnInit = function () {
    this.selectedItem = {id: 0, text: this.title};
  };
  return UnitySelectComponent;
}());
__decorate([
  core_1.Input()
], UnitySelectComponent.prototype, "title", void 0);
__decorate([
  core_1.Input()
], UnitySelectComponent.prototype, "arrIn", void 0);
__decorate([
  core_1.Output('arrOut')
], UnitySelectComponent.prototype, "change", void 0);
UnitySelectComponent = __decorate([
  core_1.Component({
    selector: 'app-unity-select',
    templateUrl: './unity-select.component.html',
    styleUrls: ['./unity-select.component.css']
  })
], UnitySelectComponent);
exports.UnitySelectComponent = UnitySelectComponent;
