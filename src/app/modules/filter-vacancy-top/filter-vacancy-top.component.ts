import {
  Component,
  Input,
  OnInit
}                        from '@angular/core';
import {DataService}     from '../../_services/data.service';
import {VacancyService}  from '../../_services/vacancy.service';
import {LocationService} from '../../_services/location.service';
import {HelpersService}  from '../../_services/helpers.service';

@Component({
  selector:    'app-filter-vacancy-top',
  templateUrl: './filter-vacancy-top.component.html',
  styleUrls:   ['./filter-vacancy-top.component.scss']
})
export class FilterVacancyTopComponent implements OnInit {
  @Input() globalMenuStudent: boolean = false;
  @Input() globalMenuCompany: boolean = false;

  openFilter: boolean = false;

  shadowBody(rm = false) {
    let body = document.getElementsByTagName('body')[0];
    if (rm) {
      return body.classList.remove('blackFon');
    }
    if (this.openFilter || this.globalMenu) {
      body.classList.add('blackFon');
    } else {
      body.classList.remove('blackFon');
    }
  }

  public address: any;
  public Location: any;
         typeVacancy            = []
  @Input('refresh') refresh: boolean
  private filterPlace: string[] = [
    'country',
    'city'
  ];
         globalMenu: boolean    = false

  constructor(public dataService: DataService,
              public vacancyService: VacancyService,
              public locationService: LocationService,
              public help: HelpersService) {


  }

  transformVacancyType(event) {
    let q        = []
    let val      = []
    let opt      = {}
    let optSlise = {}
    let e        = -1

    console.log(event);

    if (event.length == 1)
      e = event[0].id

    if (e == -1) {
      q        = [
        0,
        1,
        2,
        3,
        4,
      ]
      opt      = {
        0: true,
        1: true,
        2: true,
        3: true,
        4: true,
      }
      optSlise = {
        2: true,
        3: true,
        4: true,
      }
    }


    if (e == 0) {
      q        = [
        0,
        1
      ]
      opt      = {
        0: true,
        1: true,
        2: false,
        3: false,
        4: false,
      }
      optSlise = {
        2: false,
        3: false,
        4: false,
      }
    }
    if (e == 1) {
      q        = [
        2,
        3,
        4,
      ]
      opt      = {
        0: false,
        1: false,
        2: true,
        3: true,
        4: true,
      }
      optSlise = {
        2: true,
        3: true,
        4: true,
      }
    }
    for (var i = 0; i < q.length; i++) {
      val.push({id: q[i]})
    }
    // this.vacancyService.setFilterOption('vacancyTypeSlice', optSlise)
    this.vacancyService.setFilterOption('vacancyType', opt)
    this.setFilter('type', val, '$in')
  }

  setFilter(key, val, op) {
    //this.vacancyService.changeFilterElem = undefined
    this.vacancyService.setFilterItem(this.help.getFilterItem(key, op, val))
  }

  searchVacancy() {
    this.vacancyService.filter.skip = 0
    this.vacancyService.getVacancy()
  }

  closeFilter() {
    let body        = document.getElementsByTagName('body')[0];
    this.globalMenu = false;
    body.classList.remove('blackFon')
  }

  setLastElem(event) {
    console.log('event ', event)
    this.vacancyService.changeFilterElem = event;
  }

  setSalaryType(key, val) {
    this.vacancyService.unserFilter(key)
    this.vacancyService.setFilter(key, val, false)
    this.vacancyService.changeFilterElem = undefined;
  }

  setSalarySumm(e) {
    this.vacancyService.unserFilter('salary.summ')
    if (e.target.value)
      this.vacancyService.setFilter('salary.summ', parseInt(e.target.value), '$gte')
    this.vacancyService.changeFilterElem = undefined;
  }

  ngOnInit() {
    this.typeVacancy = Object.assign([], this.dataService.options.vacancyTypeShort)
    if (this.vacancyService.filter.options.address)
      this.address = this.vacancyService.filter.options.address
  }

  setAddress(address) {
    this.vacancyService.changeFilterElem       = undefined;
    this.address                               = address
    this.vacancyService.filter.options.address = address
    this.Location                              = this.locationService.location['vacancySearch'];
    console.log('search location', this.Location)


    if (this.address && typeof this.Location !== 'undefined') {
      let words  = [];
      let string = address.replace('/\s/g', '===').replace('/[,.]/g', '===').split('===').filter((e, i, a) => {
        return (e) ? true : false;
      });
      let keys   = Object.keys(this.Location);
      console.log(keys);
      for (var i = 0; i < keys.length; i++) {
        let k = keys[i]
        if (this.filterPlace.indexOf(k) > -1)
          if (this.Location[k]) {
            // this.vacancyService.setFilterItem(this.help.getFilterItem('Location.' + k, '$and', this.Location[k]))
            words.push(this.Location[k])
          } else {
            // this.vacancyService.unsetFilterItem('Location.' + k)
          }
      }

      if (words.length) {
        let text = [
          this.help.getPatternInString(words),
          this.help.getPatternInString(string),
        ]
        console.log('regExp ', text)
        this.vacancyService.setFilterItem(this.help.getFilterItem('address', 'query', {$in: text})
        )
      }
    } else {
      console.log('address', address);
      this.vacancyService.unsetFilterItem('address')
    }
  }

  setKeyWords(e) {
    let val = e.target.value
    this.vacancyService.setKeyWords(val)
  }

}
