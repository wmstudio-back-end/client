"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var ProgressPrcComponent = (function () {
  function ProgressPrcComponent() {
    this.deg1 = -270;
    this.deg2 = -90;
  }

  ProgressPrcComponent.prototype.changeBackground = function () {
    this.p = Math.floor(this.prc * 3.6);
    if (this.prc < 50) {
      this.deg2 = this.p - 90;
    }
    else {
      this.deg1 = this.p - 180 - 270 - 1;
    }
    if (this.prc < 50) {
      return {'background-image': 'linear-gradient(-90deg,#d2d2d2 50%, transparent 50%), linear-gradient(' + this.deg2 + 'deg, #00C3F1 50%, #d2d2d2 50%)'};
    }
    else {
      return {'background-image': 'linear-gradient(' + this.deg1 + 'deg,#00C3F1 50%, transparent 50%), linear-gradient(' + this.deg2 + 'deg, #00C3F1 50%, #d2d2d2 50%)'};
    }
  };
  ProgressPrcComponent.prototype.ngOnInit = function () {
  };
  return ProgressPrcComponent;
}());
__decorate([
  core_1.Input()
], ProgressPrcComponent.prototype, "prc", void 0);
__decorate([
  core_1.Input('master')
], ProgressPrcComponent.prototype, "masterName", void 0);
ProgressPrcComponent = __decorate([
  core_1.Component({
    selector: 'app-progress-prc',
    templateUrl: './progress-prc.component.html',
    styleUrls: ['./progress-prc.component.css']
  })
], ProgressPrcComponent);
exports.ProgressPrcComponent = ProgressPrcComponent;
