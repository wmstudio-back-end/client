import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {ProgressPrcComponent} from "./progress-prc.component";
describe('ProgressPrcComponent', () => {
  let component: ProgressPrcComponent;
  let fixture: ComponentFixture<ProgressPrcComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations:[ProgressPrcComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressPrcComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
