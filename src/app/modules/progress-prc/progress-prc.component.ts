import {Component, Input, OnInit} from "@angular/core";
@Component({
  selector: 'app-progress-prc',
  templateUrl: './progress-prc.component.html',
  styleUrls: ['./progress-prc.component.css']
})
export class ProgressPrcComponent implements OnInit {

  deg1 = -270
  deg2 = -90
  resultPrc: number = 0
  p: number = 0
  @Input() prc: number = 0;
  @Input('master') masterName: string;

  constructor() {

  }

  changeBackground(): any {
    if (this.prc>100){this.prc = 100}
    this.p = Math.floor(this.prc * 3.6)
    this.resultPrc = Math.floor(this.prc)
    if (this.prc < 50) {
      this.deg2 = this.p - 90
    } else {
      this.deg2 = this.p - 90
      //this.deg1 = this.p - 180 - 270 - 1

    }

    if (this.prc < 50) {
      return {'background-image':'linear-gradient(-90deg,#d2d2d2 50%, transparent 50%), linear-gradient(' + this.deg2 + 'deg, #00C3F1 50%, #d2d2d2 50%)'};
    } else {
      return {'background-image':'linear-gradient(' + this.deg1 + 'deg,#00C3F1 50%, transparent 50%), linear-gradient(' + this.deg2 + 'deg, #00C3F1 50%, #d2d2d2 50%)'};
    }



  }


  ngOnInit() {

  }
}
