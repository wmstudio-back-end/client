import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../../_services/user.service";
import {DataService} from "../../_services/data.service";
import {ComponentService} from "../../_services/component.service";



@Component({
    selector: 'graphic',
    templateUrl: './graphic.component.html',
    styleUrls: ['./graphic.component.scss']
})

export class LineChartDemoComponent  implements OnInit  {
  ngOnInit(): void {
    this.sttLine()
  }


  // lineChart
  @Input() resumeID: string
  @Input('filterSearch') filterSearch:boolean
  @Input('filterViwe') filterViwe:boolean
  @Input('filterBook') filterBook:boolean

  @Input('filterPeriodId') filterPeriodId:any
  @Input('filterResumeId') filterResumeId:any
  show = true
  logs = []
  weeks = ['monday','tuesday','wednesday','thursday','friday','saturday','sunday']
  years=['January', 'February', 'March', 'April', 'May', 'June', 'Jule', 'August', 'September', 'October', 'November', 'December']
  public lineChartLabels: Array<any> = []
  labelArr = {
    week:{data:this.weeks,period:86400},
    month:{data:this.getDayMonth(),period:86400*30},
    year:{data:this.years,period:86400*30},
    month3:{data:this.getlast3Month(),period:86400*30},
    month6:{data:this.getlast6Month(),period:86400*30}

}

  public lineChartData: Array<any> = [];
  constructor(
    public component:ComponentService,
    public dataService:DataService,
    public uS:UserService
  ) { }
   getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
getlast3Month(){
    return [this.years[new Date().getMonth()],this.years[new Date().getMonth()+1],this.years[new Date().getMonth()]+2]
}
  getlast6Month(){
    return [
      this.years[new Date().getMonth()],
      this.years[new Date().getMonth()+1],
      this.years[new Date().getMonth()]+2,
      this.years[new Date().getMonth()]+3,
      this.years[new Date().getMonth()]+4,
      this.years[new Date().getMonth()]+5,
    ]
  }
getDayMonth(){
  var t = []
    for (var i = new Date().getDate(); i < 30; i++) {
    t.push(i)
  }
  return t

}
  ngOnChanges(changes: any) {

    console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!",changes)
    if (changes.filterSearch||changes.filterViwe||changes.filterBook||changes.filterPeriodId||changes.filterResumeId) {

      //this.lineChartData[0].label  = "ssss"
    //: Array<any> = [
    //    {data: [65, 59, 80, 81, 56, 55], label: 'Series111 A'},
    //    {data: [28, 48, 40, 19, 86, 27], label: 'Series B'},
    //    {data: [18, 48, 77, 9, 100, 27], label: 'Series C'},
    //
    //  ];
    //  this.sttLine()
      //this.sttLine()
    }
    this.swichshow()

  }
  swichshow(){
    this.show = false
    var c = 7
    this.dataService.sendws('Events/getLogsResume',
      {
        setBookmarkResume:this.filterBook,
        viweResume:this.filterViwe,
        searchResume:this.filterSearch,
        period:this.filterPeriodId,
        resumeId:this.filterResumeId,
      }).then(data=>{
        var period = 86400
        switch (this.filterPeriodId){
          case 7*86400://604800
            this.lineChartLabels = this.labelArr.week.data;
            period = this.labelArr.week.period;
            break;
          case 30*86400://2592000
            this.lineChartLabels = this.labelArr.month.data;
            period = this.labelArr.month.period;
            break;
          case 90*86400://7776000
            this.lineChartLabels = this.labelArr.month3.data;
            period = this.labelArr.month3.period;
          break;
          case 180*86400://15552000
            this.lineChartLabels = this.labelArr.month6.data;
            period = this.labelArr.month6.period;
          break;
          case 365*86400://31536000
            this.lineChartLabels = this.labelArr.year.data;
            period = this.labelArr.year.period;
            break;
          default:

        }
      console.log(data)

      this.sttLine()
      this.lineChartData = []

      var t = {
        searchResume:{count:{},name:'Search_Appearance'},
        viweResume:{count:{},name:'CV_viwe'},
        setBookmarkResume:{count:{},name:'CV_bookmarked'}
        }

      var startdate = this.dataService.options.server_time-this.filterPeriodId
      var enddate = this.dataService.options.server_time
      for (var i = startdate; i < enddate; i+=period) {
          t.searchResume.count["__"+i]=0
          t.viweResume.count["__"+i]=0
          t.setBookmarkResume.count["__"+i]=0
      }
      for (var i = startdate; i < enddate; i+=period) {

        for (var j = 0; j < data['events'].length; j++) {

        if (data['events'][j].created_at>i&&data['events'][j].created_at<i+period){



          t[data['events'][j].event.name]['count']["__"+i]++
        }

        }
      }
      console.log(t)

      if (this.filterSearch){
          var m = []
          var s =Object.keys(t.searchResume.count)
        for (var i = 0; i < s.length; i++) {
          m.push(t.searchResume.count[s[i]])

        }

        if (m.reduce(function(a, b) { return a + b; }, 0)>0){
          this.lineChartData.push({data: m, label: 'Search_Appearance'})
        }

      }
      if (this.filterViwe){
        var m = []
        var s =Object.keys(t.viweResume.count)
        for (var i = 0; i < s.length; i++) {
          m.push(t.viweResume.count[s[i]])

        }
        if (m.reduce(function(a, b) { return a + b; }, 0)>0) {
          this.lineChartData.push({data: m, label: 'CV_viwe'})
        }
      }
      if (this.filterBook){
        var m = []
        var s =Object.keys(t.setBookmarkResume.count)
        for (var i = 0; i < s.length; i++) {
          m.push(t.setBookmarkResume.count[s[i]])

        }
        if (m.reduce(function(a, b) { return a + b; }, 0)>0) {
          this.lineChartData.push({data: m, label: 'CV_bookmarked'})
        }
      }

      //console.log(this.lineChartData)
      if (this.lineChartData.length>0){
        this.show = true
      }
    })


  }
  sttLine(){

    // this.lineChartData = []
    // var t = []
    // if (this.filterSearch){
    //   this.lineChartData.push({data: [
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100)
    //   ], label: 'filterSearch'})
    // }
    // if (this.filterViwe){
    //   this.lineChartData.push({data: [
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100)
    //   ], label: 'filterViwe'})
    // }
    // if (this.filterBook){
    //   this.lineChartData.push({data: [
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100),
    //     this.getRandomInt(0, 100)
    //   ], label: 'filterBook'})
    // }




    //this.lineChartData =  t
  }




    public lineChartOptions: any = {
        responsive: true,

    };




    public lineChartColors: Array<any> = [

        { // grey
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: '#a7a7a7',
            pointBackgroundColor: '#fff',
            pointBorderColor: '#a7a7a7',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#a7a7a7',
            borderWidth: '4',
            pointBorderWidth: '4',
            pointRadius: '6'
        },
        { // dark birch
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: '#008C98',
            pointBackgroundColor: '#fff',
            pointBorderColor: '#008C98',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#008C98',
            borderWidth: '4',
            pointBorderWidth: '4',
            pointRadius: '6'
        },
        { // blue
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: '#00C3F1',
            pointBackgroundColor: '#fff',
            pointBorderColor: '#00C3F1',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#00C3F1',
            borderWidth: '4',
            pointBorderWidth: '4',
            pointRadius: '6'
        },
        { // red
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: '#FF1E00',
            pointBackgroundColor: '#fff',
            pointBorderColor: '#FF1E00',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#FF1E00',
            borderWidth: '4',
            pointBorderWidth: '4',
            pointRadius: '6'
        },
        { // green
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: '#00C434',
            pointBackgroundColor: '#fff',
            pointBorderColor: '#00C434',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#00C434',
            borderWidth: '4',
            pointBorderWidth: '4',
            pointRadius: '6'
        },
        { // yellow
            backgroundColor: 'rgba(0, 0, 0, 0)',
            borderColor: '#FF9E00',
            pointBackgroundColor: '#fff',
            pointBorderColor: '#FF9E00',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#FF9E00',
            borderWidth: '4',
            pointBorderWidth: '4',
            pointRadius: '6',

        }


    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';




    public randomize(): void {
        let _lineChartData: Array<any> = new Array(this.lineChartData.length);
        for (let i = 0; i < this.lineChartData.length; i++) {
            _lineChartData[i] = {
                data: new Array(this.lineChartData[i].data.length),
                label: this.lineChartData[i].label
            };
            for (let j = 0; j < this.lineChartData[i].data.length; j++) {
                _lineChartData[i].data[j] = Math.floor((Math.random() * 100) + 1);
            }
        }
        this.lineChartData = _lineChartData;

    }


    // events
    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }










}



