import {NgModule}                              from "@angular/core";
import {CommonModule}                          from "@angular/common";
import {ProgressAccountComponent}              from "./progress-account/progress-account.component";
import {ProgressPrcComponent}                  from "./progress-prc/progress-prc.component";
import {MultipleSelectComponent}               from "./multiple-select/multiple-select.component";
import {ClickOutsideModule}                    from "ng-click-outside";
import {UnitySelectComponent}                  from "./unity-select/unity-select.component";
import {AccordionSelectComponent}              from "./accordion-select/accordion-select.component";
import {DownPopupComponent}                    from "./down-popup/down-popup.component";
import {FilterVacancyTopComponent}             from "./filter-vacancy-top/filter-vacancy-top.component";
import {DatepickerBirthComponent}              from "./datepicker-birth/datepicker-birth.component";
import {TranslateModule}                       from "@ngx-translate/core";
import {UploadFileComponent}                   from "./upload-file/upload-file.component";
import {UploaderComponent}                     from './uploader/uploader.component';
import {FileListComponent}                     from './file-list/file-list.component';
import {MultiselectAutocopmpleteAreaComponent} from './multiselect-autocopmplete-area/multiselect-autocopmplete-area.component';
import {SwitchToggleComponent}                 from './switch-toggle/switch-toggle.component';
import {FormsModule}                           from "@angular/forms";
import {AgmCoreModule}                         from '@agm/core';
import {AddressSelectComponent}                from './address-select/address-select.component';
import {RatingStarComponent}                   from './rating-star/rating-star.component';
import {ModalService}                          from "../_services/modal.service";
import {SortPanelFilterComponent}              from "./sort-panel-filter/sort-panel-filter.component";
import {SearchUserComponent}                   from './search-user/search-user.component';
import {LineChartDemoComponent}                from './graphic/graphic.component';
import {ChartsModule}                          from "ng2-charts";
import {FilterResumeTopComponent}              from "./filter-resume-top/filter-resume-top.component";
import {ResumeSortPanelFilterComponent}        from "./resume-sort-panel-filter/resume-sort-panel-filter.component";
import {InputSearchAutocompleteComponent}      from './input-search-autocomplete/input-search-autocomplete.component';
import {StudentContactsComponent}              from './student-contacts/student-contacts.component';
import {ResumeShortComponent}                  from './resume-short/resume-short.component';
import {RouterModule}                          from "@angular/router";
import {ModalBookmarkComponent}                from "app/modules/modal-bookmark/modal-bookmark.component";
import {NotesResumeComponent}                  from "./notes-resume/notes-resume.component";
import {NgxCarouselModule}                     from 'ngx-carousel';
import 'hammerjs';
import {ResumeListModuleComponent}             from "../components/resume/resume-list/resume-list.component";
import {AngularGooglePlaceModule}              from 'angular-google-place';
import {DeadlineComponent}                     from './_expansion/deadline/deadline.component';
import {PublishedAtComponent}                  from './_expansion/published-at/published-at.component';
import {SubmitedResumeComponent}               from './_expansion/submited-resume/submited-resume.component';
import {CoverLetterComponent}                  from './_expansion/cover-letter/cover-letter.component';
import {AppliedtovacancyComponent}             from './_expansion/appliedtovacancy/appliedtovacancy.component';
import {BookMarkComponent}                     from './_expansion/book-mark/book-mark.component';
import {SalarysummComponent}                   from './_expansion/salarysumm/salarysumm.component';
import {OptionsDataComponent}    from './_expansion/options-data/options-data.component';
import {AddressComponent}        from './_expansion/address/address.component';
import {SetDialogComponent}      from './_expansion/set-dialog/set-dialog.component';
import {VacancyCardComponent}    from "./vacancy-card/vacancy-card.component";
import {PopupModalsModule}       from "../popup-modals/popup-modals.module";
import { DisconnectFbComponent } from './disconnect-fb/disconnect-fb.component';
import {TextMaskModule}          from "angular2-text-mask";
import { LoaderComponent } from './loader/loader.component';


@NgModule({
  imports:      [
    CommonModule,
    ClickOutsideModule,
    TranslateModule,
    FormsModule,
    NgxCarouselModule,
    ChartsModule,
    AngularGooglePlaceModule,
    PopupModalsModule,
    TextMaskModule,
    RouterModule,
    // AgmCoreModule.forRoot({
    //     apiKey: 'AIzaSyADL_-gNyss3Es4ZhCGYCE2-HxUpfGQ75M',
    //     // libraries: ['places']
    // })

  ],
  declarations: [
    NotesResumeComponent,
    ProgressAccountComponent,
    ProgressPrcComponent,
    MultipleSelectComponent,
    UnitySelectComponent,
    AccordionSelectComponent,
    DownPopupComponent,
    FilterVacancyTopComponent,
    DatepickerBirthComponent,
    UploadFileComponent,
    UploaderComponent,
    FileListComponent,
    MultiselectAutocopmpleteAreaComponent,
    SwitchToggleComponent,
    AddressSelectComponent,
    RatingStarComponent,
    ResumeSortPanelFilterComponent,
    SortPanelFilterComponent,
    SearchUserComponent,
    LineChartDemoComponent,
    FilterResumeTopComponent,
    InputSearchAutocompleteComponent,
    StudentContactsComponent,
    ResumeShortComponent,
    ResumeListModuleComponent,
    ModalBookmarkComponent,
    DeadlineComponent,
    PublishedAtComponent,
    SubmitedResumeComponent,
    CoverLetterComponent,
    AppliedtovacancyComponent,
    BookMarkComponent,
    SalarysummComponent,
    OptionsDataComponent,
    AddressComponent,
    SetDialogComponent,
    VacancyCardComponent,
    DisconnectFbComponent,
    LoaderComponent


  ],
  exports:      [
    NotesResumeComponent,
    ProgressAccountComponent,
    ProgressPrcComponent,
    ResumeSortPanelFilterComponent,
    MultipleSelectComponent,
    UnitySelectComponent,
    AccordionSelectComponent,
    DownPopupComponent,
    FilterVacancyTopComponent,
    DatepickerBirthComponent,
    UploadFileComponent,
    UploaderComponent,
    FileListComponent,
    MultiselectAutocopmpleteAreaComponent,
    SwitchToggleComponent,
    AddressSelectComponent,
    RatingStarComponent,
    SortPanelFilterComponent,
    LineChartDemoComponent,
    SearchUserComponent,
    LineChartDemoComponent,
    FilterResumeTopComponent,
    InputSearchAutocompleteComponent,
    StudentContactsComponent,
    ResumeShortComponent,
    ResumeListModuleComponent,
    ModalBookmarkComponent,
    DeadlineComponent,
    PublishedAtComponent,
    SubmitedResumeComponent,
    CoverLetterComponent,
    AppliedtovacancyComponent,
    BookMarkComponent,
    SalarysummComponent,
    OptionsDataComponent,
    AddressComponent,
    SetDialogComponent,
    VacancyCardComponent,
    DisconnectFbComponent,
    LoaderComponent,
  ]
})
export class ModulesModule {
}
