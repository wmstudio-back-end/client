import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisconnectFbComponent } from './disconnect-fb.component';

describe('DisconnectFbComponent', () => {
  let component: DisconnectFbComponent;
  let fixture: ComponentFixture<DisconnectFbComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisconnectFbComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisconnectFbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
