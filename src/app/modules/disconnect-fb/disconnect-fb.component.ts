import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
}                    from '@angular/core';
import {UserService} from "../../_services/user.service";
import {DataService} from "../../_services/data.service";
import {Router}      from "@angular/router";

@Component({
  selector:    'disconnectFbModal',
  templateUrl: './disconnect-fb.component.html',
  styleUrls:   ['./disconnect-fb.component.scss']
})
export class DisconnectFbComponent implements OnInit {

  @Input('show') show: boolean      = false
  @Output('close') close            = new EventEmitter<boolean>();
                 newPass: string    = ''
                 repeatPass: string = ''
                 email: string = ''
                 notPass: boolean   = false

  constructor(public user: UserService, public dataService: DataService) {
    this.user.oAuth.subscribe((status) => {
      if (!status)
        this.show = status
    })
    this.user.notPass.subscribe((status) => {
      this.notPass = status
    })
  }

  ngOnInit() {
  }

  clickexclude(e) {
    this.close.emit(false)
  }

  save() {

    let data = {
      newPass:    this.newPass,
      repeatPass: this.repeatPass,
    }

    if (!this.user.profile.email)
      data['email'] = this.email

    this.user.unsetOAuth(data)
  }

}
