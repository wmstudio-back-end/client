"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var MultipleSelectComponent = (function () {
  function MultipleSelectComponent() {
    this.change = new core_1.EventEmitter();
    this.selectedItem = {};
    this.output = [];
    this.multipleSelect = false;
  }

  MultipleSelectComponent.prototype.changeItem = function (item, event) {
    if (event.target.checked) {
      this.selectedItem[item.id] = item;
    }
    else {
      delete this.selectedItem[item.id];
    }
    this.output = [];
    this.selectedstr = "";
    for (var key in this.selectedItem) {
      this.selectedstr += this.selectedItem[key].text + " ";
      this.output.push(this.selectedItem[key]);
    }
    this.change.emit(this.output);
  };
  MultipleSelectComponent.prototype.ngOnInit = function () {
    this.selectedstr = this.title;
  };
  return MultipleSelectComponent;
}());
__decorate([
  core_1.Input()
], MultipleSelectComponent.prototype, "title", void 0);
__decorate([
  core_1.Input()
], MultipleSelectComponent.prototype, "arrIn", void 0);
__decorate([
  core_1.Output('arrOut')
], MultipleSelectComponent.prototype, "change", void 0);
MultipleSelectComponent = __decorate([
  core_1.Component({
    selector: 'app-multiple-select',
    templateUrl: './multiple-select.component.html',
    styleUrls: ['./multiple-select.component.css']
  })
], MultipleSelectComponent);
exports.MultipleSelectComponent = MultipleSelectComponent;
