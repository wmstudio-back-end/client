import {isUndefined} from "util";
import {Component, EventEmitter, Input, OnInit, Output, ElementRef, ViewChild} from "@angular/core";
import {HelpersService} from "../../_services/helpers.service";
import {VacancyService} from "../../_services/vacancy.service";
import {TranslateService} from "@ngx-translate/core";

export interface Items {
    id: number
    text?: string
}
@Component({
    selector: 'app-multiple-select',
    templateUrl: './multiple-select.component.html',
    styleUrls: ['./multiple-select.component.css']
})

export class MultipleSelectComponent implements OnInit {
    @Input() title: string
    @Input() all: any
    @Input() arrIn: Items[]
    @Input() keyOff: string[] = []
    @Input() arrVal: any
    @Input()  error = false
    @Input() valid: boolean = false
    @Input('valueId') valueId: number[]
    @Output('arrOut') change = new EventEmitter<Items[]>()

    @Output('arrOutVal') arrOutVal = new EventEmitter<any>()
    random: string = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5)
    selectedstr: string[] = []


    selectedItem = {}
    output = []
    errorClass = false
    firstShow: boolean = false
    @ViewChild('box') box: ElementRef
    multipleSelect: boolean = false
    @Input() typeOut: number
    @Input('refresh') refresh:boolean
    @Input('change') lastChange:any
    @Input('changeName') changeName:string
    @Output('errorOut') errorOut = new EventEmitter<boolean>()
    constructor(public help: HelpersService, private translate:TranslateService) {

    }

    ngOnChanges(changes: any) {
      if (this.valid){
        var v = false
        // this.error==null?v = true:null
        // this.error==undefined?v = true:null
        // this.error==[]?v = true:null
        // this.error=={}?v = true:null
        // this.error==false?v = true:null
        // this.error==''?v = true:null

        // this.errorClass = this.error
        // this.errorOut.emit(this.errorClass)

      }
        this.errorClass = this.error
        this.errorOut.emit(this.errorClass)
        if (changes.lastChange) {
            if (typeof changes.lastChange.currentValue === "object") {
                if (changes.lastChange.currentValue[0] == this.changeName)
                    this.setSelected()
            }
        }
      if (changes.refresh){
        if (changes.refresh.currentValue!=changes.refresh.previousValue&&typeof changes.refresh.previousValue !== "undefined")
        {
          console.log('REFRESH!!!!!!!!!!!')
          this.selectedItem = {}
          this.output = []
          this.selectedstr = []

          let inputs = this.box.nativeElement.children[0].children;
          for(let i = 0; i < inputs.length; i++){
            inputs[i].children[0].checked = false
          }
          this.setSelected()
        }
      }

    }

    clickexclude(e) {
        if (this.random != e.target.id) {
            this.multipleSelect = false
        }

    }

    changeItem(item, event) {
        if (item.id==-1){
            for(var i=0;i<this.arrIn.length;i++){

                if (this.arrIn[i]['id']==-1){continue;}
                else{

                    this.arrVal[this.arrIn[i]['id']] = event.target.checked
                    this.changeItem(this.arrIn[i], event)
                }
            }
            return
        }
        // if (!event.target.checked){
        //     for(var i=0;i<this.arrIn.length;i++){
        //         if (this.arrIn[i]['id']==-1){
        //             this.arrVal[this.arrIn[i]['id']] = event.target.checked
        //             this.changeItem(this.arrIn[i], event)
        //         }
        //     }
        // }


        if (event.target.checked){
            this.selectedItem[item.id] = item
        }
        else {
            delete this.selectedItem[item.id]
        }
        this.output = []
        this.selectedstr = []
        for (var key in this.selectedItem) {
            //this.selectedstr += this.selectedItem[key].text + " "

            this.selectedstr.push(this.selectedItem[key].text)

            let itemsFilter = {
                id: this.selectedItem[key].id,
                text: this.selectedItem[key].text
            }
            if (this.keyOff.indexOf('text') > -1) {
                delete itemsFilter.text
            }
            if (this.keyOff.indexOf('id') > -1) {
                delete itemsFilter.id
            }

            this.output.push(itemsFilter)
        }


        if (!isUndefined(this.typeOut)){
            switch (this.typeOut){
                case 0:

                    var t = []
                    for(var i=0;i<this.output.length;i++){
                        t.push(this.output[i].id)
                    }
                    this.output = t
                    break;
            }
        }

        //if (!this.selectedstr.length)
          //this.selectedstr.push('All')

        this.change.emit(this.output)
        this.arrOutVal.emit(this.arrVal)

        // if (this.output.length==0){
        //     this.errorClass = true
        // }else{
        //     this.errorClass = false
        // }
      console.log('errorClass',this.output)
    }

    ngOnInit() {
        if (this.all){
            this.arrIn.unshift({id:-1,text:this.all.text||'All'})
        }

        this.setSelected()
        this.help.disWinScroll(this.box.nativeElement)
    }

    toggle() {
        this.multipleSelect = !this.multipleSelect
        if (this.multipleSelect && !this.firstShow) {
            setTimeout(() => {
                if (this.multipleSelect) {
                    this.help.disWinScroll(this.box.nativeElement);
                    this.firstShow = true
                }
            }, 100, this)
        }
    }

    setSelected(){
      if (this.valueId!=undefined){
        this.arrVal = {}
        for(var s=0;s<this.valueId.length;s++){
          for(var i=0;i<this.arrIn.length;i++){

            if (this.arrIn[i]['id']==this.valueId[s]){
              this.selectedItem[this.arrIn[i]['id']]=this.arrIn[i]
              this.arrVal[this.arrIn[i]['id']]=this.arrIn[i]
            }
          }
        }

      }
        if (typeof this.arrVal === "undefined")
            this.arrVal = {}
        else {
            this.selectedstr = []
            // console.log('this.arrVal',this.arrVal)
            if (this.arrVal)
                for (var key in this.arrIn) {
                    var id = this.arrIn[key].id
                    if (this.arrVal.hasOwnProperty(id))
                        if (this.arrVal[id]){
                          this.selectedstr.push(this.arrIn[key].text)


                        }

                }
        }
        // if (!this.selectedstr.length)
        //     this.selectedstr.push('All')


    }

}
