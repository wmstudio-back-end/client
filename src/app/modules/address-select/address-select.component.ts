import {Component, EventEmitter, Input,ElementRef, ViewChild, NgZone, OnInit, Output} from "@angular/core";
// import { MapsAPILoader } from '@agm/core';
// import { } from '@types/googlemaps';
import {UserService} from "../../_services/user.service";
import {LocationService} from "../../_services/location.service";
import {Address} from "angular-google-place";
import {ILocation} from "../../_models/user";

@Component({
    selector: 'address-select',
    templateUrl: './address-select.component.html',
    styleUrls: ['./address-select.component.scss']
})
export class AddressSelectComponent implements OnInit {

    public options: any = {};

    getAddress(place: Address) {
        console.log('Address', place);
    }

    getFormattedAddress(props: any) {
        console.log(props,this.adr_address);
        props.place_id = this.place_id
        if (this.Address) {
            if (this.locationService.location.hasOwnProperty(this.name)) {
                if (this.locationService.location[this.name].hasOwnProperty('_id'))
                    var locateId = this.locationService.location[this.name]._id;
            }
            this.locationService.location[this.name] = props;
            if (typeof locateId !== "undefined")
                this.locationService.location[this.name]._id = locateId;
        } else if (this.locationService.location.hasOwnProperty(this.name)) {
            delete this.locationService.location[this.name];
            //console.log('remove location',this.locationService.location[this.name])
        }

        var iLocation = <ILocation>{
            lat: props['lat'],
            lng: props['lng'],
            place_id: props['place_id'],
            addr: this.input.nativeElement.value,
        }
        this.Location.emit(iLocation)
        this.change.emit(this.input.nativeElement.value);
    }

    public street_number
    public street
    public city
    public state
    public district
    public country
    public postal_code
    public lat
    public lng
    public adr_address
    public place_id
    private key: string = 'AIzaSyADL_-gNyss3Es4ZhCGYCE2-HxUpfGQ75M';
  @Input() valid: boolean
    @Input('addr') Address: string;
    @Input('error') error: any;
    @Input('name') name: string;
    @Input('locate') locate: any;
    @Input('refresh') refresh: boolean
    @Input('placeholder') placeholder: string = 'Address';
    @Input() readonly: any;
    @ViewChild('input') input: ElementRef;
    @Output('Address') change                 = new EventEmitter<string>();
    @Output('Location') Location              = new EventEmitter<any>();
  @Output('errorOut') errorOut = new EventEmitter<boolean>()
    @Input('types') types: string[]           = ['address'];
  errorClass:boolean = false
    constructor(private ngZone: NgZone,
                public user: UserService,
                public locationService: LocationService) {
        this.options.type = this.types
    }

    changeAddress() {
        if (!this.Address) {
            if (this.locationService.location.hasOwnProperty(this.name))
                delete this.locationService.location[this.name];
            this.Location.emit(null)
            this.change.emit(null)
            console.log('erase address')
        }
        // this.change.emit(this.input.nativeElement.value);
    }

    ngOnChanges(changes: any) {
      if (this.valid){
        var v = false
        this.error==null?v = true:null
        this.error==undefined?v = true:null
        this.error==[]?v = true:null
        this.error=={}?v = true:null
        this.error==false?v = true:null
        this.error==''?v = true:null

        this.errorClass = v
        this.errorOut.emit(this.errorClass)

      }
        if (!changes.refresh) return;
        if (changes.refresh.currentValue != changes.refresh.previousValue && typeof changes.refresh.previousValue !== "undefined") {
            if (this.locationService.location.hasOwnProperty(this.name)) {
                delete this.locationService.location[this.name];
                //console.log('remove location',this.locationService.location[this.name])
            }
        }
        this.ngOnInit()
    }

    ngOnInit() {
        console.log(this.types);

        if (!this.Address) {
            this.Address = ''
        }

        // this.addGAPSScript()


        //     .then(() => {
        //         let autocomplete = new google.maps.places.Autocomplete(this.input.nativeElement, {
        //             types: this.types
        //         });
        //         autocomplete.addListener("place_changed", () => {
        //             this.ngZone.run(() => {
        //                 let place: google.maps.places.PlaceResult = autocomplete.getPlace();
        //                 //console.log(place.place_id);
        //                 var props                                 = this.getAddressProp(place);
        //
        //                 props['place_id'] = place.place_id
        //                 this.Address      = this.input.nativeElement.value
        //                 if (this.Address) {
        //                     if (this.locationService.location.hasOwnProperty(this.name)) {
        //                         if (this.locationService.location[this.name].hasOwnProperty('_id'))
        //                             var locateId = this.locationService.location[this.name]._id;
        //                     }
        //                     this.locationService.location[this.name] = props;
        //                     if (typeof locateId !== "undefined")
        //                         this.locationService.location[this.name]._id = locateId;
        //                 } else if (this.locationService.location.hasOwnProperty(this.name)) {
        //                     delete this.locationService.location[this.name];
        //                     //console.log('remove location',this.locationService.location[this.name])
        //                 }
        //
        //                 //console.log("props",props)
        //                 //console.log("this.Address",this.Address)
        //                 //props.addr = this.Address
        //                 var iLocation = <ILocation>{
        //                     lat: props['lat'],
        //                     lng: props['lng'],
        //                     place_id: props['place_id'],
        //                     addr: this.Address,
        //                 }
        //                 this.Location.emit(iLocation)
        //                 this.change.emit(this.Address);
        //             });
        //         });
        //     });
    }

    getAddressProp(place) {
        var props = {};

        if (place.hasOwnProperty('address_components')) {

            var AddressProp = place.address_components;

            var country = AddressProp.filter(i => i.types.indexOf('country') > -1);
            if (country.length > 0)
                props['country'] = country[0].long_name;

            var street = AddressProp.filter(i => i.types.indexOf('street_address') > -1);
            if (street.length > 0)
                props['street'] = street[0].long_name;

            var street_number = AddressProp.filter(i => i.types.indexOf('street_number') > -1);
            if (street_number.length > 0)
                props['street_number'] = street_number[0].long_name;

            var route = AddressProp.filter(i => i.types.indexOf('route') > -1);
            if (route.length > 0)
                props['route'] = route[0].long_name;

            var city = AddressProp.filter(i => i.types.indexOf('locality') > -1);
            if (city.length > 0)
                props['city'] = city[0].long_name;

            var area1 = AddressProp.filter(i => i.types.indexOf('administrative_area_level_1') > -1);
            if (area1.length > 0)
                props['area1'] = area1[0].long_name;

            var area2 = AddressProp.filter(i => i.types.indexOf('administrative_area_level_2') > -1);
            if (area2.length > 0)
                props['area2'] = area2[0].long_name;

            var area3 = AddressProp.filter(i => i.types.indexOf('administrative_area_level_3') > -1);
            if (area3.length > 0)
                props['area3'] = area3[0].long_name;

            var area4 = AddressProp.filter(i => i.types.indexOf('administrative_area_level_4') > -1);
            if (area4.length > 0)
                props['area4'] = area4[0].long_name;

            var area5 = AddressProp.filter(i => i.types.indexOf('administrative_area_level_5') > -1);
            if (area5.length > 0)
                props['area5'] = area5[0].long_name;
        }

        if (place.hasOwnProperty('geometry'))
            if (place.geometry.hasOwnProperty('location')) {
                props['lat'] = String(place.geometry.location.lat());
                props['lng'] = String(place.geometry.location.lng());
            }

        return props;
    }

    addGAPSScript() {
        if (document.getElementById("gaps")) return;
        let script = document.createElement('script')
        script.setAttribute("type", "text/javascript")
        script.setAttribute("src", "https://maps.googleapis.com/maps/api/js?libraries=places&key=" + this.key)
        script.setAttribute("id", "gaps")
        document.getElementsByTagName("head")[0].appendChild(script)
    }

}
