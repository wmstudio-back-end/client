import {
  Component,
  OnInit,
  Input
}                       from '@angular/core';
import {UserService}    from "../../_services/user.service";
import {DataService}    from "../../_services/data.service";
import {HelpersService} from "../../_services/helpers.service";

@Component({
  selector:    'search-user',
  templateUrl: './search-user.component.html',
  styleUrls:   ['./search-user.component.scss']
})
export class SearchUserComponent implements OnInit {
  @Input('type') type: string;
  @Input('users') users: any;
  @Input('uid') uid: string;
  @Input('showInfo') showInfo: number;
                 user: any;
                 port: string;
                 host: string;
                 photo: string = '/assets/img/ic_business_grey.svg';

  constructor(public userService: UserService,
              private dataService: DataService,
              public hs: HelpersService) {
    this.port = dataService.PORT_STATIC;
    this.host = dataService.HOST_STATIC;
  }

  ngOnInit() {
    this.userService.getUsers([this.uid])
  }

  checkCSize() {
    if (this.user.Company.companySize) {
      if (typeof this.dataService.options.companySize[this.user.Company.companySize] !== "undefined")
        return true;
    }
    return false;
  }

  checkUser() {
    if (this.user) return true;
    if (typeof this.userService.users[this.uid] !== "undefined") {
      this.user = this.userService.users[this.uid]
      return true;
    } else false;
  }

}
