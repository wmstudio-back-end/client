"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var progress_account_component_1 = require("./progress-account/progress-account.component");
var progress_prc_component_1 = require("./progress-prc/progress-prc.component");
var multiple_select_component_1 = require("./multiple-select/multiple-select.component");
var ng_click_outside_1 = require("ng-click-outside");
var unity_select_component_1 = require("./unity-select/unity-select.component");
var accordion_select_component_1 = require("./accordion-select/accordion-select.component");
var down_popup_component_1 = require("./down-popup/down-popup.component");
var ModulesModule = (function () {
  function ModulesModule() {
  }

  return ModulesModule;
}());
ModulesModule = __decorate([
  core_1.NgModule({
    imports: [
      common_1.CommonModule,
      ng_click_outside_1.ClickOutsideModule
    ],
    declarations: [
      progress_account_component_1.ProgressAccountComponent,
      progress_prc_component_1.ProgressPrcComponent,
      multiple_select_component_1.MultipleSelectComponent,
      unity_select_component_1.UnitySelectComponent,
      accordion_select_component_1.AccordionSelectComponent,
      down_popup_component_1.DownPopupComponent,
    ],
    exports: [
      progress_account_component_1.ProgressAccountComponent,
      progress_prc_component_1.ProgressPrcComponent,
      multiple_select_component_1.MultipleSelectComponent,
      unity_select_component_1.UnitySelectComponent,
      accordion_select_component_1.AccordionSelectComponent,
      down_popup_component_1.DownPopupComponent,
    ]
  })
], ModulesModule);
exports.ModulesModule = ModulesModule;
