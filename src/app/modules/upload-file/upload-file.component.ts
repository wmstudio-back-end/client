import {Component, Input, Output, OnInit, ElementRef, ViewChild, EventEmitter} from '@angular/core';
import {AuthenticationService} from "../../_services/authentication.service";
import {WindowRef} from "../../_services/window.service";
import {HttpService} from "../../_services/http.service";
import {createElement} from "@angular/core/src/view/element";
export interface Items {
    name: string;
    url: string;
    filename: string;
    date: string;
}


@Component({
    selector: 'form-upload-file',
    templateUrl: './upload-file.component.html',
    styleUrls: ['./upload-file.component.scss']
})
export class UploadFileComponent implements OnInit {
    @Input('type') type: string;
    @Input('name') name: string;
    @Input('attachLink') attachLink: any;
    dropFilesList: any = [];
    dropFilesListPre: any = [];
    @Input('attachImg') attachImg?: any;
    @Input('attachList') attachList?: any;
    @Input('imgHeight') imgHeight?: string;
    @Input('imgWidth') imgWidth?: string;
    @Input('multiply') multiply?: number;
    @Output('dropFileList')
    change = new EventEmitter<Items[]>();
    @ViewChild('uploadInput') input?: any;
    @ViewChild('uploadPath') uploadPath: ElementRef;
    @ViewChild('uploadForm') uploadForm: ElementRef;
    public accept: string;
    private res: any;
    private file: any;
    private dropFiles: any = [];
    private keyFiles: number = 0;

    constructor(private authServ: AuthenticationService, public winRef: WindowRef, private http: HttpService) {
    }

    ngOnInit() {
        if (this.type === 'photo') {
            this.accept = 'image/jpeg,image/png,image/gif';
            this.attachLink.addEventListener('click', function (e) {
                this.input.nativeElement.click();
            }.bind(this));
        } else if (this.type === 'pdf') {
            this.accept = 'application/pdf';
            this.input = document.createElement('input');
            this.input.setAttribute('type', 'file');
            this.input.setAttribute('multiple', '');
            this.input.setAttribute('accept', this.accept);
            this.attachLink.appendChild(this.input);
            this.attachLink.setAttribute('class', this.attachLink.getAttribute('class') + ' posRelative');
            this.input.setAttribute('class', 'fullOfParent');
            this.input.addEventListener('change', function (e) {
                this.changeFileInput(e);
            }.bind(this));

        }
    }

    changeFileInput(e) {
        switch (this.type) {
            case 'photo':
                this.changeInputPhoto()
                break;
            case 'pdf':
                this.changeInputDoc(e)
                break;
        }
    }

    changeInputDoc(e) {
        this.winRef.nativeWindow.chInput = e.target;
        if (e.target.files.length) {
            var files = e.target.files;
            for (var key in files) {
                if (typeof files[key] !== 'object')
                    continue;
                var file = files[key];
                console.log(file);
                var reader = new FileReader();
                var obj = {
                    this: this,
                    key: key,
                    file: file
                };
                reader.onload = function (event) {
                    obj.file.resizeFile = event.target.result;
                    obj.this.uploadToServer(obj.file);
                }.bind(obj);
                reader.readAsDataURL(file)
            }
        }
    }

    addListItem(file, path, id) {
        var date = this.dateToStr(file.lastModifiedDate);
        this.dropFilesList.push({
            id: this.dropFilesList.length,
            _id: id,
            name: file.name,
            url: path,
            filename: file.name,
            date: date,
            del: function (id) {
                this.removeListItem(id)
            }.bind(this)
        });
        this.change.emit(this.dropFilesList)
    }

    removeListItem(id) {
        this.dropFilesList.splice(id, 1);
        this.dropFilesListPre = this.dropFilesList;
        this.dropFilesList = [];
        for (var key in this.dropFilesListPre) {
            this.dropFilesListPre[key].id = this.dropFilesList.length;
            this.dropFilesList.push(this.dropFilesListPre[key]);
        }
        this.change.emit(this.dropFilesList)
    }

    changeInputPhoto() {
        this.attachImg.innerHTML = '';
        this.file = event.target;
        this.winRef.nativeWindow.chInput = this.file;
        this.file = this.file.files[0];
        var img = new Image();
        img.src = URL.createObjectURL(this.file);
        img.onload = function () {
            var fileImage = this.imageLoad(img);
            this.uploadToServer(this.file);
        }.bind(this);
    }

    imageLoad(image) {
        var resize = this.imageToDataUri(image, 100, 100);
        this.file.resizeFile = resize;
        var imgResize = document.createElement('img');
        imgResize.src = resize;
        this.attachImg.appendChild(imgResize);
        return imgResize;
    }

    imageToDataUri(img, width, height) {
        var canvas = document.createElement('canvas'),
            ctx = canvas.getContext('2d');
        var iw = img.width;
        var ih = img.height;
        var scale = Math.min((100 / iw), (100 / ih));
        var iwScaled = iw * scale;
        var ihScaled = ih * scale;

        canvas.width = iwScaled;
        canvas.height = ihScaled;
        ctx.drawImage(img, 0, 0, iwScaled, ihScaled);
        this.file.mimeType = 'image/jpeg';
        return canvas.toDataURL('image/jpeg', 1);
    }

    uploadToServer(file) {
        var token = this.authServ.getToken();
        var data = {
            token: token,
            file: {
                name: file.name,
                content: file.resizeFile,
                mimeType: file.mimeType,
                size: file.size,
            },
            type: this.type
        };
        console.log(data);
        this.http.postData(data).subscribe(function (res) {
            console.log(res);
                if (!res.hasOwnProperty('success'))
                    return;
                if (!res.hasOwnProperty('data'))
                    return;
                if (!res.data.hasOwnProperty('path'))
                    return;
                var path = res.data.path;
                var fileUrl = 'http://localhost:8182' + path;
            if (this.type == 'photo') {
                var image = new Image();
                image.onload = function () {
                    this.attachImg.innerHTML = '';
                    this.attachImg.appendChild(image);
                }.bind(this);
                image.src = fileUrl;
                if (!res.data.hasOwnProperty('id'))
                    return;
                var fileId = res.data.id;
                this.uploadPath.nativeElement.value = fileId;
            } else {
                this.addListItem(file, fileUrl,res.data.id);
            }
        }.bind(this));
    }

    dateToStr(date) {
        console.log(date);
        return date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear() + ' (' + date.getHours() + ':' + date.getMinutes() + ')';
    }
}
