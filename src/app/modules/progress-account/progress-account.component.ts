import {Component, Input, OnInit} from "@angular/core";
@Component({
  selector: 'app-progress-account',
  templateUrl: './progress-account.component.html',
  styleUrls: ['./progress-account.component.css']
})
export class ProgressAccountComponent implements OnInit {
  deg1 = 90
  deg2 = -90
  p: number
  @Input() prc: number;
  @Input() photo: string;
  @Input() point: number;
  @Input() red: boolean = false;
  @Input('master') masterName: string;

  constructor() {

  }
  
  changeBackground(): any {
    let curerntColor = '#FF1E00'

    this.p = Math.floor(this.prc * 3.6)
    if (this.prc < 50) {
      this.deg2 = this.p - 90
    } else {
      this.deg1 = 90+this.p - 180

    }
    
    //0deg = 75%

      //#d2d2d2 gray
      //#00C3F1 aqua
      //#FF1E00 red
      //#F1DC00 yellow
  if( this.prc > 35 && this.prc < 80 ) {
      curerntColor = '#f2cf12'
  } else if (this.prc > 80) {
      curerntColor = '#00C3F1'
  }

    if (this.prc < 50) {
      return {'background-image':'linear-gradient(-90deg,#d2d2d2 50%, transparent 50%), linear-gradient(' + this.deg2 + 'deg,' + curerntColor + ' 50%, #d2d2d2 50%)'};
    } else {
      return {'background-image':'linear-gradient(90deg,' + curerntColor + ' 50%, transparent 50%), linear-gradient(' + this.deg1 + 'deg, ' + curerntColor + ' 50%, #d2d2d2 50%)'};
    }

  }
  
  ngOnInit() {

  }

}
