import {async, ComponentFixture, TestBed} from "@angular/core/testing";
import {ProgressAccountComponent} from "./progress-account.component";
describe('ProgressAccountComponent', () => {
  let component: ProgressAccountComponent;
  let fixture: ComponentFixture<ProgressAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        declarations:[ProgressAccountComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgressAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
