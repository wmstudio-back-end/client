import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../../_services/user.service";
import {User} from "../../_models/index";
import {DataService} from "../../_services/data.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-student-contacts',
  templateUrl: './student-contacts.component.html',
  styleUrls: ['./student-contacts.component.scss']
})
export class StudentContactsComponent implements OnInit {
  @Input() uid:string
  @Input() rating:number
  @Input() recommend:boolean = true
  @Input() ratingView:boolean = true
  @Input() contactView:boolean = true
  @Input() userOldView:boolean = false
  userOld = ''
  openModal = false

  User:User
  recCount = 0
  constructor( public uS:UserService,public dS:DataService,public router: Router,) {

  }
  ngOnChanges(e: any) {
    if (e.uid && e.uid.hasOwnProperty('currentValue')) {

      this.initUser()
    }
    // console.log('CHANGE!!!!!',e)
    // this.ngOnInit()
  }
  ngOnInit() {
    // console.log('INIIIIIIIIIIIT',this.uid)
     this.initUser()



    //this.dS.sendws('Recommendation/getRecommendations',{uid:this.uid,type:1,query:{status:1}}).then(data=>{
    //  this.modalRecomm = data['res']
    //})

      this.uS.getCountRecommFromUid(this.uid).then(data=>{
        data.hasOwnProperty('count')? this.recCount = data['count']:null
      })






  }
  initUser(){
    if (this.uS.users[this.uid]){
      this.User = this.uS.users[this.uid]
      if (this.rating==null){
        this.rating = this.User.rating
      }
      this.userOld = this.uS.yearsOld(this.User._id).toString()
      this.ratingView = this.User.Settings.progress_in_edu2
    }else{
      this.uS.getUsers([this.uid]).then(user=>{
        if (this.uS.users[this.uid]) {
          this.User = this.uS.users[this.uid]
          if (this.rating==null){
            this.rating = this.User.rating
          }
          this.userOld = this.uS.yearsOld(this.uid).toString()
          this.ratingView = this.User.Settings.progress_in_edu2
        }
      })
    }

  }




}
