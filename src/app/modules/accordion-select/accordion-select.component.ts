import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from "@angular/core";
import {HelpersService} from "../../_services/helpers.service";
export interface Items {
    id: number;
    text: string;

}
@Component({
    selector: 'app-accordion-select',
    templateUrl: './accordion-select.component.html',
    styleUrls: ['./accordion-select.component.css']
})
export class AccordionSelectComponent implements OnInit {
    @Input('title') title: string
    @Input('clear') clear: boolean
    @Input('arrIn') arrIn: Items[]
    @Input('arrVal') arrVal: any
    @Output('arrOut') change = new EventEmitter<Items[]>()
    @Input() refresh:boolean;
    @Output('lastElem') lastElement = new EventEmitter<any>()
    @Output('arrOutVal') arrOutVal = new EventEmitter<any>()
    selectedstr: string
    output=[]
    selectedItem = {}
    random: string;
    accordionAcitve: boolean = false
    firstShow: boolean = false
    @Input('change') lastChange:any
    @Input('changeName') changeName:string
    @ViewChild('box') box: ElementRef

    constructor(public help: HelpersService) {
        this.random = help.randomString(5)
    }

    changeItem(item, event) {
        this.lastElement.emit(event.target)
        if (event.target.checked)
        {
          this.selectedItem[item.id] = item
        }
        else
            delete this.selectedItem[item.id]
        this.output = []
        this.selectedstr = ""
        for (var key in this.selectedItem) {
            this.output.push(this.selectedItem[key])
        }
        this.change.emit(this.output)
        this.arrOutVal.emit(this.arrVal)
    }

    ngOnChanges(changes: any) {
        if (changes.lastChange) {
            if (typeof changes.lastChange.currentValue === "object") {
                if (changes.lastChange.currentValue[0] == this.changeName)
                    this.setSelected()
            }
        }
        if (!changes.refresh) return;

        if (changes.refresh.currentValue!=changes.refresh.previousValue&&typeof changes.refresh.previousValue !== "undefined")
        {
            // this.selectedItem = {}
            // this.output = []
            // let inputs = this.box.nativeElement.children[0].children;
            // for(let i = 0; i < inputs.length; i++){
            //     inputs[i].children[0].checked = false
            // }
            this.setSelected()
        }
    }

    ngOnInit() {
        // console.log('typeof',this.title,this.arrVal,(typeof this.arrVal === "undefined"))
        // this.selectedstr = this.title
        this.setSelected()
    }

    setSelected(){
        this.output = []
        if (typeof this.arrVal === "undefined") {
            this.arrVal = {}
            return;
        }
        // console.log('this.arrVal',this.arrVal,this.arrIn)
        if (!this.arrVal) return false;
        for (var key in this.arrIn) {
            var id = this.arrIn[key].id
            if (typeof this.arrVal[id] === "undefined")
                continue;
            if (this.arrVal[id])
                this.output.push(this.arrIn[key])
        }
    }

    toggle() {
        this.accordionAcitve = !this.accordionAcitve
        if (this.accordionAcitve && !this.firstShow) {
            setTimeout(() => {
                if (this.accordionAcitve) {
                    this.help.disWinScroll(this.box.nativeElement);
                    this.firstShow = true
                }
            }, 600, this)
        }
    }
}
