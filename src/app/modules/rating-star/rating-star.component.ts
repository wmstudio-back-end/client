import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
}                from '@angular/core';
import {forEach} from "@angular/router/src/utils/collection";
import {_}       from "@biesbjerg/ngx-translate-extract";

@Component({
  selector: 'rating-star',
  template: `
              <div class="star-wrap" [class.select]="select" #wrap (mouseleave)="setStar()">
                <div *ngFor="let stars of starsRating; let n = index" (click)="selectStar(n)"
                     class="star yellow{{ starsRating[n]}}"></div>
                <div *ngIf="starResult2" class="starResult">{{getValue()}}</div>
                <div *ngIf="dieStar && floor(ratingValue) == 0" class="dieStar">{{"ratingValue.0" | translate}}
                </div>
                <div *ngIf="dieStar && floor(ratingValue) == 1" class="dieStar">{{"ratingValue.1" | translate}}
                </div>
                <div *ngIf="dieStar && floor(ratingValue) == 2" class="dieStar">{{"ratingValue.2" | translate}}
                </div>
                <div *ngIf="dieStar && floor(ratingValue) == 3" class="dieStar">{{"ratingValue.3" | translate}}
                </div>
                <div *ngIf="dieStar && floor(ratingValue) == 4" class="dieStar">{{"ratingValue.4" | translate}}
                </div>
                <div *ngIf="dieStar && floor(ratingValue) == 5" class="dieStar">{{"ratingValue.5" | translate}}
                </div>
              </div>`
  ,
  styleUrls: ['./rating-star.component.scss']
})
export class RatingStarComponent implements OnInit {
  @Input() starResult: boolean = true
  @Input() starResult2         = true
  @Input() dieStar: boolean    = true;
  @Input() ratingValue: number = 0;
  @Input() select: boolean     = false;
  @Input() refresh: boolean
  @Output('selectVal') emit    = new EventEmitter<number>()
  @ViewChild('wrap') wrap: ElementRef
           star                = 0
           selected            = -1
           hover               = -1
           starsRating         = [
             0,
             0,
             0,
             0,
             0
           ]


  constructor() {
  }


  floor = Math.floor;

  ngOnInit() {
    this.getStarResult()
  }

  getValue() {
    if (!this.ratingValue) {
      return 0
    }
    if (this.ratingValue < 0) {
      return 0
    }
    return parseFloat(this.ratingValue.toFixed(1))
  }

  getStarResult() {
    this.star = this.ratingValue

    let float = true;
    for (let i = 0; i < this.starsRating.length; i++) {

      if (this.star >= i + 1) {
        this.starsRating[i] = 100

      } else {
        this.starsRating[i] = 0
        if (float) {
          let ost = this.star - i;
          for (let b = 1; b <= 4; b++) {

            if (1 / 4 * b >= ost) {
              this.starsRating[i] = (1 / 4 * b - (1 / 4)) * 100;
              break;
            }
          }
          ;

        }
        float = false;
      }
    }
  }

  ngOnChanges(changes: any) {

    //   if (!this.select) return
    //   if (!changes.ratingValue) return
    //   if (changes.ratingValue.currentValue != changes.ratingValue.previousValue && typeof changes.ratingValue.previousValue !== "undefined") {
    //     this.getStarResult()
    //   }
    //
    //
    this.getStarResult()
    if (!changes.refresh) return;
    if (changes.refresh.currentValue != changes.refresh.previousValue && typeof changes.refresh.previousValue !== "undefined") {
      this.setStar(-1)
      this.selected = -1
    }

  }

  //
  // ngAfterContentChecked(){
  //   this.hoverStar()
  // }

  hoverStar() {
    if (!this.select) return;

    for (let i = 0; i < this.wrap.nativeElement.children.length; i++) {
      let star = this.wrap.nativeElement.children.item(i)
      // console.log('star',star)
      star.addEventListener('mouseover', function (e) {
        // console.log(e.target.getAttribute('data-num'))
        this.setStar(parseInt(e.target.getAttribute('data-num')))
      }.bind(this))
    }
  }

  selectStar(num: number) {
    if (!this.select) return;
    this.setStar(num)
    this.selected = num
    this.emit.emit(num + 1)
  }

  // clearStar() {
  //   if(this.ratingValue != 0) {
  //       this.ratingValue = 0;
  //       this.starsRating = [0,0,0,0,0]
  //   }
  // }

  setStar(n?: number) {
    if (!this.select) return;
    n = (n || n == 0) ? n : this.selected
    if (this.hover == n) return;
    this.hover     = n
    let starRating = []
    for (var i = 0; i < 5; i++) {
      starRating[i] = i <= n ? 100 : 0
    }
    // console.log('setStar',starRating,n)
    this.starsRating = starRating
  }

}
