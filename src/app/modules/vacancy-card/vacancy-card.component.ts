import {
  Component, EventEmitter,
  Input,
  OnInit, Output
}                       from '@angular/core';
import {IVacancy}       from "../../_models/vacancy";
import {User}           from "../../_models/user";
import {
  DataService,
  UserService
}                       from "../../_services/index";
import {HelpersService} from "../../_services/helpers.service";

@Component({
  selector:    'app-vacancy-card',
  templateUrl: './vacancy-card.component.html',
  styleUrls:   ['./vacancy-card.component.scss']
})
export class VacancyCardComponent implements OnInit {
  @Input() vacancy
  @Input() exp_deadline         = false
  @Input() exp_published_at     = false
  @Input() exp_top_published_at = false
  @Input() exp_submitedResume   = false
  @Input() exp_totalsubs        = false
  @Input() exp_coverLetter      = false
  @Input() exp_appliedtovacancy = false
  @Input() exp_bookMark         = false
  @Input() exp_address          = false
  @Input() class                = null
  @Output() routeDetail = new EventEmitter<string>()
  totalsubs = 0
  coverModal
  coverModalVac
  content        = ''
  subscribeCount = []

  constructor(
    public dS: DataService,
    public helpers: HelpersService,
    public uS: UserService,
  ) {
  }

  ngOnInit() {
    this.vacancy.hightFrame     = this.vacancy.PROMhightFrame > this.helpers.getUnixTimestamp() ? true : false
    this.vacancy.premiumListing = this.vacancy.PROMpremiumListing > this.helpers.getUnixTimestamp() ? true : false
    if (this.exp_totalsubs){
      this.dS.sendws('Vacancy/getTotalSubscribe',{_id:this.vacancy._id}).then(data=>{
        this.totalsubs = data['totalsubs']||0
      })
    }
  }


}
