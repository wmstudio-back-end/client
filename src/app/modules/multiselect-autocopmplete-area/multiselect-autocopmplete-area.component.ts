import {Component, ElementRef, ViewChild, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {DataService} from "../../_services/data.service";
import {TranslateService} from "@ngx-translate/core";
export interface Items {
  id: number;
  text: string;
  new?:boolean

}

@Component({
  selector: 'multiselect-autocopmplete-area',
  templateUrl: './multiselect-autocopmplete-area.component.html',
  styleUrls: ['./multiselect-autocopmplete-area.component.scss']
})
export class MultiselectAutocopmpleteAreaComponent implements OnInit {
  @Input() skills : Items[];
  @Input() title : string;
  @Output() outList = new EventEmitter<Items[]>();
  @Input() keyOff: string[];
  @Input() max: number = 50;
  random : string = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);
  suitableItem = [];
  @Input('items') selectedItem:any[] = [];
  addNewSchool : boolean = false;
  addTableSkills : boolean = false;
  viewLimit = 10
  hoverIndex = -1;
  searchValue: string = ''
  findList : boolean = false;
  @ViewChild('mpslInput') mpslInput: ElementRef;

  constructor(public dataService: DataService,public translate: TranslateService,) {
  }
  clickexclude(e) {
    if (e.target.className=='mpsl-findList'||e.target.className=='mpsl-input'){
     return
    } else {
      this.findList = false;
    }

  }
  ngOnInit() {
   // this.skills.splice(0,50)
  }

  clickBlock(e){
    var target = e.target;
    if (target.className == "mpsl-autoCompleteArea"&&this.mpslInput)
      this.mpslInput.nativeElement.focus()
  }

  findText(text: string, e) {
    this.findList = true;
    if (e.keyCode == 37 || e.keyCode == 39) {
      return
    }
    console.log(e)
    if (e.keyCode == 40 || e.keyCode == 38 || e.keyCode == 13||e.key == ';'||e.key == ','||e.key == '.') {
      if (e.keyCode == 13||e.key == ';'||e.key == ','||e.key == '.') {
          if (e.key == ';'||e.key == ','||e.key == '.'){

            text = text.substring(0, text.length-1);
          }

        if (this.suitableItem[this.hoverIndex]) {
          this.checkItem(this.suitableItem[this.hoverIndex])
        }else{
          if (text.length>1){
            let newSkill = {
              id:Math.floor(Date.now() / 1000),
              text:text,
              new:true
            }
           // console.log('selectedItem/skills',this.selectedItem,this.skills)

            this.selectedItem.push(newSkill)
            // this.skills.push(newSkill)
            this.emitOutput(this.selectedItem)

          }
        }
        e.target.value = null
        this.findList = false;
        return
      }
      e.keyCode == 40 ? this.hoverIndex++ : this.hoverIndex--
      this.hoverIndex < 0 ? this.hoverIndex = 0 : null
      this.hoverIndex > this.suitableItem.length - 1 ? this.hoverIndex = this.suitableItem.length - 1 : null
      for (var key in this.suitableItem) {
        this.suitableItem[key].hover = false
      }
      if (this.suitableItem[this.hoverIndex]){
        this.suitableItem[this.hoverIndex].hover = true
      }

    } else {


    this.suitableItem = []
    if(text.length > 0) {
      let add = true
      for(var key in this.skills) {
        add = true


        if(this.skills[key].text&&this.translate.get(this.skills[key].text)['value'].toLowerCase().indexOf(text.toLowerCase()) > -1) {

          for(var key2 in this.selectedItem) {
            if(this.skills[key].id == this.selectedItem[key2].id) {
              add = false
              break
            }
          }
          if (add) {

            this.suitableItem.push(this.skills[key]);
            if (this.suitableItem.length > this.viewLimit) {
              break;
            }
          }


        }
      }
      this.suitableItem.sort(this.sortItems)
    }
    }
  }



  filters(key) {


  }
sortItems(a, b){

    if(a.text.toLowerCase() < b.text.toLowerCase()){
      return -1;
    }else if(a.text.toLowerCase() > b.text.toLowerCase()){
      return 1;
    }
    return 0;

}
  deleteSelectedItem(item){
    for(var key in this.selectedItem) {

      if (this.selectedItem[key].id==item.id){
        this.selectedItem.splice(parseInt(key),1)
        this.suitableItem.push(item)
        this.suitableItem.sort(this.sortItems)
        break;
      }

    }
    this.emitOutput(this.selectedItem)
  }
  checkItem(item){
    this.hoverIndex = -1
    if (!this.selectedItem){
      this.selectedItem = [];
    }
    this.mpslInput.nativeElement.value = ''
    this.selectedItem.push(item)


    for(var key in this.suitableItem) {
      //console.log(this.suitableItem[key].id+'===='+item.id+'')
        if (this.suitableItem[key].id+''==item.id+''){

          this.suitableItem.splice(parseInt(key),1)
          break;
        }
    }


    this.emitOutput(this.selectedItem)

  }
  emitOutput(e){
    let output = []
    for(var k in e){
      if (!e[k].hasOwnProperty('id')){continue}
      let itemsFilter = {
        id:e[k].id,
        text:e[k].text,
        new:e[k].new
      }
      for(var v in this.keyOff){

        if (this.keyOff.indexOf(this.keyOff[v])>-1){delete itemsFilter[this.keyOff[v]]}
      }
      output.push(itemsFilter)
    }

    this.outList.emit(output);
  }

}
