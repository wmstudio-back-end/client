import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserService} from "../../_services/user.service";
import {DataService} from "../../_services/data.service";

@Component({
  selector: 'file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss']
})
export class FileListComponent implements OnInit {
  @Input('inputStyle') inputStyle: string = 'fileList';
  @Input('isDelete') isDelete: boolean = true

  @Input('FilesList') FilesList: any[];
  @Output('change') change = new EventEmitter<any>();
  i:number = 0;
  constructor(public user:UserService,public dS:DataService
  ) {
  }
  deleteItems(i){
    this.FilesList.splice(i,1)
    this.change.emit(this.FilesList)

  }
  open(url){
    window.open( url );
  }
  ngOnInit() {
    // console.log("this.FilesListthis.FilesListthis.FilesList",this.FilesList);
  }

  checkItem (item){

    if (typeof item === "undefined")
      return false;
    return item.hasOwnProperty('path');
  }


  console(item) {
    console.log(item)

  }
}
