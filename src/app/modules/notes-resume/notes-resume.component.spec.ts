import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotesResumeComponent } from './notes-resume.component';

describe('NotesResumeComponent', () => {
  let component: NotesResumeComponent;
  let fixture: ComponentFixture<NotesResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotesResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
