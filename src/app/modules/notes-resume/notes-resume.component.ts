import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {UserService} from "../../_services/user.service";
import {DataService} from "../../_services/data.service";
import {DownPopupService} from "../../_services/down-popup.service";


@Component({
  selector:   'app-notes-resume',
  templateUrl:'./notes-resume.component.html',
  styleUrls:  ['./notes-resume.component.scss']
})
export class NotesResumeComponent implements OnInit {
  @Input() notesFromResume:any
  @Input() resume:any
  @Input() updater:any
  notesText = ''
  notesRating = 0
  editNoteId = ''
  addNewNotes = true
  @Output() notesRatingAll = new EventEmitter<number>()
  constructor(public uS:UserService,public dS:DataService,public popS:DownPopupService) { }

  ngOnInit() {
    console.log("INIT")
    this.uploadNotes(this.resume)
  }
  editNote(note){
    this.editNoteId = note._id
    this.notesText = note.text
    this.notesRating = note.rating
    this.addNewNotes = true
  }
  checkNotes(){

    if (this.uS.first_profile&&this.uS.first_profile._id){
      for (var i = 0; i < this.notesFromResume.length; i++) {
        if (this.notesFromResume[i].uid==this.uS.first_profile._id){
          this.addNewNotes = false
        }

      }
    }else{
      for (var i = 0; i < this.notesFromResume.length; i++) {
        if (this.notesFromResume[i].uid==this.uS.profile._id){
          this.addNewNotes = false
        }

      }
    }


  }
  updateRating(){
    var ratingAll = 0
    for (var i = 0; i < this.notesFromResume.length; i++) {

      ratingAll+=this.notesFromResume[i].rating
    }
    this.notesRatingAll.emit(ratingAll/i)
  }
  uploadNotes(r){
    this.editNoteId = ''
    var uids = [this.uS.profile._id]
    if (this.uS.first_profile._id){
      uids.push(this.uS.first_profile._id)
    }
    this.dS.sendws('Resume/GetNotesFromResumeId',{_id:r._id,companyId:this.uS.profile._id}).then(notes=>{
      var ids = []

      for (var i = 0; i < notes['notes'].length; i++) {
        ids.push(notes['notes'][i].uid)

      }

      this.uS.getUsers(ids).then(data=>{
        this.notesFromResume = notes['notes']
        console.log('==================',this.notesFromResume)
        this.updateRating()
        this.checkNotes()

      })

    })
  }
  sendNotes(r){
    if (!this.uS.accessPackage.projBmark){
      this.popS.show_warning(this.dS.getOptionsFromId('errors',10).text)
      return
    }
    if (this.uS.accessRight==3){
      this.popS.show_info(this.dS.getOptionsFromId('errors',28).text)
      return
    }
    if (this.editNoteId==''){
      this.dS.sendws('Resume/SetNotesFromResumeId',{companyId:this.uS.profile._id,_id:r._id,text:this.notesText,rating:this.notesRating}).then(notes=>{
        this.notesFromResume.push(notes['notes'])
        this.notesText = ''
        this.notesRating = 0
        this.checkNotes()
        this.updateRating()
      })
    }else{
      if (this.notesText!=''){
        this.dS.sendws('Resume/UpdateNotesFromResumeId',{_id:r._id,text:this.notesText,rating:this.notesRating,editNoteId:this.editNoteId}).then(notes=>{
          for (var i = 0; i < this.notesFromResume.length; i++) {
            if (this.notesFromResume[i]._id==this.editNoteId){
              this.notesFromResume[i].text = this.notesText
              this.notesFromResume[i].rating = this.notesRating
            }
          }
          console.log(this.notesFromResume)
          this.editNoteId=''
          this.notesText = ''
          this.notesRating = 0
          this.checkNotes()
          this.updateRating()
        })
      }

    }

  }
}
