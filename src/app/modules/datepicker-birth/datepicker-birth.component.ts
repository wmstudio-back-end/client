import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
@Component({
  selector: 'datepicker-birth',
  templateUrl: './datepicker-birth.component.html',
  styleUrls: ['./datepicker-birth.component.scss']
})
export class DatepickerBirthComponent implements OnInit {
  @Input() title: string;

  @Output('date')
  date = new EventEmitter<string>();

  selectedItem = new Date().toDateString()
  
  constructor() {
  }
  
  changeItem(item) {
    this.date.emit(this.selectedItem)
  }
  
  ngOnInit() {
  }

}
