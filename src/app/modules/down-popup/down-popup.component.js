"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var DownPopupComponent = (function () {
  function DownPopupComponent() {
    this.icon = {
      "default": "",
      "success": "done",
      "info": "error",
      "warning": "warning"
    };
  }

  DownPopupComponent.prototype.ngOnInit = function () {
  };
  return DownPopupComponent;
}());
__decorate([
  core_1.Input()
], DownPopupComponent.prototype, "text", void 0);
__decorate([
  core_1.Input()
], DownPopupComponent.prototype, "fixed", void 0);
__decorate([
  core_1.Input()
], DownPopupComponent.prototype, "type", void 0);
DownPopupComponent = __decorate([
  core_1.Component({
    selector: 'app-down-popup',
    templateUrl: './down-popup.component.html',
    styleUrls: ['./down-popup.component.scss']
  })
], DownPopupComponent);
exports.DownPopupComponent = DownPopupComponent;
