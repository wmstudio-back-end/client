import {Component, Input, OnInit} from "@angular/core";
import {DownPopupService} from "../../_services/down-popup.service";
import { NgxCarousel } from 'ngx-carousel';
import {DataService} from "../../_services/data.service";



@Component({
  selector: 'app-down-popup',
  templateUrl: './down-popup.component.html',
  styleUrls: ['./down-popup.component.scss']
})
export class DownPopupComponent implements OnInit {
  @Input() text: string;
  @Input() buttonUrl: string;
  @Input() type: string;
  public carouselOne: NgxCarousel;
  slider = window.innerWidth > 900 ? true : false;



  icon = {
    "default":"",
    "undefined":"success",
    "success":"done",
    "info":   "error",
    "warning":"warning"
  }

  constructor(public popS:DownPopupService, public dS:DataService) {
  }
  deleteNotify(_id){
  if (_id){
    this.dS.sendws('Notify/deleteNotify',{_id:_id}).then(data=>{
      console.log(data)
    })
  }


    for (var i = 0; i < this.popS.pops.length; i++) {
      if (this.popS.pops[i]._id==_id){
        this.popS.pops.splice(i,1)
        this.popS.initPops()
        break
      }

    }

  }

  ngOnInit() {

      this.carouselOne = {
          grid: {xs: 1, sm: 1, md: 1, lg: 1, all: 0},
          slide: 1,
          speed: 400,
          interval: 4000,
          point: {
              visible: true,
              pointStyles: `
              .ngxcarouselPoint {
                position: absolute;
                width: 100%;
                text-align: center;
                bottom: 0;
                margin-bottom: 11px;
               }
              
              .ngxcarouselPoint li {
                width: 11px;
                height: 11px;
                border-radius: 50%;
                display: inline-block;
                 background: transparent;
               border: 2px solid rgba(255,255,255,0.7);
                margin-right: 8px;
               
              }
              .ngxcarouselPoint li.active {
                  background: #fff;
                 
              }
              `

          },
          load: 4,
          touch: true,
          loop: false,
          custom: 'banner'
      }
  }

}
