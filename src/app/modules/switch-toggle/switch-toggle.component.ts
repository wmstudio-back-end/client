import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';


@Component({
  selector: 'switch-toggle',
  templateUrl: './switch-toggle.component.html',
  styleUrls: ['./switch-toggle.component.scss']
})


export class SwitchToggleComponent implements OnInit {
    @Output('checkedState') checkedState = new EventEmitter<boolean>();
    @Input('trueTitle') trueTitle:string
    @Input('falseTitle') falseTitle:string
    @Input('defaultInput') defaultInput:boolean
    @Input('readonly') readonly:boolean = false
    InputStatus: boolean = false;
    random:string = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5);

  constructor() {}

  ngOnInit() {
      this.InputStatus = this.defaultInput
      this.setTitle()
  }
  ngOnChanges(changes: any) {

    if (changes.defaultInput) {

      this.ngOnInit()
    }

  }
checkInput(){


    this.checkedState.emit(this.InputStatus)
    this.setTitle()


}
setTitle(){
    if (this.InputStatus){return this.trueTitle}else{return this.falseTitle}
}

}
