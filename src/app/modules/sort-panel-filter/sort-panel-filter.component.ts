import {Component, Input,EventEmitter, OnInit, Output} from "@angular/core";
import {ComponentService}                              from "../../_services/component.service";
import {DataService}                                   from "../../_services/data.service";
import {VacancyService}                                from "../../_services/vacancy.service";
import {UserService}                                   from "../../_services";
@Component({
    selector: 'app-sort-panel-filter-component',
    templateUrl: './sort-panel-filter.component.html',
    styleUrls: ['./sort-panel-filter.component.scss']
})
export class SortPanelFilterComponent implements OnInit {
    @Input() viewCompact: boolean = false
    @Input() viewRange: boolean = false
    @Input() viewSelect: boolean = false
    @Input() savedSearchCompanyMenu: boolean = false
    @Input() btns: any
    @Output('btn') sortBtns = new EventEmitter<string>()
    @Output('sortbtns') setSortBtns = new EventEmitter<any>()
    @Output('compact') compact = new EventEmitter<boolean>()
    comp:boolean = false
    openFilter:boolean = false
    constructor(public dataService : DataService, public vacancyService:VacancyService, public us:UserService) {
      this.comp = this.us.preferCompactView
    }

    ngOnInit() {
    }

    shadowBody(rm = false) {
        let body = document.getElementsByTagName('body')[0];
        if (rm)
          return body.classList.remove('blackFon');
        if(this.openFilter)
            body.classList.add('blackFon');
        else
            body.classList.remove('blackFon');
    }

    sortVacancy(key,val?){
        if (val) this.setSortBtns.emit({key:key,val:val})
        else this.sortBtns.emit(key)
    }

    setOlderThan(val){
        if (typeof val === "string"){
            val = parseInt(val)
        }
        if (typeof val === "number")
            val = {id:val}
        let postDate:Date = new Date()
        postDate.setMinutes(0)
        postDate.setSeconds(0)
        postDate.setHours(0)
        postDate.setMilliseconds(0)
        let now:number = (new Date()).getDate()
        let day = 0
        switch (val.id){
            case 0:
                day = 3
            break;
            case 1:
                day = 7
            break;
            case 2:
                day = 14
            break;
            case 3:
                day = 31
            break;
            case 4:
                day = 0
            break;
        }
        console.log(day)
        if (day){
            postDate.setDate(now - day)
            let value =  postDate.getTime() / 1000 + (86400 * 30)
            this.vacancyService.setFilterItem(this.vacancyService.getFilterItem('published_at','$gte',value))
        } else this.vacancyService.unserFilter('published_at','$gte');

        this.searchVacancy()
    }

    searchVacancy(){
        this.vacancyService.filter.skip = 0
        this.vacancyService.getVacancy()
    }

  changeComp() {
    this.comp=!this.comp;
    this.compact.emit(this.comp)
    this.us.setPreferCompactView(this.comp)
  }
}
