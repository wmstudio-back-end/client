import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
}                    from '@angular/core';
import {UserService} from "../../_services/user.service";
import {DataService} from "../../_services/data.service";
import {
  ActivatedRoute,
  Router
}                    from "@angular/router";
import {DownPopupService} from "../../_services/down-popup.service";

@Component({
  selector:    'app-modal-bookmark',
  templateUrl: './modal-bookmark.component.html',
  styleUrls:   ['./modal-bookmark.component.scss']
})
export class ModalBookmarkComponent implements OnInit {
  @Input() boomarkId: string
  @Input() show     = false
  @Output() close   = new EventEmitter<boolean>();
           projects = []
           newprojectName: string
           Status: number
           Action: number
           ProjectId: number

  constructor(public uS: UserService, public dS: DataService, private router: Router, private popS:DownPopupService) {
  }

  ngOnInit() {
    this.projects = []
    if (this.uS.profile.Company){
      for (var i = 0; i < this.uS.profile.Company.projects.length; i++) {
        this.projects.push({
          id:   this.uS.profile.Company.projects[i]._id,
          text: this.uS.profile.Company.projects[i].name
        })

      }
    }

  }


  clickexclude(e) {
    //console.log('====CLICK======',e.target.classList)
    if (
      !e.target.classList.contains('contact-item')
    ) {

      //console.log('====CLOSE======',e.target)
      this.close.emit(false)
    }
  }

  save() {

    //this.dataService.sendws('User/setBookmarks', {id:id}).then((data) => {

    console.log("newprojectName", this.newprojectName)
    console.log("Status", this.Status)
    console.log("Action", this.Action)
    console.log("ProjectId", this.ProjectId)
    console.log("boomarkId", this.boomarkId)
    if (this.newprojectName && this.newprojectName != '') {
      this.dS.sendws("Vacancy/createNewProject", {name: this.newprojectName}).then((data) => {
        this.uS.profile.Company.projects.push(data['project'])

        this.ProjectId = data['project']._id
        this.setBookmark()

      });
    } else {
      this.setBookmark()
    }

  }

  setBookmark() {
    console.log(this.router.url)
    var savedFrom = 1
    this.dS.sendws("Events/setEvent", {
      event: {
        name:   'setBookmarkResume',
        option: {_id: this.boomarkId}
      }
    }).then((data) => {

    })

    this.router.url.indexOf('/resume/search') > -1 ? savedFrom = 0 : null;
    // this.popS.show_info(this.dS.getOptionsFromId('errors',13).text);
    this.popS.show_info(this.dS.getOptionsFromId('errors', this.uS.profile.type_profile == 'Company' ? 13 : 15).text);
    this.dS.sendws('User/setBookmarks',
      {
        id:        this.boomarkId,
        action:    this.Action,
        from:      this.Status,
        project:   this.ProjectId,
        type:      this.Status,
        savedFrom: savedFrom
      }).then((data) => {
      this.uS.profile.bookmarks.items.push(data['bookmark'])
      this.close.emit(false)
      this.ngOnInit()
    })

  }
}
