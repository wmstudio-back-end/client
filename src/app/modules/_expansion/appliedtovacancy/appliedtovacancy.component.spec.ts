import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppliedtovacancyComponent } from './appliedtovacancy.component';

describe('AppliedtovacancyComponent', () => {
  let component: AppliedtovacancyComponent;
  let fixture: ComponentFixture<AppliedtovacancyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppliedtovacancyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppliedtovacancyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
