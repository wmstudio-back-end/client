import {Component, Input, OnInit} from '@angular/core';
import {DataService, UserService} from "../../../_services";

@Component({
  selector: 'exp-appliedtovacancy',
  templateUrl: './appliedtovacancy.component.html',
  styleUrls: ['./appliedtovacancy.component.css']
})
export class AppliedtovacancyComponent implements OnInit {
  @Input() vacancyId: any;
  constructor(public uS:UserService,public dS:DataService) { }

  ngOnInit() {
  }

}
