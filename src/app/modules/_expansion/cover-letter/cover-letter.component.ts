import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataService, UserService} from "../../../_services";

@Component({
  selector: 'exp-coverLetter',
  templateUrl: './cover-letter.component.html',
  styleUrls: ['./cover-letter.component.css']
})
export class CoverLetterComponent implements OnInit {
  @Input() vacancyId: any;
  @Output() coverLetter = new EventEmitter<{}>();
  coverModal
  coverModalVac
  content = ''
  constructor(private uS:UserService,public dS:DataService) { }

  ngOnInit() {
  }
  getCoverLetter(id){
    this.content = ''
    for (var s = 0; s < this.uS.profile.subscribeVacancy.length; s++) {
      //ids.push(this.uS.profile.subscribeVacancy[s].vacancyId)
      if (this.uS.profile.subscribeVacancy[s].vacancyId==id){
        if (this.uS.profile.subscribeVacancy[s].content){

          var data = {
            title:'',
            text:this.uS.profile.subscribeVacancy[s].content
          }
         var a= false
          if (this.uS.profile.subscribeVacancy[s].coverLetterId){
            for (var i = 0; i < this.uS.profile.CoverLetter.length; i++) {
              if (this.uS.profile.CoverLetter[i]._id==this.uS.profile.subscribeVacancy[s].coverLetterId){
                a = true
                data = Object.assign({},this.uS.profile.CoverLetter[i])
              }

            }
          }
          if (!a){
             data.title = this.uS.profile.subscribeVacancy[s].content.length>7?this.uS.profile.subscribeVacancy[s].content.substring(0,7)+'...':this.uS.profile.subscribeVacancy[s].content
          }
          data.text = this.uS.profile.subscribeVacancy[s].content
          return data
        }

      }
    }
    return ''
  }

}
