import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'exp-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {
  @Input() address: any;
  constructor() { }

  ngOnInit() {
  }

}
