import { Component, Input, OnInit} from '@angular/core';
import {DataService} from "../../../_services";

@Component({
  selector: 'exp-salarysumm',
  templateUrl: './salarysumm.component.html',
  styleUrls: ['./salarysumm.component.css']
})
export class SalarysummComponent implements OnInit {
  @Input() salary
  @Input() published_at
  summ = ''
  constructor(public dS:DataService) { }

  ngOnInit() {

    if (this.salary.summ&&this.salary.summ2){
      this.summ = this.salary.summ+' - '+this.salary.summ2
    }else if(this.salary.summ){
      this.summ = this.salary.summ+''
    }else if(this.salary.summ2){
      this.summ = this.salary.summ2+''
    }
    this.summ = this.summ.replace(new RegExp ('_', 'g'), '')
  }

}
