import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalarysummComponent } from './salarysumm.component';

describe('SalarysummComponent', () => {
  let component: SalarysummComponent;
  let fixture: ComponentFixture<SalarysummComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalarysummComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalarysummComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
