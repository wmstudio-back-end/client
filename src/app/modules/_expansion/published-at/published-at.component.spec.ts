import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedAtComponent } from './published-at.component';

describe('PublishedAtComponent', () => {
  let component: PublishedAtComponent;
  let fixture: ComponentFixture<PublishedAtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublishedAtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublishedAtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
