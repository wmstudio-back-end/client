import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../../../_services";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector: 'exp-published_at',
  templateUrl: './published-at.component.html',
  styleUrls: ['./published-at.component.css']
})
export class PublishedAtComponent implements OnInit {
  @Input() dateTime: any;
  @Input() days:boolean = false;
  date:number = 0
  dateT = 0
  constructor(public dS:DataService,public hS:HelpersService) { }

  ngOnInit() {
    if (this.dateTime){

      var t = this.hS.getUnixTimestamp()  -  (this.dateTime - 86400*30);
      if (t<3600){this.date = ~~(t/60);this.dateT = 0;}
      else if (t<86400){this.date = ~~(t/60/60);this.dateT = 1;}
      else {this.date = ~~(t/60/60/24);this.dateT = 2;}

    }
  }

}
