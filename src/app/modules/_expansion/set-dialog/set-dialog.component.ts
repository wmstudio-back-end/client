import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../../../_services";
import {MessageService} from "../../../_services/message.service";
import {Router} from "@angular/router";

@Component({
  selector: 'exp-set-dialog',
  templateUrl: './set-dialog.component.html',
  styleUrls: ['./set-dialog.component.css']
})
export class SetDialogComponent implements OnInit {
  @Input() userId: any;
  constructor(
    public uS:UserService,
    public mS:MessageService,
    private router: Router,) { }

  ngOnInit() {
  }

}
