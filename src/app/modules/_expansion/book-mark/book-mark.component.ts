import {Component, Input, OnInit} from '@angular/core';
import {DataService, UserService} from "../../../_services";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector: 'exp-bookMark',
  templateUrl: './book-mark.component.html',
  styleUrls: ['./book-mark.component.css']
})
export class BookMarkComponent implements OnInit {
  @Input() vacancyId: any;
  @Input() dateOutput: number;
  date:number = 0
  dateT = 0
  constructor(public uS:UserService,public dS:DataService,public hS:HelpersService) { }

  ngOnInit() {
    if (this.dateOutput){

      var t = this.hS.getUnixTimestamp()  -  this.dateOutput;
       if (t<3600){this.date = ~~(t/60);this.dateT = 0;}
      else if (t<86400){this.date = ~~(t/60/60);this.dateT = 1;}
      else {this.date = ~~(t/60/60/24);this.dateT = 2;}

    }

  }

}
