import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../../../_services";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector: 'options-data',
  templateUrl: './options-data.component.html',
  styleUrls: ['./options-data.component.css']
})
export class OptionsDataComponent implements OnInit {
  @Input() type: any;
  @Input() data: any;
  @Input() title: any;
  @Input() options: any;
  @Input() dataValues: any;
  @Input() prefixEnd: any;
  constructor(public dS:DataService,public helpers:HelpersService) { }

  ngOnInit() {
    console.log(this.data)
  }

}
