import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../../../_services";

@Component({
  selector: 'exp-submitedResume',
  templateUrl: './submited-resume.component.html',
  styleUrls: ['./submited-resume.component.css']
})
export class SubmitedResumeComponent implements OnInit {
  @Input() vacancyId: any;
  constructor(private uS:UserService) { }

  ngOnInit() {
  }
  getResumeName(id){
    for (var s = 0; s < this.uS.profile.subscribeVacancy.length; s++) {
      //ids.push(this.uS.profile.subscribeVacancy[s].vacancyId)
      if (this.uS.profile.subscribeVacancy[s].vacancyId==id){
        for (var i = 0; i < this.uS.profile.Student.resumes.length; i++) {
          if (this.uS.profile.Student.resumes[i]._id==this.uS.profile.subscribeVacancy[s].resumeId){
            return this.uS.profile.Student.resumes[i].name

          }
        }
      }

    }
    return ''
  }
  getResumeId(id){
    for (var s = 0; s < this.uS.profile.subscribeVacancy.length; s++) {
      //ids.push(this.uS.profile.subscribeVacancy[s].vacancyId)
      if (this.uS.profile.subscribeVacancy[s].vacancyId==id){
        for (var i = 0; i < this.uS.profile.Student.resumes.length; i++) {
          if (this.uS.profile.Student.resumes[i]._id==this.uS.profile.subscribeVacancy[s].resumeId){
            return this.uS.profile.Student.resumes[i]._id

          }
        }
      }

    }
    return ''
  }
}
