import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitedResumeComponent } from './submited-resume.component';

describe('SubmitedResumeComponent', () => {
  let component: SubmitedResumeComponent;
  let fixture: ComponentFixture<SubmitedResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitedResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitedResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
