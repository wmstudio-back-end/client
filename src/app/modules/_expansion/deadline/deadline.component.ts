import {Component, Input, OnInit} from '@angular/core';
import {DataService} from "../../../_services";
import {HelpersService} from "../../../_services/helpers.service";

@Component({
  selector: 'exp-deadline',
  templateUrl: './deadline.component.html',
  styleUrls: ['./deadline.component.css']
})
export class DeadlineComponent implements OnInit {

  @Input() dateTime: any;
  constructor(public dS:DataService,public hS:HelpersService) { }

  ngOnInit() {
  }

}
