import {
  Component,
  OnInit
}                         from "@angular/core";
import {
  AuthenticationService,
  DataService
}                         from "./_services/index";
import {TranslateService} from "@ngx-translate/core";
import {MessageService}   from "./_services/message.service";
import {
  Router,
  NavigationEnd
}                         from "@angular/router";
import {DownPopupService} from "./_services/down-popup.service";
import {UserService}      from "./_services/user.service";
import {Message}          from "./_services/data.service";

@Component({
  selector:    'app',
  templateUrl: 'app.component.html',
  styleUrls:   ['app.component.scss']


})

export class AppComponent implements OnInit {
  email: string    = '';
  password: string = ''
  loading          = false;
  returnUrl: string;
  prod: boolean    = true;
  private message  = <Message>{};


  constructor(public dataService: DataService,
              public authenticationService: AuthenticationService,
              private translate: TranslateService,
              private router: Router,
              public popS: DownPopupService,
              public uS: UserService,
  ) {

  }

  ngOnInit() {
    // this.translate.setDefaultLang('en');
    this.translate.addLangs([
      "en",
      "et",
      "ru"
    ]);
    this.translate.use('ru')
    this.translate.use('en')
    this.translate.use('et')
    //let browserLang = this.translate.getBrowserLang();
    //this.translate.use(browserLang.match(/EN|ET|RU/) ? browserLang : 'EN');
    // this.translate.use('ru')
    // this.translate.use('en')
    // this.translate.use('et')
      this.router.events.subscribe((evt) => {
        if (!(evt instanceof NavigationEnd)) {
          return;
        }
        window.scrollTo(0, 0)
      });
    if ((window.location.href.indexOf('talents.fund') == -1 && window.location.href.indexOf('wms-dev.xyz') == -1)) {
      setTimeout(function () {
        if (!this.uS.profile)
          this.loading = true;
      }.bind(this), 2000)
    }
  }


  onDeactivate() {
    document.body.scrollTop = 0;
    // Alternatively, you can scroll to top by using this other call:
    // window.scrollTo(0, 0)
  }

  logout() {
    this.authenticationService.logout()
  }

  loginUser() {
    this.message = <Message>{

      data: {
        email:    this.email,
        password: this.password
      }
    }
    console.log('new message from client to websocket: ', this.message);
    this.dataService.send('Authentication/auth', this.message.data);
    this.loading = true;
    // this.authenticationService.login(this.model.username, this.model.password)
    //     .subscribe(
    //         data => {
    //             this.router.navigate([this.returnUrl]);
    //         },
    //         error => {
    //             this.alertService.error(error);
    //             this.loading = false;
    //         });
  }
}
