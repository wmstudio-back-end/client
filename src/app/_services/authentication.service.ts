﻿import {Injectable}       from "@angular/core";
import "rxjs/add/operator/map";
import {CookieService}    from "ngx-cookie";
import {Router}           from "@angular/router";
import {UserService}      from "./user.service";
import {DataService}      from "./data.service";
import {TranslateService} from "@ngx-translate/core";
import {DownPopupService} from "./down-popup.service";
import {HelpersService}   from "./helpers.service";
import {Notify}           from "../_models/notify";
import {promise}          from "selenium-webdriver";
import setDefaultFlow = promise.setDefaultFlow;

@Injectable()
export class AuthenticationService {
  public auth: boolean = false;
  private cookieTime
  private message      = {}
         token: string

  constructor(private router: Router,
              private cookieService: CookieService,
              public user: UserService,
              private help: HelpersService,
              public dataService: DataService,
              private popS: DownPopupService,
              private translate: TranslateService) {
    this.message = this.dataService.messages["Authentication/identify"].subscribe(this.login.bind(this));
    this.message = this.dataService.messages["Authentication/auth"].subscribe(this.login.bind(this))
    this.message = this.dataService.messages["Authentication/reg"].subscribe(this.login.bind(this))
    this.message = this.dataService.messages["Authentication/signout"].subscribe(this.logout.bind(this))
  }

  getToken() {
    return this.token;
  }

  checkLogin() {
    if (this.auth) {
      console.log("token", this.cookieService.get('token'))
      return true
    }
    return false
  }

  login(data: any) {


    if (!data) {
      return;
    }


    if (data.hasOwnProperty('options')) {
      this.dataService.options           = Object.assign({}, data.options)
      this.dataService.options['status'] = [
        {
          class: 'active',
          text:  'Active'
        },
        {
          class: 'suspended',
          text:  'Suspended'
        },
        {
          class: 'invited',
          text:  'Invited'
        }
      ]


      setTimeout(function () {
        this.translate.setTranslation("en", data.options.translate.EN, true);
        this.translate.setTranslation("et", data.options.translate.ET, true);
        this.translate.setTranslation("ru", data.options.translate.RU, true);

      }.bind(this), 500)
      for (var key in data.options.search) {
        this.dataService.options[key] = []

        for (var key2 in data.options.search[key]) {
          if (typeof data.options.search[key][key2] === 'string') {
            this.dataService.options[key].push({
              id:   parseInt(key2),
              text: data.options.search[key][key2]
            })
          }
        }
      }

      for (var key in data.options.withoutTrasnslate) {
        this.dataService.options[key] = []

        for (var key2 in data.options.withoutTrasnslate[key]) {
          this.dataService.options[key].push({
            id:   parseInt(key2),
            text: data.options.withoutTrasnslate[key][key2]
          })
          //console.log(key,data.options.translate.en[key][key2])
        }
      }
      for (var key in data.options.translate.EN) {

        if (!this.dataService.options.hasOwnProperty(key)) {
          this.dataService.options[key] = []

          for (var key2 in data.options.translate.EN[key]) {

            var numb = (isNaN(parseInt(key2)) ? key2 : parseInt(key2))
            var t    = {
              id:   ~~key2,
              text: key + '.' + key2
            }

            this.dataService.options[key].push(t)
            //console.log(key,data.options.translate.en[key][key2])
          }


        } else {
          for (var key2 in data.options.translate.EN[key]) {

            for (var i = 0; i < this.dataService.options[key].length; i++) {
              if (this.dataService.options[key][i].id == key2) {
                this.dataService.options[key][i].text = key + '.' + this.dataService.options[key][i].id
                break
              }

            }
          }
        }


      }
      console.log('login', this.dataService.options)

    }

    if (data.hasOwnProperty('packages')) {
      this.dataService.packages         = data.packages
      this.dataService.options.packages = []
      for (var i = 0; i < this.dataService.packages.length; i++) {
        this.dataService.options.packages.push({
          id:   this.dataService.packages[i]._id,
          text: this.dataService.packages[i].name
        })
      }
    }


    console.log('3 start', data.hasOwnProperty("profile") && data.profile.hasOwnProperty("_id") && data.hasOwnProperty('options'))
    if (data.hasOwnProperty("profile") && data.profile.hasOwnProperty("_id") && data.hasOwnProperty('options')) {


      this.dataService.translaterInit()
      this.auth  = true
      this.token = data.hasOwnProperty('token') ? data.token : this.cookieService.get('token');
      this.listenCookie()
      this.user.setProfile(data.profile)
      if (this.user.profile.notification) {
        for (var i = 0; i < this.user.profile.notification.length; i++) {
          var notInd = this.help.findObject(this.dataService.options.notifications, 'pkey_id', this.user.profile.notification[i].options_notification)
          var not    = this.dataService.options.notifications[notInd]


          this.popS.pops.push(<Notify>{
            type:       not['icon'],
            _id:        this.user.profile.notification[i]._id,
            text:       'notification.' + not['pkey_id'],
            buttonUrl:  not['url'],
            buttonName: 'btn_notification.' + not['pkey_id'],
            show:       false
          })
          //console.log('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!',not['icon'])
          var n = this.dataService.options.notifications[notInd]


        }
      }


      // if (this.router.url == '/main') {
      //     this.router.navigate(['account']);
      // } else if (this.router.url == '/') {
      //     console.log('account');
      //     this.router.navigate(['account']);
      // } else {
      //     console.log('this.router.url', this.router.url)
      //     this.router.navigate([this.router.url]);
      // }
      // var t =
      //   `method/
      // ${setDefaultFloe}
      // /methodName`

      // this.router.navigate(['/My-resume-views-statistics']);
      // this.router.navigate([this.router.url]);
    }
    if (data.profile == null) {
      console.log("NOT PROFILE")
      this.cookieService.removeAll()
      if (window.location.href.indexOf('account.talents.fund') > -1) {
        window.location.href = '//talents.fund/logout';
        return false;
      } else if (window.location.href.indexOf('account.wms-dev.xyz') > -1) {
        window.location.href = '//wms-dev.xyz/logout';
        return false;
      }

      // this.router.navigate(['login']);
      //this.auth = false
      // window.location.href = '/login';
    }
    console.log('3 end login', this.auth)
  }


  public logout() {
    this.cookieService.removeAll()
    this.auth = false
    if (window.location.href.indexOf('account.talents.fund') > -1) {
      window.location.href = '//talents.fund/logout';
      return false;
    } else if (window.location.href.indexOf('account.wms-dev.xyz') > -1) {
      window.location.href = '//wms-dev.xyz/logout';
      return false;
    }
    this.router.navigate(['main']);
    window.location.href = '/';
    return false;
  }

  listenCookie() {
    this.cookieService.put('token', this.token, {
      expires: new Date(new Date().getTime() + 60 * 60 * 2 * 1000),
      path:    '/'
    })
    this.cookieTime = setInterval(function () {
      if (!this.cookieService.get('token')) {
        clearInterval(this.cookieTime);
        this.logout();
      }
    }.bind(this), 1000 * 30)
  }
}


