import {
  EventEmitter,
  Injectable
}                               from "@angular/core";
import {User}                   from "../_models/user";
import {DataService}            from "./data.service";
import {UserService}            from "./user.service";
import {DatePipe}               from "@angular/common";
import {HelpersService}         from "./helpers.service";
import {Router}                 from "@angular/router";
import {IResume}                from "../_models/resume";
import {Options}                from "../_models/resumeOptions";
import {IRequestRecommendation} from "app/_models/request-recommendation";
import {TranslateService}       from "@ngx-translate/core";
import {DownPopupService}       from "./down-popup.service";

export interface SaveSearch {
  _id?: string
  uid?: string
  name: string
  period: number
  project?: number
  alertEmail: boolean
  options?: Options
  filter?: any
  count: number
  query?: any
  discovered?: number
}

export interface Filter {
  query: {
    user: any,
    resume: any,
    recommendation: any,
    location: any,
    workExp: any,
    school: any
  }
  skip: number
  limit: number
  sort: any
  items: fItems[]
  options: Options
}

export interface fItems {
  key: string
  operand: string
  values: any
  ikey: string
}

@Injectable()
export class ResumeService {
  resumes: IResume[]    = []
  fullkeysResume        = 52
  occupationField       = []
  resumeType            = []
  sort: any
  rEmit: any            = new EventEmitter<IResume[]>();
  uEmit: any            = new EventEmitter<User[]>();
  resume: any           = new EventEmitter();
  users: any            = new EventEmitter();
  resumesId: any        = new EventEmitter();
  afterFilter: any      = new EventEmitter();
  filter: Filter
  lastChange: string    = ''
  changeFilterElem: any;
  countItems: number    = 0;
  timer: any            = null;
  filterV               = {}
  sortV                 = {}
  skip                  = 0;
  limit                 = 0;
  job: boolean          = false
  intern: boolean       = false
  tipCountTimer: any;
  tipElem: any;
  clear: boolean        = false;
  keyWordsFields: any   = {}
  lastChangeOption: any = {}
  saveSearch: SaveSearch
  viewItems: any        = []
  keywordsFields        = {
    text: {
      1: [
        'objective',
        'schools.schoolName',
        'schools.speciality',
        'accomplishmentsInformation',
        // 'workExp.location',
      ],
      2: [
        'internshipOptions.possible_internship',
        'internshipOptions.locations',
        'jobOptions.possible_position',
        'internshipOptions.additonalInformation',
        'jobOptions.additonalInformation',
        'jobOptions.locations.addr',
      ],
      3: [
        'workExp.companyName',
        'workExp.position',
        'workExp.jobDesciprion',
        'workExp.accomplishmentsiInformation',
        'informationAboutComputerSkills',
        'certificationsAndLicences.name',
        'certificationsAndLicences.issuer',
        'otherSoftware.otherSoft'
      ],
      4: [
        'participationInCommunities',
        'idditonalInfo',
        'interestsAndHobbies',
        'yourMainValues',
        'characterTraits.text',
        'strenghs.text',
        'additional',
      ],
    },
    options: {
      1: {
        majors: [
          'schools.major',
          'schools.minor'
        ]
      },
      2: {
        occupationField: [
          'internshipOptions.occupationField',
          'jobOptions.occupationField'
        ],
        fieldActivity: 'jobOptions.fieldActivity',
      },
      3: {
        workExp: 'workExp.fieldOfActivity',
        drivingCategory: 'drivingLicense.category'
      },
      4: {},

    },
    location:{},
    software: {
      1: ['officeTools.officeTool'],
      2: [],
      3: [],
      4: [],
    },
    skills: {
      1: [
        'pickedSkills',
      ],
      2: [],
      3: [
        'otherSpecificSkills'
      ],
      4: [],
    }
  }

  constructor(private dataService: DataService,
              private userService: UserService,
              public help: HelpersService,
              private router: Router,
              private popS: DownPopupService,
              private translate: TranslateService) {

    this.dataService.messages["EVENT/verifyProgressEducation"].subscribe(this.verifyProgressEducation.bind(this))
    this.filter         = {
      skip: 0,
      query: {
        user: {},
        resume: {},
        recommendation: {},
        school: {},
        workExp: {},
        location: {}
      },
      limit: 8,
      sort: {},
      items: [],
      options: {
        whereToSearch: {0: true},
        interestedIn: {0: true},
        sex: {
          0: false,
          1: false
        },
        ageFrom: '',
        ageTo: '',
        countItems: {},
        onlyVerified: false,
        fieldActivity: {},
        languages: {},
        expectedGraduationMonth: {},
        expectedGraduationYear: {},
        educationLevel: {},
        majors: {},
        occupationField: {},
        industry: {},
        typeOfVacancy: {},
        statusForEmployment: {},
        address: 'Estonia',
        city: '',
        keywords: '',
        workTime: {},
        languagesSkills: {},
        previosWork: false,
        recommendations: false,
        profileScore: -1,
        avgProgressEdu: {},
        vacancyType: {},
        workExp: false
      }
    }
    this.saveSearch     = {
      name: '',
      period: 0,
      alertEmail: true,
      count: 0
    }
    this.keyWordsFields = {
      fieldActivity: this.dataService.options.fieldActivity,
      occupationField: this.dataService.options.occupationField,
      // 'keySkills.text':true,
    }
  }
  verifyProgressEducation(data){
    for (var i = 0; i < this.userService.profile.Student.schools.length; i++) {
      if (data._id==this.userService.profile.Student.schools[i]._id){
        this.userService.profile.Student.schools[i] = data
      }

    }
  }
  setViweResume(_id) {

    this.dataService.sendws("Events/setEvent", {event: {name: 'viweResume', option: {_id: _id}}})
      .then((data) => {

      })
    if (this.userService.profile.type_profile == 'Company') {
      for (var i = 0; i < this.userService.profile.subscribeCandidate.length; i++) {
        if (this.userService.profile.subscribeCandidate[i].resumeId == _id) {
          this.userService.profile.subscribeCandidate[i].isview = 1
          this.dataService.sendws("Vacancy/setViewSubscritionVacancy", {resumeId: _id})
            .then((data) => {

            })
        }

      }
    }
  }

  setSaveSearch(id?:string) {
    if (!id){
      this.saveSearch     = {
        name: '',
        period: 0,
        alertEmail: true,
        count: 0
      }

      return;
    }
    if (!this.userService.profile.savedSearches.length) return;
    let obj = this.userService.profile.savedSearches.find((e, i, a) => {
      return (e._id == id) ? true : false
    });
    if (!obj) return;
    obj.filter          = (typeof obj.filter === "string") ? JSON.parse(obj.filter) : obj.filter;
    obj.query           = (typeof obj.query === "string") ? JSON.parse(obj.query) : obj.query;
    this.saveSearch     = obj
    this.filter.options = obj.options
    this.filter.items   = obj.filter
    this.filter.query   = obj.query
  }

  // Filter search functions


  clearFilter() {
    this.filter.options = {
      whereToSearch: {0: true},
      interestedIn: {0: true},
      sex: {
        0: false,
        1: false
      },
      ageFrom: '',
      ageTo: '',
      countItems: {},
      onlyVerified: false,
      fieldActivity: {},
      languages: {},
      expectedGraduationMonth: {},
      expectedGraduationYear: {},
      educationLevel: {},
      majors: {},
      occupationField: {},
      industry: {},
      typeOfVacancy: {},
      statusForEmployment: {},
      address: '',
      city: '',
      keywords: '',
      workTime: {},
      languagesSkills: {},
      previosWork: false,
      recommendations: false,
      profileScore: -1,
      avgProgressEdu: {},
      vacancyType: {},
      workExp: false
    }
    this.clear          = !this.clear
    console.log('clear filter', this.clear)
    this.removeTip()
    this.filter.query   = {
      user: {},
      resume: {},
      recommendation: {},
      school: {},
      workExp: {},
      location: {}
    }
    this.filter.items   = []
    this.job            = true
    this.intern         = true
    this.getResume()
    this.saveSearch = {
      name: '',
      period: 0,
      alertEmail: false,
      count: 0
    }
  }

  setFilterOption(key, val) {
    this.filter.options[key] = val
    this.lastChangeOption    = {}
    this.lastChangeOption[0] = key
    // console.log(this.lastChangeOption)
    // console.log(key, val)

    //keywords search are dependencing from where to search field
    if (key == 'whereToSearch')
      this.setKeyWords(this.filter.options.keywords)

    if (key == 'interestedIn') {
      let vals = []
      for (let i of Object.keys(val)) {
        if (val[i])
          vals.push(i)
      }
      if (vals.indexOf('2') != -1) {
        this.job = true
      } else this.job = false
      if (vals.indexOf('1') != -1) {
        this.intern = true
      } else this.intern = false
      if (vals.indexOf('0') != -1 || !vals.length) {
        this.job    = true
        this.intern = true
        return
      }
      // console.log('job - ' + this.job, 'intern - ' + this.intern)
    }
  }

  getQuery(val) {
    let items = this.filter.items
    if (items.length == 0) {
      this.filter.query = {
        user: {},
        resume: {},
        recommendation: {},
        school: {},
        workExp: {},
        location: {}
      }
      return;
    }
    for (let i in items) {

    }
    var i = []
    for (var k in val) {
      i.push(val[k].id)
    }
  }

  setQueryItem(val) {
    let values
    let unset: boolean = false
    if (val.key == '$and$or' || val.key == '$text') {
      this.setFilter(val.key, val.values, val.ikey, val.operand)
      return;
    }
    switch (typeof val.values) {
      case 'string':
        values = val.values
        break;
      case 'number':
        values = val.values
        break;
      case 'object':
        if (Array.isArray(val.values)) {
          if (val.values.length == 0) {
            this.unserFilter(val.key, val.ikey, val.operand);
            unset = true;
          } else {
            values = []
            for (var k = 0; k < val.values.length; k++) {
              if (typeof val.values[k].id !== "undefined")
                values.push(val.values[k].id)
              else values.push(val.values[k])
            }
          }
        } else {
          if (typeof val.values['id'] !== "undefined")
            values = val.values.id
          else {
            this.unserFilter(val.key, val.ikey, val.operand)
            unset = true
          }
        }
        break;
      default:
        this.unserFilter(val.key, val.ikey, val.operand)
        unset = true
        break;
    }
    if (!unset)
      this.setFilter(val.key, values, val.ikey, val.operand)
  }

  /**
   *
   * @param key Field
   * @param val Field value
   * @param i Collection
   * @param operand
   * @param count Need get count results
   */
  setFilter(key: string, val, i: string, operand?: string, count?: boolean) {
    switch (operand) {
      case '$in':
        // console.log(key, val)
        this.filter.query[i][key] = {'$in': val}
        break;
      case '$all':
        // console.log(key, val)
        this.filter.query[i][key] = {'$all': val}
        break;
      case '$and':
        this.setFilterAnd(key, val, i)
        count = true
        break;
      case '$or':
        this.setFilterAnd(key, val, i, true, true)
        break;
      case '$gte':
        if (typeof this.filter.query[i][key] !== "undefined")
          this.filter.query[i][key]['$gte'] = val
        else
          this.filter.query[i][key] = {'$gte': val}
        break;
      case '$lte':
        if (typeof this.filter.query[i][key] !== "undefined")
          this.filter.query[i][key]['$lte'] = val
        else
          this.filter.query[i][key] = {'$lte': val}
        break;
      case '$text':
        this.filter.query[i][key] = val
        break;
      case '=':
        this.filter.query[i][key] = val
        break;
      case '$and$or':
        this.setFilterAnd('$or', val, i)
        break;
      default:
        this.filter.query[i][key] = val
        break;
    }
    // console.log('filter options',this.filter.options)
    // console.log('query',this.filter.query)
    if (!count) this.getCountResumes(true)
  }

  setFilterAnd(key: string, val, ikey: string, or?: boolean, count?: boolean) {
    let op = (or) ? '$or' : '$and'
    if (typeof this.filter.query[ikey][op] === "undefined")
      this.filter.query[ikey][op] = [];
    else {
      let i = this.filter.query[ikey][op].findIndex((e, i, a) => {
        return (Object.keys(e)[0] == key) ? true : false
      })
      if (i > -1) this.filter.query[ikey][op].splice(i, 1)
    }
    let obj  = {}
    obj[key] = val
    this.filter.query[ikey][op].push(obj);
    if (!count) this.getCountResumes(true)
  }

  unserFilter(key: string, i: string, operand?: string, count?: boolean) {
    switch (operand) {
      case '$and':
        if (typeof this.filter.query[i]['$and'] === "undefined") break;
        let index = this.filter.query[i]['$and'].findIndex((e, i, a) => {
          return (Object.keys(e)[0] == key) ? true : false
        })
        if (index > -1) this.filter.query[i]['$and'].splice(index, 1)
        if (!Object.keys(this.filter.query[i]['$and']).length)
          delete this.filter.query[i]['$and']
        break;
      case '$or':
        if (typeof this.filter.query[i]['$or'] === "undefined") break;
        let indexOr = this.filter.query[i]['$or'].findIndex((e, i, a) => {
          return (Object.keys(e)[0] == key) ? true : false
        })
        if (indexOr > -1) this.filter.query[i]['$or'].splice(indexOr, 1)
        if (!Object.keys(this.filter.query[i]['$or']).length)
          delete this.filter.query[i]['$or']
        break;
      case '$and$or':
        let indexAO = this.filter.query[i]['$and']
        if (indexAO)
          indexAO.findIndex((e, i, a) => {
            return (Object.keys(e)[0] == key) ? true : false
          })
        else break;
        if (indexAO > -1) delete this.filter.query[i]['$and'][indexAO]
        break;
      case '$gte':
        if (typeof this.filter.query[i][key] !== "undefined") {
          if (this.filter.query[i][key].length == 1) delete this.filter.query[i][key]
          else delete this.filter.query[i][key]['$gte']
          if (!Object.keys(this.filter.query[i][key]).length)
            delete this.filter.query[i][key]
        }
        break;
      case '$lte':
        if (typeof this.filter.query[i][key] !== "undefined") {
          if (this.filter.query[i][key].length == 1) delete this.filter.query[i][key]
          else delete this.filter.query[i][key]['$lte']
          if (!Object.keys(this.filter.query[i][key]).length)
            delete this.filter.query[i][key]
        }
        break;
      default:
        if (typeof this.filter.query[i][key] !== "undefined")
          delete this.filter.query[i][key]
        break;
    }
    if (!count) this.getCountResumes()
  }

  setSort(key: string, val) {
    this.filter.sort[key] = val
  }

  sortSchool(a, b) {
    if (a.hasOwnProperty('endDateYear') &&
      a.hasOwnProperty('endDateMonth') &&
      b.hasOwnProperty('endDateYear') &&
      b.hasOwnProperty('endDateMonth') &&
      a.endDateYear &&
      a.endDateMonth &&
      b.endDateYear &&
      b.endDateMonth
    ) {
      if (a.endDateYear > b.endDateYear) return -1;
      if (a.endDateYear < b.endDateYear) return 1;
      a.endDateMonth = parseInt(a.endDateMonth)
      b.endDateMonth = parseInt(b.endDateMonth)
      if (a.endDateMonth > b.endDateMonth) return -1;
      if (a.endDateMonth < b.endDateMonth) return 1;
      return 0;
    }
    if (a.hasOwnProperty('expectedGraduationYear') &&
      a.hasOwnProperty('expectedGraduationMonth') &&
      b.hasOwnProperty('expectedGraduationYear') &&
      b.hasOwnProperty('expectedGraduationMonth')
    ) {
      if (a.expectedGraduationYear > b.expectedGraduationYear) return -1;
      if (a.expectedGraduationYear < b.expectedGraduationYear) return 1;
      a.expectedGraduationMonth = parseInt(a.expectedGraduationMonth)
      b.expectedGraduationMonth = parseInt(b.expectedGraduationMonth)
      if (a.expectedGraduationMonth > b.expectedGraduationMonth) return -1;
      if (a.expectedGraduationMonth < b.expectedGraduationMonth) return 1;
      return 0;
    }
    if (a.hasOwnProperty('expectedGraduationMonth'))
      return -1;
    else return 1;
  }

  // Request and render result

  showInputCount(parent) {
    let tipElem        = document.createElement('p')
    tipElem.className  = 'checkbox__number'
    tipElem.innerHTML  = '' + this.countItems
    this.tipElem       = parent.appendChild(tipElem)
    this.tipCountTimer = setTimeout(() => {
      this.removeTip()
    }, 3000, this)

  }

  getCountResumes(elem?) {
    let __this = this
    clearTimeout(__this.timer)
    this.timer = setTimeout(function () {
      __this.removeTip()
      __this.dataService.sendws("Resume/getCountResume", {query: __this.filter.query})
        .then((data) => {
          if (typeof data['count'] !== "undefined" && __this.userService.accessPackage.search) {
            __this.countItems = parseInt(data['count'])
            if (!elem) return true;
            if (typeof __this.changeFilterElem === "undefined") return true;
            if (!__this.changeFilterElem.checked) return true;
            let label = __this.changeFilterElem.nextElementSibling;
            __this.showInputCount(label)
          }
        })
    }, 300, __this)
  }

  setResume(resumes: IResume[]) {
    let newResumesId = [];
    for (var i = 0; i < resumes.length; i++) {
      newResumesId.push(resumes[i]._id)
      if (this.help.findObject(this.resumes, '_id', resumes[i]._id) < 0) {
        this.resumes.push(resumes[i])
      } else {
        this.resumes[this.help.findObject(this.resumes, '_id', resumes[i]._id)] = resumes[i]
      }
    }
    this.resumesId.emit(newResumesId)
    // console.log('resumesId emit', newResumesId)
    //for (var a in resumes) {
    //
    //    this.resume[resumes[a]['_id']] = resumes[a]
    //}
    //this.rEmit.emit(this.resume)
  }

  getresumeFromId(id, type?) {

    return new Promise((resolve, reject) => {
      for (var i = 0; i < this.resumes.length; i++) {
        if (this.resumes[i]._id == id) {
          var resume = this.resumes[i]
          return resolve(this.getResumeData(resume))
        }
      }
      //this.resumes.push(<IResume>{_id:id})
      this.dataService.sendws("Resume/getResumeFromId", {_id: id})
        .then((data) => {
          if (data['resume']) {
            let resume: IResume = data['resume']
            this.resumes.push(resume)
            //this.resumes[this.help.findObject(this.resumes,'_id',id)]=resume
            if (type) {
              return resolve(this.prepareResume(resume))
            }
            else {
              return resolve(this.getResumeData(resume))
            }
          } else {
            reject('error')
          }
        })
    });
  }


  getResumeData(resume) {
    let e = resume
    // if (e.internshipOptions && e.internshipOptions.preferredPeriod) {
    //   resume.internshipOptions.preferredPeriod = this.preferredPeriod(e.internshipOptions.preferredPeriod)
    // }
    // if (e.jobOptions && e.jobOptions.preferredPeriod) {
    //   resume.jobOptions.preferredPeriod = this.preferredPeriod(e.jobOptions.preferredPeriod)
    // }


    return resume;
  }


  getResume() {


    this.viewItems = []
    return new Promise((resolve, reject) => {

      this.dataService.sendws("Resume/getResumes", {
        skip: this.filter.skip,
        limit: this.filter.limit,
        query: this.filter.query,
        sort: this.filter.sort
      })
        .then((data) => {

          if (data['count']==0){
            this.popS.show_success(this.dataService.getOptionsFromId('errors',33).text)
          }
          // console.log('88877777777777777777', data)
          this.countItems = data['count']
          this.setResume(data['resume'])
          this.afterFilter.emit(data['count']);
          resolve(data)
          //  this.prepareResumes(data)
        })
    })
  }

  reqResume(skip, limit) {
    this.dataService.sendws("Resume/getResume", {
      skip: skip,
      limit: limit,
      query: this.filter.query,
      sort: this.filter.sort
    })
      .then((data) => {
        this.setResume(data['resume'])
      })
  }

  setResumes(data) {
    data['resume'] = this.usersToResume(data['resume'])
    this.resume.emit(data)
    console.log('resumes', data)
  }

  removeTip() {
    if (typeof this.tipElem !== "undefined") {
      this.tipElem.remove()
      this.tipElem = undefined
    }
    clearTimeout(this.tipCountTimer)
  }

  // Save search function

  saveSearches(obj) {
    if (typeof this.saveSearch['_id'] !== "undefined")
      var id = this.saveSearch['_id']
    if (typeof this.saveSearch['uid'] !== "undefined")
      var uid = this.saveSearch['_id']
    this.saveSearch = obj
    if (typeof id !== "undefined") this.saveSearch._id = id
    if (typeof uid !== "undefined") this.saveSearch.uid = uid
    this.saveSearch.options = this.filter.options
    this.saveSearch.filter  = JSON.stringify(this.filter.items)
    this.saveSearch.count   = this.countItems
    this.saveSearch.query   = JSON.stringify(this.filter.query)
    this.dataService.sendws("User/saveSearch", this.saveSearch)
      .then((data) => {
        if (data.hasOwnProperty('ok') && data['ok']) {
          return this.userService.setSavedSearches(this.saveSearch)
        } else {
          // this.saveSearch.query  = this.filter.query
          // this.saveSearch    = data
          // this.saveSearch.filter = this.filter.items
          this.userService.setSavedSearches(data)
        }

      })
  }

  // Helpers fuctions

  private prepareResumes(data) {
    if (data.hasOwnProperty('resume')) {
      var usersId = [];
      let date    = new DatePipe('en-US');
      data['resume'].forEach((e, i, a) => {

        this.viewItems.push(false)
        data['resume'][i].created_at = date.transform(e.created_at * 1000, 'dd.MM.yyyy')
        data['resume'][i].deadLine   = date.transform(e.deadLine * 1000, 'dd.MM.yyyy')
        if (usersId.indexOf(e.uid) == -1)
          usersId.push(e.uid);
        if (data['resume'][i].workExp.length) {
          for (let iw = 0; iw < data['resume'][i].workExp.length; iw++) {
            let work      = data['resume'][i].workExp[iw];
            let endDate   = new Date()
            let startDate = new Date()
            if (work.startDateMonth || work.startDateMonth == 0) startDate.setMonth(work.startDateMonth + 1)
            if (work.startDateYear) startDate.setFullYear(work.startDateYear)
            if (!work.currentlyWorking) {
              if (work.endDateMonth || work.endDateMonth == 0) endDate.setMonth(parseInt(work.endDateMonth))
              if (work.endDateYear) endDate.setFullYear(work.endDateYear)
            }

            data['resume'][i].workExp[iw]['startDate'] = date.transform(startDate.getTime(), 'MM.yyyy')
            data['resume'][i].workExp[iw]['endDate']   = date.transform(endDate.getTime(), 'MM.yyyy')
            data['resume'][i].workExp[iw]['compDate']  = this.help.dateCompare(startDate, endDate)
          }
        }
        data['resume'][i].bookmark                            = this.userService.checkBookmark(e._id)
        data['resume'][i].rating                              = e.rating
        data['resume'][i].internshipOptions.preferredWorkTime = this.preferredWorkTime(e.internshipOptions.preferredWorkTime)
        data['resume'][i].internshipOptions.preferredPeriod   = this.preferredPeriod(e.internshipOptions.preferredPeriod)
        data['resume'][i].jobOptions.preferredPeriod          = this.preferredPeriod(e.jobOptions.preferredPeriod)
        data['resume'][i].jobOptions.preferredWorkTime        = this.preferredWorkTime(e.jobOptions.preferredWorkTime)
        data['resume'][i].jobOptions.jobType                  = this.jobAndInternshipType(e.jobOptions.jobType, true)
        data['resume'][i].internshipOptions.internshipType    = this.jobAndInternshipType(e.internshipOptions.internshipType)
        if (e.schools.length) {
          data['resume'][i].schools = e.schools = e.schools.sort(this.sortSchool)
          if (e.schools[0].hasOwnProperty('expectedGraduationYear') &&
            e.schools[0].hasOwnProperty('expectedGraduationMonth')) {
            if (e.schools[0].expectedGraduationYear) {
              if (!e.schools[0].expectedGraduationMonth)
                data['resume'][i].expGradDate = '' + e.schools[0].expectedGraduationYear
              else {
                data['resume'][i].expGradDate = e.schools[0].expectedGraduationMonth + '.' + e.schools[0].expectedGraduationYear
              }
            }
          }
        }
        data['resume'][i].showInternship = this.checkShowIntern(e)
        data['resume'][i].showJob        = this.checkShowJob(e)
        data['resume'][i].recommends     = this.getRecommends(e.recommends)
        this.resumes.push(data['resume'][i])
      });
      if (typeof data['count'] !== "undefined")
        this.countItems = parseInt(data['count'])
      if (usersId)
        this.userService.getUsers(usersId)
          .then((users) => {
            this.setResumes(data)
          })
      else {
        this.setResumes(data)
      }
    }
  }

  private getRecommends(data) {
    return data.filter((e, i, a) => {
      return (e.status = 1 && e.visibility) ? true : false;
    });
  }

  private checkShowIntern(item) {
    return (!item.internshipOptions.init && (item.internshipOptions.internshipType != "" || item.internshipOptions.preferredWorkTime.length)) ? true : false;
  }

  private checkShowJob(item) {
    return (item.jobOptions.jobType != "" || item.jobOptions.preferredWorkTime.length) ? true : false;
  }

  private prepareResume(resume) {
    // console.log('prepare resume')
    var usersId       = [];
    let date          = new DatePipe('en-US');
    let e             = resume
    resume.created_at = date.transform(e.created_at * 1000, 'dd.MM.yyyy')
    resume.deadLine   = date.transform(e.deadLine * 1000, 'dd.MM.yyyy')
    if (usersId.indexOf(e.uid) == -1)
      usersId.push(e.uid);
    resume.bookmark                            = this.userService.checkBookmark(e._id)
    resume.rating                              = e.rating
    resume.internshipOptions.preferredWorkTime = this.preferredWorkTime(e.internshipOptions.preferredWorkTime)
    resume.internshipOptions.preferredPeriod   = this.preferredPeriod(e.internshipOptions.preferredPeriod)
    resume.jobOptions.preferredPeriod          = this.preferredPeriod(e.jobOptions.preferredPeriod)
    resume.jobOptions.preferredWorkTime        = this.preferredWorkTime(e.jobOptions.preferredWorkTime)
    resume.jobOptions.jobType                  = this.jobAndInternshipType(e.jobOptions.jobType, true)
    resume.internshipOptions.internshipType    = this.jobAndInternshipType(e.internshipOptions.internshipType)
    if (resume.workExp) {
      for (let i = 0; i < resume.workExp.length; i++) {
        let work      = resume.workExp[i];
        let endDate   = new Date()
        let startDate = new Date()
        if (work.startDateMonth || work.startDateMonth == 0) startDate.setMonth(work.startDateMonth + 1)
        if (work.startDateYear) startDate.setFullYear(work.startDateYear)
        if (!work.currentlyWorking) {
          if (work.endDateMonth || work.endDateMonth == 0) endDate.setMonth(parseInt(work.endDateMonth))
          if (work.endDateYear) endDate.setFullYear(work.endDateYear)
        }
        resume.workExp[i]['startDate'] = date.transform(startDate.getTime(), 'MM.yyyy')
        resume.workExp[i]['endDate']   = date.transform(endDate.getTime(), 'MM.yyyy')
        resume.workExp[i]['compDate']  = this.help.dateCompare(startDate, endDate)
      }
    }
    if (e.schools) {
      resume.schools = e.schools = e.schools.sort(this.sortSchool)
      if (e.schools[0].hasOwnProperty('expectedGraduationYear') &&
        e.schools[0].hasOwnProperty('expectedGraduationMonth')) {
        if (e.schools[0].expectedGraduationYear) {
          if (!e.schools[0].expectedGraduationMonth)
            resume.expGradDate = '' + e.schools[0].expectedGraduationYear
          else {
            resume.expGradDate = e.schools[0].expectedGraduationMonth + '.' + e.schools[0].expectedGraduationYear
          }
        }
      }
    }
    if (e.uploadFiles) {
      for (let i = 0; i < e.uploadFiles.length; i++) {
        let file              = e.uploadFiles[i]
        file.path             = this.help.getStaticUrl(file.path)
        resume.uploadFiles[i] = file
      }
    }
    if (usersId.indexOf(resume.uid) == -1)
      usersId.push(resume.uid);
    if (resume.recommends) {
      for (let i = 0; i < resume.recommends.length; i++) {
        let rec: IRequestRecommendation = resume.recommends[i]
        if (rec.req_id)
          usersId.push(rec.req_id)
        if (rec.educatorId)
          usersId.push(rec.educatorId)
      }
    }

    let data = {resume: [resume]}
    if (usersId)
      return new Promise((resolve, reject) => {
        this.userService.getUsers(usersId)
          .then((users) => {
            return resolve(this.usersToResume(data['resume']))
          })
      })
    else {
      return this.usersToResume(data['resume'])
    }
  }

  private getUser(user) {
    let date = new DatePipe('en-US');
    if (typeof user['birthday'] !== "undefined")
      user['old'] = (new Date()).getFullYear() - (new Date(user.birthday * 1000)).getFullYear()
    return user
  }

  private usersToResume(resumes) {
    for (let i = 0; i < resumes.length; i++) {
      resumes[i] = this.userToResume(resumes[i])
      resumes[i] = this.getRecommendsUser(resumes[i])
    }
    return resumes
  }

  private userToResume(resume) {
    if (this.userService.users.hasOwnProperty(resume.uid))
      resume.user = this.getUser(this.userService.users[resume.uid])
    return resume;
  }

  private getRecommendsUser(resume) {
    if (!resume.recommends) return resume;
    for (let i = 0; i < resume.recommends.length; i++) {
      let recomend: IRequestRecommendation = resume.recommends[i],
          user_id                          = null,
          schoolI,
          workExpI
      if (recomend.type == 0) {
        recomend.typeName = 'Educator'
        if (recomend.hasOwnProperty('educatorId'))
          user_id = recomend.educatorId
        if (recomend.hasOwnProperty('schoolId') && (schoolI = resume.schools.findIndex((e, i, a) => {
          return (e._id == recomend.schoolId) ? true : false
        })) !== -1)
          recomend.name = resume.schools[schoolI].schoolName

      }
      if (recomend.type == 1) {
        recomend.typeName = 'Organisation'
        if (recomend.hasOwnProperty('workExpId') && (workExpI = resume.workExp.findIndex((e, i, a) => {
          return (e._id == recomend.workExpId) ? true : false
        })) !== -1)
          recomend.answerer = resume.workExp[workExpI].companyName
      }
      if (recomend.type == 2) {
        if (recomend.hasOwnProperty('relation') && recomend.relation >= 0)
          recomend.typeName = this.dataService.optionText('Relation', recomend.relation)
      }
      if (recomend.hasOwnProperty('req_id'))
        user_id = recomend.req_id
      resume.recommends[i] = recomend
      if (user_id == null || !this.userService.users.hasOwnProperty(user_id)) continue
      let user                      = this.userService.users[user_id]
      resume.recommends[i].answerer = user.first_name + ' ' + user.last_name
      resume.recommends[i].user     = user
    }

    return resume
  }

  preferredPeriod(e) {
    let date        = new DatePipe('en-US');
    let period: any = {}
    if (!e) return null;
    if (e.hasOwnProperty('start'))
      period['start'] = date.transform(e.start * 1000, 'dd.MM.yyyy')
    else if (e.hasOwnProperty('end'))
      period['start'] = date.transform((new Date()).getTime(), 'dd.MM.yyyy')
    if (e.hasOwnProperty('end'))
      period['end'] = date.transform(e.end * 1000, 'dd.MM.yyyy')
    if (!e.hasOwnProperty('start') && !e.hasOwnProperty('end')) return null;
    return period;
  }

  jobAndInternshipType(data, job?: boolean) {
    let text: string = '';
    let word: string = 'job';
    let options: any = this.dataService.options.jobType;
    let keys: any    = Object.keys(data)
    if (!job) {
      word    = 'internship'
      options = this.dataService.options.internshipType
    }
    for (let i of keys) {
      if (data[keys[i]]!=undefined) {
        if (text) text += ', '
        var str = ''
        options[keys[i]] ? str = this.help.getTranslate(options[keys[i]].text) : null;
        text += this.help.ucFirst(str.replace(word, ''))
      }
    }
    return text
  }

  preferredWorkTime(workTime) {
    let times: any = []
    for (let key in workTime) {
      if (workTime[key]) times.push({id: key})
    }
    return times
  }

  getArrayItemByKey(key) {
    let array = this.filter.items
    let i     = array.findIndex((e, i, a) => {
      return (e.key == key) ? true : false;
    })
    return (i > -1) ? array[i] : {};
  }

  unsetFilterItem(key) {
    let array = this.filter.items
    let i     = array.findIndex((e, i, a) => {
      return (e.key == key) ? true : false;
    })
    if (i > -1) this.filter.items.splice(i, 1)
  }

  setFilterItem(val) {
    let array = this.filter.items
    // console.log(array)
    let i     = array.findIndex((e, i, a) => {
      return (e.key == val.key) ? true : false;
    })
    if (i > -1) array[i] = val
    else array.push(val)
    this.filter.items = array
    // console.log(this.filter.items)
    this.setQueryItem(val)
  }

  setKeyWords(val) {
    if (!val) {
      this.unserFilter('$text', 'resume', '$text')
      return;
    }
    let values       = [];
    val              = val.replace('/\s/g', '===')
      .replace('/[,.]/g', '===')
      .split('===')
      .filter((e, i, a) => {
        return (e) ? true : false;
      })
    /**
     * regex for mongo
     * @type {string}
     */
    let text: string = '.*';
    /**
     * regex for options
     * @type {string}
     */
    let regText      = '';

    for (let i = 0; i < val.length; i++) {
      text += (i == 0) ? '' : '.';
      regText += (i == 0) ? '' : '|';
      text += val[i] + '.*';
      regText += val[i];
    }
    this.unsetFilterItem('$text')
    if (val.length) {
      let search = [];
      this.whereToSearch()
        .forEach((e) => {

          // text
          for (let i = 0; i < this.keywordsFields.text[e].length; i++) {
            let obj                             = {}
            obj[this.keywordsFields.text[e][i]] = {$regex: text};
            search.push(obj);
          }

          //options
          let keys = Object.keys(this.keywordsFields.options[e]);

          for (let i = 0; i < keys.length; i++) {
            if (typeof this.keywordsFields.options[e][keys[i]] == 'string') {
              let obj = {}
              var res = this.dataService.getOptionId(keys[i], regText);
              if (res.length) {
                obj[this.keywordsFields.options[e][keys[i]]] = {$in: res};
                search.push(obj);
              }
            } else {
              var res = this.dataService.getOptionId(keys[i], regText);
              for (let k = 0; k < this.keywordsFields.options[e][keys[i]].length; k++) {
                let obj = {}
                if (res.length) {
                  obj[this.keywordsFields.options[e][keys[i]][k]] = {$in: res};
                  search.push(obj);
                }
              }
            }
          }

          //skills
          for (let i = 0; i < this.keywordsFields.skills[e].length; i++) {
            let obj                               = {}
            obj[this.keywordsFields.skills[e][i]] = {new: true, $regex: text};
            search.push(obj);

            obj     = {}
            var res = this.dataService.getOptionId('otherSpecificSkills', regText);
            if (res.length)
              obj[this.keywordsFields.skills[e][i] + '.id'] = {$in: res};
            search.push(obj);
          }

        })
      this.setFilterItem(this.getFilterItem('$text', '$text', {$or: search}, 'resume'))
    }
    console.log(val)
  }

  getFilterItem(key: string, op: string, val, i: string) {
    return {
      key: key,
      operand: op,
      values: val,
      ikey: i
    }
  }

  /**
   * @returns {Array}
   */
  private whereToSearch() {
    let __this = this
    let all    = [
      1,
      2,
      3,
      4
    ]
    let items  = all.filter(e => {
      return __this.filter.options.whereToSearch[e];
    })
    return items.length ? items : all
  }

  public setVerify(event) {
    if (event.target.checked)
      this.setFilterItem(this.getFilterItem('proof', '=', 1, 'user'))
    else
      this.unserFilter('proof', 'user')
  }
}
