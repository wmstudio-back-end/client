import {Injectable}                       from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot
}                                         from "@angular/router";
import {UserService}                      from "./user.service";
import {UserDataStudentComponent}         from "../components/account/userdata/Student/userdata.component";
import {UserDataOrganizationComponent}    from "../components/account/userdata/Organization/userdata.component";
import {UserDataCompanyComponent}         from "../components/account/userdata/Company/userdata.component";
import {UserDataAdminComponent}           from "../components/account/userdata/Admin/userdata.component";
import {UserDataEducatorComponent}        from "../components/account/userdata/Educator/userdata.component";
import {SettingStudentComponent}          from "../components/account/setting/Student/student.component";
import {SettingOrganizationComponent}     from "../components/account/setting/Organization/setting.component";
import {SettingCompanyComponent}          from "../components/account/setting/Company/company.component";
import {SettingAdminComponent}            from "../components/account/setting/Admin/admin.component";
import {SettingEducatorComponent}         from "../components/account/setting/Educator/educator.component";
import {ProfileStudentComponent}          from "../components/account/profile/Student/student.component";
import {ProfileOrganizationComponent}     from "../components/account/profile/Organization/profile.component";
import {ProfileCompanyComponent}          from "../components/account/profile/Company/company.component";
import {ProfileAdminComponent}            from "../components/account/profile/Admin/admin.component";
import {ProfileEducatorComponent}         from "../components/account/profile/Educator/educator.component";
import {SidebarLinkStudentComponent}      from "../components/sidebar/sidebar-link/sidebar-link-student/sidebar-link-student.component";
import {SidebarLinkOrganizationComponent} from "../components/sidebar/sidebar-link/sidebar-link-organization/sidebar-link-organization.component";
import {SidebarLinkCompanyComponent}      from "../components/sidebar/sidebar-link/sidebar-link-company/sidebar-link-company.component";
import {SidebarLinkAdminComponent}        from "../components/sidebar/sidebar-link/sidebar-link-admin/sidebar-link-admin.component";
import {SidebarLinkEducatorComponent}     from "../components/sidebar/sidebar-link/sidebar-link-educator/sidebar-link-educator.component";

@Injectable()
export class ComponentService {
  componentSidebarLink = null;
  componentProfile     = null;
  componentUserData    = null
  componentSetting     = null
  topFilter            = false

  constructor(private router: Router, public user: UserService) {
    this.canActivate2()
  }

  UserData    = {
    Student:      {
      component: UserDataStudentComponent,
      inputs:    {
        showNum: 9
      }
    },
    Organization: {
      component: UserDataOrganizationComponent,
      inputs:    {
        showNum: 9
      }
    },
    Company:      {
      component: UserDataCompanyComponent,
      inputs:    {
        showNum: 9
      }
    },
    Admin:        {
      component: UserDataAdminComponent,
      inputs:    {
        showNum: 9
      }
    },
    Educator:     {
      component: UserDataEducatorComponent,
      inputs:    {
        showNum: 9
      }
    }
  }
  Setting     = {
    Student:      {
      component: SettingStudentComponent,
      inputs:    {
        showNum: 9
      }
    },
    Organization: {
      component: SettingOrganizationComponent,
      inputs:    {
        showNum: 9
      }
    },
    Company:      {
      component: SettingCompanyComponent,
      inputs:    {
        showNum: 9
      }
    },
    Admin:        {
      component: SettingAdminComponent,
      inputs:    {
        showNum: 9
      }
    },
    Educator:     {
      component: SettingEducatorComponent,
      inputs:    {
        showNum: 9
      }
    }
  }
  Profile     = {
    Student:      {
      component: ProfileStudentComponent,
      inputs:    {
        showNum: 9
      }
    },
    Organization: {
      component: ProfileOrganizationComponent,
      inputs:    {
        showNum: 9
      }
    },
    Company:      {
      component: ProfileCompanyComponent,
      inputs:    {
        showNum: 9
      }
    },
    Admin:        {
      component: ProfileAdminComponent,
      inputs:    {
        showNum: 9
      }
    },
    Educator:     {
      component: ProfileEducatorComponent,
      inputs:    {
        showNum: 9
      }
    }
  }
  SidebarLink = {
    Student:      {
      component: SidebarLinkStudentComponent,
      inputs:    {
        showNum: 9
      }
    },
    Organization: {
      component: SidebarLinkOrganizationComponent,
      inputs:    {
        showNum: 9
      }
    },
    Company:      {
      component: SidebarLinkCompanyComponent,
      inputs:    {
        showNum: 9
      }
    },
    Admin:        {
      component: SidebarLinkAdminComponent,
      inputs:    {
        showNum: 9
      }
    },
    Educator:     {
      component: SidebarLinkEducatorComponent,
      inputs:    {
        showNum: 9
      }
    }
  }

  canActivate2() {
    if (!this.user.profile) {
      return false
    }
    if (!this.SidebarLink.hasOwnProperty(this.user.profile['type_profile'])) {
      return false
    }
    if (!this.Profile.hasOwnProperty(this.user.profile['type_profile'])) {
      return false
    }

    this.componentSidebarLink = this.SidebarLink[this.user.profile['type_profile']];
    this.componentProfile     = this.Profile[this.user.profile['type_profile']]
    this.componentUserData    = this.UserData[this.user.profile['type_profile']]
    this.componentSetting     = this.Setting[this.user.profile['type_profile']]


    return true;
  }

}
