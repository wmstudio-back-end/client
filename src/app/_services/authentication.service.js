"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
require("rxjs/add/operator/map");
var AuthenticationService = (function () {
  function AuthenticationService(router, cookieService, user, dataService) {
    this.router = router;
    this.cookieService = cookieService;
    this.user = user;
    this.dataService = dataService;
    this.auth = false;
    this.message = {};
    this.message = this.dataService.messages["Authentication/identify"].subscribe(this.login.bind(this));
    this.message = this.dataService.messages["Authentication/auth"].subscribe(this.login.bind(this));
    this.message = this.dataService.messages["Authentication/reg"].subscribe(this.login.bind(this));
  }

  AuthenticationService.prototype.checkLogin = function () {
    if (this.auth) {
      console.log(this.cookieService.get('token'));
      return true;
    }
    return false;
  };
  AuthenticationService.prototype.login = function (data) {
    if (!data) {
      return;
    }
    if (data.hasOwnProperty('options')) {
      this.dataService.options = data.options;
    }
    if (data.hasOwnProperty("profile") && data.profile.hasOwnProperty("_id")) {
      this.auth = true;
      this.cookieService.put("token", data.profile._id);
      this.user.setProfile(data.profile);
      this.router.navigate(['account']);
      console.log(this.router.initialNavigation());
      // this.router.navigate([this.router.url]);
    }
  };
  AuthenticationService.prototype.logout = function () {
    console.log("OUT");
    this.cookieService.removeAll();
    console.log(this.cookieService.get('token'));
    this.router.navigate(['main']);
    this.auth = false;
  };
  return AuthenticationService;
}());
AuthenticationService = __decorate([
  core_1.Injectable()
], AuthenticationService);
exports.AuthenticationService = AuthenticationService;
