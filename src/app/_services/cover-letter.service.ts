import {Injectable} from '@angular/core';

@Injectable()
export class CoverLetterService {
  coverLetters = [
    {
      title:       'Cover Letters 1',
      description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic ',
      date:        '20.10.2016 (09:46)'
    },
    {
      title:       'Cover Letters 2',
      description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic ',
      date:        '20.10.2016 (09:47)'
    },
    {
      title:       'Cover Letters 3',
      description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic ',
      date:        '20.10.2016 (09:48)'
    },
    {
      title:       'Cover Letters 4',
      description: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic ',
      date:        '20.10.2016 (09:49)'
    }
  ];

  constructor() {
  }

}
