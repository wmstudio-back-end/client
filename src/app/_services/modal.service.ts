import { Injectable } from '@angular/core';
import {RequestRecommendationComponent} from "../components/modal/request-recommendation/request-recommendation.component";
import {DddComponent} from "../components/modal/ddd/ddd.component";

@Injectable()
export class ModalService {
    public init = null
   modals = {
       requestRecommendation : {
           component: RequestRecommendationComponent,
           inputs: {
               option: {}
           }
       },

       dddComponent : {
           component: DddComponent,
           inputs: {
               showNum: 9
           }
       }
   };


  constructor() {
        //this.init = this.requestRecommendation
  }
    public setModal(type,opt){
      this.init = null
      this.init = this.modals['dddComponent'];
      this.init = this.modals[type];
      this.modals[type].inputs.option = opt
        console.log("-----------------------",type)
    }
    public hideModal(type){
        console.log('-------------55555555555555', type)
        this.init = null
    }

}
