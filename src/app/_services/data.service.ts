
import {
    EventEmitter,
    Injectable
} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {CookieService} from "ngx-cookie";
import {TranslateService} from "@ngx-translate/core";
import {Http} from "@angular/http";
import {IVacancy} from "../_models/vacancy";
import {DownPopupService} from "./down-popup.service";
import EmailMask from "text-mask-addons/dist/emailMask.js";
import createNumberMask from "text-mask-addons/dist/createNumberMask";
import {Router} from "@angular/router";
import * as io from "socket.io-client";

let HOST = window.location.hostname;
let PROTOCOL = window.location.protocol;
let wsProtocol = PROTOCOL == 'http:' ? 'wss' : 'wss';
let PORT_S = '8182';
let WS_HOST = HOST;
let HOST_STATIC = HOST;
let PORT = "7001"

// if (window.location.hostname == 'localhost') {
//   HOST = '192.168.102.231';
// }
if (window.location.port == '4200') {
    HOST = 'wms-dev.xyz';
    WS_HOST = HOST;
    PORT_S = '443';
    PORT = '443';
    HOST_STATIC = 'static.' + HOST
}

if (window.location.hostname == 'account.wms-dev.xyz') {
    HOST = 'wms-dev.xyz';
    WS_HOST = HOST;
    PORT_S = '443';
    PORT = '443';
    HOST_STATIC = 'static.' + HOST
}

if (window.location.hostname == 'account.talents.fund') {
  HOST        = 'talents.fund';
  WS_HOST     = 'https://talents.fund';
  PORT_S      = '443';
  PORT        = '443';
  HOST_STATIC = 'static.' + HOST
}

if (window.location.hostname == '192.168.102.231') {
    HOST = '192.168.102.231';
}
// if (window.location.hostname == 'dev-wms.ru') {
//   HOST = 'dev-wms.ru';
// }
if (window.location.hostname == '192.168.102.132') {
    HOST = '192.168.102.231';
}


// }
// if (window.location.hostname == 'localhost') {
//   HOST = '193.189.89.145';
//   HOST = 'talents.fund';
//
// }
// const CHAT_URL = 'dev-wms.ru';
// const CHAT_URL = '192.168.102.231';


export interface Message {
    method: string,
    data: any,
    requestId: number,
    //error:string,
}


@Injectable()
export class DataService {

    public emailMask = EmailMask
    public yearMask: Array<string | RegExp> = [
        /[1-2]/,
        /[0-9]/,
        /[0-9]/,
        /[0-9]/
    ]
    public numMask4: Array<string | RegExp> = [
        /[0-9]/,
        /[0-9]/,
        /[0-9]/,
        /[0-9]/
    ]
    //public phoneMask: RegExp=   /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i
    public phoneMask = createNumberMask({
        prefix: '+',
        includeThousandsSeparator: false,
        thousandsSeparatorSymbol: '-',
        suffix: ''
    })
    public req = {}
    public vacancies = <IVacancy>{};
    public messages = {
        "Authentication/identify": new EventEmitter(),
        "Authentication/auth": new EventEmitter(),
        "Authentication/reg": new EventEmitter(),
        "User/getUsers": new EventEmitter(),
        "User/editProfile": new EventEmitter(),
        "User/projectInsert": new EventEmitter(),
        "Vacancy/createNew": new EventEmitter(),
        "Vacancy/getVacancy": new EventEmitter(),
        "Location/setLocation": new EventEmitter(),
        "Message/setMessage": new EventEmitter(),
        "User/getOnline": new EventEmitter(),
        "User/Disconnect": new EventEmitter(),
        "User/Connect": new EventEmitter(),
        "Authentication/signout": new EventEmitter(),
        "User/smartId": new EventEmitter(),
        "EVENT/applyOrder": new EventEmitter(),
        "EVENT/publicVacancy": new EventEmitter(),
        "EVENT/updateVacancy": new EventEmitter(),
        "EVENT/subscribeToVacancy": new EventEmitter(),
        "EVENT/newRecommendation": new EventEmitter(),
        "EVENT/newReqRecommendation": new EventEmitter(),
        "EVENT/verifyProgressEducation": new EventEmitter(),
    }
    //public messages: EventEmitter<any> = new EventEmitter()
    private message = <Message>{};
    options: any
    packages: any
    private data: Observable<Message>;
    public methods = {}
    ws: any
    wsRecconect: any
    msg: any
    requestId: number = 0
    PROTOCOL = PROTOCOL
    PORT = PORT
    // HOST = '192.168.102.231'
    HOST = HOST
    WS_HOST = WS_HOST
    // HOST = "localhost"
    PORT_STATIC = PORT_S;
    // HOST_STATIC = 'localhost';
    HOST_STATIC = HOST_STATIC
    public fileupload = {
        photo: [
            "JPG",
            "JPEG",
            "Exif",
            "TIFF",
            "GIF",
            "BMP",
            "PNG",
            "PPM",
            "PGM",
            "PBM",
            "PNM",
            "jpg",
            "jpeg",
            "png"
        ],
        docs: [
            "key",
            "key",
            "odp",
            "pps",
            "ppt",
            "pptx",
            "doc",
            "docx",
            "odt",
            "pdf",
            "rtf",
            "tex",
            "txt",
            "wks",
            "wps",
            "wpd",
            "bmp",
            "gif",
            "jpeg",
            "jpg",
            "png",
            "psd",
            "tif",
            "tiff"
        ]
    }


    constructor(private cookieService: CookieService,
                public translate: TranslateService,
                public http: Http,
                private router: Router,
                public popS: DownPopupService,) {

        // this.messages["Vacancy/getVacancy"].subscribe(this.addedVacancy.bind(this))
        // if (window.hasOwnProperty('PROFILE')){
        //   this.HOST = window['PROFILE'].HOST
        //   this.PORT = window['PROFILE'].PORT
        //   this.PORT_STATIC = window['PROFILE'].PORT_STATIC
        //   this.HOST_STATIC = window['PROFILE'].HOST_STATIC
        //   this.options = window['PROFILE'].options
        // }

        console.log("HOST", this.HOST)


        // setTimeout(function(){
        //
        //   if (window.hasOwnProperty('PROFILE')){
        //     this.HOST = window['PROFILE'].HOST
        //     this.PORT = window['PROFILE'].PORT
        //     this.PORT_STATIC = window['PROFILE'].PORT_STATIC
        //     this.HOST_STATIC = window['PROFILE'].HOST_STATIC
        //     this.options = window['PROFILE'].options
        //   }
        //
        //
        //
        // }.bind(this),1000)
        this.connectWs()
    }

    getPackages() {
        var t = []
        for (var i = 0; i < this.packages.length; i++) {
            t.push({
                id: this.packages[i]._id,
                text: this.packages[i].name
            })

        }
        return t
    }

    ObjectKeysLength(ob) {
        var t = 0

        function l(b) {
            var e = 0
            if (typeof b == 'object') {

                for (var key in b) {

                    if (typeof b[key] == 'object' && !Array.isArray(b[key])) {
                        e += l(b[key])
                    } else {
                        e++
                    }
                }
            } else {
                return 1
            }

            return e
        }

        for (var key in ob) {

            t += l(ob[key])


        }
        return t
    }

    getOptionsFromId(key, id) {

        if (id == undefined)
            return {
                id: 0,
                text: "-",
                error: true
            };
        if (this.options.hasOwnProperty(key)) {
            id = (typeof id === 'string') ? id : id.toString()
            for (var i = 0; i < this.options[key].length; i++) {
                if (this.options[key][i]['id'] == id + "") {
                    return this.options[key][i]
                }
            }
        }
        return {
            id: 0,
            text: "NOT FOUND",
            error: true
        }
    }

    getOptionId(key: string, text: string) {
        let regexp = new RegExp(text, "i");
        let result = [];
        let options = this.options.translate[this.translate.currentLang.toUpperCase()]
        if (!options.hasOwnProperty(key)) return result;
        let keys = Object.keys(options[key])
        for (let i = 0; i < keys.length; i++) {
            // console.log(options[key][keys[i]],regexp.test(options[key][keys[i]]))
            if (regexp.test(options[key][keys[i]])) {
                let key = (typeof keys[i] === 'string') ? parseInt(keys[i]) : keys[i];
                result.push(key);
            }
        }
        return result;
    }

    optionText(key, id) {

        if (id == undefined)
            return "-";
        if (this.options.hasOwnProperty(key)) {
            id = (typeof id === 'string') ? id : id.toString()
            for (var i = 0; i < this.options[key].length; i++) {
                if (this.options[key][i]['id'] == id + "") {
                    return this.options[key][i].text;
                }
            }
        }


        return "NOT FOUND"
    }

    dateConvert(unix_timestamp) {

        if (typeof unix_timestamp == 'string') {
          return new Date(unix_timestamp)
            // return new Date(parseInt(unix_timestamp.toString()
            //     .substr(0, 8), 16) * 1000)
        }

        if (typeof unix_timestamp == 'number') {
            return new Date(unix_timestamp * 1000);
        }

    }

    convertPickeDate(unix_timestamp) {
        if (!unix_timestamp) {
            return ''
        }
        var yyyy = new Date(unix_timestamp * 1000).getFullYear();
        var mm = new Date(unix_timestamp * 1000).getMonth() + 1;
        var dd = new Date(unix_timestamp * 1000).getDate();

        return {
            year: yyyy,
            month: mm,
            day: dd
        }

    }

    convertPickeRangeDate(unix_timestamp) {
        if (!unix_timestamp) {
            return null
        }
        var str = unix_timestamp.split('-')


        return {
            beginDate: {
                year: new Date(str[0] * 1000).getFullYear(),
                month: new Date(str[0] * 1000).getMonth() + 1,
                day: new Date(str[0] * 1000).getDate()
            },
            endDate: {
                year: new Date(str[1] * 1000).getFullYear(),
                month: new Date(str[1] * 1000).getMonth() + 1,
                day: new Date(str[1] * 1000).getDate()
            }
        };

    }

    addedVacancy(data) {

        for (var i = 0; i < data.vacancy.length; i++) {
            this.vacancies[data.vacancy[i]._id] = data.vacancy[i]
        }
    }

    translaterInit() {


        let l = []
        //console.log('this.translate.getLangs()this.translate.getLangs()',this.translate.set())
        var langs = this.translate.getLangs()
        for (var i = 0; i < this.options.languages.length; i++) {
            //this.options.languages[i].value = this.options.languages[i].value.toLowerCase()

            for (var j = 0; j < langs.length; j++) {
                if (langs[j].toUpperCase() == this.options.languages[i].value) {
                    l.push(this.options.languages[i])
                    break;
                }

            }

            //if (this.translate.getLangs().indexOf(this.options.languages[i].value) > -1) {
            //
            //
            //}
        }

        console.log('translate.currentLang', l)


        // this.options.translater = l


        let sort = {
            24: 3,
            27: 1,
            94: 2,
        }

        let langTranslater = {
            24: 'English',
            27: 'Eesti',
            94: 'Русский',
        }

        this.options.translater.sort((a, b) => {
            return sort[a.id] - sort[b.id];
        })
        l.sort((a, b) => {
            return sort[a.id] - sort[b.id];
        })

        this.options.translaterDefault = JSON.parse(JSON.stringify(l))

        this.options.translater.map((e) => {
            e.text = langTranslater[e.id]
            return e;
        })


    }

    initLangFromId(id: number) {

        var l = 'en'
        for (var i = 0; i < this.options.languages.length; i++) {
            if (this.options.languages[i].id == id) {

                l = this.options.languages[i].value.toLowerCase()
                break;
            }
        }

        setTimeout(function () {
            this.translate.setDefaultLang(l);
            this.translate.use(l)

        }.bind(this), 1)

        //this.translate.use('ru')

        //this.translate.set('inputLabel.First_name2', 'assss')


    }

    connectWs() {
        console.log(this.cookieService.get('SOCKET'))
        let host = this.WS_HOST == 'wms-dev.xyz' ? 'https://wms-dev.xyz' : this.WS_HOST
        console.log(this.WS_HOST, host)

        this.ws = io(host + ':' + this.PORT);

        // this.ws = new WebSocket(wsProtocol + '://' + this.HOST + ':' + this.PORT)
        this.serviceWs()
        clearInterval(this.wsRecconect)
    }

    serviceWs() {
        // this.ws.onopen    = function () {
        //     console.log('connect to: ' + this.HOST + ':' + this.PORT + ' token: ' + this.cookieService.get('token'))
        //     if (this.cookieService.get('token') && !window.hasOwnProperty('PROFILE')) {
        //         //this.send('Authentication/identify', {_id:this.cookieService.get('token')})
        //     }
        // }.bind(this);
        this.ws.on('message', function (data) {
            this.cookieService.put('token', this.cookieService.get('token'), {
                expires: new Date(new Date().getTime() + 60 * 60 * 2 * 1000),
                path: '/'
            })
            data = JSON.parse(data)
            console.log('RES SERVER ', data)
            if (data.error) {
                if (data.error.code) {
                    this.popS.show_info(this.getOptionsFromId('errors', data.error.code).text)
                }
                else if (data.error.errorId) {
                    this.popS.show_info(this.getOptionsFromId('errors', data.error.errorId).text)
                }
                else {
                    this.popS.show_info(data.error.text || data.error.error.text)
                }

                if (!data.data) {
                    return
                }
            }

            if (this.req[data.requestId]) {
                this.req[data.requestId].emit(data.data)
            } else {
                let m = data.method.split('/')
              if (this.messages[m[1] + '/' + m[2]]){
                this.messages[m[1] + '/' + m[2]].emit(data.data);
              }else{
                console.log("INCOMING MESS",data)
              }

            }

        }.bind(this));
        this.ws.onclose = function (event) {
            this.wsRecconect = setInterval(function () {
                this.connectWs()
                console.log("RECONNECT onclose")
            }.bind(this), 1000)
        }.bind(this);
        this.ws.onmessage = function (event) {
            this.cookieService.put('token', this.cookieService.get('token'), {
                expires: new Date(new Date().getTime() + 60 * 60 * 2 * 1000),
                path: '/'
            })
            let data = JSON.parse(event.data)
            console.log('RES SERVER ', data)
            if (data.error) {
                console.log(data.error)
                if (data.error.errorId) {
                    this.popS.show_info(this.getOptionsFromId('errors', data.error.errorId).text)
                } else {
                    this.popS.show_info(data.error.text)
                }

                if (!data.data) {
                    return
                }
            }
            if (this.req[data.requestId]) {
                this.req[data.requestId].emit(data.data)
            } else {
                let m = data.method.split('/')
                //console.log(m)
                this.messages[m[1] + '/' + m[2]].emit(data.data);

            }


        }.bind(this);
        this.ws.onerror = function (error) {
            console.log("NO RECONNECT onerror")
            // this.wsRecconect = setInterval(function(){
            //     this.connectWs()
            //     console.log("RECONNECT onerror")
            // }.bind(this),1000)
        }.bind(this);
    }

    send(method: string, data: any) {
        this.requestId++
        this.message = <Message>{
            method: 'worker/' + method,
            data: data,
            error: null,
            requestId: this.requestId
        }
        this.ws.send(JSON.stringify(this.message))
        console.log('SEND TO SERVER ', this.message)
    }

    sendws(method: string, data: any, server?: string) {
        if (!server) {
            server = 'worker'
        }

        this.requestId++
        this.message = <Message>{
            method: server + '/' + method,
            data: data,
            error: null,
            requestId: this.requestId
        }
        this.req[this.requestId] = new EventEmitter()
        this.ws.send(JSON.stringify(this.message))
        console.log('SEND TO SERVER ', this.message)
        return new Promise((resolve, reject) => {
            this.req[this.requestId].subscribe((data) => {
                return resolve(data)
            })
        });
    }

    closeWS() {
    }


}
