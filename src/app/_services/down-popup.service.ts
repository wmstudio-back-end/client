import { Injectable } from '@angular/core';
import {Notify} from "../_models/notify";
import {DataService} from "./data.service";

export interface Language {
  _id:string;
  type: string;
  text: string;
  buttonUrl: string;
  buttonName: string;
}

@Injectable()
export class DownPopupService {
  pops:Notify[] = []
  type = ''
  fixed = false
  text = ''
  constructor(
  //
  ) { }
  initPops(){
    for (var i = 0; i < this.pops.length; i++) {
        this.pops[i].show=false//true
    }
  }
  getPops(){
    var t = []
    var itm = 0
    for (var i = 0; i < this.pops.length; i++) {
      if (itm<3&&this.pops[i].show==true){
        t.push(<Notify>this.pops[i])
        itm++
      }
    }
    return t
  }
  closePop(_id){
    for (var i = 0; i < this.pops.length; i++) {
      if (this.pops[i]._id==_id){
        this.pops[i].show = false
        break
      }

    }
  }

  show_default(text,_id?,url?,buttonName?){
    if (!_id){this.timeoutPopUp()}
    this.pops.unshift(<Notify>{
      type:'default',
      _id:_id,
      text: text,
      buttonUrl:url,
      buttonName:buttonName,
      show:true
    })
  }
  show_success(text,_id?,url?,buttonName?){
    if (!_id){this.timeoutPopUp()}
    this.pops.unshift({
      type:'success',
      _id:_id,
      text: text,
      buttonUrl:url,
      buttonName:buttonName,
      show:true
    })
  }
  show_info(text,_id?,url?,buttonName?){
  if (!_id){this.timeoutPopUp()}
    this.pops.unshift(<Notify>{
      type:'info',
      _id:_id,
      text: text,
      buttonUrl:url,
      buttonName:buttonName,
      show:true
    })

  }
  show_warning(text,_id?,url?,buttonName?){
    if (!_id){this.timeoutPopUp()}
    this.pops.unshift(<Notify>{
      type:'warning',
      _id:_id,
      text: text,
      buttonUrl:url,
      buttonName:buttonName,
      show:true
    })

  }



  timeoutPopUp(){
    setTimeout(function(){
      for (var i = 0; i < this.pops.length; i++) {
        if (!this.pops[i]._id){
          // this.pops[i].show = false
          this.pops.splice(i,1)
          i--

        }

      }
    }.bind(this),5000)
  }


}
