"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var CHAT_URL = window.location.hostname;
if (window.location.hostname == 'localhost') {
  CHAT_URL = '192.168.102.231';
}
if (window.location.hostname == '192.168.102.231') {
  CHAT_URL = 'wms-dev.xyz';
}
if (window.location.hostname == 'dev-wms.ru') {
  CHAT_URL = 'dev-wms.ru';
}
var DataService = (function () {
  function DataService(cookieService) {
    // this.methods = {
    //     //'Authentication/identify':this.authenticationService.login,
    // }
    this.cookieService = cookieService;
    this.messages = {
      "Authentication/identify": new core_1.EventEmitter(),
      "Authentication/auth": new core_1.EventEmitter(),
      "Authentication/reg": new core_1.EventEmitter(),
      "User/getUsers": new core_1.EventEmitter(),
      "User/editProfile": new core_1.EventEmitter(),
    };
    //public messages: EventEmitter<any> = new EventEmitter()
    this.message = {};
    this.methods = {};
    this.requestId = 0;
    this.connectWs();
  }

  DataService.prototype.connectWs = function () {
    this.ws = new WebSocket('ws://' + CHAT_URL + ':7001');
    this.serviceWs();
    clearInterval(this.wsRecconect);
  };
  DataService.prototype.serviceWs = function () {
    this.ws.onopen = function () {
      console.log('connect to: ' + CHAT_URL + ' token: ' + this.cookieService.get('token'));
      if (this.cookieService.get('token')) {
        this.send('Authentication/identify', {_id: this.cookieService.get('token')});
      }
    }.bind(this);
    this.ws.onclose = function (event) {
      this.wsRecconect = setInterval(function () {
        this.connectWs();
        console.log("RECONNECT onclose");
      }.bind(this), 1000);
    }.bind(this);
    this.ws.onmessage = function (event) {
      var data = JSON.parse(event.data);
      console.log('RES SERVER ', data);
      //this.messages = data
      var m = data.method.split('/');
      this.messages[m[1] + '/' + m[2]].emit(data.data);
      //
      // if (data.hasOwnProperty(m[1]+'/'+m[2])){
      //     if (!data.data){return;}
      //
      //     //this.methods[m[1]+'/'+m[2]](this.messages.data)
      // }else{
      //     console.log("нет подписки на этот метод")
      //
      // }
      //this.switcherService.method[this.messages.method](this.messages)
    }.bind(this);
    this.ws.onerror = function (error) {
      console.log("NO RECONNECT onerror");
      // this.wsRecconect = setInterval(function(){
      //     this.connectWs()
      //     console.log("RECONNECT onerror")
      // }.bind(this),1000)
    }.bind(this);
  };
  DataService.prototype.send = function (method, data) {
    this.requestId++;
    this.message = {
      method: 'worker/' + method,
      data: data,
      error: null,
      requestId: this.requestId,
    };
    this.ws.send(JSON.stringify(this.message));
    //this.messages.next(this.message)
    console.log('SEND TO SERVER ', this.message);
  };
  DataService.prototype.closeWS = function () {
  };
  return DataService;
}());
DataService = __decorate([
  core_1.Injectable()
], DataService);
exports.DataService = DataService;
