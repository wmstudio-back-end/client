import {Injectable}  from "@angular/core";
import {
  Headers,
  Http,
  Response
}                    from "@angular/http";
import "rxjs"
import {Observable}  from "rxjs/Observable";
import "rxjs/add/operator/map";
import "rxjs/add/operator/catch";
import "rxjs/add/observable/throw";
import {DataService} from "./data.service";

@Injectable()
export class HttpService {
  PORT: string;
  HOST: string;
  PROTOCOL: string;

  constructor(private http: Http, public ds: DataService) {
    this.PORT     = ds.PORT_STATIC
    this.HOST     = ds.HOST_STATIC
    this.PROTOCOL = ds.PROTOCOL
  }

  postData(obj: any) {
    let HOST   = this.HOST
    // let HOST = 'localhost'
    let PORT   = this.PORT

    const body = JSON.stringify(obj);

    let headers = new Headers({'Content-Type': 'application/json;charset=UTF-8'});

    console.log(this.PROTOCOL + '//' + HOST + ':' + PORT)
    let response = this.http.post(this.PROTOCOL + '//' + HOST + ':' + PORT, body, {headers: headers})
      .map((resp: Response) => resp.json())
      .catch((error: any) => {

        return Observable.throw(error);
      });

    return response;
  }
}
