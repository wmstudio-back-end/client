import {Injectable, Pipe} from '@angular/core';

import {DataService} from "./data.service";
import {UserService} from "./user.service";

@Injectable()
export class MessageService {
  public serviceReady = false
  public filterStatus:number = 0
  public dialogs = {}
  public dialogsCopy = {}
  public dialogActive = ''
  public messages:any[] = []
  public countNew:number = 0
  private LIMIT:number = 10
  audio = new Audio();

  constructor(private dS:DataService,private uS:UserService) {

    if (this.uS.profile.dialogs) {
      for (var i = 0; i < this.uS.profile.dialogs.length; i++) {
        this.setDialogs(this.uS.profile.dialogs[i].id)
        this.dialogActive = this.uS.profile.dialogs[i].id

      }
    }
    this.audio.src = "/assets/blink.mp3";
    this.audio.load();

    this.dS.messages["Message/setMessage"].subscribe(this.setMessage.bind(this));

  }
  getCountNewMessagessFromDialog(uid){
    return new Promise((resolve, reject) => {
      this.dS.sendws('Message/getCountNewMessagessFromDialog',{uid:uid}).then(data=>{
        resolve(data['res'])
      })
    });


  }
  setFilter(e){
    this.filterStatus =e.id
  }
  deleteDialog(id){
    this.dS.sendws('Message/deleteDialog',{dialog_id:id}).then(messages=>{
      for(var i=0;i<this.uS.profile.dialogs.length;i++){
        if (this.uS.profile.dialogs[i].id==id){
          this.uS.profile.dialogs.splice(i,1)
          break;
        }
      }
      for(var i=0;i<this.uS.profile.dialogs.length;i++){
        this.dialogActive = this.uS.profile.dialogs[i].id
        break;
      }
      delete this.dialogs[id]
    })

  }

  setDialogs(id){
    if (this.dialogActive==this.uS.profile._id){
      return
    }
    this.getMessage(id,0,this.LIMIT).then(messages=>{
    var notread = 0
      if (messages['messages'])
      {
        for (var i = 0; i < messages['messages'].length; i++) {
          var add = true
          for (var j = 0; j < this.messages.length; j++) {
            if (this.messages[j]._id==messages['messages'][i]._id){
              add = false
            }

          }
          if (add){
            this.messages.unshift(messages['messages'][i])
          }
          if (messages['messages'][i].status==0&&messages['messages'][i].sender!=this.uS.profile._id){
            notread++
          }
        }
      }
      this.countNew+=notread
      this.messages.sort(function(a,b){
        return b.date-a.date
      })

      this.uS.getUsers([id]).then(user=>{

        if (Object.keys(user).length>0){
          user = user[0]
          this.dialogs[id] =  {
            messages : messages['messages'],
            user:{
              name: user['first_name']+' '+user['last_name'],
              subname:user['type_profile']=='Company'?user['Company'].nameCompany:null
            },
            notread:notread,
            blocked:false,
            unLoad:true,
            skip:0,
            id:id
          }

          this.dialogActive = id
          this.dialogs = this.sortDialogs(this.dialogs)
        }



      })
    })




  }
  array_unique(arr) {
  var tmp_arr = new Array();
  for (var i = 0; i < arr.length; i++) {
    if (tmp_arr.indexOf(arr[i]) == -1) {
      tmp_arr.push(arr[i]);
    }
  }
  return tmp_arr;
}

  sortDialogs(o) {
    console.log('SOOOOORT')
  var sorted = {},
    key, a = [];

  for (key in o) {
    if (o.hasOwnProperty(key)) {
      a.push(key);
    }
  }

  a.sort((a,b)=>{
    var aS = 0
    var bS = 0
    if (this.dialogs[a].messages.length==0&&this.dialogs[b].messages.length==0){aS = 0;bS = 0;}
    if (this.dialogs[a].messages.length==0){aS = -1}else{aS = this.dialogs[a].messages[this.dialogs[a].messages.length-1].date;}
    if (this.dialogs[b].messages.length==0){bS = -1}else{bS = this.dialogs[b].messages[this.dialogs[b].messages.length-1].date;}

    //
    //console.log("____________",this.dialogs[a].messages.length)
    //return this.dialogs[a].messages[this.dialogs[a].messages.length-1].date-
    //this.dialogs[b].messages[this.dialogs[b].messages.length-1].date
    //
    //return 1
    return bS-aS
  });

  for (key = 0; key < a.length; key++) {
    sorted[a[key]] = o[a[key]];
  }
  return sorted;
}





  //
  //
  //dialogsActivate(m,opt?){
  //  let ids = []
  //
  //  if (m.length>0){
  //    for(let i=0;i<m.length;i++){
  //      this.countNew+=m[i].notread
  //      this.initDialog({
  //          dialog:m[i],
  //          skip:0,
  //          limit:this.LIMIT,
  //          options:opt
  //        }
  //      )
  //
  //    }
  //
  //    this.dialogActive = m[m.length-1]._id
  //
  //  }
  //}
  setMessage(data){
    var id = ''
    if (data.sender==this.uS.profile._id){
      id = data.receiver
    }else{
      this.audio.play();
      this.countNew++
      id = data.sender
    }

     this.messages.push(data)
    if (this.dialogs.hasOwnProperty(id)){
      if (data.sender!=this.uS.profile._id){
        this.dialogs[id].notread++
      }
      this.dialogs[id].count++
      this.dialogs[id].messages.push(data)

    }else{
      this.setDialogs(id)


    }
    this.dialogs = this.sortDialogs(this.dialogs)

  }
  getMessage(id,skip,limit,opt?){
    return new Promise((resolve, reject) => {
      this.dS.sendws('Message/getMessage',{dialog_id:id,skip:skip,limit:limit,opt:opt}).then(messages=>{
        resolve(messages)
      })
    })
  }

  getDialog(id){
    this.dialogActive = id
  }

  isRead(){
    if (this.dialogs[this.dialogActive]&&this.dialogs[this.dialogActive].notread>0){
      this.countNew-=this.dialogs[this.dialogActive].notread
      this.dialogs[this.dialogActive].notread = 0
      this.dS.sendws('Message/isRead',{dialog_id:this.dialogActive})
      for(var i=0;i<this.dialogs[this.dialogActive].messages.length;i++){
        this.dialogs[this.dialogActive].messages[i].status = 1
      }
    }

  }
//  initDialog(o){
//console.log("INIIIIIT",o)
//    return new Promise((resolve, reject) => {
//
//    var id = o.dialog._id
//
//    this.dialogActive = id
//
//        //if (this.dialogs[id].notread>0){
//        //  this.dS.send('Message/isRead',{dialog_id:id})
//        //  this.countNew-=this.dialogs[id].notread
//        //  this.dialogs[id].notread=0
//        //}
//        //
//        //if ((o.skip+o.limit)<this.dialogs[id].count){
//        //  resolve(true)
//        //}else if((o.skip+o.limit)>=this.dialogs[id].count){
//        //
//        //  this.getMessage(id,o.skip,o.limit).then(messages=>{
//        //    this.dialogs[id].messages.push(messages['messages'])
//        //  })
//        //}
//
//
//        this.getMessage(id,o.skip,o.limit,o.options).then(messages=>{
//
//          this.messages = this.messages.concat(messages['messages'])
//
//         this.uS.getUsers([id]).then(user=>{
//
//           this.dialogs[id] = {
//             messages : messages['messages'],
//             user:{
//               name: this.uS.users[id].first_name+' '+this.uS.users[id].last_name,
//               subname:this.uS.users[id].type_profile=='Company'?this.uS.users[id].Company.nameCompany:null
//             },
//             notread:o.dialog.notread,
//             active:false,
//             online:this.uS.users[id].online,
//             count:o.dialog.count,
//             skip:o.skip,
//             limit:o.limit,
//             id:o.dialog.id
//           }
//           resolve(true)
//         })
//
//
//
//
//        })
//
//
//
//
//
//    })
//
//
//  }
//  getMessageNext(){
//    return new Promise((resolve, reject) => {
//      if(!this.dialogs[this.dialogActive].unLoad){
//        resolve(true)
//        return
//      }
//      var skip = this.dialogs[this.dialogActive].skip
//      var limit = this.LIMIT
//      var count = this.dialogs[this.dialogActive].count
//      console.log(skip + limit, count)
//
//      //if (skip + limit < count) {
//        this.getMessage(this.dialogActive, skip + limit, this.LIMIT).then(data => {
//          this.dialogs[this.dialogActive].messages =
//            data['messages'].concat(this.dialogs[this.dialogActive].messages)
//          //this.dialogs[this.dialogActive].messages.sort(function(a,b){
//          //  console.log(a.date)
//          //  return a._id-b._id
//          //})
//          //this.dialogs[this.dialogActive].count+=data['messages'].length
//          if (data['messages'].length>0){
//
//            this.dialogs[this.dialogActive].skip += data['messages'].length
//          }else{
//            this.dialogs[this.dialogActive].unLoad = false
//          }
//
//          //console.log("ADDDDDDDDDDDDDDDDDDDDDD", data)
//          //console.log("skip", this.dialogs[this.dialogActive].skip)
//          //console.log("limit", this.LIMIT)
//          //console.log("count", this.dialogs[this.dialogActive].count)
//          //console.log("limit+this.LIMIT", skip + this.LIMIT)
//          resolve(true)
//        })
//      //}
//    })
//  }
  //findMessageText(text){
  //  return new Promise((resolve, reject) => {
  //    if (text==''){
  //      this.dialogs = {}
  //      this.dialogsActivate(this.uS.profile.messages)
  //    }else{
  //      this.dS.sendws('Message/findMessageText',{text:text}).then(messages=>{
  //
  //        this.dialogs = {}
  //        this.dialogsActivate(messages['messages'],{text:text})
  //
  //        console.log('============================',messages)
  //        resolve(messages)
  //      })
  //    }
  //
  //  })
  //}
  transform(value, args:string[]) : any {
    let keys = [];
    for (let key in value) {
      keys.push({key: key, value: value[key]});
    }
    return keys;
  }
}
