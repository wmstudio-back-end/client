﻿import {
  EventEmitter,
  Injectable
}                         from "@angular/core";
import {
  Headers,
  Http,
  RequestOptions
}                         from "@angular/http";
import {User}             from "../_models/index";
import {DataService}      from "./data.service";
import {TranslateService} from "@ngx-translate/core";
import {IaccessPackage}   from "../_models/service";
import {DownPopupService} from "./down-popup.service";
import {Router}           from "@angular/router";
import {
  FacebookService,
  InitParams,
  LoginOptions,
  LoginResponse
}                         from "ngx-facebook";

interface Skills {
  id: number;
  level: number;
  use: boolean;
}

@Injectable()
export class UserService {

  public profile: User;
  public users: any                    = {};
  public type: string;
  private host: string;
  private port: string;
  public oAuth                         = new EventEmitter<boolean>()
  public notPass                       = new EventEmitter<boolean>()
  public accessRight                   = 0
  public first_profile: User;
  public initProfile                   = true;
  public userDataComplited             = 0
  public preferCompactView: boolean    = false
  public accessPackage: IaccessPackage = <IaccessPackage>{
    publish:    false,
    applicants: false,
    rating:     false,
    search:     false,
    notify:     false,
    match:      false,
    projBmark:  false,
  }

  constructor(private http: Http,
              public dataService: DataService,
              public translate: TranslateService,
              public popS: DownPopupService,
              private router: Router,
              private fb: FacebookService) {


    let initParams: InitParams = {
      appId:   '179104739527560',
      xfbml:   true,
      version: 'v2.12'
    }

    fb.init(initParams)

    this.port = dataService.PORT_STATIC;
    this.host = dataService.HOST_STATIC;
    dataService.messages["User/Connect"].subscribe(data => {
      if (this.users[data.uid]) {
        this.users[data.uid].online = true
      }
    })
    dataService.messages["User/Disconnect"].subscribe(data => {
      if (this.users[data.uid]) {
        this.users[data.uid].online = false
      }
    })
    dataService.messages["User/smartId"].subscribe(data => {
      this.setSmartId(data.profile)
    })
    dataService.messages["EVENT/newRecommendation"].subscribe(data => {
      console.log('DATA',data)
      this.profile.recommendations.unshift(data)

    })
    dataService.messages["EVENT/newReqRecommendation"].subscribe(data => {
      var added = true
      console.log("--------=========START")
      for (var i = 0; i < this.profile.recommendations.length; i++) {
        if (this.profile.recommendations[i]._id==data._id){
          console.log('!!!!!!!!!!!!!!!!EDIT')
          this.profile.recommendations[i] = data
          added = false
        }
      }
      if (added){
        console.log('!!!!!!!!!!!!!!!!unshift')
        this.profile.recommendations.unshift(data)
      }


    })

  }

  // complitedUserdata() {
  //   this.userDataComplited = 0
  //   this.isValidObj(this.profile, 'first_name') ? this.userDataComplited += 1 : null
  //   this.isValidObj(this.profile, 'last_name') ? this.userDataComplited += 1 : null
  //   this.isValidObj(this.profile, 'phone') ? this.userDataComplited += 5 : null
  //   this.isValidObj(this.profile, 'email') ? this.userDataComplited += 1 : null
  //   this.isValidObj(this.profile, 'personalId') ? this.userDataComplited += 2.5 : null
  //   this.isValidObj(this.profile, 'birthday') ? this.userDataComplited += 1 : null
  //   this.isValidObj(this.profile, 'Location') ? this.userDataComplited += 5 : null
  //   this.isValidObj(this.profile.Student, 'gender') ? this.userDataComplited += 1 : null
  //   return this.userDataComplited
  // }

  isValidObj(ob, str) {


    if (ob.hasOwnProperty(str) && ob[str] !== '' && ob[str] !== {} && ob[str] !== []) {
      return true
    }
    return false
  }

  getMainPhoto(profile) {
    var photo = '/assets/img/presona.svg';
    switch (profile.type_profile) {
      case 'Student':
        photo = '/assets/img/persona-logo.svg';
        break;
      case 'Company':
        photo = '/assets/img/business-logo.svg';
        break;
      case 'Admin':
        photo = '/assets/img/logo.svg';
        break;
      case 'Educator':
        photo = '/assets/img/school-logo.svg';
        break;
    }
    if (typeof profile.Photo !== "undefined" && profile.Photo !== null)
      profile.Photo = profile.photo = '//' + this.host + ':' + this.port + profile.Photo
    else profile.Photo = profile.photo = photo;
  }

  getSchools(ids) {
    return new Promise((resolve, reject) => {
      var q = {}
      for (var i = 0; i < ids.length; i++) {
        if (
          this.users[ids[i]]
          && this.users[ids[i]].Student
          && this.users[ids[i]].Student.schools
        ) {
          continue
        }
        //this.users[ids[i]].Student.schools = []
        q[ids[i]] = null
      }

      if (Object.keys(q).length > 0) {
        this.dataService.sendws('Schools/getSchoolUDFromUids', {ids: Object.keys(q)}).then(data => {

          for (var i = 0; i < Object.keys(q).length; i++) {
            //console.log("----------------",this.users[Object.keys(q)[i]])
            //console.log("----------------",Object.keys(q)[i])
            if (!this.users[Object.keys(q)[i]]) {
              continue
            }
            this.users[Object.keys(q)[i]].Student.schools = []
            for (var j = 0; j < data['result'].length; j++) {
              if (data['result'][j].uid == Object.keys(q)[i]) {
                this.users[Object.keys(q)[i]].Student.schools.push(data['result'][j])
              }
            }
            this.users[Object.keys(q)[i]].Student.schools.sort((a, b) => {
              return a.educationLevel - b.educationLevel
            })
          }

          resolve(data['result'])
        })
      } else {
        resolve(null)
      }

    })
  }

  setUsers(users: User[]) {
    for (let i = 0; i < users.length; i++) {
      let user                 = this.prepareUser(users[i])
      this.users[users[i]._id] = user

      //this.users[users[i]._id].online = false
    }
  }

  getOnline(usersIds) {
    return new Promise((resolve, reject) => {
      this.dataService.sendws('User/getOnline', {ids: usersIds}, 'wsproxy').then(data => {
        for (var id in data['ids']) {
          if (id != null) {
            this.users[id].online = data['users'][id]
          }
        }
        resolve(data['users'])
      })
    })
  }

  displayAs(profile) {
    return [
      {
        id:   0,
        text: profile.first_name + ' ' + profile.last_name
      },
      {
        id:   1,
        text: profile.first_name + ' ' + profile.last_name.substr(0, 1).toUpperCase() + '.'
      },
      {
        id:   2,
        text: profile.first_name.substr(0, 1).toUpperCase() + '.' + ' ' + profile.last_name.substr(0, 1).toUpperCase() + '.'
      }
    ]
  }

  getCompanyName(id) {
    if (this.users[id].type_profile == 'Company') {
      return this.users[id].Company.nameCompany
    }
    return ''
  }

  getUserName(id, t = 0, nameView = true) {
    if (!this.users[id]) {
      return ''
    }
    var name = ''
    switch (t) {
      case 0:
        if (!this.users[id].hasOwnProperty('displayAs')) {
          name = this.users[id].first_name + ' ' + (nameView ? this.users[id].last_name : '')
          return name
        }
        switch (this.users[id].displayAs) {
          case 0:
            name = this.users[id].first_name + ' ' + (nameView ? this.users[id].last_name : '')
            break;
          case 1:
            name = this.users[id].first_name + ' ' + (nameView ? this.users[id].last_name.substr(0, 1).toUpperCase() + '.' : '')
            break;
          case 2:
            name = this.users[id].first_name.substr(0, 1).toUpperCase() + '.' + ' ' + (nameView ? this.users[id].last_name.substr(0, 1).toUpperCase() + '.' : '')
            break;
          default:
            name = this.users[id].first_name + ' ' + (nameView ? this.users[id].last_name : '')
            break
        }
        // if (this.users[id].type_profile == 'Company' && this.users[id].Company.nameCompany) {
        //     name = this.users[id].Company.nameCompany
        // }
        break;
      case 1:
        if (this.users[id].type_profile == 'Company') {
          name = this.users[id].Company.nameCompany
        } else {
          name = this.users[id].first_name + ' ' + this.users[id].last_name
        }
        break;
    }
    return name
  }

  getSchoolUDFromUid(uid) {

    return new Promise((resolve, reject) => {
      if (this.users[uid] && this.users[uid].Student.schools) {

        resolve(this.users[uid].Student.schools)
      } else {
        this.dataService.sendws('Schools/getSchoolUDFromUid', {_id: uid}).then(data => {
          this.users[uid].Student.schools = data['result']
          this.users[uid].Student.schools.sort(this.sortingSchools)
          resolve(data)
        })
      }

    })
  }

  getRecommendation(uid) {
    return new Promise((resolve, reject) => {
      this.dataService.sendws('Recommendation/getRecommendations', {
        uid:   uid,
        type:  1,
        query: {status: 1}
      }).then(data => {

        this.users[uid].recommendations = data['res']
        resolve(data)
      })

    })

  }

  sortingSchools(a, b) {
    if (a.curently == true) {
      return 1
    }
    if (b.curently == true) {
      return 1
    }

    if (a.endDateYear == b.endDateYear) {
      if (a.endDateMonth > b.endDateMonth) {
        return -1
      }
      if (a.endDateMonth < b.endDateMonth) {
        return 1
      }
      return 0
    }
    //expectedGraduationMonth
    //expectedGraduationYear


    if (parseInt(a.endDateYear) > parseInt(b.endDateYear)) {
      return -1
    }
    if (parseInt(a.endDateYear) < parseInt(b.endDateYear)) {
      return 1
    }
    //return a.startDateYear-b.startDateYear
    return 0
  }

  getUsers(usersIds: string[]) {
    return new Promise((resolve, reject) => {
      var req_user: User[] = []
      var usquery          = []
      for (let i = 0; i < usersIds.length; i++) {
        if (this.users.hasOwnProperty(usersIds[i])) {
          req_user.push(this.users[usersIds[i]])
        } else {
          usquery.push(usersIds[i])
        }
      }
      let items = [
        'Address',
        'Company',
        'Student',
        'Photo',
        'photo',
        'Settings',
        'currentLang',
        'email',
        'first_name',
        'last_name',
        'online',
        'Location',

        'phone',
        'contact_info',
        'ready_to_work',
        'type_profile',
        'displayAs',
        '_id',

        'birthday',
        'rating',
        'last_visit',
        'proof',
        'userDataComplited'
      ];
      if (usquery.length > 0) {
        this.dataService.sendws('User/getUsers', {
          ids:   usquery,
          items: items
        }).then(data => {
          var addedU = []
          for (var i = 0; i < data['users'].length; i++) {
            let user             = this.prepareUser(data['users'][i])
            this.users[user._id] = user
            addedU.push(this.users[data['users'][i]._id])
          }
          resolve(addedU.concat(req_user))
          //this.getOnline(usquery).then(data=>{
          //  resolve(addedU.concat(req_user))
          //})
        })
      } else {
        resolve(req_user)
      }
    });
  }

  getSchoolNames(id) {
    var str = []
    for (var i = 0; i < this.users[id].Student.schools.length; i++) {
      str.push(this.users[id].Student.schools[i].schoolName)
    }
    return str.join(', ')
  }

  prepareUser(u) {
    let photo = '/assets/img/persona-logo.svg';
    switch (u.type_profile) {
      case 'Student':
        photo = '/assets/img/persona-logo.svg';
        break;
      case 'Company':
        photo = '/assets/img/business-logo.svg';
        break;
      case 'Admin':
        photo = '/assets/img/logo.svg';
        break;
      case 'Educator':
        photo = '/assets/img/school-logo.svg';
        break;
    }
    if (u.hasOwnProperty('photo'))
      if (u.photo.length)
        u.photo = '//' + this.host + ':' + this.port + u.photo[0].url
      else u.photo = photo
    else u.photo = photo
    //u.online = false
    return u;
  }

  yearsOld(uid) {
    return this.users[uid].birthday?new Date().getFullYear() - new Date(this.users[uid].birthday * 1000).getFullYear():'';
  }

  getCountRecommFromUid(uid) {
    return new Promise((resolve, reject) => {
      this.dataService.sendws('Recommendation/getCountRecommFromUid', {uid: uid}).then(data => {
        resolve(data['res'])
      })
    });
  }

  setProfile(user: User) {
    if (this.initProfile) {
      this.first_profile = user
    } else {

    }


    this.profile           = user

    this.preferCompactView = typeof user.preferCompactView !== "undefined"?user.preferCompactView:false

    this.userDataComplited = this.profile.userDataComplited || this.GetPrsUserdataComplete(true)
    !this.profile.currentCountry ? this.profile.currentCountry = 0 : null
    if (this.profile.photo) {
      this.profile.photo = this.dataService.HOST_STATIC + ':' + this.dataService.PORT_STATIC + this.profile.photo
    }
    this.type = this.profile.type_profile
    if (this.type == 'Student')
      if (!this.profile.Student.language)
        this.profile.Student.language = []
    this.getMainPhoto(this.profile)

    this.users[this.profile._id] = this.profile
    this.initaccessPackage()
    this.initLang()

    setTimeout(function () {
      var text = this.translate.get('errors.' + 48)['value'].replace(/<email>/g, this.profile.email)
      if (this.profile.email)
        if (this.profile.email_trusted !== 1 && confirm(text)) {
          this.dataService.sendws('User/sendEmailToVerify', {})

        }
    }.bind(this), 1000)


  }

  setPreferCompactView(value){
    this.preferCompactView = value
    this.profile.preferCompactView = value
    this.editProfile({'preferCompactView':value});
  }

  initaccessPackage() {
    if (this.profile.type_profile == 'Company' && (!this.profile.service || !this.profile.service.active)) {
      this.profile.service.active = {
        orderId:        null,
        packageId:      null,
        periodId:       null,
        vacancy:        0,
        premiumListing: 0,
        hightFrame:     0,
        insocialMedia:  0,
        targetGroup:    0,
        bannerofEmail:  0,
        internship:     0
      }
    }
    if (this.profile.type_profile == 'Company' && this.profile.service.active.orderId && this.profile.service.orders && this.profile.service.orders.length > 0) {

      for (var i = 0; i < this.profile.service.orders.length; i++) {

        if (this.profile.service.orders[i]._id == this.profile.service.active.orderId) {
          for (var j = 0; j < this.dataService.packages.length; j++) {
            if (this.dataService.packages[j]._id + '' == this.profile.service.orders[i].service.subscription.packageId + '') {
              this.accessPackage = <IaccessPackage>{
                publish:    this.dataService.packages[j].settings.publish,
                applicants: this.dataService.packages[j].settings.applicants,
                rating:     this.profile.service.orders[i].status > 0 && this.dataService.packages[j].settings.rating ? true : false,
                search:     this.profile.service.orders[i].status > 0 && this.dataService.packages[j].settings.search ? true : false,
                notify:     this.profile.service.orders[i].status > 0 && this.dataService.packages[j].settings.notify ? true : false,
                match:      this.profile.service.orders[i].status > 0 && this.dataService.packages[j].settings.match ? true : false,
                projBmark:  this.dataService.packages[j].settings.projBmark,

              }
            }
          }
        }
      }
    }
  }

  GetPrsCompanyInformationComplete() {
    var all = 0
    var i   = 0
    var UD  = this.dataService.options.CompletePrc.companyinfo[this.profile.type_profile]
    for (var key in UD) {
      if (typeof UD[key] == 'object') {
        for (var key2 in UD[key]) {
          if (this.profile[key] != undefined && this.profile[key][key2] !== undefined && this.profile[key][key2] !== '') {
            if (Array.isArray(this.profile[key][key2])) {
              if (this.profile[key][key2].length > 0) {
                i += UD[key][key2]
              }
            } else {
              i += UD[key][key2]
            }
          }
          all += UD[key][key2]
        }
        continue
      }
      all += UD[key]
      if (this.profile[key] !== undefined && this.profile[key] !== '') {
        i += UD[key]
      }
    }
    return Math.floor(i * 100 / all)
  }

  GetPrsUserdataComplete(s = false) {
    var all = 0
    var i   = 0
    var UD  = this.dataService.options.CompletePrc.userdata[this.profile.type_profile]
    for (var key in UD) {
      if (typeof UD[key] == 'object') {
        for (var key2 in UD[key]) {
          if (this.profile[key] && this.profile[key][key2] != undefined && this.profile[key][key2] !== '') {
            i += UD[key][key2]
          }
          all += UD[key][key2]
        }
        continue
      }
      all += UD[key]
      if (this.profile[key] != undefined && this.profile[key] != '') {
        i += UD[key]
      }
    }
    if (!s) {
      return Math.floor(i * 100 / all)
    }
    return all


  }

  initLang() {

    if (this.profile.hasOwnProperty('currentLang')) {
      this.dataService.initLangFromId(this.profile.currentLang)
    }
  }

  setSkills(skills: Skills) {
    this.profile.skills.push(skills)
    this.dataService.sendws("User/addSkills", {skills: skills});
  }

  private assignProfile(newObj) {
    let profile = this.profile
    for (var key in newObj) {
      if (typeof newObj[key] === 'string' || typeof newObj[key] === 'number') {
        profile[key] = newObj[key]
        continue
      } else if (typeof newObj[key] === 'object') {
        profile[key] = this.assignProfile(newObj[key])
        continue
      }
    }
    return profile
  }

  editProfile(newObj: any,callback = function (data) {}) {

    //this.profile = this.assignProfile(newObj)

   if (newObj.newemail == this.profile.email||newObj.newemail == this.profile.newemail) {
      delete newObj.newemail
    }else{
     this.profile.newemail = newObj.newemail
   }
    this.dataService.sendws("User/editProfile", newObj).then(data=>{
      if (data['userDataComplited']){

        this.userDataComplited = data['userDataComplited']
      }
      if (data['email']) {
        this.popS.show_info(this.dataService.getOptionsFromId('errors', 50).text);
      }
      if (data['data']){
          this.profile = Object.assign(this.profile,data['data'])
      }

      callback(data)
    });
  }

  setPhoto(newObj: any) {
    //this.profile = this.assignProfile(newObj)
    this.dataService.sendws("User/setPhoto", newObj);
  }

  editCompany(newObj: any) {
    this.dataService.sendws("User/editProfile", {Company: newObj});
  }

  getProfile() {
    return this.profile
  }

  private jwt() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      let headers = new Headers({'Authorization': 'Bearer ' + currentUser.token});
      return new RequestOptions({headers: headers});
    }
  }

  setSavedSearches(search) {
    if (typeof this.profile.savedSearches === "undefined")
      this.profile.savedSearches = [];
    console.log('array length', this.profile.savedSearches.length)
    if (!this.profile.savedSearches.length) {
      this.profile.savedSearches.push(search)
      return;
    }
    let index: number = this.profile.savedSearches.findIndex((e, i, a) => {
      return (e._id == search._id) ? true : false;
    })
    if (index == -1)
      this.profile.savedSearches.push(search)
    else
      this.profile.savedSearches[index] = search
  }

  iSBookmark(vacId) {
    for (var i = 0; i < this.profile.bookmarks.items.length; i++) {
      if (this.profile.bookmarks.items[i].resumeId == vacId) {
        return true
      }

    }
    return false
  }

  setBookmark(id: string) {
    if (!this.accessPackage.projBmark && this.profile.type_profile == 'Company') {
      this.popS.show_warning(this.dataService.getOptionsFromId('errors', 10).text)
      return
    }
    this.popS.show_info(this.dataService.getOptionsFromId('errors', this.profile.type_profile == 'Company' ? 13 : 15).text);
    this.dataService.sendws("Events/setEvent", {
      event: {
        name:   'setBookmarkResume',
        option: {_id: id}
      }
    }).then((data) => {

    })
    var savedFrom = 1


    this.router.url.toString().indexOf('/resume/search') > -1 ? savedFrom = 0 : null;

    this.dataService.sendws('User/setBookmarks', {
      id:        id,
      savedFrom: savedFrom
    }).then((data) => {
      //this.profile.bookmarks = data
      this.profile.bookmarks.items.push(data['bookmark'])
    })
  }

  unsetBookmark(id: string) {
    if (!this.accessPackage.projBmark && this.profile.type_profile == 'Company') {
      this.popS.show_warning(this.dataService.getOptionsFromId('errors', 10).text)
      return
    }
    this.popS.show_info(this.dataService.getOptionsFromId('errors', this.profile.type_profile == 'Company' ? 14 : 16).text);
    //this.profile.bookmarks.items.splice(this.profile.bookmarks.items.indexOf(id), 1)
    for (var i = 0; i < this.profile.bookmarks.items.length; i++) {
      if (this.profile.bookmarks.items[i].resumeId == id) {
        this.profile.bookmarks.items.splice(i, 1)
        break
      }
    }
    this.dataService.sendws('User/unsetBookmark', {id: id}).then((data) => {
      //this.profile.bookmarks = data
    })
  }

  checkBookmark(id: string) {
    if (!this.profile.bookmarks) return false;
    if (typeof this.profile.bookmarks.items === "undefined") return false;
    for (var i = 0; i < this.profile.bookmarks.items.length; i++) {
      if (this.profile.bookmarks.items[i].resumeId == id) {
        return true
      }
    }
    return false
    //return (this.profile.bookmarks.items.findIndex((e,i,a)=>{return (e==id)?true:false;}) > -1)?true:false;
  }

  changePassword(currentPass, newPass, repeatPass) {
    // mess.currentPass
    // mess.newPass
    // mess.repeatPass

    if (
      currentPass
      && newPass
      && repeatPass
      && newPass.length > 5
      && repeatPass.length > 5
      && newPass == repeatPass
    ) {
      this.dataService.sendws('User/changePassword', {
        currentPass: currentPass,
        newPass:     newPass,
        repeatPass:  repeatPass
      }).then((data) => {
        if (data["changePassword"]) {
          this.popS.show_success(this.dataService.getOptionsFromId('errors', 22).text)
        }
      })
    } else {
      if (!currentPass) {
        this.popS.show_warning(this.dataService.getOptionsFromId('errors', 7).text);
      }
      if (newPass != repeatPass) {
        this.popS.show_warning(this.dataService.getOptionsFromId('errors', 8).text);
      }
      if ((!newPass || newPass.length < 6) || (!repeatPass || repeatPass.length < 6)) {
        this.popS.show_warning(this.dataService.getOptionsFromId('errors', 9).text);
      }
    }
  }

  unsetOAuth(data) {
    data = data ? data : {}
    data = Object.assign(data, {_id: this.profile._id})
    this.dataService.sendws('Authentication/unsetOAuth', data).then((data) => {
      console.log(data)
      if (data.hasOwnProperty('error')) {
        console.log(data['error']);
      } else {
        delete this.profile.oAuth;
        this.oAuth.emit(false);
      }
    })
  }

  checkPass(next) {
    this.dataService.sendws('User/checkPass', {}).then((data) => {
      if (!data['isset']['pass']) this.notPass.emit(true)
      next(data['isset'])
    })
  }

  checkOAuth() {
    if (this.profile.hasOwnProperty('oAuth'))
      this.oAuth.emit(true)
    else this.oAuth.emit(false)
  }

  setOAuth() {
    // login without options
    const options: LoginOptions = {
      scope:                   'public_profile,email',
      return_scopes:           true,
      enable_profile_selector: true
    };
    this.fb.login(options)
      .then((response: LoginResponse) => {
        if (response.authResponse) {
          let uid         = response.authResponse.userID,
              accessToken = response.authResponse.accessToken;
          this.fb.api('/me?fields=id,first_name,last_name,email,website,gender,picture').then((res) => {
            console.log('fb me', res);
            this.dataService.sendws('Authentication/setOAuth', {
              oauth_type:  'fb',
              oauth_id:    uid,
              accessToken: accessToken
            }).then((data) => {
              console.log(data)
              if (data.hasOwnProperty('data')) {
                this.profile.oAuth = data['data'];
                this.oAuth.emit(true);
              }
            })
          }).catch(e => console.error('Error ', e))
        }
      })
      .catch(e => console.error('Error logging in'));
    // this.oAuth.emit(true);
    // this.dataService.sendws('Authentication/unsetOauth', { _id:this.profile._id }).then((data) => {
    //   console.log(data)
    //   this.profile.oauth = null;
    // })
  }

  setSmartId(data) {
    this.profile = Object.assign(this.profile, data)
    window['dataLayer'].push({
      'event': 'personal_id_autification'
    });
  }


}
