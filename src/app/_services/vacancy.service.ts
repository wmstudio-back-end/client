
import {
  EventEmitter,
  Injectable
}                         from "@angular/core";
import {IVacancy}         from "../_models/vacancy";
import {User}             from "../_models/user";
import {DataService}      from "./data.service";
import {UserService}      from "./user.service";
import {DatePipe}         from "@angular/common";
import {HelpersService}   from "./helpers.service";
import {Router}           from "@angular/router";
import {Filter}           from "../_models/filter";
import {SaveSearch}       from "../_models/saveSearch";
import {resolve}          from "url";
import {DownPopupService} from "./down-popup.service";


@Injectable()
export class VacancyService {
  occupationField       = []
  vacancyType           = []
  sort: any
  vEmit: any            = new EventEmitter<IVacancy[]>();
  uEmit: any            = new EventEmitter<User[]>();
  vacancy: any          = new EventEmitter();
  users: any            = new EventEmitter();
  filter: Filter
  lastChange: string    = ''
  changeFilterElem: any;
  countItems: number    = 0;
  filterV               = {}
  sortV                 = {}
  skip                  = 0;
  limit                 = 0;
  tipCountTimer: any;
  tipElem: any;
  clear: boolean        = false;
  keyWordsFields: any   = {}
  lastChangeOption: any = {}
  saveSearch: SaveSearch
  keywordsFields        = {
    text:    [
      'positionTitle',
      'shotDescription',
      'weOffer',
      'requirements',
      'roleTasks',
    ],
    // My expectations / Job options - Interested in job (Types)
    options: {
      otherSpecificSkills: 'keySkills.id',
      occupationField:     ['occupationField'],
      fieldActivity:       'fieldActivity',
      characterTraits:     'Desired_personality_traits.id'
    },
  }

  constructor(private dataService: DataService,
              private userService: UserService,
              public help: HelpersService,
              private popUp: DownPopupService,
              private router: Router) {
    this.dataService.messages["EVENT/publicVacancy"].subscribe(this.eventApplyVacancy.bind(this))
    this.dataService.messages["EVENT/subscribeToVacancy"].subscribe(this.eventSubscribeToVacancy.bind(this))
    this.dataService.messages["EVENT/updateVacancy"].subscribe(this.eventUpdateVacancy.bind(this))
    this.filter         = {
      skip:    0,
      query:   {},
      limit:   8,
      sort:    {
        'activInstruction.premiumListing': -1,
        'published_at':   -1
      },
      items:   [],
      options: {
        occupationField:  {},
        vacancyType:      {
            0: true,
            1: true,
            2: true,
            3: true,
            4: true
        },
        vacancyTypeSlice: {
          2: true,
          3: true,
          4: true
        },
        countItems:       {},
        fieldActivity:    {},
        languages:        {},
        educationLevel:   {},
        workExp:          {},
        workTime:         {},
        salary:           {},
        address:          'Estonia',
        keywords:         '',
        reqLanguages:     {},
        minSalary:        0,
        olderThan:        4
      }
    }
    this.saveSearch     = {
      name:       '',
      period:     0,
      alertEmail: true,
      count:      0,
      count_new:[],
      count_total:[],
      count_unread:[]
    }
    this.keyWordsFields = {
      fieldActivity:   this.dataService.options.fieldActivity,
      occupationField: this.dataService.options.occupationField,
      // 'keySkills.text':true,
    }
  }

  // setViweVacancy(_id){
  //   this.dataService.sendws("Events/setEvent", {event: {name: 'viweVacancy', option: {_id: _id}}})
  //     .then((data) => {
  //
  //     })
  //
  // }
  eventSubscribeToVacancy(data){
    this.popUp.show_info('на вашу вакансию откликнулись');
    this.userService.profile.subscribeCandidate.push(data)

  }
  eventUpdateVacancy(data){
     this.popUp.show_info('вакансия изменена администратором');
    for (var i = 0; i < this.userService.profile.Company.vacancy.length; i++) {
      if (this.userService.profile.Company.vacancy[i]._id==data._id){
        this.userService.profile.Company.vacancy[i] = data
      }

    }
  }
  eventApplyVacancy(data){
    this.popUp.show_info('вакансия прошла модерацию');
    for (var i = 0; i < this.userService.profile.Company.vacancy.length; i++) {
      if (this.userService.profile.Company.vacancy[i]._id==data._id){
        this.userService.profile.Company.vacancy[i] = data
      }

    }
  }
  changePublish(e, vacancy,callback= function (data) {}) {

    if (e&&vacancy.deadLine < this.help.getUnixTimestamp()||vacancy.deadLine==0) {
      this.popUp.show_success(this.dataService.getOptionsFromId('errors', 21).text);
      return
    }
    var isAction = false
    if (
      (e&&vacancy.published_at<this.help.getUnixTimestamp())
      &&(!this.userService.profile.service
      ||!this.userService.profile.service.active
      ||!this.userService.profile.service.active.packageId)
    ){

      //this.popUp.show_info(this.dataService.getOptionsFromId('errors',2).text)
      //this.popUp.show_info(this.dataService.getOptionsFromId('errors',49).text)
      isAction = true
      e=false

    }else{
      for (var i = 0; i < this.userService.profile.service.orders.length; i++) {
        if (this.userService.profile.service.orders[i]._id==this.userService.profile.service.active.orderId){
          if (this.userService.profile.service.orders[i].status==1){

          }else{
            this.popUp.show_info(this.dataService.getOptionsFromId('errors',49).text)


            isAction = true
            e=false
          }
        }
      }
    }


    let valid = true

    valid ? valid = this.help.isValidObject('languageID',vacancy) : null
    valid ? valid = this.help.isValidObject('positionTitle',vacancy) : null
    valid ? valid = this.help.isValidObject('fieldActivity',vacancy) : null
    valid ? valid = this.help.isValidObject('type',vacancy) : null
    valid ? valid = this.help.isValidObject('occupationField',vacancy) : null
    valid ? valid = this.help.isValidObject('deadLine',vacancy) : null
    if (vacancy.type_temp==1) {
      valid ? valid = this.help.isValidObject('address',vacancy) : null
      valid ? valid = this.help.isValidObject('Worktime',vacancy) : null
      /**
       * В создании вакансии убрать из числа обязательных к заполнению данных следующие:
       * 1. Поле требуемые языки. Сейчас требуется ввести минимум один язык. http://joxi.ru/eAOEMXRfxaNKKm
       */
      // valid ? valid = this.help.isValidObject('LanguagesRequired',vacancy) : null
      // valid ? valid = this.help.isValidObject('deadLine',vacancy) : null
      // valid ? valid = vacancy.showInfo == 1 ? this.help.isValidObject('contactEmail',vacancy) : true : null
      valid ? valid =
        (vacancy.descriptionStep == 1 && this.help.isValidObject('roleTasks',vacancy))
        || (vacancy.descriptionStep == 2 && this.help.isValidObject('uploadFiles',vacancy))
        || (vacancy.descriptionStep == 3 && this.help.isValidObject('vacancyUrl',vacancy))
        : null
    }
    if (vacancy.type_temp==2) {
      valid ? valid = this.help.isValidObject('languageID',vacancy) : null
      valid ? valid = this.help.isValidObject('positionTitle',vacancy) : null
      valid ? valid = this.help.isValidObject('fieldActivity',vacancy) : null
      valid ? valid = this.help.isValidObject('occupationField',vacancy) : null
      valid ? valid = this.help.isValidObject('type',vacancy) : null
      valid ? valid = this.help.isValidObject('proofUploadFiles',vacancy)||this.help.isValidObject('proofUrl',vacancy) : null

      // this.vacancy.proofUploadFiles.length > 0 || this.vacancy.proofUrl ?  valid = true : valid = false;
    }
    if (vacancy.aplicatCont){
      if (!this.help.isValidUrl(vacancy.aplicatCont)){
        vacancy.aplicatCont = "http://"+vacancy.aplicatCont
      }

    }


    vacancy.publish = e
    if (!vacancy.publish&&vacancy.proof==1){
      vacancy.proof = 0
    }
    if (!valid) {
      this.popUp.show_info(this.dataService.getOptionsFromId('errors', 11).text);
      callback(false)
      return
    }
    callback(true)
    this.dataService.sendws('Vacancy/updateVacancy', {vacancy: vacancy}).then((data) => {

      if (data['vacancy']) {


        var isNewVacancy = true
        var popup = 0
        if (data['vacancy']) {
          for (var i = 0; i < this.userService.profile.Company.vacancy.length; i++) {
            if (data['vacancy']._id == this.userService.profile.Company.vacancy[i]._id) {
              isNewVacancy = false

              this.userService.profile.Company.vacancy[i] = data['vacancy']

              break;
            }
          }
          if (isNewVacancy) {
            this.userService.profile.Company.vacancy = [data['vacancy'],...this.userService.profile.Company.vacancy]

          }
          if (data['vacancy'].proof == 1 && data['vacancy'].publish == true && data['vacancy'].type_temp == 1) {
            popup = 17
          }
          if (data['vacancy'].proof == 1 && data['vacancy'].publish == true && data['vacancy'].type_temp == 2) {
            popup = 52
          }
          if (data['vacancy'].proof == 2 && data['vacancy'].publish == true) {
            popup = 18
          }
          if (data['vacancy'].proof == 2 && data['vacancy'].publish == false) {
            popup = 19
          }
          if (data['vacancy'].proof == 0 && data['vacancy'].publish == true && data['vacancy'].type_temp == 2) {
            popup = 52
          }
          if (popup>0){
            this.popUp.show_success(this.dataService.getOptionsFromId('errors', popup).text);
          }
        }
      }
      if (data['profile']) {
        let type = 'job'
        if (data['vacancy'].type == 0 || data['vacancy'].type == 1){
          type = 'internship'
        }
        /**
         * @type dataLayer
         */
        window['dataLayer'].push({
          'type': type,
          'event': 'vacancy_used'
        });
        this.userService.profile.service.active = data['profile']
      }

      if (data['error']) {
        alert(data['error'].message)
        console.log(data['error'])
      }
    })


  }

  saveSearches(obj) {


    var id  = undefined
    var uid = undefined
    if (typeof this.saveSearch['_id'] !== "undefined")
      id = this.saveSearch['_id']
    if (typeof this.saveSearch['uid'] !== "undefined")
      uid = this.saveSearch['_id']
    this.saveSearch = obj
    if (typeof id !== "undefined") this.saveSearch._id = id
    if (typeof id !== "undefined") this.saveSearch.uid = uid
    this.saveSearch.options = this.filter.options
    this.saveSearch.filter  = JSON.stringify(this.filter.items)
    this.saveSearch.count   = this.countItems
    this.saveSearch.query   = JSON.stringify(this.filter.query)
    this.dataService.sendws("User/saveSearch", this.saveSearch).then((data) => {

      this.saveSearch.query  = this.filter.query
      this.saveSearch._id    = data['_id']
      this.saveSearch.filter = this.filter.items
      this.saveSearch = <SaveSearch>data
      this.userService.setSavedSearches(data)
    })
  }

  setSaveSearch(id?) {
    if (!id) {
      this.saveSearch = {
        name:         '',
        period:       0,
        alertEmail:   true,
        count:        0,
        count_new:    [],
        count_total:  [],
        count_unread: []
      }
      return;
    }
    if (!this.userService.profile.savedSearches.length) return;
    let obj = this.userService.profile.savedSearches.find((e, i, a) => {
      return (e._id == id) ? true : false
    });
    if (!obj) return;
    obj.filter          = (typeof obj.filter === "string") ? JSON.parse(obj.filter) : obj.filter;
    obj.query           = (typeof obj.query === "string") ? JSON.parse(obj.query) : obj.query;
    this.saveSearch     = obj
    this.filter.options = obj.options
    this.filter.items   = obj.filter
    this.filter.query   = obj.query
  }

  getVacancyFromId(id) {
    return new Promise((resolve, reject) => {
      this.dataService.sendws("Vacancy/getVacancy", {
        skip:  0,
        limit: 0,
        query: {_id: id},
        sort:  {}
      }).then((data) => {
        return resolve(this.prepareVacancy(data['vacancy'][0]))
      })
    });
  }

  getVacancyFromIds(ids) {
    return new Promise((resolve, reject) => {
      this.dataService.sendws("Vacancy/getVacancyFromIds", {

        ids: ids,

      }).then((data) => {
        var v = data['vacancy']
        for (var i = 0; i < data['vacancy'].length; i++) {
          data['vacancy'][i] = this.prepareVacancy(data['vacancy'][i])

        }
        // return resolve(this.prepareVacancy(data['vacancy'][0]))
        return resolve(this.prepareVacancy(data))
      })
    });
  }

  clearFilter() {
    this.filter         = {
      skip:    0,
      query:   {},
      limit:   8,
      sort:    {
        'activInstruction.premiumListing': -1,
        'published_at':   -1
      },
      items:   [],
      options: {
        occupationField:  {},
        vacancyType:      {
          0: true,
          1: true,
          2: true,
          3: true,
          4: true
        },
        vacancyTypeSlice: {
          2: true,
          3: true,
          4: true
        },
        countItems:       {},
        fieldActivity:    {},
        languages:        {},
        educationLevel:   {},
        workExp:          {},
        workTime:         {},
        salary:           {},
        address:          'Estonia',
        keywords:         '',
        reqLanguages:     {},
        minSalary:        0,
        olderThan:        4
      }
    }
    this.clear          = !this.clear
    console.log('clear filter', this.clear)
    this.removeTip()
    this.getVacancy()
  }

  setFilterOption(key, val) {
    this.filter.options[key] = val
    this.lastChangeOption    = {}
    this.lastChangeOption[0] = key

    console.log(this.lastChangeOption)
    console.log(key, val)
  }

  getQuery(val) {
    let items = this.filter.items
    if (items.length == 0) {
      this.filter.query = {};
      return;
    }
    for (let i in items) {

    }
    var i = []
    for (var k in val) {
      i.push(val[k].id)
    }
  }

  setQueryItem(val) {
    let values
    let unset: boolean = false
    if (val.key == '$and$or' || val.key == '$text' || val.operand == 'query') {
      this.setFilter(val.key, val.values, val.operand)
      return;
    }
    switch (typeof val.values) {
      case 'string':
        // console.log('string',val)
        values = val.values
        break;
      case 'number':
        // console.log('number',val)
        values = val.values
        break;
      case 'object':
        // console.log('object',val)
        if (Array.isArray(val.values)) {
          if (val.values.length == 0) {
            this.unserFilter(val.key, val.operand);
            unset = true;
          } else {
            values = []
            for (var k = 0; val.values.length > k; k++) {
              values.push(val.values[k].id)
            }
            /**
             * set correct value for education
             */
            if (val.key == 'minEducation') {
              if (values && values.length) {
                let vals = [];
                for (let i = 0; i < values.length; i++) {
                  switch (values[i]) {
                    //Primary
                    case 0:
                      vals = vals.concat([
                        0,
                        1
                      ]);
                      break;
                    //Secondary
                    case 1:
                      vals = vals.concat([
                        2,
                        3
                      ]);
                      break;
                    //Vocational
                    case 2:
                      vals = vals.concat([
                        4,
                        5
                      ]);
                      break;
                    //Higher
                    case 3:
                      vals = vals.concat([
                        6,
                        7,
                        8,
                        9,
                        10,
                        11,
                        12,
                        13
                      ]);
                      break;
                  }
                }
                values = vals
              }
            }
          }
        } else {
          if (typeof val.values['id'] !== "undefined")
            values = val.values.id
          else {
            this.unserFilter(val.key, val.operand)
            unset = true
          }
        }
        break;
      default:
        console.log('default', val)
        this.unserFilter(val.key, val.operand)
        unset = true
        break;
    }
    if (!unset)
      this.setFilter(val.key, values, val.operand)
  }

  setFilter(key, val, operand?) {
    switch (operand) {
      case '$in':
        console.log(key, val)
        this.filter.query[key] = {'$in': val}
        break;
      case '$all':
        console.log(key, val)
        this.filter.query[key] = {'$all': val}
        break;
      case '$and':
        this.setFilterAnd(key, val)
        break;
      case '$gte':
        this.filter.query[key] = {'$gte': val}
        break;
      case '$text':
        this.filter.query[key] = val
        break;
      case '=':
        this.filter.query[key] = val
        break;
      case 'query':
        this.filter.query[key] = val
        break;
      case '$and$or':
        this.setFilterAnd('$or', val)
    }
    // console.log('filter options',this.filter.options)
    // console.log('query',this.filter.query)
    this.getCountVacancies(true)
  }

  setFilterAnd(key, val) {
    if (typeof this.filter.query['$and'] === "undefined")
      this.filter.query['$and'] = [];
    else {
      let i = this.filter.query['$and'].findIndex((e, i, a) => {
        return Object.keys(e)[0] == key
      })
      if (i > -1) this.filter.query['$and'].splice(i, 1)
    }
    let obj  = {}
    obj[key] = val
    this.filter.query['$and'].push(obj);
    // console.log(this.filter.query)
    this.getCountVacancies(true)
  }

  unserFilter(key, operand?) {
    switch (operand) {
      case '$and':
        let index = this.filter.query['$and'].findIndex((e, i, a) => {
          return (Object.keys(e)[0] == key) ? true : false
        })
        if (index > -1) this.filter.query['$and'].splice(index, 1)
        break;
      case '$and$or':
        let index1 = this.filter.query['$and']
        if (index1)
          index1.findIndex((e, i, a) => {
            return (Object.keys(e)[0] == key) ? true : false
          })
        else break;
        if (index1 > -1) delete this.filter.query['$and'][index1]
        break;
      default:
        if (typeof this.filter.query[key] !== "undefined")
          delete this.filter.query[key]
        break;
    }
    // console.log(key+' delet from filter')
    console.log('this.filter', this.filter)
    this.getCountVacancies()
  }

  setSort(key, val) {
    this.filter.sort[key] = val
  }

  getVacancy() {
    this.dataService.sendws("Vacancy/getVacancy", {
      skip:  this.filter.skip,
      limit: this.filter.limit,
      query: this.filter.query,
      sort:  this.filter.sort
    }).then((data) => {
      if (data['count']==0){
        this.popUp.show_success(this.dataService.getOptionsFromId('errors',32).text)
      }
      console.log('data', data)
      this.prepareVacancies(data)
    })
  }

  getCountVacancies(elem?) {
    this.removeTip()
    this.dataService.sendws("Vacancy/getCountVacancy", {query: this.filter.query}).then((data) => {
      // console.log('count vacancies',data)
      if (typeof data['count'] !== "undefined") {
        this.countItems = parseInt(data['count'])
        if (!elem) return true;
        if (typeof this.changeFilterElem === "undefined") return true;
        if (!this.changeFilterElem.checked) return true;
        let label = this.changeFilterElem.nextElementSibling;
        this.showInputCount(label)
      }
    })
  }

  showInputCount(parent) {
    console.log("parent:", parent)
    let tipElem        = document.createElement('p')
    tipElem.className  = 'checkbox__number'
    tipElem.innerHTML  = '' + this.countItems
    this.tipElem       = parent.appendChild(tipElem)
    this.tipCountTimer = setTimeout(() => {
      this.removeTip()
    }, 3000, this)
  }

  removeTip() {
    if (typeof this.tipElem !== "undefined") {
      this.tipElem.remove()
      this.tipElem = undefined
    }
    clearTimeout(this.tipCountTimer)
  }

  private prepareVacancies(data) {
    if (data.hasOwnProperty('vacancy')) {
      var usersId = [];
      let date    = new DatePipe('en-US');
      data['vacancy'].forEach((e, i, a) => {
        data['vacancy'][i].deadLineTime = e.deadLine + 86400 * 3
        // data['vacancy'][i].updated_at = date.transform(e.updated_at * 1000, 'dd.MM.yyyy')
        // data['vacancy'][i].published_at = date.transform(e.published_at * 1000, 'dd.MM.yyyy')
        // data['vacancy'][i].created_at = date.transform(e.created_at * 1000, 'dd.MM.yyyy')
        // data['vacancy'][i].deadLine = date.transform(e.deadLine * 1000, 'dd.MM.yyyy')

        if (usersId.indexOf(e.uid) == -1)
          usersId.push(e.uid);
        data['vacancy'][i].bookmark = this.userService.checkBookmark(e._id)
      });
      if (typeof data['count'] !== "undefined")
        this.countItems = parseInt(data['count'])
      this.vacancy.emit(data)
      this.userService.getUsers(usersId).then((data) => {
        this.users.emit({})
      })
    }
  }

  getPostedTime(timestamp) {
    console.log(timestamp)
    let postDate = new Date(timestamp),
        curDate  = new Date(),
        Y,
        M,
        D,
        h,
        m
    Y            = curDate.getFullYear() - postDate.getFullYear()
    M            = curDate.getMonth() - postDate.getMonth()
    D            = curDate.getDate() - postDate.getDate()
    h            = curDate.getHours() - postDate.getHours()
    m            = curDate.getMinutes() - postDate.getMinutes()
    if (Y) return Y + ' year';
    if (M) return M + ' month';
    if (D) {
      if (D >= 7)
        return Math.floor(D / 7) + ' week'
      return D + ' day';
    }
    if (h) return h + ' hour';
    if (m) return m + ' min';
  }

  private prepareVacancy(vacancy) {

    var usersId        = [];
    let date           = new DatePipe('en-US');
    vacancy.created_at = (vacancy.created_at) ? vacancy.created_at : ''
    vacancy.deadLine   = (vacancy.deadLine) ? vacancy.deadLine : ''
    if (vacancy.contract) {
      // vacancy.contract.start = date.transform(vacancy.contract.start.time * 1000, 'dd.MM.yyyy')
      // vacancy.contract.end = date.transform(vacancy.contract.end.time * 1000, 'dd.MM.yyyy')
      if (vacancy.contract.hasOwnProperty('start') && vacancy.contract.start) {
        vacancy.contract.start = date.transform(vacancy.contract.start.time * 1000, 'dd.MM.yyyy')
      }
      if (vacancy.contract.hasOwnProperty('end') && vacancy.contract.end) {
        vacancy.contract.end = date.transform(vacancy.contract.end.time * 1000, 'dd.MM.yyyy')
      }
    }
    vacancy.bookmark = this.userService.checkBookmark(vacancy._id)
    if (usersId.indexOf(vacancy.uid) == -1)
      usersId.push(vacancy.uid);
    this.userService.getUsers(usersId).then((data) => {
      this.users.emit({})
    })
    return vacancy;
  }

  setVacancy(vacancys: IVacancy[]) {

    for (var a in vacancys) {
      this.vacancy[vacancys[a]['_id']] = vacancys[a]
    }
    this.vEmit.emit(this.vacancy)
  }

  reqVacancy(skip, limit) {
    this.dataService.sendws("Vacancy/getVacancy", {
      skip:  skip,
      limit: limit,
      query: this.filter.query,
      sort:  this.filter.sort
    }).then((data) => {
      this.setVacancy(data['vacancy'])
    })
  }

  getArrayItemByKey(key) {
    let array = this.filter.items
    let i     = array.findIndex(e => {
      return e.key == key;
    })
    return (i > -1) ? array[i] : {};
  }

  unsetFilterItem(key) {
    let i = this.filter.items.findIndex(e => {
      return e.key == key;
    })
    if (i > -1) this.filter.items.splice(i, 1)
    this.unserFilter(key)
  }

  setFilterItem(val) {
    let array = this.filter.items
    // console.log(array)
    let i     = array.findIndex((e, i, a) => {
      return e.key == val.key;
    })
    if (i > -1) array[i] = val
    else array.push(val)
    this.filter.items = array
    // console.log(this.filter.items)
    this.setQueryItem(val)
  }

  setKeyWords(val) {
    if (!val) {
      console.log('remove $text')
      this.unsetFilterItem('$text')
      this.unserFilter('$text')
      return;
    }

    let values       = [];
    val              = val.replace('/\s/g', '===').replace('/[,.]/g', '===').split('===').filter((e, i, a) => {
      return !!e;
    })
    let text: string = '.*';
    let regText      = '';
    for (let i = 0; i < val.length; i++) {
      text += (i == 0) ? '' : '.';
      regText += (i == 0) ? '' : '|';
      text += val[i] + '.*';
      regText += val[i];
    }
    this.unsetFilterItem('$text')
    if (val.length) {
      let search = [];
      for (let i = 0; i < this.keywordsFields.text.length; i++) {
        let obj                          = {}
        obj[this.keywordsFields.text[i]] = {
          $regex:   text,
          $options: 'i'
        };
        search.push(obj);
      }
      let keys = Object.keys(this.keywordsFields.options);
      for (let i = 0; i < keys.length; i++) {
        if (typeof this.keywordsFields.options[keys[i]] == 'string') {
          let obj = {}
          var res = this.dataService.getOptionId(keys[i], regText);
          if (res.length) {
            obj[this.keywordsFields.options[keys[i]]] = {$in: res};
            search.push(obj);
          }
        } else {
          var res = this.dataService.getOptionId(keys[i], regText);
          for (let k = 0; k < this.keywordsFields.options[keys[i]].length; k++) {
            let obj = {}
            if (res.length) {
              obj[this.keywordsFields.options[keys[i]][k]] = {$in: res};
              search.push(obj);
            }
          }
        }
      }
      this.setFilterItem(this.getFilterItem('$text', '$text', {$or: search}))
    }
  }

  getFilterItem(key, op, val) {
    return {
      key:     key,
      operand: op,
      values:  val
    }
  }

  getRegExpString(items: any) {
    let text: string = '.*';
    for (let i = 0; i < items.length; i++) {
      text += (i == 0) ? '' : '.';
      text += items[i] + '.*';
    }
    return text;
  }

}
