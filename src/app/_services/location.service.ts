import { Injectable } from '@angular/core';
import {DataService} from "./data.service";
import {Headers, Http, RequestOptions} from "@angular/http";
import {UserService} from "./user.service";
@Injectable()
export class LocationService {
  public location:any;
  constructor(public dataService:DataService,public userService:UserService) {
    this.location = {};
    // this.dataService.sendws("Locations/getLocations", location).then(data => {
    //   if (typeof data === "object"){
    //     for(var e in data){
    //       var el = data[e];
    //       this.location[el.type] = el;
    //     }
    //   }
    //   // if (data.hasOwnProperty('ok')){
    //   //   data = (data['ok'] == 1)? location : false;
    //   // } else if (!data.hasOwnProperty('_id'))
    //   //   data = false;
    // });
  }

  public setLocation(name){
    let location = this.location[name]
    if (typeof location !== "undefined" && location !== null){
      location.type = name;
      this.dataService.sendws("Locations/setLocation", location).then(data => {
        if (data.hasOwnProperty('ok')){
          data = (data['ok'] == 1)? location : false;
        } else if (!data.hasOwnProperty('_id'))
          data = false;
        if (data !== false)
          this.location[name] = data;
      });
    }
  }

}
