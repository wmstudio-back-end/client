import {
  EventEmitter,
  Injectable
}                         from '@angular/core';
import {Ng2DeviceService} from 'ng2-device-detector';
import {DataService}      from "./data.service";
import {TranslateService} from '@ngx-translate/core';
import {DownPopupService} from "./down-popup.service";

export interface item {
  key: string,
  value: any
}

Array.prototype['indexObject']        = function (key, val) {
  for (var i = 0; i < this.length; i++) {
    if (this[i].hasOwnProperty(key) && this[i][key] == val) {
      return i
    }
  }
  return -1
}
Array.prototype['countObjectfromVal'] = function (ob) {
  var c = 0
  for (var i = 0; i < this.length; i++) {
    var add = true
    for (var key in ob) {
      if (this[i][key] != ob[key]) {
        add = false
      }
    }
    if (add) {
      c++
    }
  }
  return c
}

@Injectable()
export class HelpersService {
  public device: any
  public date: Date = new Date()
  public hideMenu   = new EventEmitter<boolean>()
  public ie: boolean = false;

  constructor(public deviceService: Ng2DeviceService, private ds: DataService, public tS: TranslateService,public popUp:DownPopupService) {
    this.device = this.deviceService.getDeviceInfo()
    window['ie_browser'] = false
    if (this.device.browser == 'ie'){
      this.ie = true
      window['ie_browser'] = true
      console.log('is internet explorer',this.ie);
    }
  }

  getUnixTimestamp() {
    return Math.round(new Date().getTime() / 1000);
  }

  timeConverter(UNIX_timestamp) {
    var a      = new Date(UNIX_timestamp * 1000);
    var months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ];
    var year   = a.getFullYear();
    var month  = months[a.getMonth()];
    var date   = a.getDate();
    var hour   = a.getHours();
    var min    = a.getMinutes();
    var sec    = a.getSeconds();
    var time   = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
    return time;
  }

  Math_ceil(i) {
    return Math.ceil(i)
  }

  isMobile() {
    if (this.checkAgent('mobile')) return true
    if (window.outerWidth < 768) return true
    return false
  }

   isUrlValid(userInput) {
  var res = userInput.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
  if(res == null)
    return false;
  else
    return true;
}
  findObject(arr, key, value) {
    if (!arr) {
      return -1
    }
    var i = 0;
    while (i < arr.length) {
      if (arr[i][key] == value) {
        return i
      }
      i++;
    }
    return -1;
  }

  checkAgent(a) {
    let mobile  = [
      'iphone',
      'andriod',
      'mobile',
      'windows-phone'
    ]
    let tablet  = [
      'ipad',
      'andriod',
      'tablet',
      'windows-tablet'
    ]
    this.device = this.deviceService.getDeviceInfo()
    if (a == 'mobile') {
      if (!this.device) return false
      if (this.device.hasOwnProperty('device') && mobile.indexOf(this.device) != -1)
        return true
    } else if (a == 'tablet') {
      if (this.device.hasOwnProperty('device') && tablet.indexOf(this.device) != -1)
        return true
      return false
    } else if (a == 'desktop') {
      if (this.device.hasOwnProperty('device') && tablet.indexOf(this.device) == -1 && mobile.indexOf(this.device) == -1)
        return true
    }
  }

  getYears(start, end, reverse?: boolean) {
    let nowfullYear = new Date().getFullYear()

    let startYear = nowfullYear - start
    nowfullYear   = nowfullYear + end
    let list: any = this.yearGenerate(startYear, nowfullYear)
    if (reverse)
      return list.reverse()
    else return list
  }

  confirm(id) {

    if (confirm(this.tS.get('systemalert.' + id)['value'])) {
      return true
    }
    return false
  }

  alert(id) {
    this.popUp.show_warning(this.tS.get('systemalert.' + id)['value']);
    // alert(this.tS.get('systemalert.' + id)['value'])

  }

  toInt(e) {
    return e
  }
  isValidObject(str,object) {

    var r = false
    if (str.indexOf('.') > -1) {
      var t = str.split('.')

      if (object.hasOwnProperty(t[0]) && object[t[0]].hasOwnProperty(t[1]) && Object.keys(object[t[0]][t[1]]).length > 0) {
        r = true
      }
    }
    switch (typeof object[str]) {
      case 'string':
        if (object.hasOwnProperty(str) && object[str] != "") {
          r = true
        }
        break;
      case 'number':
        if (object.hasOwnProperty(str)) {
          r = true
        }
        break;
      case 'object':

        if (object.hasOwnProperty(str) && object[str] && Object.keys(object[str]).length > 0) {
          r = true
        }


        break;


    }
    return r

  }
  randomString(length) {

    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

    if (!length) {
      length = Math.floor(Math.random() * chars.length);
    }

    var str = '';
    for (var i = 0; i < length; i++) {
      str += chars[Math.floor(Math.random() * chars.length)];
    }
    return str;
  }

  yearGenerate(startYear, endYear) {
    let arr = [];
    if (!startYear) {
      startYear = new Date().getFullYear() - 20
    }
    if (!endYear) {
      endYear = new Date().getFullYear()
    }
    for (let i = 0; i <= endYear - startYear; i++) {
      arr.push(
        {
          id:   startYear + i,
          text: startYear + i
        })
    }

    return arr.reverse();
  }

  month = [
    {
      "id":   1,
      "text": '01'
    },
    {
      "id":   2,
      "text": '02'
    },
    {
      "id":   3,
      "text": '03'
    },
    {
      "id":   4,
      "text": '04'
    },
    {
      "id":   5,
      "text": '05'
    },
    {
      "id":   6,
      "text": '06'
    },
    {
      "id":   7,
      "text": '07'
    },
    {
      "id":   8,
      "text": '08'
    },
    {
      "id":   9,
      "text": '09'
    },
    {
      "id":   10,
      "text": '10'
    },
    {
      "id":   11,
      "text": '11'
    },
    {
      "id":   12,
      "text": '12'
    }

  ]


  startSmartIdLogin() {
    function gup(url, name) {
      name        = name.replace(/[[]/, "\[").replace(/[]]/, "\]");
      var regexS  = "[\?&]" + name + "=([^&#]*)";
      var regex   = new RegExp(regexS);
      var results = regex.exec(url);
      if (results == null)
        return "";
      else
        return results[1];
    }

    //Oauth login window for Smart ID service, use correct client_id value here.
    var loginUri = 'https://id.smartid.ee/oauth/authorize?client_id=IHRsGQcdl9tAz8EoBh8yL00bvOIeNg8O&redirect_uri=https%3A%2F%2Ftalents.fund%3A8888%2Fverify&response_type=code';

    //calculate login window size and position on screen
    var w    = 640;
    var h    = 640;
    var left = (screen.width / 2) - (w / 2);
    var top  = (screen.height / 2) - (h / 2);

    //open login window where user is identified
    var win = window.open(loginUri, "Smart ID login", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    //set timer and check response after every 100ms
    var pollTimer = window.setInterval(function () {
      try {
        console.log(win.document.URL);

        //if url contains code then user has been returned
        if (win.document.URL.indexOf("code") !== -1) {

          //clear polling job
          window.clearInterval(pollTimer);
          var url = win.document.URL;

          //get authentication code value
          var code = gup(url, 'code');

          //close the login popup window
          win.close();

          //use the code and redirect to the url that handles the server side login
          //from this on the process is regular Oauth login flow.
          window.location.href = "https://example.com/handlelogin?code=" + code;
        }
      } catch (e) {
      }
    }, 100);
  }


  getEvalationSystem(system, val) {
    if (!val) return null
    let step: number = system / 5
    for (let i = 5; i >= 0; i--) {
      if (val <= system) {
        system -= step
      } else {
        return i + 1
      }
    }
  }

  disWinScroll(element) {
    if (element.offsetHeight > this.getChildrenHeight(element))
      return false;
    element.addEventListener('DOMMouseScroll', function (e) {
      let e0    = e;
      let delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    })
    element.addEventListener('mousewheel', function (e) {
      let e0    = e;
      let delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    })
  }

  getChildrenHeight(parent) {
    if (parent.childCount < 1)
      return 0;
    let height: number = 0;
    parent.childNodes.forEach((e, i, a) => {
      if (e.nodeName != '#text')
        height += e.offsetHeight
    })
    return height;
  }

  getArrayItemByKey(array, key) {
    let i = array.findIndex((e, i, a) => {
      return (e.key == key) ? 1 : 0;
    })
    return (i > -1) ? array[i] : {};
  }

  setArrayItemByKey(array, val) {
    let i = array.findIndex((e, i, a) => {
      return (e.key == val.key) ? 1 : 0;
    })
    if (i > -1) array[i] = val
    else array.push(val)
    return array;
  }

  getFilterItem(key, op, val) {
    return {
      key:     key,
      operand: op,
      values:  val
    }
  }

  ucFirst(str) {
    if (!str) return str;
    return str[0].toUpperCase() + str.slice(1);
  }

  replaceAndUp(str, word) {
    return this.ucFirst(str.replace(word, '').replace(' ', ''))
  }

  getYear() {
    return this.date.getFullYear()
  }

  getMonth() {
    return this.date.getMonth()
  }

  getShort(str, type) {
    if (type == 'edu') return str.replace(' education', '')
  }

  dateCompare(date1: Date, date2?: Date) {
    date2    = (date2) ? date2 : this.date
    let date = {
      Y: 0,
      M: 0,
      D: 0,
      h: 0,
      m: 0,
      s: 0
    }
    date.Y   = Math.abs(date1.getFullYear() - date2.getFullYear())
    date.M   = Math.abs(date1.getMonth() - date2.getMonth())
    date.D   = Math.abs(date1.getDate() - date2.getDate())
    date.h   = Math.abs(date1.getHours() - date2.getHours())
    date.m   = Math.abs(date1.getMinutes() - date2.getMinutes())
    date.s   = Math.abs(date1.getSeconds() - date2.getSeconds())
    return date
  }

  getStaticUrl(url) {
    return '//' + this.ds.HOST_STATIC + ':' + this.ds.PORT_STATIC + url
  }

  workTimeIcon(int) {
    let returnProp = "";
    switch (int) {
      case 0:
        returnProp = "ic_timelapse_blue";
        break;
      case 1:
        returnProp = "ic_tonality_birch";
        break;
      case 2:
        returnProp = "autorenew-black";
        break;
      case 3:
        returnProp = "ic_brightness_3_purple";
        break;
      case 4:
        returnProp = "ic_swap_calls_green";
        break;
      default:
        returnProp = "ic_timelapse_blue";
    }

    return returnProp;
  }


  getTranslate(key: string) {
    return this.tS.instant(key)
  }

  verifyLength(max, str) {
    if (str && str.length > max) {
      str = str.substring(0, max)
    }
    return str

  }

  checkerInputs(defaults) {
    return new function () {
      this.defaultsValues = defaults ? defaults : {};

      /**
       * check change values or value
       * @param items
       * @param item | if isset than check only it change value
       */
      this.checkValues = function (items: any, item?: item) {
        if (item && this.defaultsValues.hasOwnProperty(item.key)) {
          return JSON.stringify(this.defaultsValues[item.key]) != JSON.stringify(item.value) ? true : false;
        } else if (items) {
          return JSON.stringify(this.defaultsValues) != JSON.stringify(items) ? true : false;
        }
        return true;
      }
    }
  }


  isValidUrl(str) {
    try {
      new URL(str);
      return true;
    } catch (_) {
      return false;
    }
  }

  getRegExpString(items: any) {
    let text: string = '.*';
    for (let i = 0; i < items.length; i++) {
      text += (i == 0) ? '' : '.';
      text += items[i] + '.*';
    }
    return text;
  }

  /**
   * Pattern
   * @param items
   * @returns {string}
   */
  getPatternString(items: any) {
    let text: string = '';
    for (let i = 0; i < items.length; i++) {
      text += (i == 0) ? '' : '|';
      text += items[i];
    }
    return text;
  }

  /**
   * Like ^(?=.*Россия)(?=.*Воткинск).*$
   * @param $items
   */
  getPatternInString(items: any) {
    let text: string = '^'
    for (let i = 0; i < items.length; i++) {
      text += '(?=.*' + items[i] + ')'
    }
    return text + '.*$';
  }

  /**
   * Get query param from url
   */
  getParameterByName(name: string, url?: string) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  getLogo() {
    return "";
  }

  loadPhoto(){
    this.checkIe();
    console.log(arguments);
    console.log(this.device);
    console.log(this.ie);
  }

  checkIe(){
    if (!this.ie && this.device.browser == 'ie'){
      this.ie = true
      window['ie_browser'] = true
      console.log('is internet explorer',this.ie);
    }
  }

}
