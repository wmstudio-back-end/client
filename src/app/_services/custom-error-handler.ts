import {
  ErrorHandler,
  Injectable
} from '@angular/core';
import {DownPopupService} from "./down-popup.service";
import {DataService} from "./data.service";

@Injectable()
export class CustomErrorHandler extends ErrorHandler {
  constructor(private popS: DownPopupService) {

    super();

  }

  public handleError(error: any) {
    // You can add your own logic here.
    // It is not required to delegate to the original implementation
    super.handleError(error);
    //alert('Непредвиденная ошибка сценария, reload page ')
    //window.location.assign(window.location.toString())
    this.popS.show_warning('Ops! Something went wrong!')


    // console.log("EEEEEEEEERRRRRRRRRROOOOOORRRR",error)


  }

}
