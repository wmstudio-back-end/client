import {Injectable} from '@angular/core';
import {Icart, IcartPrice, Iservice, Isubscriptions} from "../_models/service";
import {DataService} from "./data.service";
import {UserService} from "./user.service";
import {DownPopupService} from "./down-popup.service";
import {HelpersService} from "./helpers.service";

@Injectable()
export class ShopService {
  public cart: Icart
  public cartPrice: IcartPrice = <IcartPrice>{
    TotalPrice: 0,
    subscription: 0,
    subscriptionDebt: 0,
    Discount: 0,
    service: {
      vacancy: 0,
      premiumListing: 0,
      hightFrame: 0,
      insocialMedia: 0,
      targetGroup: 0,
      bannerofEmail: 0
    }
  }
  public orderedServices = {
    vacancy: 0,
    premiumListing: 0,
    hightFrame: 0,
    insocialMedia: 0,
    targetGroup: 0,
    bannerofEmail: 0
  }
  public subscribe
  public allPrice = 0

  constructor(public dS: DataService, private uS: UserService, private popUp: DownPopupService, public hS: HelpersService) {
    this.onInit()
    this.dS.messages["EVENT/applyOrder"].subscribe(this.eventApplyCart.bind(this))
  }

  eventApplyCart(data) {
    this.popUp.show_info('вам подтвердили оплату счета');
    this.uS.profile.service = data

    this.initSubscribe()
    this.uS.initaccessPackage()
  }

  orderedCount() {
    for (var i = 0; i < this.uS.profile.service.orders.length; i++) {
      if (this.uS.profile.service.orders[i].status == 0) {
        this.uS.profile.service.orders[i].service.vacancy ? this.orderedServices.vacancy = this.uS.profile.service.orders[i].service.vacancy : null
        this.uS.profile.service.orders[i].service.premiumListing ? this.orderedServices.premiumListing = this.uS.profile.service.orders[i].service.premiumListing : null
        this.uS.profile.service.orders[i].service.hightFrame ? this.orderedServices.hightFrame = this.uS.profile.service.orders[i].service.hightFrame : null
        this.uS.profile.service.orders[i].service.insocialMedia ? this.orderedServices.insocialMedia = this.uS.profile.service.orders[i].service.insocialMedia : null
        this.uS.profile.service.orders[i].service.targetGroup ? this.orderedServices.targetGroup = this.uS.profile.service.orders[i].service.targetGroup : null
        this.uS.profile.service.orders[i].service.bannerofEmail ? this.orderedServices.bannerofEmail = this.uS.profile.service.orders[i].service.bannerofEmail : null
      }
    }
  }

  initSubscribe() {
    if (this.uS.profile.cart) {
      this.cart = <Icart>this.uS.profile.cart
    }
    if (this.uS.profile.type_profile == 'Company' && this.uS.profile.service) {

      this.uS.profile.service.orders.sort((a, b) => {
        return b.created_at - a.created_at
      })
      for (var i = 0; i < this.uS.profile.service.orders.length; i++) {


        if (!this.uS.profile.service.orders[i].service.hasOwnProperty('subscription')) {
          continue
        }
        if (!this.uS.profile.service.orders[i].service.subscription.packageId) {
          continue
        }
        if (this.uS.profile.service.active && this.uS.profile.service.active.orderId && this.uS.profile.service.orders[i]._id == this.uS.profile.service.active.orderId) {
          this.subscribe = this.uS.profile.service.orders[i]
        }
      }
    }
  }

  onInit() {
    this.clearCart()
    this.initSubscribe()
    this.getPriceCart()
    this.orderedCount()

  }

  cancelSubscribe() {
    this.dS.sendws('Service/cancelSubscribe', {}).then(order => {
      for (var i = 0; i < this.uS.profile.service.orders.length; i++) {
        if (order['order']._id == this.uS.profile.service.orders[i]._id) {
          this.uS.profile.service.orders[i] = order['order']
          this.subscribe = this.uS.profile.service.orders[i]
        }

      }

      //this.uS.profile.service.active.packageId = null
      //this.uS.profile.service.active.orderId = null
      //this.subscribe = null

      console.log(this.uS.profile.service)

    })

  }

  clearCart() {
    this.cartPrice.TotalPrice = 0
    this.cart = <Icart>{
      subscriptions: <Isubscriptions>{
        packageId: null,
        periodId: null
      },
      promocode: [],
      services: <Iservice>{
        vacancy: 0,
        premiumListing: 0,
        hightFrame: 0,
        insocialMedia: 0,
        targetGroup: 0,
        bannerofEmail: 0
      }
    }

  }

  objectIdtoInt(id) {
    return new Date(parseInt(id.substring(0, 8), 16) * 1000).getTime() / 1000 +
      new Date(parseInt(this.uS.profile._id.substring(0, 8), 16) * 1000).getTime() / 1000

  }

  getPackage(id) {
    for (var i = 0; i < this.dS.packages.length; i++) {
      if (this.dS.packages[i]._id == id) {
        return this.dS.packages[i]
      }
    }
    return this.getUserPackage()
  }

  getUserPackage() {

    for (var i = 0; i < this.uS.profile.service.orders.length; i++) {

      if (this.uS.profile.service.orders[i]._id == this.uS.profile.service.active.orderId) {
        return this.getPackage(this.uS.profile.service.orders[i].service.subscription.packageId)
      }
    }

    return this.dS.packages[0]

  }

  countCart() {
    var c = 0
    if (this.cart.subscriptions.packageId != undefined && this.cart.subscriptions.periodId != undefined) c++;
    for (var key in this.cart.services) {
      if (this.cart.services[key] > 0) c++

    }

    return c
  }

  public applyCart() {
    //this.cart.promocode.push('111')
    this.popUp.show_info(this.dS.getOptionsFromId('errors', 12).text);
    this.dS.sendws('Service/applyCart', {
      cart: this.cart,
      subscribe: this.subscribe
    }).then(service => {
      this.clearCart()
      this.uS.profile.service.orders.unshift(service['order'])
      this.orderedCount()
      if (service['order'].service.hasOwnProperty('subscription')) {
        this.subscribe = service['order']
        this.uS.profile.service.active.orderId = service['order']._id
      }
      if (service.hasOwnProperty('changedOrder') && service['changedOrder']) {
        for (var i = 0; i < this.uS.profile.service.orders.length; i++) {
          if (this.uS.profile.service.orders[i]._id == service['changedOrder']._id) {
            this.uS.profile.service.orders[i] = service['changedOrder']
          }
        }
      }
      this.clearCart()
      //this.subscribe = service['order']
      console.log("applyCartservice CART", service)
    })
  }


  priceSubscription(packageId, periodId, startdate, type = false) {
    var pack = null
    var price = 0
    for (var i = 0; i < this.dS.packages.length; i++) {
      if (this.dS.packages[i]._id == packageId) {
        pack = this.dS.packages[i]
      }
    }
    price = pack.price
    var msPerDay = 24 * 60 * 60 * 1000;
    var today: any = new Date(startdate * 1000)
    var lastDate: any = new Date(startdate * 1000)
    lastDate.setMonth(lastDate.getMonth() + periodId)
    lastDate.setDate(0)
    lastDate.setHours(23)
    lastDate.setMinutes(59)
    lastDate.setSeconds(59)
    //var daysLeft = Math.round((lastDate.getTime()  - today.getTime() )/msPerDay)-1;
    //console.log('daysLeft!!!!!!!!!!!!!!!!!!!!!!!!!',daysLeft)

    var today2: any = new Date(this.dS.options.server_time * 1000)
    var dayThisMon = new Date(today2.getFullYear(), today2.getMonth() + 1, 0).getDate()
    var day = today2.getDate()
    var daydeatach = dayThisMon - day

    if (type && periodId > 0) {
      periodId -= new Date(lastDate - today).getMonth()

    } else {
      if (periodId == 0) {
        periodId = 1
      }
      periodId--
    }
    var dayPrice = price / dayThisMon
    return parseFloat((price * (periodId) + dayPrice * daydeatach).toFixed(2));
  }

  addCart(order) {

    var incart = true
    var pack = this.getUserPackage()
    if (this.subscribe && this.subscribe.service && this.subscribe.service.subscription && order.hasOwnProperty('subscriptions')) {
      console.log(order.subscriptions.periodId)


      var t = null
      var n = null

      for (var i = 0; i < this.dS.packages.length; i++) {
        if (this.dS.packages[i]._id == this.subscribe.service.subscription.packageId) {
          t = this.dS.packages[i]
        }
        if (this.dS.packages[i]._id == order.subscriptions.packageId) {
          n = this.dS.packages[i]
          pack = this.dS.packages[i]
        }
      }
      console.log(this.subscribe.service.subscription.periodId)
      if (this.subscribe.service.subscription.periodId != 0 && order.subscriptions.periodId == 0) {
        incart = false
        this.hS.alert(8)
        //alert("не1111щего")
        return
      }
      if (n && t && n.price < t.price) {
        incart = false
        this.hS.alert(6)
        return
        // alert("нельзя перейти на пакет дешевле текущего")
      }
      if (n && t && n._id == t._id) {
        if (this.subscribe.service.subscription.periodId == order.subscriptions.periodId) {
          incart = false
          this.hS.alert(7)
          return
          // alert("выбран уже существующий пакет")
        }
        if (this.subscribe.service.subscription.periodId > 0 && order.subscriptions.periodId == 0) {
          incart = false
          this.hS.alert(8)
          return
          // alert("нельзя выбрать бессрочный период, пока не закончится текущий")
        }
        if (this.subscribe.service.subscription.periodId != 0 && order.subscriptions.periodId < this.subscribe.service.subscription.periodId) {
          incart = false
          this.hS.alert(9)
          return
          // alert("выбран период, больший расчетного периода оплаты")
        }
        console.log('TOT ', t)
        console.log('NEW ', n)
        console.log(this.subscribe.service.subscription.periodId)
        console.log(order.subscriptions.periodId)
      }
      console.log("TOTAL   ", this.subscribe)


      var priceT = this.priceSubscription(this.subscribe.service.subscription.packageId, this.subscribe.service.subscription.periodId, this.subscribe.created_at, true)
      console.log("текущий пакет стоит ", priceT)
      var priceN = this.priceSubscription(order.subscriptions.packageId, order.subscriptions.periodId, this.dS.options.server_time)
      console.log("выбранный пакет стоит ", priceN)
      //выписано счетов на сумму
      console.log("выписано неоплаченных счетов на сумму", this.cartPrice.subscriptionDebt)
      console.log("осталось доплатить:", priceN - this.cartPrice.subscriptionDebt)
      //this.cartPrice.subscriptionDebt


      if (incart && priceN - this.cartPrice.subscriptionDebt < 0) {
        incart = false
        this.hS.alert(10)
        // alert("выбран период, дешевле остатка текущего на  "+Math.floor(this.cartPrice.subscriptionDebt-priceN)+ " $")
      }


    }

    //console.log("priceT", this.priceSubscription(order.subscriptions.packageId,order.subscriptions.periodId,this.dS.options.server_time))

    if (order.hasOwnProperty('subscriptions') && incart) {
      this.cart.subscriptions = order.subscriptions;
    }
    if (order.hasOwnProperty('promocode') && order.promocode) {
      this.cart.promocode.push(order.promocode)
      this.cart.promocode = Array.from(new Set(this.cart.promocode))
    }

    if (order.hasOwnProperty('promocodeDel')) {
      this.cart.promocode.splice(order.promocodeDel, 1)
    }


    if (order.hasOwnProperty('services')) {

      for (var key in order.services) {
        this.cart.services[key] += parseInt(order.services[key])


      }
    }


    // this.getPriceCart()
    this.dS.sendws('Service/addCart', this.cart).then(cart => {
      console.log("SERVICE CART", this.cart)
      this.getPriceCart()
    })
    // if (incart){
    //    this.dS.sendws('Service/addCart',this.cart).then(cart=>{
    //     console.log("SERVICE CART",this.cart)
    //
    //   })
    // }


  }

  addPromo(el, component) {
    this.dS.sendws('Service/checkPromo', {code: el.value, cart: this.cart}).then(data => {
      console.log("SERVICE CHECK PROMO", data)
      if (data.hasOwnProperty('success') && data['success']) {
        this.addCart({promocode: el.value});
        el.value = ''
      } else {
        component.promoError = true
      }
    })
  }

  getPriceCart() {

    this.dS.sendws('Service/PriceCart', {
      cart: this.cart,
      subscribe: this.subscribe
    }).then(price => {
      this.cartPrice = <IcartPrice>price

    })

  }

  getPriceVacancy() {
    if (this.cart.subscriptions && this.cart.subscriptions.packageId) {
      for (var i = 0; i < this.dS.packages.length; i++) {
        if (this.dS.packages[i]._id == this.cart.subscriptions.packageId) {
          return this.dS.packages[i].settings.priceVacancy
        }
      }
    }
    if (this.subscribe && this.subscribe.service && this.subscribe.service.subscription) {
      for (var i = 0; i < this.dS.packages.length; i++) {
        if (this.dS.packages[i]._id == this.subscribe.service.subscription.packageId) {
          return this.dS.packages[i].settings.priceVacancy
        }
      }
    }
  }
}
