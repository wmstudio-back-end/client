"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
  };
Object.defineProperty(exports, "__esModule", {value: true});
var core_1 = require("@angular/core");
var account_student_component_1 = require("../components/__home/account/Student/account-student.component");
var account_company_component_1 = require("../components/__home/account/Company/account-company.component");
var account_admin_component_1 = require("../components/__home/account/Admin/account-admin.component");
var sidebar_link_company_component_1 = require("../components/__home/sidebar/sidebar-link/sidebar-link-company/sidebar-link-company.component");
var sidebar_link_student_component_1 = require("../components/__home/sidebar/sidebar-link/sidebar-link-student/sidebar-link-student.component");
var sidebar_link_admin_component_1 = require("../components/__home/sidebar/sidebar-link/sidebar-link-admin/sidebar-link-admin.component");
var educator_component_1 = require("../components/__home/account/Educator/educator.component");
var sidebar_link_educator_component_1 = require("../components/__home/sidebar/sidebar-link/sidebar-link-educator/sidebar-link-educator.component");
var ComponentService = (function () {
  function ComponentService(router, user) {
    this.router = router;
    this.user = user;
    this.componentSidebarLink = null;
    this.componentAccount = null;
    this.topFilter = false;
    this.Account = {
      Student: {
        component: account_student_component_1.AccountStudentComponent,
        inputs: {
          showNum: 9
        }
      },
      Company: {
        component: account_company_component_1.AccountCompanyComponent,
        inputs: {
          showNum: 9
        }
      },
      Admin: {
        component: account_admin_component_1.AccountAdminComponent,
        inputs: {
          showNum: 9
        }
      },
      Educator: {
        component: educator_component_1.AccountEducatorComponent,
        inputs: {
          showNum: 9
        }
      }
    };
    this.SidebarLink = {
      Student: {
        component: sidebar_link_student_component_1.SidebarLinkStudentComponent,
        inputs: {
          showNum: 9
        }
      },
      Company: {
        component: sidebar_link_company_component_1.SidebarLinkCompanyComponent,
        inputs: {
          showNum: 9
        }
      },
      Admin: {
        component: sidebar_link_admin_component_1.SidebarLinkAdminComponent,
        inputs: {
          showNum: 9
        }
      },
      Educator: {
        component: sidebar_link_educator_component_1.SidebarLinkEducatorComponent,
        inputs: {
          showNum: 9
        }
      }
    };
  }

  ComponentService.prototype.canActivate = function (route, state) {
    if (!this.SidebarLink.hasOwnProperty(this.user.profile['type_profile'])) {
      return false;
    }
    if (!this.Account.hasOwnProperty(this.user.profile['type_profile'])) {
      return false;
    }
    this.componentSidebarLink = this.SidebarLink[this.user.profile['type_profile']];
    this.componentAccount = this.Account[this.user.profile['type_profile']];
    route.data.topFilter == 0 ? this.topFilter = false : this.topFilter = true;
    return true;
  };
  return ComponentService;
}());
ComponentService = __decorate([
  core_1.Injectable()
], ComponentService);
exports.ComponentService = ComponentService;
