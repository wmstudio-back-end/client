import {
  CUSTOM_ELEMENTS_SCHEMA,
  ErrorHandler,
  NgModule
}                      from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule}   from "@angular/forms";
import {
  Http,
  HttpModule
}                      from "@angular/http";

import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {
  TranslateLoader,
  TranslateModule
}                                from "@ngx-translate/core";
import {TranslateHttpLoader}     from "@ngx-translate/http-loader";
import {ClickOutsideModule}      from "ng-click-outside";
import {PageScrollService}       from 'ng2-page-scroll';
import {AppComponent}            from "./app.component";
import {routing}                 from "./app.routing";

import {
  AlertService,
  AuthenticationService,
  DataService,
  UserService,
  WebsocketService,
  EmbedVideoService
} from "./_services/index";

import {StyleguideComponent}            from "./components/styleguide/styleguide.component";
import {ToolBar}                        from "./components/styleguide/toolbar/toolbar.component";
import {ToolBarService}                 from "./_services/toolbar.service";
import {ProgressPrcComponent}           from "./modules/progress-prc/progress-prc.component";
import {ProgressAccountComponent}       from "./modules/progress-account/progress-account.component";
import {ModulesModule}                  from "./modules/modules.module";
import {SwitcherService}                from "./_services/switcher.service";
import {ComponentService}               from "./_services/component.service";
import {ModalService}                   from "./_services/modal.service";
import {ModalModule}                    from "./components/modal/modal.module";
import {RequestRecommendationComponent} from "./components/modal/request-recommendation/request-recommendation.component";
import {WindowRef}                      from "./_services/window.service";
import {HttpService}                    from "./_services/http.service";
import {MyDatePickerModule}             from "mydatepicker";
import {MyDateRangePickerModule}        from "mydaterangepicker";
import {VacancyService}                 from "./_services/vacancy.service";
import {
  CookieModule,
  CookieService
}                                       from 'ngx-cookie';
import {HelpersService}                 from "./_services/helpers.service";

import {MessageService}          from "./_services/message.service";
import {LocationService}         from "./_services/location.service";
import {CoverLetterService}      from "./_services/cover-letter.service";
import {ResumeService}           from "./_services/resume.service";
import {NgxPaginationModule}     from "ngx-pagination";
import {Ng2DeviceDetectorModule} from 'ng2-device-detector';
import {CustomErrorHandler}      from "./_services/custom-error-handler";
import {PdfViewerModule}         from "ng2-pdf-viewer";
import {DownPopupService}        from "./_services/down-popup.service";
import {ShopService}             from "./_services/shop.service";
import {FacebookModule}          from 'ngx-facebook';
import {CommonModule}            from "@angular/common";
import {Ng2CompleterModule}      from "ng2-completer";
import {SelectModule}            from "ng2-select";
import {SidebarModule}           from "./components/sidebar/sidebar.module";
import {InlineSVGModule}         from 'ng-inline-svg';
// import {ClipboardModule} from "ngx-clipboard/dist";

import {TextMaskModule}          from "angular2-text-mask";

import {MenuTopHomeComponent} from "./components/menu-top-home/menu-top-home.component";
import {FooterComponent}      from "./components/footer/footer.component";

import {MyResumeViewsStatisticsComponent} from "./components/Tools/my-resume-views-statistics/my-resume-views-statistics.component";
import {FAQAndTipsComponent}              from "./components/Tools/faq-and-tips/faq-and-tips.component";
import {NewsComponent}                    from "./components/Tools/news/news.component";
import {NewsDetailComponent}              from "./components/news/news-detail/news-detail.component";
import {MatchCandidatesComponent}         from "./components/Tools/match-candidates/match-candidates.component";
import {NewsListComponent}                from "./components/news/news-list/news-list.component";

import {EducatorRecommendationsComponent}    from "./components/Tools/educator-recommendations/educator-recommendations.component";
import {EducatorMakeRecommendationComponent} from "./components/Tools/educator-make-recommendation/educator-make-recommendation.component";
import {OrganizationProfileComponent}        from "./components/Tools/organization-profile/organization-profile.component";
import {MessagesComponent}                   from "./components/messages/messages.component";

import {SavedSearchComponent}                from "./components/Tools/saved-search/saved-search.component";
import {CompanyDetailsComponent}             from "./components/Tools/company-details/company-details.component";
import {CoverLettersComponent}               from "./components/Tools/cover-letter/cover-letters-component";
import {CreateCoverLetterComponent}          from "./components/Tools/create-cover-letter/create-cover-letter-component";
import {ModalMakeRecommComponent}            from "./components/modal/modal-make-recomm/modal-make-recomm.component";
import {CompaniesRatingModalComponent}       from "./components/Tools/student-companies-ratings/companies-rating-modal/companies-rating-modal.component";
import {CompanyMyServicesComponent}          from "./components/Tools/company-my-services/company-my-services.component";
import {EducatorsListComponent}              from "./components/Tools/educators-list/educators-list.component";
import {OrganizationDetailsComponent}        from "./components/Tools/organization-details/organization-details.component";
import {InvoicesComponent}                   from "./components/Tools/invoices/invoices.component";
import {OrganizationStatisticsComponent}     from "./components/Tools/organization-statistics/organization-statistics.component";
import {HiddenVacancyComponent}              from "./components/Tools/hidden-vacancy/hidden-vacancy.component";
import {PublicCompanyRatingsComponent}       from "./components/Tools/public-company-ratings/public-company-ratings.component";
import {PublicCompanyRatingsSearchComponent} from "./components/Tools/public-company-ratings-search/public-company-ratings-search.component";
import {StudentCompaniesRatings}             from "./components/Tools/student-companies-ratings/student-companies-ratings.component";
import {VacancyModule}                       from "./components/vacancy/vacancy.module";
import {AccountModule}               from "./components/account/account.module";
import {CreatVacancyService}         from "./components/vacancy/create/create.service";
import {CreateResumeService}         from "./components/resume/create/create.service";
import {Router}                      from "@angular/router";
import {StudentVacancyListComponent} from './components/Tools/student-vacancy-list/student-vacancy-list.component';
import {
  HttpClient,
  HttpClientModule
}                                    from "@angular/common/http";
import {LoaderComponent}             from "./modules/loader/loader.component";

//import {NgPipesModule} from 'ngx-pipes';

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpss) {

  return new TranslateHttpLoader(httpss, "./assets/i18n/", ".json");
}
import { GoogleChartsModule } from 'angular-google-charts';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
// @ts-ignore
// @ts-ignore
@NgModule({

  imports:      [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    ClickOutsideModule,
    ModulesModule,
    Ng2DeviceDetectorModule.forRoot(),
    MyDatePickerModule,
    MyDateRangePickerModule,
    BrowserAnimationsModule,
    CookieModule.forRoot(),
    GoogleChartsModule.forRoot(),
    ScrollToModule.forRoot(),
    PdfViewerModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide:    TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps:       [HttpClient]
      }
    }),
    ModalModule,
    CommonModule,
    AccountModule,
    FormsModule,
    Ng2CompleterModule,
    SelectModule,

    ClickOutsideModule,
    ModulesModule,
    SidebarModule,
    NgxPaginationModule,
    VacancyModule,
    ModalModule,
    AccountModule,
    // ClipboardModule,
    TextMaskModule,
    FacebookModule.forRoot(),
    InlineSVGModule.forRoot(),
    GoogleChartsModule.forRoot()

  ],
  declarations: [
    AppComponent,
    StyleguideComponent,
    ToolBar,

    MenuTopHomeComponent,
    FooterComponent,


    MyResumeViewsStatisticsComponent,
    FAQAndTipsComponent,
    NewsComponent,
    // CandidateResumeComponent,
    NewsDetailComponent,
    MatchCandidatesComponent,
    NewsListComponent,

    EducatorRecommendationsComponent,
    EducatorMakeRecommendationComponent,
    OrganizationProfileComponent,
    MessagesComponent,

    SavedSearchComponent,
    CompanyDetailsComponent,
    CoverLettersComponent,
    CreateCoverLetterComponent,
    StudentCompaniesRatings,
    PublicCompanyRatingsSearchComponent,
    PublicCompanyRatingsComponent,
    HiddenVacancyComponent,
    OrganizationStatisticsComponent,
    InvoicesComponent,
    OrganizationDetailsComponent,
    EducatorsListComponent,
    CompanyMyServicesComponent,
    CompaniesRatingModalComponent,
    ModalMakeRecommComponent,
    StudentVacancyListComponent,
  ],
  providers:    [


    AlertService,
    AuthenticationService,
    UserService,
    WebsocketService,
    SwitcherService,
    ComponentService,
    ShopService,
    DownPopupService,
    ToolBarService,
    ProgressPrcComponent,
    ProgressAccountComponent,
    DataService,
    WindowRef,
    HttpService,
    EmbedVideoService,
    ModalService,
    VacancyService,
    ResumeService,
    CreatVacancyService,
    CookieService,
    CreateResumeService,
    HelpersService,
    MessageService,
    LocationService,
    PageScrollService,
    CoverLetterService,
    {
      provide:  ErrorHandler,
      useClass: CustomErrorHandler
    }


  ],
  exports:      [],
  schemas:      [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap:    [AppComponent]

})

export class AppModule {


}
