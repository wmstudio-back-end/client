import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {ClickOutsideModule} from "ng-click-outside";
import {TranslateModule} from "@ngx-translate/core";
import {FormsModule} from "@angular/forms";
import { AgmCoreModule } from '@agm/core';

import {ChartsModule} from "ng2-charts";
import {RouterModule} from "@angular/router";

import { NgxCarouselModule } from 'ngx-carousel';
import 'hammerjs';

import {AngularGooglePlaceModule} from 'angular-google-place';








@NgModule({
  imports: [
    CommonModule,
    ClickOutsideModule,
    TranslateModule,
    FormsModule,
    NgxCarouselModule,
    ChartsModule,
    AngularGooglePlaceModule,

    RouterModule,
    // AgmCoreModule.forRoot({
    //     apiKey: 'AIzaSyADL_-gNyss3Es4ZhCGYCE2-HxUpfGQ75M',
    //     // libraries: ['places']
    // })

  ],
  declarations: [




  ],
  exports: [



  ]
})
export class PopupModalsModule {
}
