import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output
} from '@angular/core';

import {Router}         from "@angular/router";
import {UserService}    from "../../_services/user.service";
import {DataService}    from "../../_services/data.service";
import {MessageService} from "../../_services/message.service";


@Component({
  selector:    'app-modal-recommendation',
  templateUrl: './modal-recommendation.component.html',
  styleUrls:   ['./modal-recommendation.component.scss']
})
export class ModalRecommendationComponent implements OnInit {
  @Input('modalRecomm') modal
  @Input() openModal                = false
  @Output() closeModal              = new EventEmitter<boolean>()
                        modalRecomm = {}
                        schoolName

  public load = false

  constructor(
    public us: UserService,
    public ds: DataService,
    public mS: MessageService,
    public router: Router,
  ) {

  }

  getSchoolName(schoolId) {
    this.ds.sendws('Schools/getSchoolFromId', {schoolId: schoolId}).then(data => {
      this.schoolName = data['result'].addr

    })


    //if (this.us.users[uid].Student.schools){
    //  for (var i = 0; i < this.us.users[uid].Student.schools.length; i++) {
    //    for (var j = 0; j < this.us.profile.Student.schools.length; j++) {
    //      if (this.us.profile.Student.schools[i].schoolId==uid){
    //        return this.us.profile.Student.schools[i].schoolName
    //      }
    //
    //    }
    //
    //  }
    //}
    return 'as'

  }

  ngOnChanges(ch) {
    if (ch.openModal) {
      if (this.modal) {
        this.modalRecomm = this.modal
        if (this.modal.schoolId) {
          this.getSchoolName(this.modal.schoolId)
        }
      }

      console.log("*************************", this.modalRecomm)
    }
  }

  ngOnInit() {
    //this.modalRecomm = this.modal


  }

}
