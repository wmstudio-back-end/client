var registerType = '1';
$(document).ready(function () {

    $(".LoginPopup-c input").focus(function(e) {
        $('header.fixed .mobileMenu').animate({
            scrollTop: $(e.target).offset().top
        }, 1000);
    });


  //------------------ivan-----------------------------
  $(window).scroll(function () {
    if ($(window).scrollTop() > $("header").height()) {
      $('header').addClass("fixed");
    }
    else {
      $('header').removeClass("fixed");
    }
  });
  //---------------------
  $(".hamburger ").click(function () {
    $("body").toggleClass('blackout hiden');// класс который добавляет затемнение
  });
  //---------------------
  $("header").css('height', $('.header-wrap').height());
  //------------------ivan-----------------------------

  var hamburger = document.getElementById('hamburger');
  hamburger.onclick = function () {
    this.classList.toggle('is-active');
    $('.mobileMenu').slideToggle(300);
  };
  $('.bsLink-item').click(function () {
    $('.bsLink-item').removeClass('active')
    $(this).addClass('active')
    registerType = $(this).attr('dir');

    $('.mainTab-box').removeClass('active')
    $('.mainTab-box[dir=' + $(this).attr('dir') + ']').addClass('active')
  });


  $('.regTabs__link').click(function () {
    $('.regTabs__link').removeClass('active')
    $(this).addClass('active')
    $('.regTabs__item').removeClass('active');
    $('.regTabs__item[dir=' + $(this).attr('dir') + ']').addClass('active');
  });

  $('#btnLogin, #btnLoginMb').click(function () {
    $('.optMenuLogin').toggle();

    $(".LoginPopup").toggleClass('active');
    $('.mobileMenu ').toggleClass('heightLp')
  });

  $('#selectCountryM, #selectLangM').click(function () {
    $('.mobileMenu ').toggleClass('heightCf')
  });

  $('.hamburger').click(function () {
    $(".LoginPopup").removeClass('active');
    $(".mobileMenu").removeClass('heightLp');

  });

  if (window.location.hash) {

    var hash = window.location.hash.substr(1);
    console.log(hash);
    $('.regTabs__link[dir=' + hash + ']').click();
  }

  $('.topMenu__link-h').click(function () {
    $('.topMenu__link-h').removeClass('active');
    $(this).addClass('active');
    $('.hiddenMenu-h-desktop').removeClass('active');
    $('.hiddenMenu-h-desktop[dir=' + $(this).attr('dir') + ']').addClass('active');

    // $("body").addClass('blackout hiden');
  });


  $('.blackoutTp').click(function () {
    $('.topMenu__link-h').removeClass('active');
    $('.hiddenMenu-h-desktop').removeClass('active');
  });


  $('.topLinkM').click(function () {
    $('.topLinkM').removeClass('active');
    $(this).addClass('active');
    $('.topLinkMDrop').removeClass('active');
    $('.topLinkMDrop[dir=' + $(this).attr('dir') + ']').addClass('active');
  });

  // SLIDER
  //


  $(document).click(function (e) {
    try {
      if (!$(".hiddenMenu-h-desktop").is(e.target) && $(".hiddenMenu-h-desktop").has(e.target).length === 0) {
        if (e.target.className.indexOf('topMenu__link-h') + 1) {
          return;
        }
        $(".hiddenMenu-h-desktop").removeClass('active')
        $('.topMenu__link-h').removeClass('active');
        // $("body").removeClass('blackout hiden');
      }
      if (!$(".optCountry-item .optMenu").is(e.target) && $(".optCountry-item .optMenu").has(e.target).length === 0) {

        if ((e.target.className.indexOf('optCountry__full') + 1)) {
          return;
        }
        $(".optCountry-item").removeClass('active');
        // $(".optCountry-item .optMenu").removeClass('active');
        // $('.topMenu__link-h').removeClass('active');
      }
    } catch (error) {
      console.log(error);
    }

  });


  $('.optCountry-item').click(function () {
    // $('.optCountry-item').removeClass('active');

    $(this).addClass("active");

    // if(!$(this).parent().parent().hasClass('active')){
    //     $(this).parent().parent().addClass('active');
    // }


  });
  $('.optCountry-itemM').click(function () {
    $('.optCountry-itemM .optMenu').removeClass('active');
    $('.optCountry-itemM .optMenu[dir=' + $(this).attr('dir') + ']').addClass('active');
  });

  //+ linkLs
  $(".linkLs").click(function () {
    $('.linkLs').removeClass('active');
    $(this).addClass('active')
  });
  //- linkLs
});

function tap_slider(d) {
  var arr = []
  for (var i = 0; i < $('.boxLs__link').length; i++) {
    arr.push($('.boxLs__link')[i])
  }
  d > 0 ? arr.unshift(arr.pop()) : arr.push(arr.shift())
  $('.boxLs-wrap').html(arr)
}






function registerForm(form) {
  var msg = $('#' + form).serialize();
  $.ajax({
    type: 'POST',
    url: 'http://' + window.location.hostname + ':8888',
    data: msg,
    success: function (data) {

      var data = JSON.parse(data)
      console.log(data)
      if (data.hasOwnProperty('error')) {
        alert(data.error.text)
      } else {
        if (data.data){
          document.cookie = "token=" + data.data.profile._id;
        }else{
          document.cookie = "token=" + data.profile._id;
        }

        window.location = '/';
      }

    },
    error: function (xhr, str) {
      console.error('Error: ' + xhr.responseCode);
    }
  });

}
