import { MUUUUUPage } from './app.po';

describe('muuuuu App', () => {
  let page: MUUUUUPage;

  beforeEach(() => {
    page = new MUUUUUPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
