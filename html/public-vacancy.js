function publicVacancy() {

}


publicVacancy.prototype.renderTemplate = function (data, company,randomVacancies ) {

    var tmpl = _.template(document.getElementById('vacancy-render').innerHTML);

    var result = tmpl(
                {
                    vacancy: data,
                    company: company,
                    randomVacancies : randomVacancies.data.res,
                    profile: profile,
                    subscribeExist:subscribeExist
                }
        );

    var wrapper = document.getElementById('vacancy-wrap');

    wrapper.innerHTML = result;

}


publicVacancy.prototype.shareInit = function(){
    new Ya.share({
        element: 'the_share',
        elementStyle: {
            'type': 'none',
            'quickServices': ['facebook', 'twitter', 'linkedin', 'gplus']
        },
        link: document.location.href,
        title: document.title
    });

    new Ya.share({
        element: 'mobile_share',
        elementStyle: {
            'type': 'none',
            'quickServices': ['facebook', 'twitter', 'linkedin', 'gplus']
        },
        link: document.location.href,
        title: document.title
    });
}


publicVacancy.prototype.parseDate = function(unix) {
    var date = new Date(unix*1000);
    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;

    var str = day + "." + month + "." + date.getFullYear();

    return str;

}


publicVacancy.prototype.getDeadlineDate = function(deadline) {
    var time =   deadline - window.PROFILE.options.server_time;
    var result = {
        time: 0,
        type:0,
        passed: false
    };


    if(time >= 1) {
        if(time > 86400) {
            result.time = ath.ceil(time / 86400);
            result.type = 'days'
        } else {
            result.time = Math.ceil(time / 3600)
            result.type = 'hours'
        }

    } else {
        result.passed = true;
    }

    console.log(result);
    return result;


}


publicVacancy.prototype.ratingStarResult = function(ratingValue) {
    var starsRating = [0,0,0,0,0];
    var star = ratingValue;
    let float = true;

    for(let i = 0; i < starsRating.length; i++) {

        if(star>=i+1){
            starsRating[i]=100;

        } else {
            starsRating[i]=0;
            if(float) {
                let ost = star-i;
                for(let b = 1; b <= 4; b++) {

                    if(1 / 4 * b >= ost) {
                        starsRating[i]= (1 / 4 * b - (1/4)) * 100;
                        break;
                    }
                };

            }
            float = false;
        }
    }

    var tmpl = '<% starsRating.forEach(function(staritem) { %>\
                        <div class="star yellow<%=staritem%>"></div>\
                    <% }) %>';


    return _.template(tmpl)({starsRating: starsRating});
}


publicVacancy.prototype.getCompanyNameForRandomVacancies = function(vacUID) {
    var result = {}
    randomVacanciesUser.data.users.forEach(function(item){
        if(item._id == vacUID) {
            result.nameCompany = item.Company.nameCompany;
            result.rating = item.Company.rating;


        }
    });

    return result;


}

publicVacancy.prototype.viewModal = function () {
    function openModal() {
        $('.applyModal').addClass('active');
    }
    function closeModal() {
        $('.applyModal').removeClass('active');
    }


    $('.callModal').on("click", function () {
        openModal()
    })

    $('.closeModal').on("click", function () {
        closeModal();
    })



}

publicVacancy.prototype.applyVacancy = function () {
    $('#applyVacancy').on("click",function () {
        var applyObject = {
            content: $('#applyModal #ApplyContent').val(),
            coverLetterId: $('#applyModal #coverLetter').attr('data-id'),
            resumeId: $('#applyModal #selectResume').attr('data-id'),
            vacancyId: vacancyData._id

        };

        sendMessages("Vacancy/subscribeToVacancy", applyObject).then(function(data){
            console.log("!!!",data)
        });






    })
}

publicVacancy.prototype.subscribeExist = function () {

    window.PROFILE.profile.subscribeVacancy.forEach(function (sub) {
        if(sub.vacancyId == vacancyData._id) {
            subscribeExist = true;
            console.log('уже отправлено');

        }


    })

    return false;



}

publicVacancy.prototype.select = function(){
    function closeSelect(el) {
        el.removeClass('active');
    }

    function openSelect(el) {
        el.addClass('active');
    }



    $('.utsInput').on("click", function (e) {
        openSelect($(this).next('.utsBox'));
    });

    $('.utsBox .mpsBox__cross').on("click", function (e) {
       closeSelect($(this).closest('.utsBox'));
    });

    $('.utsBox .utsBox-ul li').on("click", function (e) {
        var val = $(this).find('a').text();
        var dataAttr = $(this).find('a').attr('data-id');
        var input = $(this).closest('.unitySelect').find('.utsInput')
        console.log('dataAttr',dataAttr)


        input.val(val)
        input.attr('data-id', dataAttr);

        closeSelect($(this).closest('.utsBox'));
    });



}


var vacancyString = window.location.href.split("/").reverse()[0],
    vacancyId = [],
    request = 0,
    messages = {},
    socket,
    vacancyData,
    options,
    companyInf,
    randomVacancies,
    randomVacanciesUser,
    subscribeExist,
    profile,
    vacanciesUid = [];


    vacancyId.push(vacancyString);



var Vacancy = new publicVacancy();






document.addEventListener("DOMContentLoaded", function() {

    console.log('ws://'+window.PROFILE.HOST+':'+ window.PROFILE.PORT)
    socket = new WebSocket('ws://'+window.PROFILE.HOST+':'+ window.PROFILE.PORT)
    socket.onopen = function() {
        console.log("Соединение установлено.");




        sendMessages("Vacancy/getVacancyFromIds", {
            ids: vacancyId
        }).then(function(data){
            console.log("!!!",data)
        });






    };

    socket.onmessage = function(event) {

        console.log("Получены данные123 ", JSON.parse(event.data));
        var data = JSON.parse(event.data);
        if(data.method=='worker/Public/getCompany') {
            companyInf = data.data.res;


            console.log('инфо о компании', companyInf)

            sendMessages("Vacancy/getRandomVacancies", {}).then(function(data){
                console.log("рандом вакансииsssssssssssssssssss",data)

            })

        }

        else if(data.method=='worker/Vacancy/getVacancyFromIds') {
            console.log('вакансия', data);
            vacancyData = data.data.vacancy[0];

            sendMessages("Public/getCompany", {
                companyId: vacancyData.uid
            }).then(function(data){

                console.log("!!!",data)
            });



        }

        else if (data.method=='worker/Authentication/identify') {
            console.log('инфо о пользователе', data);
            options = data.data.options;
            profile = data.data.profile;

        }

        else if (data.method=='worker/Vacancy/getRandomVacancies') {

            randomVacancies = data;
            console.log(randomVacancies);


            randomVacancies.data.res.forEach(function(item){
                vacanciesUid.push(item.uid);
            });

            console.log('список юидов',vacanciesUid)

            sendMessages("User/getUsers", {
                ids: vacanciesUid,items:["Company.nameCompany","Company.rating"]
            }).then(function(data){
                console.log("!!!",data)
            });



        }

        else if (data.method=='worker/User/getUsers') {
            randomVacanciesUser = data;

            console.log('список коммпаний к рандомным вакансиям',randomVacanciesUser);
        }


        if (vacancyData&&companyInf&&randomVacancies&&randomVacanciesUser&&profile){
            // console.log("===vacancyData",vacancyData)
            // console.log("===companyInf",companyInf)
            // console.log("===randomVacancies",randomVacancies)
            // console.log("===randomVacanciesUser",randomVacanciesUser)



            Vacancy.subscribeExist();
            Vacancy.renderTemplate(vacancyData, companyInf, randomVacancies, subscribeExist);
            Vacancy.viewModal();
            Vacancy.shareInit();
            Vacancy.select();
            Vacancy.applyVacancy();



            // copy-link button

            var clipboard = new Clipboard('#copy-link', {
                text: function() {
                    return document.querySelector('#PageURL').value;
                }
            });
            clipboard.on('success', function(e) {

                e.clearSelection();
            });
            $("#PageURL").val(location.href);

            if (navigator.vendor.indexOf("Apple")==0 && /\sSafari\//.test(navigator.userAgent)) {
                $('#copy-link').on('click', function() {
                    var msg = window.prompt("Copy this link", location.href);

                });
            }




        }
    }





});


function sendMessages(method, data) {
    socket.send(JSON.stringify({"method":"worker/"+method,"data":data,"error":null,"requestId": request}))
    messages["worker/"+method] = {}
    return messages["worker/"+method][request] = new Promise(function(resolve, reject){
        request++;
        return resolve(data);
    })
}


function printPdf(){


    const elementToPrint = window.document.getElementById('vacancy_print');
    const pdf = new jsPDF('p', 'pt', 'a4');
    var specialElementHandlers = {
        '#editor': function (element, renderer) {
            return true;
        }
    };
    var options = {
        margin: {top: 10, right: 10, bottom: 10, left: 10, useFor: 'content'},
        'pagesplit':true,
        'elementHandlers': specialElementHandlers
    };

    let margins = {
        top: 100,
        bottom: 100,
        left: 40,
        width:750
    };
    pdf.addHTML(elementToPrint,10, 10,options, () => {
        pdf.save('web.pdf');
},margins);
}






