function publicCompanySearch() {

}

publicCompanySearch.prototype.renderTemplate = function (data, company,randomVacancies ) {

    var tmpl = _.template(document.getElementById('companies-render').innerHTML);

    var result = tmpl(
        {
            // vacancy: data,
            // company: company,
            // randomVacancies : randomVacancies.data.res,
            // profile: profile,
            // subscribeExist:subscribeExist
        }
    );

    var wrapper = document.getElementById('page-wrapper');

    wrapper.innerHTML = result;

    console.log('render')

}


publicCompanySearch.prototype.saveComment = function (data) {
    var commentObj = {};




    $('#sendRateButton').on('click', function(){
        commentObj.comment_recommend = $("#sendRate input[name=recommend]:checked").val() == 'yes' ? true : false;
        commentObj.comment_text = $("#sendRate textarea[name=comment_text]").val();
        commentObj.comment_author = window.PROFILE.profile._id;
        commentObj.rating_culture =  parseInt($('#culture .br-current-rating')[0].innerText);
        commentObj.rating_work =  parseInt($('#work .br-current-rating')[0].innerText);
        commentObj.rating_salary =  parseInt($('#salary .br-current-rating')[0].innerText);
        commentObj.rating_managment =  parseInt($('#managment .br-current-rating')[0].innerText);
        commentObj.rating_career =  parseInt($('#career .br-current-rating')[0].innerText);
        commentObj.comment_anonim =  $('#sendRate #anonim').prop('checked') ? true : false;
        commentObj.companyId =  companyId;

        $("#sendRate textarea[name=comment_text]").val('')
        console.log(commentObj);
        sendMessages("Public/newReview", commentObj).then(function(data){
            console.log("!!!",data)
        });

        $('#sendRate').removeClass('active');



    });











}







publicCompanySearch.prototype.ratingStarResult = function(ratingValue) {
    var starsRating = [0,0,0,0,0];
    var star = ratingValue;
    let float = true;

    for(let i = 0; i < starsRating.length; i++) {

        if(star>=i+1){
            starsRating[i]=100;

        } else {
            starsRating[i]=0;
            if(float) {
                let ost = star-i;
                for(let b = 1; b <= 4; b++) {

                    if(1 / 4 * b >= ost) {
                        starsRating[i]= (1 / 4 * b - (1/4)) * 100;
                        break;
                    }
                };

            }
            float = false;
        }
    }

    var tmpl = '<% starsRating.forEach(function(staritem) { %>\
                        <div class="star yellow<%=staritem%>"></div>\
                    <% }) %>';


    return _.template(tmpl)({starsRating: starsRating});
}


var vacancyString = window.location.href.split("/").reverse()[0],
    request = 0,
    messages = {},
    socket,
    options,
    profile,
    companyId;

var arrResult = [];


var CompanySearch = new publicCompanySearch();


document.addEventListener("DOMContentLoaded", function() {


    console.log('ws://'+window.PROFILE.HOST+':'+ window.PROFILE.PORT)
    socket = new WebSocket('ws://'+window.PROFILE.HOST+':'+ window.PROFILE.PORT)
    socket.onopen = function() {
        console.log("Соединение установлено.");

    };

    socket.onmessage = function(event) {

        console.log("Получены данные123 ", JSON.parse(event.data));
        var data = JSON.parse(event.data);
        if (data.method=='worker/Authentication/identify') {
            options = data.data.options;
            profile = data.data.profile;
            console.log(options,profile)

        } else if (data.method=='worker/Company/searchCompaniesFromName') {
            arrResult = data.data.company;




        }


    }


    CompanySearch.renderTemplate();
    CompanySearch.saveComment();
    //star rating select
    $(".comment-bar").barrating({
        theme: 'fontawesome-stars'
    });


    // обработка инпута

    var queryString = 's';

    var type;

    $('#input').keyup(function(e){

        queryString = $(this).val();



        if (queryString != ""){
            if(e.keyCode == 13 ) {
                $('#posBox').removeClass('active');

                sendMessages('Company/searchCompaniesFromName', {nameCompany: queryString, skip : 0, limit: 10, type: 1 })
                console.log("все",arrResult)
                var resultTemplate = '<% arr.forEach(function(item) { %>\
                       <div class="viewRating" >\
                    <!--<div class="adBox">ad</div> голубая рамка ad-->\
                    <div class="row-wrap">\
                    <div class="col-v4 col-t10 ">\
                    <div class="eyes-ic-wrap" >\
                    <i class="icon ic-visibility_off_gray"></i>\
                    <p class="minText grey">No logo</p>\
                </div>\
                <!--<img alt="" class="vacantBox__logo" src="/assets/img/logo/mcdonald-s-15.svg">-->\
                <div class="rating-wrap">\
                    <p class="vacantBox__boldText"><%=item.Company.nameCompany%></p>\
\                   <div class=star-wrap><%=CompanySearch.ratingStarResult(item.Company.rating)%></div>\
                </div>\
                </div>\
                <div class="col-v3 col-t5 col-p10">\
                    <a class="button" href=public-company/<%=item._id%>>View company detailed rating</a>\
\
                </div>\
                <div class="col-v3 col-t5  col-p10">\
                    <button class="button shaded openModal" data-cid="<%= item._id%>">Rate company and leave feedback</button>\
                </div>\
                </div>\
                </div>\
                    <% }) %>';

                var resultTemplateRender =   _.template(resultTemplate)({arr : arrResult})


                $('#resultBlock').empty();

                if(arrResult.length < 1) {
                    $('#resultBlock').textContent = 'ничего не найдено';
                } else {



                    $('#resultBlock').append(resultTemplateRender);
                    modalListener()






                }





            } else {
                $('#resultDrop').empty();



                sendMessages('Company/searchCompaniesFromName', {nameCompany: queryString, skip : 0, limit: 10, type: 0 })
                console.log("имя",arrResult)
                if(arrResult.length > 0) {

                    $('#posBox').addClass('active');

                    arrResult = arrResult.filter(function(el){
                        return el.toLowerCase().indexOf(queryString.toLowerCase()) > -1;
                    });


                    var resultDropTemp = '<% arr.forEach(function(item) { %>\
                        <li><a class="posLink"><%=item%></a></li>\
                    <% }) %>';

                    $('#resultDrop').append(_.template(resultDropTemp)({arr : arrResult}));

                } else {
                    $('#posBox').removeClass('active');
                }



            }



        } else {
            arrResult = [];
        }


        function modalListener() {
            $('.openModal').on('click', function (e) {
                $('#sendRate').addClass('active');
                companyId = e.target.dataset.cid;

            })

            $('#sendRate .cancel-black').on('click', function () {
                $('#sendRate').removeClass('active');
            })
        }

        modalListener()






    })









});




function sendMessages(method, data) {
    socket.send(JSON.stringify({"method":"worker/"+method,"data":data,"error":null,"requestId": request}))
    messages["worker/"+method] = {}
    return messages["worker/"+method][request] = new Promise(function(resolve, reject){
        request++;
        return resolve(data);
    })
}


