function controllerService() {

}
controllerService.prototype.getFunctionByMethod = function (method) {
    var func = {
        'worker/Public/getReviews':'getReviews',
        'worker/Public/getCompany':'getCompany',
        'worker/Authentication/identify':'identify',
        'worker/Public/getReviewsComments':'',

    };

    return (func.hasOwnProperty(method))?func[method]:null;
}

controllerService.prototype.getCompany = function (data) {
    console.log('getCompanyDATA',data);
}

controllerService.prototype.getReviews = function (data) {
    console.log('getReviews',data);
}

controllerService.prototype.renderData = function (method,res) {
    var func = this.getFunctionByMethod(method)
    console.log('getMethodFunction',func)
    return (func)?this[func](res):null;
}


controllerService.prototype.modalReview = function () {

    let modal = $('#giveReviews');

    $('#callModaReview').on('click', function() {
        modal.addClass('active');
    })

    modal.find('.cancel-black').on('click', function () {
        modal.removeClass('active');
    })

}

controllerService.prototype.createElement = function (tag,props, ...children) {
    const element = document.createElement(tag);

    Object.keys(props).forEach(key => element[key] = props[key]);

    if(children.length > 0) {
        children.forEach(child => {
            if(typeof child === "string") {
            child = document.createTextNode(child);
        }

        element.appendChild(child);
    });
    }

    return element;
};

controllerService.prototype.saveComment = function (data) {
    var commentObj = {};
    var box = document.getElementById('reviewWrap');



    $('#saveComment').on('click', function(){
        commentObj.comment_recommend = $("#giveReviews input[name=recommend]:checked").val() == 'yes' ? true : false;
        commentObj.comment_text = $("#giveReviews textarea[name=comment_text]").val();
        commentObj.comment_author = data.data.profile.first_name + data.data.profile.first_name;
        commentObj.rating_culture =  parseInt($('#culture .br-current-rating')[0].innerText);
        commentObj.rating_work =  parseInt($('#work .br-current-rating')[0].innerText);
        commentObj.rating_salary =  parseInt($('#salary .br-current-rating')[0].innerText);
        commentObj.rating_managment =  parseInt($('#managment .br-current-rating')[0].innerText);
        commentObj.rating_career =  parseInt($('#career .br-current-rating')[0].innerText);
        commentObj.comment_anonim =  $('#giveReviews #anonim').prop('checked') ? true : false;
        commentObj.companyId =  companyId

        $("#giveReviews textarea[name=comment_text]").val('')

        sendMessages("Public/newReview", commentObj).then(function(data){
            console.log("!!!",data)
        });

        $('#giveReviews').removeClass('active');


        var commonRating = parseInt((commentObj.rating_culture +  commentObj.rating_work + commentObj.rating_salary + commentObj.rating_managment + commentObj.rating_career) / 5);
        var date = new Date().getTime() / 1000;


        var tmpl = '<div class="boxSide-item">\
                    <div class="postRating">\
                        <div class="postRating-h">\
                            <div class="col-v4 col-p10">\
                                <p class="subMin-title"> Overall rating:</p>\
                                <div class="star-wrap">\
                                    <%= controller.ratingStarResult(commonRating) %>\
                                </div>\
                            </div>\
                            <div class="col-v4 col-p10">\
                                <p class="subMin-title"> Recommends: </p>\
                                <p class="statusPr">\
                                        <% if(commentObj.comment_recommend) { %>\
                                            <i class="icon ic_thumb_up_birch"></i> Yes\
                                        <% } else { %>\
                                            <i class="icon ic_thumb_down_gray"></i> No\
                                        <% } %>\
                                </p>\
                                    </div>\
                                    <p class="postRating__text"><%=commentObj.comment_text%></p>\
                                    <div class="postRating-f">\
                                        <div class="row-wrap">\
                                            <div class="col-v5 col-p7">\
                                                <i class="icon ic_verified_user_birch"></i>\
                                                <i class="icon ic_work_gray"></i>\
                                                <p class="minText"><%=userData.data.profile.first_name%> <%=userData.data.profile.last_name%></p>\
                                                <span class="dateText"><%=controllerService.prototype.parseDate(date)%></span>\
                                            </div>\
                                            <div class="col-v5 col-p3 right"><a class="blueLink">Report abuse</a></div>\
                                        </div>\
                                    </div>\
                                    <div class="replyPs active dropLink"><a class="blueLink">0 replies</a></div><!-- active -->\
                                        <div class="nestingPs active dropBlock">\
                                                        <div class="formPs">\
                                                                    <div class="formPs-link">\
                                                                        <a class="blueLink">View more replies</a>\
                                                                    </div>\
                                                                    <p class="textField">Your comment</p>\
                                                                    <textarea class="textarea"></textarea>\
                                                                    <input class="addItem_id" type="text" hidden >\
                                                                    <button class="button min saveSubComment">Submit</button>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    </div>\
                                                    </div>';
        var tmplComplete = _.template(tmpl)({commentObj: commentObj, date:date, commonRating: commonRating, userData : userData})
        $(box).append(tmplComplete);


    });











}


controllerService.prototype.saveSubReview = function() {
    var subCommentObj = {};


    $('.saveSubComment').on('click', function(e){
        subCommentObj.reply_text = $(this).closest('.formPs').find('.textarea').val();
        subCommentObj.cid = $(this).closest('.formPs').find('.addItem_id').val();

        $(this).closest('.formPs').find('.textarea').val('');

        console.log(subCommentObj);
        sendMessages("Public/newReviewComment", subCommentObj).then(function(data){
            console.log("!!!",data)
        });

        var date = new Date().getTime() / 1000;
        var box = $(this).closest('.nestingPs').find('.reply-wrap');



        var tmpl = '<div class="postRating ">\
                        <p class="postRating__text">\
                            <%=subCommentObj.reply_text%>\
                        </p>\
                    <div class="postRating-f">\
                        <div class="row-wrap">\
                            <div class="col-v5  col-p7">\
                                <i class="icon suspended-red"></i>\
                                <i class="icon ic_work_gray"></i>\
                                <p class="minText"><%=userData.data.profile.first_name%> <%=userData.data.profile.last_name%></p>\
                                <span class="dateText"><%=controllerService.prototype.parseDate(date)%></span>\
                            </div>\
                            <div class="col-v5 col-p3 right">\
                                <a class="blueLink">Report abuse</a>\
                            </div>\
                        </div>\
                    </div>\
                </div>';

        var tmplComplete = _.template(tmpl)({subCommentObj: subCommentObj, date:date, userData : userData})
        $(box).append(tmplComplete);















    })





}





controllerService.prototype.ratingStarResult = function(ratingValue) {
    var starsRating = [0,0,0,0,0];
    var star = ratingValue;
    let float = true;

    for(let i = 0; i < starsRating.length; i++) {

        if(star>=i+1){
            starsRating[i]=100;

        } else {
            starsRating[i]=0;
            if(float) {
                let ost = star-i;
                for(let b = 1; b <= 4; b++) {

                    if(1 / 4 * b >= ost) {
                        starsRating[i]= (1 / 4 * b - (1/4)) * 100;
                        break;
                    }
                };

            }
            float = false;
        }
    }

    var tmpl = '<% starsRating.forEach(function(staritem) { %>\
                        <div class="star yellow<%=staritem%>"></div>\
                    <% }) %>';


    return _.template(tmpl)({starsRating: starsRating});
}




controllerService.prototype.parseDate = function(unix) {
        var date = new Date(unix*1000);
        var month = date.getMonth() + 1;
        var day = date.getDate();
        var hour = date.getHours();
        var min = date.getMinutes();
        var sec = date.getSeconds();

        month = (month < 10 ? "0" : "") + month;
        day = (day < 10 ? "0" : "") + day;
        hour = (hour < 10 ? "0" : "") + hour;
        min = (min < 10 ? "0" : "") + min;
        sec = (sec < 10 ? "0" : "") + sec;

        var str = date.getFullYear() + "." + month + "." + day + " (" +  hour + ":" + min  + ")";

        return str;

}

controllerService.prototype.addDataToElement = function (container, data) {
    var el = document.querySelector(container);
    if(data) {
        el.textContent = data;
    } else {
        el.textContent = '-';
    }
}

controllerService.prototype.renderLocations = function (addr, container) {
    let cont = document.getElementById(container);

    let addrIcon = document.createElement('i');
    addrIcon.classList = 'icon ic_location_on_black';

    let addrEl = document.createElement('span');
    addrEl.classList = 'vacantBox__boldText block';

    addrEl.appendChild(addrIcon);
    addrEl.appendChild(document.createTextNode(addr));

    cont.appendChild(addrEl);
}


controllerService.prototype.renderAllCompanyRating = function (companyRating) {
    el = document.getElementById('companyAllRating');

    el.innerHTML = this.ratingStarResult(3)


}


controllerService.prototype.writeVacanciesCount = function (data) {
    let el = document.getElementById('vacancies-count');
    el.style.color = 'inherit';
    el.innerText = data.vacancies.length;
}


controllerService.prototype.overallRating = function(data) {
  var sum = 0;

  sum = (data.rating_career + data.rating_culture +  data.rating_managment + data.rating_salary + data.rating_work) / 5;

  return sum;

}


controllerService.prototype.pricentRecommend = function(data) {
    var countRecommend = 0;
    var pricentRecomend = 0;
    var box = document.getElementById('pricentRecommend');
    data.forEach(function(item){
        if(item.comment_recommend == true) {
            countRecommend++;
        }
    });

    pricentRecomend = parseInt((countRecommend * 100) / data.length) + '%';

    box.innerHTML = pricentRecomend;


}


controllerService.prototype.uidArr = function(data) {
    var idArr = [];

    data.forEach(function (user) {
        if(idArr.indexOf(user.uid) == -1) {
            idArr.push(user.uid);
            user.replys.forEach(function(reply){
                if(idArr.indexOf(reply.uid) == -1) {
                    idArr.push(reply.uid);
                }
            })

        }
    })

    console.log('массив айди коментов getUsers',idArr);



    return idArr;



}


controllerService.prototype.findUserFullName = function(data) {
    data.forEach(function(item) {
        var el = $('.' + item._id);

        if(el != null) {
            el.each(function(index){
                console.log($(this));
                $(this).html(item.first_name + ' ' + item.last_name);
            })

        }

    })


}


controllerService.prototype.commonCriterionRating = function(data) {
    var box = document.getElementById('criterionRating');
    var obj = {
        rating_career: 0,
        rating_culture: 0,
        rating_managment: 0,
        rating_salary: 0,
        rating_work:0
    };


    data.forEach(function(item){
       obj.rating_career += item.rating_career;
       obj.rating_culture += item.rating_culture;
       obj.rating_managment += item.rating_managment;
       obj.rating_salary += item.rating_salary;
       obj.rating_work += item.rating_work;
    });


    for(var key in obj) {
        obj[key] = obj[key] / data.length;

    }





    var tmpl = '<div class="rating-item">\
                  <p class="blueText ellipsis">Culture &amp; atmosphere: </p>\
                  <div class="star-wrap">\
                    <%= controller.ratingStarResult(obj.rating_culture)%>\
                  </div>\
                 </div>\
                 <div class="rating-item">\
                  <p class="blueText ellipsis">Work conditions:</p>\
                  <div class="star-wrap">\
                    <%= controller.ratingStarResult(obj.rating_work)%>\
                  </div>\
                 </div>\
                 <div class="rating-item">\
                  <p class="blueText ellipsis">Compliance of salary:</p>\
                  <div class="star-wrap">\
                    <%= controller.ratingStarResult(obj.rating_salary)%>\
                  </div>\
                 </div>\
                 <div class="rating-item">\
                  <p class="blueText ellipsis">Management quality:</p>\
                  <div class="star-wrap">\
                    <%= controller.ratingStarResult(obj.rating_managment)%>\
                  </div>\
                 </div>\
                 <div class="rating-item">\
                  <p class="blueText ellipsis">Career opportunities:</p>\
                  <div class="star-wrap">\
                    <%= controller.ratingStarResult(obj.rating_career)%>\
                  </div>\
                 </div>';



    box.innerHTML = _.template(tmpl)({obj : obj});



}

controllerService.prototype.renderVacancySidebar = function(data, options) {
    var box = document.getElementById('vacancy-sidebar');

    var tmpl = '<% data.forEach(function(item) { %> \
                    <div class="tapeSd-featured-item">\
                        <a class="blueLink"><%=item.positionTitle%></a>\
                        <p class="title"><span class="vacantBox__address">\
                        <i class="material-icons">&#xE0C8;</i>\
                        <% if(item.address != undefined ) { %>\
                            <%= item.address %>\
                        <% } else { %>\
                            -\
                        <% } %>\
                        </span>\
                        </p>\
                        <p class="vacantBox__text">Ocupation field: \
                            <span class="vacantBox__boldText">\
                            <% if (item.occupationField != undefined) { %>\
                                <%= options.occupationField[item.occupationField].text %>\
                                <% } else { %>\
                                -\
                                <% } %>\
                            </span>\
                        </p>\
                        <p class="vacantBox__price">50€<span _ngcontent-c4="">/h</span></p>\
                        <div class="rating-wrap"><p class="vacantBox__boldText"></p>\
                        </div>\
                    </div\
                <% }) %>';

    box.innerHTML =  _.template(tmpl)({data: data, options: options});
    console.log(options.occupationField[data[0].occupationField].text)


}

controllerService.prototype.renderReviews = function(data) {
    var box = document.getElementById('reviewWrap');

    var tmpl = '<% data.forEach(function(item) { %> \
                    <div class="boxSide-item">\
                    <div class="postRating">\
                        <div class="postRating-h">\
                            <div class="col-v4 col-p10">\
                                <p class="subMin-title"> Overall rating:</p>\
                                <div class="star-wrap">\
                                    <%= controller.ratingStarResult(controller.overallRating(item)) %>\
                                </div>\
                            </div>\
                            <div class="col-v4 col-p10">\
                                <p class="subMin-title"> Recommends: </p>\
                                <p class="statusPr">\
                                        <% if(item.comment_recommend) { %>\
                                            <i class="icon ic_thumb_up_birch"></i> Yes\
                                        <% } else { %>\
                                            <i class="icon ic_thumb_down_gray"></i> No\
                                        <% } %>\
                                </p>\
                                    </div>\
                                    <p class="postRating__text"><%=item.comment_text%></p>\
                                    <div class="postRating-f">\
                                        <div class="row-wrap">\
                                            <div class="col-v5 col-p7">\
                                                <i class="icon ic_verified_user_birch"></i>\
                                                <i class="icon ic_work_gray"></i>\
                                                <p class="minText <%=item.uid%>"></p>\
                                                <span class="dateText"><%=controllerService.prototype.parseDate(item.created_at)%></span>\
                                            </div>\
                                            <div class="col-v5 col-p3 right"><a class="blueLink">Report abuse</a></div>\
                                        </div>\
                                    </div>\
                                    <div class="replyPs active dropLink"><a class="blueLink"><%=item.replys.length%> replies</a></div><!-- active -->\
                                        <div class="nestingPs active dropBlock">\
                                        <div class="reply-wrap">\
                                            <% item.replys.forEach(function(Ritem) { %>\
                                                <div class="postRating ">\
                                                    <p class="postRating__text">\
                                                            <%=Ritem.reply_text%>\
                                                    </p>\
                                                    <div class="postRating-f">\
                                                        <div class="row-wrap">\
                                                                    <div class="col-v5  col-p7">\
                                                                        <i class="icon suspended-red"></i>\
                                                                        <i class="icon ic_work_gray"></i>\
                                                                        <p class="minText <%= Ritem.uid%>"></p>\
                                                                                <span class="dateText"><%=controllerService.prototype.parseDate(Ritem.created_at)%></span>\
                                                                                \
                                                                            </div>\
                                                                            <div class="col-v5 col-p3 right">\
                                                                                <a class="blueLink">Report abuse</a>\
                                                                            </div>\
                                                                </div>\
                                                                </div>\
                                                            </div\
                                                            </div>\
                                                    <% }) %>\
                                                    </div>\
                                                        <div class="formPs">\
                                                                    <div class="formPs-link">\
                                                                        <a class="blueLink">View more replies</a>\
                                                                    </div>\
                                                                    <p class="textField">Your comment</p>\
                                                                    <textarea class="textarea"></textarea>\
                                                                    <input class="addItem_id" type="text" hidden value="<%=item._id%>">\
                                                                    <button class="button min saveSubComment">Submit</button>\
                                                                </div>\
                                                            </div>\
                                                        </div>\
                                                    </div>\
                                                    </div>\
                                                    </div>\
                                                <% }) %>';
    box.innerHTML =  _.template(tmpl)({data: data});
}


controllerService.prototype.renderFieldOfActiv = function (data, options) {
 if (data) {
   var arr = [];
   data.forEach(function(item){
     arr.push(options[item].text)
     console.log(options[item].text);
   })

   // console.log('fieldddssssss',arr.join());

   document.getElementById('fieldActivity').innerHTML = arr.join(', ');
 }



}


var socket,
    request = 0,
    companyId = window.location.href.split("/").reverse()[0],
    reviews,
    arrReviews = [],
    Talents,
    messages = {},
    controller = new controllerService(),
    companyObject = {},
  dbOptions,
    userIdArr,
    uidArr = [],
    userData;



document.addEventListener("DOMContentLoaded", function() {
    controller.modalReview();
    $(".comment-bar").barrating({
        theme: 'fontawesome-stars'
    });


console.log('ws://'+window.PROFILE.HOST+':'+ window.PROFILE.PORT)
    socket = new WebSocket('ws://'+window.PROFILE.HOST+':'+ window.PROFILE.PORT)
    socket.onopen = function() {
      dbOptions = window['PROFILE'].options
        console.log("Соединение установлено.");
        sendMessages("Public/getCompany", {
            companyId: companyId
        }).then(function(data){
            console.log("!!!",data)
        })

        sendMessages("Public/getReviews", {
            companyId: companyId
        }).then(function(data){
            console.log("!!!",data)
        })

        sendMessages("Vacancy/getVacancyFromUid", {
            companyId: companyId
        }).then(function(data){
            console.log("vacancies666666666666666666666",data)
        })


        sendMessages("Vacancy/getRandomVacancies", {}).then(function(data){
            console.log("рандом вакансииsssssssssssssssssss",data)
        })
    };

    socket.onmessage = function(event) {

        console.log("Получены данные123 " , JSON.parse(event.data));
        var data = JSON.parse(event.data);
        var uidArr;
        if (messages.hasOwnProperty(data.method)&&data.method=='worker/Public/getCompany'){




            controller.renderData(data.method,data.data.res)
            console.log('опции', dbOptions);
            controller.addDataToElement('#companyName', data.data.res.Company['nameCompany']);
            // controller.addDataToElement('#fieldActivity', data.data.res.Company.activityField);
            controller.renderFieldOfActiv(data.data.res.Company.activityField, dbOptions.fieldActivity);
            controller.addDataToElement('#companySize', data.data.res.Company.companySize);
            controller.addDataToElement('#Established', data.data.res.Company.Established);
            controller.addDataToElement('#legalFormBusiness', data.data.res.Company.legalFormBusiness);
            controller.renderAllCompanyRating();
            controller.renderLocations(data.data.res.Location.addr, 'CompanyLocations');
            (function(){
                $('')
            })()
            console.log("IN DATA",messages[data.method])
            console.log(messages[data.method][data.requestId])


        }
        else if (data.method=='worker/User/getUsers') {
            uidArr = data;
            console.log('конечный массив пользователей', uidArr);
            controller.findUserFullName(data.data.users);

        }

        else if (messages.hasOwnProperty(data.method)&&data.method=='worker/Public/getReviews') {
            controller.renderData(data.method,data.data.res);
            controller.renderReviews( data.data.res);
            controller.commonCriterionRating( data.data.res);
            controller.pricentRecommend( data.data.res);
            controller.saveSubReview();
            controller.saveComment(userData);
            var usersArr = controller.uidArr(data.data.res);
            sendMessages("User/getUsers", {
                ids: usersArr,items:["first_name","last_name"]
            }).then(function(data){
                console.log("!!!",data)
            })

            var drops =  $('.dropBlock');
            $('.dropLink').on('click',function(){
                    $(this).siblings().toggleClass('active');
                });

        }

        else if (data.method=='worker/Vacancy/getVacancyFromUid') {
            console.log('вакансии', data);
            controller.writeVacanciesCount(data.data);


        }

        else if (data.method=='worker/Authentication/identify') {
            console.log('инфо о пользователе', data);
            userData = data;
            dbOptions = data;
        }

        else if (data.method=='worker/Vacancy/getRandomVacancies') {
            console.log('рандомные вакансии', data);
            var options = dbOptions;
            controller.renderVacancySidebar(data.data.res , dbOptions)

        } else if (data.method=="worker/Public/newReviewComment") {
            console.log('новый подкомент', data);

        }

        else if (data.method=="worker/Public/newReview") {
            console.log('новый подкомент', data);

        }else{
          console.log("DEFAULT",data)
        }
    };

});
    function sendMessages(method, data) {
        socket.send(JSON.stringify({"method":"worker/"+method,"data":data,"error":null,"requestId": request}))
        messages["worker/"+method] = {}
        return messages["worker/"+method][request] = new Promise(function(resolve, reject){
           request++;
           return resolve(data);
       })
    }
